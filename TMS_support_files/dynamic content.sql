use cargo_transport_test;

# contract records

insert into contract (id, customer_id, tsp_id, signed) values (1, 1, 2, DATE_ADD(now(), Interval -450 day));
insert into contract (id, customer_id, tsp_id, signed) values (2, 1, 3, DATE_ADD(now(), Interval -800 day));
insert into contract (id, customer_id, tsp_id, signed) values (3, 1, 12, DATE_ADD(now(), interval -350 day));
insert into contract (id, customer_id, tsp_id, signed) values (4, 1, 13, DATE_ADD(now(), interval -425 day));
insert into contract (id, customer_id, tsp_id, signed) values (5, 1, 10, DATE_ADD(now(), interval -740 day));
insert into contract (id, customer_id, tsp_id, signed) values (6, 4, 2, DATE_ADD(now(), Interval -250 day));
insert into contract (id, customer_id, tsp_id, signed) values (7, 4, 3, DATE_ADD(now(), Interval -600 day));
insert into contract (id, customer_id, tsp_id, signed) values (8, 4, 9, DATE_ADD(now(), interval -356 day));
insert into contract (id, customer_id, tsp_id, signed) values (9, 4, 11, DATE_ADD(now(), interval -445 day));
insert into contract (id, customer_id, tsp_id, signed) values (10, 4, 13, DATE_ADD(now(), interval -540 day));
insert into contract (id, customer_id, tsp_id, signed) values (11, 6, 10, DATE_ADD(now(), Interval -400 day));
insert into contract (id, customer_id, tsp_id, signed) values (12, 6, 3, DATE_ADD(now(), Interval -610 day));
insert into contract (id, customer_id, tsp_id, signed) values (13, 6, 9, DATE_ADD(now(), interval -556 day));
insert into contract (id, customer_id, tsp_id, signed) values (14, 6, 11, DATE_ADD(now(), interval -645 day));
insert into contract (id, customer_id, tsp_id, signed) values (15, 6, 13, DATE_ADD(now(), interval -530 day));
insert into contract (id, customer_id, tsp_id, signed) values (16, 7, 10, DATE_ADD(now(), Interval -333 day));
insert into contract (id, customer_id, tsp_id, signed) values (17, 7, 3, DATE_ADD(now(), Interval -422 day));
insert into contract (id, customer_id, tsp_id, signed) values (18, 7, 9, DATE_ADD(now(), interval -512 day));
insert into contract (id, customer_id, tsp_id, signed) values (19, 7, 11, DATE_ADD(now(), interval -643 day));
insert into contract (id, customer_id, tsp_id, signed) values (20, 7, 12, DATE_ADD(now(), interval -323 day));
insert into contract (id, customer_id, tsp_id, signed) values (21, 8, 2, DATE_ADD(now(), Interval -543 day));
insert into contract (id, customer_id, tsp_id, signed) values (22, 8, 10, DATE_ADD(now(), Interval -654 day));
insert into contract (id, customer_id, tsp_id, signed) values (23, 8, 13, DATE_ADD(now(), interval -321 day));
insert into contract (id, customer_id, tsp_id, signed) values (24, 8, 11, DATE_ADD(now(), interval -467 day));
insert into contract (id, customer_id, tsp_id, signed) values (25, 8, 12, DATE_ADD(now(), interval -523 day));

