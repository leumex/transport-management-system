-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: cargo_transport_test
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'W. Ludwig & Co. Holzhandelsges. mbH. Billbrookdeich 309, 22113',44),(2,'Hand.mij. A. Smit en Zoon B.V. Neonweg 41, 3812 RG',73),(3,'Gosselin Amsterdam The Netherlands Mortelmolen 3, 1185 XV Amstelveen',71),(4,'Beijleveld Houtimport B.V. Sluisjesdijk 155, 3087 AG',72),(5,'JSC Severstal Mira str., 30, 162600',20),(6,'Dalhoff Larsen & Horneman A/S Ellebjergvej 50, 2450',60),(7,'Agri-Norcold A/S Birkedam 11, 6000',62),(8,'thyssenkrupp Stahlkontor, Zweigniederlassung der thyssenkrupp Schulte GmbH Riesestraße 12-18, 44287',48),(9,'Mercedes-Benz Werk Untertürkheim Mercedesstraße 132, 70327',47),(10,'JSC Pinskdrev Ivana Chuklaja, 1',2),(11,'Gosselin Antwerp Belgium Belcrownlaan 23, 2100',11),(12,'Spissky Stavnik s.r.o. Herberska 15-18, 35235',79),(13,'Decormat s.r.o. Hlighnizka area H5 gates 4',58),(14,'10051 , Churostna, 13 appartments 8 , rear entrance',58),(15,'Tuin Visie, Rutherfordweg 75, 3542 CN',80),(16,'TDL Textile Erohova str., 3, 156002',19),(17,'Colgate-palmolive Industriel, 60 Avenue du Vermandois, 60200',81),(18,'Procter & Gamble, Komsomolskaja avenue, 64  , 301691',82),(19,'PRODUX concepts & services AG, Industriestrasse 27, 4703',83),(20,'Holzbau Lins GmbH ,  Niederfeldstrasse, 5 , 36391',84),(21,'Murom ZAO, Kirova avenue, 21',85),(22,'Nuova Complat, via Campodoro, 53, 35010',86),(23,'Tjazhpressmash JSC, Promyshlennaya str., 5, 390042',87),(24,'INTERFER Edelstahl Handelsgesellschaft mbH, Ferdinand Richtler str. 44 Tor 8, 41028',88),(25,'Severstal-metiz JSC, 50th year annivesary of Great October str., 1/33 , 162610',20),(26,'Metalsinter Srl, Via Messina, 9, 20831',89),(27,'Vtorzvetmet JSC, Elevatornaya str., 41, 160012',90),(28,'Siegfried Jacob Metalwerke GmbH & Co. KG , Jacobstrasse 41-45, 58256',91),(30,'AVTO-VAZ LTD, Babushkina, 8',25),(31,'Donaldson NV, Pathoekeweg 166, 8000',12),(32,'Ryazan Drama Theatre, Theatre square, 7a, 390023',87),(33,'NO NAIL BOXES SA, 25-Salzbaach, 9559',98),(34,'SVEZA-Kostroma, Komsomolskaya, 2  156000',19),(35,'Saitec Sarl, 5 Bd Pascal, 85300',99),(36,'SvetlogorskKhimvolokno OJSC, Zavodskaja str., 5 , 247439',100),(37,'Castle Chemicals Ltd, Gateway House, Styal Road, Heald Green, M22 5WY',77),(38,'Klaipeda Container Terminal, Minijos g. 180, 93269',70),(39,'Lakokraska JSV , Ignatova str, 71, 231300',8),(40,'Nestle Petcare Purina Sarl c/o Zone Industrielle La Plaine, Rue Charles, Nungesser, 42340',92),(41,'MaxiZoo Sp.z.o. aleja Rozdzienskiego 97, 40001',38),(42,'Exoply d.o.o. , Obratna cona Ugar 26, 1310',101),(43,'J.u.A. Frischeis Ges.m.b.H. Gerbergasse, 2, 2000',102),(44,'J.u.A. Frischeis Ges.m.b.H. Prinz-Eugen-Strasse 13, 4020',103),(45,'Baillou Handelsgesellschaft m.b.H. Industriestrasse B 9, 2345',105),(46,'JAF-HOLZ Spol s.r.o. Druzedni 702, 72526',109),(47,'JAF-HOLZ Spol. s.r.o., Skaly 215, 76362',110),(48,'SVEZA-Ust-Izhora, Fanernaya str. 5, 196643',16),(49,'JAF-HOLZ Spol. s.r.o.  U Bile haldy 1123, 33701',108),(50,'SVEZA-Novator, Novator village, Velikiy Ustjug, 162350',111),(51,'Voronezhsintezkauchuk JSC, Leninskiy avenue, 2 , 394014',18),(52,'Svobodny Sokol Ltd, Zavodskaya square, 1, 398007',113),(53,'Specta Interpack JSC, Zelenaya str. 10, 156019',19),(54,'Cargotrans Verwaltungs GmbH, Luetgendortmunder Hellweg 242, 44388',48),(55,'Wood&Ply Sarl, 42 Rue du Progres, 69680',115),(56,'Orlimex s.r.o. , Osik 50, 56967',112),(57,'Fandok OJSV, Lenina str., 95, 213802',7),(58,'Belshina OJSV, Minskoe highway, 213824',7),(59,'DLH Danmark A/S, Nordkajen 21, 6000',62),(60,'Carl Goetz GmbH, Hombergstrasse, 181, 32049',118),(61,'MAROTRANS Logistik GmbH & Co. KG, Rheinische str. 31, 42781',117),(62,'BLG Handelslogistik GmbH & Co. KG, Hugo-Junkers-str., 7, 60386',46),(63,'Wigbold Houthandel en Bouwstoffen BV, Smirnoffsraat, 1, 9716 JP',119),(64,'PRODUX concepts + services AG, Industriestrasse 27, 4703',83),(65,'Specsplav LLC Okruzhna str. 116, 50025',33),(66,'Metinvest-SMC, Shirokovskoe shosse, 8, 50000',33),(67,'Agrana Frut Ukraine, S. Zylinskogo, 32, 21022',32),(68,'Elektrotyazhmash, Moskovskyi avenue, 299, 61000',31),(69,'Plitochnyi zavod JSC, Mosckovskyi avenue, 297, 61000',31),(70,'Yugas Ltd, Novomoskovska highway, 23/1, 65000',30),(71,'Dia Oviedo, Poligno Espiritu Santo, 66, 33010',65),(72,'Cherepovetskij fanerno-mebelnyi combinat, Proeczhaya str, 4, 162604',20),(73,'SKBZ OJSV, Fabrichnaya str, 1, 231800',5),(74,'Slominmebel OJSV, Torgovaya, 9,  231799',5),(75,'DLH Poland Sp.o.o. , aleja Rozdzienskiego, 170, 40203',38),(76,'Hurtownia KALDEX, Kossutha, 5 ,  40836',38),(77,'DLH  Global Sp.o.o. Wyscigowa, 58,  53012',42),(78,'Pfjeiderer Polska sp.z o.o. , Strezhomska, 42AB, 53611',42),(79,'Ball, Zavodskaya, 1, Novaya Olkhovka village, 143300',123),(80,'Carlsberg A/S, Vestre Ringvej, 111, 7000',124),(81,'Kloepfer Holz GmbH & Co KG, Louis-Krages-Strasse 30, 28237',125),(82,'SVEZA Manturovo JSC, Matrosova str. 2b, 157305',121),(83,'Meplax BV, Burgemeester Krollaan 15, 5126 PT',126),(84,'Nestle Purina Vorsino CJSV, Kaluga region, 249020',120),(85,'Nestle Deutschland AG, Lyoner str. 23, 60523',46),(86,'Cora Domenico & Figli S.p.a. viale Verona, 1 , 36077',127),(87,'Emiliana Imballaggi Spa, via P. Gobetti, 12, 42042 RE',128),(88,'Parfino plywood mill Ltd, Kirova str. 52, Velikij Novgorod region, 175130',129),(89,'Gosselin Moscow Russia , Profsouznaya str, 56 , 117393',17),(90,'Gosselin Warsaw Poland, Nowa 23, 05500',130),(91,'Thyssenkrupp Industrial Solutions AG, Friedrich-Uhde-Strasse, 15, 44141',48),(92,'Thyssenkrupp AG, Thyssen Allee, 1, building Q1, 45143',131),(93,'Thyssenkrupp Germany, 26316',132),(94,'Thyssenkrupp Resource Technologies, Ernst-Heckel-Strasse, 1, 66386',133),(95,'Thyssenkrupp Industrial Solutions, Schleebergsrasse 12, 59320',134),(96,'PSA Peugeot Citroen, 1 lmp. des Pres Moussus, 70000',135),(97,'His PSA Peugeot Citroen, 91 Bd Solidarite, 57070 ',136);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `bids`
--

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
INSERT INTO `contract` VALUES (1,1,2,'2020-05-09'),(2,1,3,'2019-05-25'),(3,1,12,'2020-08-17'),(4,1,13,'2020-06-03'),(5,1,10,'2019-07-24'),(6,4,2,'2020-11-25'),(7,4,3,'2019-12-11'),(8,4,9,'2020-08-11'),(9,4,11,'2020-05-14'),(10,4,13,'2020-02-09'),(11,6,10,'2020-06-28'),(12,6,3,'2019-12-01'),(13,6,9,'2020-01-24'),(14,6,11,'2019-10-27'),(15,6,13,'2020-02-19'),(16,7,10,'2020-09-03'),(17,7,3,'2020-06-06'),(18,7,9,'2020-03-08'),(19,7,11,'2019-10-29'),(20,7,12,'2021-04-01'),(21,8,2,'2020-02-06'),(22,8,10,'2019-10-18'),(23,8,13,'2020-09-15'),(24,8,11,'2020-04-22'),(25,8,12,'2020-02-26');
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (23,'Austria'),(1,'Belarus'),(13,'Belgium'),(21,'Chroatia'),(8,'Czech Republic'),(9,'Denmark'),(6,'France'),(5,'Germany'),(17,'Great Britain'),(7,'Italy'),(11,'Latvia'),(12,'Lithuania'),(15,'Luxembourg'),(14,'Netherlands'),(4,'Poland'),(2,'Russia'),(16,'Slovenia'),(10,'Spain'),(22,'Switzerland'),(3,'Ukraine');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `line`
--

LOCK TABLES `line` WRITE;
/*!40000 ALTER TABLE `line` DISABLE KEYS */;
INSERT INTO `line` VALUES (2,1,4),(3,2,5),(4,3,6),(5,4,7),(6,2,8),(7,2,9),(8,5,10),(9,6,11),(10,7,12),(11,8,13),(12,2,14),(13,9,15),(14,10,16),(15,11,17),(16,12,18),(17,13,19),(18,14,20),(19,15,21),(20,16,22),(21,17,23),(22,20,27),(23,21,25),(24,21,24),(25,21,28),(26,22,127),(27,23,128),(28,23,129),(29,24,130),(30,25,131),(31,26,132),(32,27,133),(33,15,15),(34,14,41),(35,16,44),(36,15,42),(37,15,43),(38,14,45),(39,16,46),(40,15,47),(41,14,48),(42,14,49),(43,15,50),(44,16,51),(45,16,53),(46,16,53),(47,14,83),(48,15,86),(49,16,84),(50,15,85),(51,14,87),(52,15,88),(53,15,89),(54,16,90),(55,14,22),(56,14,94),(57,16,93),(58,15,92),(59,15,82),(60,16,96),(61,16,99),(62,14,23),(63,15,97),(64,15,98),(65,14,100),(66,14,101),(67,15,102),(68,15,103),(69,15,104),(70,16,107),(71,15,105),(72,14,106),(73,16,95),(74,16,127),(75,13,128),(76,13,129),(77,28,10),(78,28,4),(79,32,11),(80,29,4);
/*!40000 ALTER TABLE `line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `locality`
--

LOCK TABLES `locality` WRITE;
/*!40000 ALTER TABLE `locality` DISABLE KEYS */;
INSERT INTO `locality` VALUES (1,'Minsk',1),(2,'Pinsk',1),(3,'Rechitsa',1),(4,'Zhlobin',1),(5,'Slonim',1),(6,'Smorgon',1),(7,'Bobryisk',1),(8,'Lida',1),(9,'Mogilev',1),(10,'Gent',13),(11,'Antwerp',13),(12,'Brugge',13),(13,'Brussel',13),(14,'Liege',13),(15,'Charleroi',13),(16,'St.Petersburg',2),(17,'Moscow',2),(18,'Voronezh',2),(19,'Kostroma',2),(20,'Cherepovets',2),(21,'Kursk',2),(22,'Nizhniy Novgorod',2),(23,'Ekaterinburg',2),(24,'Ekaterinburg',2),(25,'Samara',2),(26,'Saratov',2),(27,'Ufa',2),(28,'Kiev',3),(29,'Lvov',3),(30,'Odessa',3),(31,'Kharkov',3),(32,'Vinnitsa',3),(33,'Krivoi Rog',3),(34,'Warszawa',4),(35,'Lodz',4),(36,'Warszawa',4),(37,'Krakow',4),(38,'Katowice',4),(39,'Poznan',4),(40,'Gdansk',4),(41,'Bialystok',4),(42,'Wroclaw',4),(43,'Berlin',5),(44,'Hamburg',5),(45,'Muenchen',5),(46,'Frankfurt',5),(47,'Stuttgart',5),(48,'Dortmund',5),(49,'Erfurt',5),(50,'Drezden',5),(51,'Paris',6),(52,'Lion',6),(53,'Marcel',6),(54,'Roma',7),(55,'Milan',7),(56,'Turin',7),(58,'Prague',8),(60,'Copenhagen',9),(61,'Aarhus',9),(62,'Kolding',9),(63,'Madrid',10),(64,'Barcelona',10),(65,'Oviedo',10),(66,'Riga',11),(67,'Liepaja',11),(68,'Vilnius',12),(69,'Kaunas',12),(70,'Klaipeda',12),(71,'Amsterdam',14),(72,'Rotterdam',14),(73,'Amersfoort',14),(74,'Luxembourg',15),(75,'Lubljana',16),(76,'London',17),(77,'Manchester',17),(78,'Liege',13),(79,'Plzen',8),(80,'Utrecht',14),(81,'Compiegne',6),(82,'Novomoskovsk',2),(83,'Kestenholz',22),(84,'Sinntal',5),(85,'Murom',2),(86,'Villafranca Padovana',7),(87,'Ryazan',2),(88,'Neuss',5),(89,'Seregno',7),(90,'Vologda',2),(91,'Ennepetal',5),(92,'Veauche',6),(93,'Nantes',6),(97,'Amsterdam',14),(98,'Wiltz',15),(99,'Challans',6),(100,'Svetlogorsk',1),(101,'Ribnica',16),(102,'Stockerau',23),(103,'Linz',23),(104,'Weiner Neustadt',23),(105,'Brunn am Gebirge',23),(106,'Zlin',8),(107,'Brno',8),(108,'Rokycany',8),(109,'Ostrava',8),(110,'Tlumachov',8),(111,'Novator',2),(112,'Osik u Litomychly',8),(113,'Lipetsk',2),(114,'Bilbao',10),(115,'Chasseiu',6),(116,'Klagenfurt',23),(117,'Haan',5),(118,'Herford',5),(119,'Groningen',14),(120,'Vorsino',2),(121,'Manturovo',2),(122,'Shar\'ya',2),(123,'Naro-Fominsk',2),(124,'Federicia',9),(125,'Bremen',5),(126,'Gilze',14),(127,'Altavilla Vicentina',7),(128,'Fabricco',7),(129,'Parfino',2),(130,'Stara Iwiczna',4),(131,'Essen',5),(132,'Varel',5),(133,'St. Ingbert',5),(134,'Ennigerloh',5),(135,'Noidans-les-Vesoul',6),(136,'Metz',6);
/*!40000 ALTER TABLE `locality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` VALUES (13,'free',3,32),(4,'free',5,8),(26,'free',10,1),(5,'free',10,4),(14,'free',10,33),(25,'over Ukraine prohibited',10,47),(21,'via Kozlovichi only',10,47),(24,'over Ukraine prohibited',10,49),(28,'free',10,81),(6,'over Ukraine prohibited',15,16),(7,'over Ukraine prohibited',17,18),(8,'free',19,20),(118,'free',21,1),(119,'free',21,3),(121,'free',21,6),(126,'free',21,20),(9,'free',21,22),(122,'free',21,43),(124,'free',21,56),(120,'free',21,60),(125,'free',21,61),(123,'free',21,81),(130,'free',21,86),(10,'free',23,24),(11,'free',25,26),(12,'free',27,28),(16,'free',33,35),(15,'free',34,1),(41,'free',34,4),(44,'free',34,6),(42,'free',34,19),(43,'free',34,20),(45,'free',34,33),(46,'free',34,43),(47,'free',34,44),(48,'free',34,46),(49,'free',34,49),(53,'free',34,55),(52,'free',34,60),(51,'free',34,77),(50,'free',34,81),(17,'free',36,37),(18,'sanitary inspection in Benyakoni',38,39),(19,'free',40,41),(20,'free',42,45),(83,'free',48,4),(86,'free',48,6),(84,'free',48,19),(85,'free',48,20),(87,'free',48,33),(88,'free',48,43),(89,'free',48,44),(90,'free',48,46),(22,'free',48,49),(94,'free',48,55),(93,'free',48,60),(92,'free',48,77),(82,'free',48,81),(96,'free',50,4),(99,'free',50,6),(23,'free',50,7),(97,'free',50,19),(98,'free',50,20),(100,'free',50,33),(101,'free',50,43),(102,'free',50,44),(103,'free',50,46),(104,'free',50,49),(107,'free',50,55),(106,'free',50,60),(105,'free',50,77),(95,'free',50,81),(27,'over Ukraine prohibited',79,80),(127,'free',82,83),(128,'sanitary inspection in Benyakoni',84,40),(129,'via Kozlovichi only',84,85),(131,'free',88,87),(132,'over Ukraine prohibited',89,11),(133,'free',89,90);
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `spot_orders`
--

LOCK TABLES `spot_orders` WRITE;
/*!40000 ALTER TABLE `spot_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `spot_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tender`
--

LOCK TABLES `tender` WRITE;
/*!40000 ALTER TABLE `tender` DISABLE KEYS */;
INSERT INTO `tender` VALUES (1,'Test tender1',1,'2020-12-20','2020-01-08','2020-01-12','2021-01-14','2021-05-12',232,'contracted'),(2,'Testtender 2021',1,'2021-07-04',NULL,NULL,'2021-07-09','2021-12-31',50,'created'),(5,'test_tender_4',4,'2021-08-02',NULL,'2021-09-01','2021-09-16','2022-09-11',50,'created'),(6,'test_tender_5',4,'2021-07-23','2021-07-25','2021-09-01','2021-09-16','2022-09-11',50,'created');
/*!40000 ALTER TABLE `tender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tender_has_line`
--

LOCK TABLES `tender_has_line` WRITE;
/*!40000 ALTER TABLE `tender_has_line` DISABLE KEYS */;
INSERT INTO `tender_has_line` VALUES (2,1,3),(1,1,4),(3,1,5),(6,2,3),(7,2,7),(4,2,12),(5,2,13);
/*!40000 ALTER TABLE `tender_has_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tender_line_query`
--

LOCK TABLES `tender_line_query` WRITE;
/*!40000 ALTER TABLE `tender_line_query` DISABLE KEYS */;
/*!40000 ALTER TABLE `tender_line_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tender_participation`
--

LOCK TABLES `tender_participation` WRITE;
/*!40000 ALTER TABLE `tender_participation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tender_participation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `transport_order`
--

LOCK TABLES `transport_order` WRITE;
/*!40000 ALTER TABLE `transport_order` DISABLE KEYS */;
INSERT INTO `transport_order` VALUES (1,'2021-01-31 13:27:24','2021-11-03',7,2,1,NULL,'ASSIGNED'),(2,'2021-01-31 13:27:27','2021-11-03',7,2,NULL,1,'CREATED');
/*!40000 ALTER TABLE `transport_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `truckload`
--

LOCK TABLES `truckload` WRITE;
/*!40000 ALTER TABLE `truckload` DISABLE KEYS */;
INSERT INTO `truckload` VALUES (1,'steel bars',21500,60,'tilt'),(2,'plywood',21900,33,'tilt'),(3,'linen fabrics',19000,1000,'box'),(4,'washing powder',21200,33,'no matter'),(5,'wire',19800,345,'no matter'),(6,'wire',21100,400,'tilt'),(7,'copper waste',21000,30,'no matter'),(8,'music instruments',12000,46,'box'),(9,'plywood',21900,30,'tilt'),(10,'wooden boxes',17200,32,'tilt'),(11,'polyamide fiber ',21803,233,'tilt'),(12,'spirit',19000,20,'no matter'),(13,'pet food',16700,66,'no matter'),(14,'plywood',21000,16,'tilt'),(15,'plywood',21850,20,'tilt'),(16,'plywood',21550,18,'tilt'),(17,'plywood',21500,32,'tilt'),(18,'rubber',20460,20,'no matter'),(19,'iron bars',21750,8,'tilt'),(20,'cans',4000,20,'no matter'),(21,'plywood',21700,32,'tilt'),(22,'plywood',21800,33,'tilt'),(23,'pet food',20560,40,'box'),(24,'plywood',21600,16,'tilt'),(25,'plywood',21200,17,'tilt'),(26,'removals',12850,125,'box'),(27,'pov and removals',18000,5,'tilt'),(28,'flat-rolled products',21500,6,'tilt'),(29,'reinforcement in bundles',21200,12,'tilt'),(30,'birch plywood',21750,18,'tilt'),(31,'ends',20150,20,'no matter'),(32,'hardware',20100,25,'no matter');
/*!40000 ALTER TABLE `truckload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tsp_performance`
--

LOCK TABLES `tsp_performance` WRITE;
/*!40000 ALTER TABLE `tsp_performance` DISABLE KEYS */;
INSERT INTO `tsp_performance` VALUES (7,2,1,'OUTSTANDING','2021-01-31 21:42:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-11-03',7,2,NULL,1,'ASSIGNED');
/*!40000 ALTER TABLE `tsp_performance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'customer','sveza','SVEZA','2020-05-28','$2a$10$SmmwRJnjIZwY3fHcgQEyWukUptHawyn0Ev6sPkXU4uJHPaF4/LRry','$2a$10$SmmwRJnjIZwY3fHcgQEyWu'),(2,'transport service provider','tels','TELS','2020-05-29','$2a$10$FU7ziTHOeXqapWhdqzEdb.Ym7m04BnnyWXVfGeUND34P/nNUe91Xa','$2a$10$FU7ziTHOeXqapWhdqzEdb.'),(3,'transport service provider','asstra','Asstra','2021-07-04','$2a$10$UWntG5j7xvA0/v.vyYUkneNGBUKkJZ8VLnDXJwE7bPoqo5zxmmfQ.','$2a$10$UWntG5j7xvA0/v.vyYUkne'),(4,'customer','naro','Ball','2021-07-04','$2a$10$j7M6Tl2y7Thq.4Kyr2Mh5O4566hprJ8yS6O87PhsqA5t8dwYxSZRS','$2a$10$j7M6Tl2y7Thq.4Kyr2Mh5O'),(5,'admin','admin','Admin','2021-07-30','$2a$10$o3u6X.FHMoQUkZasjwxznuwLkUkKUsQTZ22LvvfRCcVbpetG4VQei','$2a$10$o3u6X.FHMoQUkZasjwxznu'),(6,'customer','steel','Severstal','2021-07-30','$2a$10$Pf5ox7C5WWfQGQE0lV4zceZ62nhpud7/I5PaH0cYqQ1fglN8mHfJm','$2a$10$Pf5ox7C5WWfQGQE0lV4zce'),(7,'customer','murom','Woodbridge','2021-07-30','$2a$10$S/1fvOoBssYhIyDn/.DfHuNCmUicBXqNVWb0NtcBWkoqqdcNgZBg6','$2a$10$S/1fvOoBssYhIyDn/.DfHu'),(8,'customer','embassy','Gosselin Mobility','2021-07-30','$2a$10$P4ZanvG2yzf4gYK9OcdgH.i0VBpkoLXoPnqESHAeymWbgsYEWRwdG','$2a$10$P4ZanvG2yzf4gYK9OcdgH.'),(9,'transport service provider','vitalik','Alev-trans LLC','2021-07-30','$2a$10$D3ayHnah18G/XbFuhjNG8.VzdP6.eQFGQnpUltZ2PSqKciu1Cydn.','$2a$10$D3ayHnah18G/XbFuhjNG8.'),(10,'transport service provider','vladimir','Cargo Trust Ltd','2021-07-30','$2a$10$Xah5rzuIY8EfzP3813FEmu7dcKSlh3tHUEPL1lP4PxwkFQbYP9jHa','$2a$10$Xah5rzuIY8EfzP3813FEmu'),(11,'transport service provider','privolny','Ostwesttranscar JSC','2021-07-30','$2a$10$CLw/WICApZGvfTgikOPt8u9dtgLJyEwNOnvS.PsvATItgWLwQOMgG','$2a$10$CLw/WICApZGvfTgikOPt8u'),(12,'transport service provider','jenty','Primum','2021-07-30','$2a$10$rTiSHIdg62HM239ugxagxuSIkQoRwQE6Bf6a80qqK31mGFxeaLmTq','$2a$10$rTiSHIdg62HM239ugxagxu'),(13,'transport service provider','jenty2','R Group','2021-07-30','$2a$10$MJzDLdT6Nob0uwe.qanofOjceKkV6y1DnhP7B3Rah6SjakmrRMtsi','$2a$10$MJzDLdT6Nob0uwe.qanofO');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cargo_transport_test'
--

--
-- Dumping routines for database 'cargo_transport_test'
--
/*!50003 DROP PROCEDURE IF EXISTS `repeat_loop_example` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `repeat_loop_example`()
wholeblock:BEGIN
  DECLARE x INT;
  DECLARE str VARCHAR(255);
  SET x = 5;
  SET str = '';

  REPEAT
    SET str = CONCAT(str,x,',');
    SET x = x - 1;
    UNTIL x <= 0
  END REPEAT;

  SELECT str;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-17 12:44:06
