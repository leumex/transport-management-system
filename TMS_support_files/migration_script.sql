-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: cargo_transport_test_oct21
-- Source Schemata: cargo_transport_test
-- Created: Sun Oct 17 12:35:15 2021
-- Workbench Version: 8.0.15
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema cargo_transport_test_oct21
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `cargo_transport_test_oct21` ;
CREATE SCHEMA IF NOT EXISTS `cargo_transport_test_oct21` ;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.address
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `details` VARCHAR(100) NOT NULL,
  `locality_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_locality_idx` (`locality_id` ASC) VISIBLE,
  CONSTRAINT `fk_locality`
    FOREIGN KEY (`locality_id`)
    REFERENCES `cargo_transport_test_oct21`.`locality` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 98
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.bids
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`bids` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `spot_order_id` INT(11) NOT NULL,
  `bid` DOUBLE NOT NULL,
  `tsp_id` INT(11) NOT NULL,
  `made_at` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_bids_spot_orders1_idx1` (`spot_order_id` ASC) VISIBLE,
  INDEX `fk_bids_user1_idx1` (`tsp_id` ASC) VISIBLE,
  CONSTRAINT `fk_bids_spot_orders1`
    FOREIGN KEY (`spot_order_id`)
    REFERENCES `cargo_transport_test_oct21`.`spot_orders` (`id`),
  CONSTRAINT `fk_bids_user1`
    FOREIGN KEY (`tsp_id`)
    REFERENCES `cargo_transport_test_oct21`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.contract
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`contract` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `customer_id` INT(11) NOT NULL,
  `tsp_id` INT(11) NOT NULL,
  `signed` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_contract_user1_idx` (`customer_id` ASC) VISIBLE,
  INDEX `fk_contract_user2_idx` (`tsp_id` ASC) VISIBLE,
  CONSTRAINT `fk_contract_user1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `cargo_transport_test_oct21`.`user` (`id`),
  CONSTRAINT `fk_contract_user2`
    FOREIGN KEY (`tsp_id`)
    REFERENCES `cargo_transport_test_oct21`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 55
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.country
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`country` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.line
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`line` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `truckload_id` INT(11) NOT NULL,
  `route_line` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_line_route1_idx` (`route_line` ASC) VISIBLE,
  INDEX `fk_line_truckload1_idx` (`truckload_id` ASC) VISIBLE,
  CONSTRAINT `fk_line_route1`
    FOREIGN KEY (`route_line`)
    REFERENCES `cargo_transport_test_oct21`.`route` (`id`),
  CONSTRAINT `fk_line_truckload1`
    FOREIGN KEY (`truckload_id`)
    REFERENCES `cargo_transport_test_oct21`.`truckload` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 81
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.locality
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`locality` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `country_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_locality_country_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_locality_country`
    FOREIGN KEY (`country_id`)
    REFERENCES `cargo_transport_test_oct21`.`country` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 137
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.route
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`route` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `transit_conditions` ENUM('free', 'over Ukraine prohibited', 'via Kozlovichi only', 'customs in Bryansk', 'sanitary inspection in Benyakoni', 'through Belarus prohibited') NOT NULL DEFAULT 'free',
  `departure` INT(11) NOT NULL,
  `destination` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `address_unique` (`departure` ASC, `destination` ASC, `transit_conditions` ASC) VISIBLE,
  INDEX `fk_route_start_idx` (`departure` ASC) VISIBLE,
  INDEX `fk_route_destination_idx` (`destination` ASC) VISIBLE,
  CONSTRAINT `fk_route_address1`
    FOREIGN KEY (`departure`)
    REFERENCES `cargo_transport_test_oct21`.`address` (`id`),
  CONSTRAINT `fk_route_address2`
    FOREIGN KEY (`destination`)
    REFERENCES `cargo_transport_test_oct21`.`address` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 134
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.spot_orders
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`spot_orders` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `spot_deadline` TIMESTAMP NOT NULL,
  `transport_order_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_spot_orders_transport_order1_idx` (`transport_order_id` ASC) VISIBLE,
  CONSTRAINT `fk_spot_orders_transport_order1`
    FOREIGN KEY (`transport_order_id`)
    REFERENCES `cargo_transport_test_oct21`.`transport_order` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.tender
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`tender` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(60) NOT NULL,
  `customer_id` INT(11) NOT NULL,
  `set_date` DATE NULL DEFAULT NULL,
  `release_date` DATE NULL DEFAULT NULL,
  `competition_deadline` DATE NULL DEFAULT NULL,
  `tender_start` DATE NOT NULL,
  `tender_end` DATE NOT NULL,
  `estimated_quantity` INT(11) NOT NULL,
  `state` ENUM('created', 'released', 'contracted', 'in_action', 'completed') NOT NULL DEFAULT 'created',
  PRIMARY KEY (`id`),
  INDEX `fk_tender_user1_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_tender_user1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `cargo_transport_test_oct21`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.tender_has_line
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`tender_has_line` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tender_id` INT(11) NOT NULL,
  `line_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `tender_line_unique` (`tender_id` ASC, `line_id` ASC) VISIBLE,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_tender_idx` (`tender_id` ASC) VISIBLE,
  INDEX `fk_line_idx` (`line_id` ASC) VISIBLE,
  CONSTRAINT `fk_tender_has_line_tender1`
    FOREIGN KEY (`tender_id`)
    REFERENCES `cargo_transport_test_oct21`.`tender` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tender_has_lines_line1`
    FOREIGN KEY (`line_id`)
    REFERENCES `cargo_transport_test_oct21`.`line` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.tender_line_query
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`tender_line_query` (
  `id` INT(11) NOT NULL,
  `rate` DOUBLE NOT NULL,
  `delivery_term` INT(11) NOT NULL,
  `awarded_share` DOUBLE NULL DEFAULT NULL,
  `tender_has_line_id` INT(11) NOT NULL,
  `tender_participation_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_tender_line_query_tender_has_line1_idx` (`tender_has_line_id` ASC) VISIBLE,
  INDEX `fk_tender_line_query_tender_participation1_idx` (`tender_participation_id` ASC) VISIBLE,
  CONSTRAINT `fk_tender_line_query_tender_has_line1`
    FOREIGN KEY (`tender_has_line_id`)
    REFERENCES `cargo_transport_test_oct21`.`tender_has_line` (`id`),
  CONSTRAINT `fk_tender_line_query_tender_participation1`
    FOREIGN KEY (`tender_participation_id`)
    REFERENCES `cargo_transport_test_oct21`.`tender_participation` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.tender_participation
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`tender_participation` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `invitation_date` DATE NOT NULL,
  `feedback_date` DATE NULL DEFAULT NULL,
  `tender_id` INT(11) NOT NULL,
  `tsp_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tender_participation_tender1_idx` (`tender_id` ASC) VISIBLE,
  INDEX `fk_tender_participation_user1_idx` (`tsp_id` ASC) VISIBLE,
  CONSTRAINT `fk_tender_participation_tender1`
    FOREIGN KEY (`tender_id`)
    REFERENCES `cargo_transport_test_oct21`.`tender` (`id`),
  CONSTRAINT `fk_tender_participation_user1`
    FOREIGN KEY (`tsp_id`)
    REFERENCES `cargo_transport_test_oct21`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.transport_order
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`transport_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `issued` TIMESTAMP NOT NULL,
  `loading_date` DATE NOT NULL,
  `desired_delivery_term` INT(11) NULL DEFAULT NULL,
  `line` INT(11) NOT NULL,
  `tender_id` INT(11) NULL DEFAULT NULL,
  `customer_id` INT(11) NULL DEFAULT NULL,
  `state` ENUM('CREATED', 'ANNOUNCED', 'ASSIGNED', 'IN_ACTION', 'COMPLETED', 'WITHDRAWN', 'ACCEPTED') NOT NULL DEFAULT 'CREATED',
  PRIMARY KEY (`id`),
  INDEX `trip_fk` (`line` ASC) VISIBLE,
  INDEX `fk_transport_order_tender1_idx` (`tender_id` ASC) VISIBLE,
  INDEX `fk_transport_order_user1_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_transport_order_tender1`
    FOREIGN KEY (`tender_id`)
    REFERENCES `cargo_transport_test_oct21`.`tender` (`id`),
  CONSTRAINT `fk_transport_order_user1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `cargo_transport_test_oct21`.`user` (`id`),
  CONSTRAINT `trip_fk`
    FOREIGN KEY (`line`)
    REFERENCES `cargo_transport_test_oct21`.`line` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.truckload
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`truckload` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `weight` DOUBLE NOT NULL,
  `load_units` INT(11) NOT NULL,
  `truck_type` ENUM('tilt', 'box', 'no matter') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.tsp_performance
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`tsp_performance` (
  `shipment_id` INT(11) NOT NULL AUTO_INCREMENT,
  `tsp_id` INT(11) NOT NULL,
  `transport_order_id` INT(11) NOT NULL,
  `tsp_response` ENUM('OUTSTANDING', 'ACCEPTED', 'WITHDRAWN', 'DECLINED') NOT NULL DEFAULT 'OUTSTANDING',
  `notified` TIMESTAMP NOT NULL,
  `comment` VARCHAR(1000) NULL DEFAULT NULL,
  `response_time` TIMESTAMP NULL DEFAULT NULL,
  `truck_plates` VARCHAR(45) NULL DEFAULT NULL,
  `arrived_for_loading` TIMESTAMP NULL DEFAULT NULL,
  `arrived_for_unloading` TIMESTAMP NULL DEFAULT NULL,
  `left_loading` TIMESTAMP NULL DEFAULT NULL,
  `left_unloading` TIMESTAMP NULL DEFAULT NULL,
  `order_loading_date` DATE NULL DEFAULT NULL,
  `order_delivery_term` INT(11) NULL DEFAULT NULL,
  `order_line_id` INT(11) NULL DEFAULT NULL,
  `order_tender_id` INT(11) NULL DEFAULT NULL,
  `order_customer_id` INT(11) NULL DEFAULT NULL,
  `order_state` ENUM('ASSIGNED', 'ACCEPTED', 'IN_ACTION', 'COMPLETED', 'WITHDRAWN', 'CANCELLED', 'INTERRUPTED') NOT NULL DEFAULT 'ASSIGNED',
  PRIMARY KEY (`shipment_id`),
  INDEX `fk_tsp_performance_user1_idx` (`tsp_id` ASC) VISIBLE,
  INDEX `fk_tsp_performance_transport_order1_idx` (`transport_order_id` ASC) VISIBLE,
  CONSTRAINT `fk_tsp_performance_transport_order1`
    FOREIGN KEY (`transport_order_id`)
    REFERENCES `cargo_transport_test_oct21`.`transport_order` (`id`),
  CONSTRAINT `fk_tsp_performance_user1`
    FOREIGN KEY (`tsp_id`)
    REFERENCES `cargo_transport_test_oct21`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table cargo_transport_test_oct21.user
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `cargo_transport_test_oct21`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role` ENUM('customer', 'transport service provider', 'admin') NOT NULL,
  `login` CHAR(20) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `registration_date` DATE NULL DEFAULT NULL,
  `password` CHAR(255) NULL DEFAULT NULL,
  `salt` CHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = utf8;
SET FOREIGN_KEY_CHECKS = 1;
