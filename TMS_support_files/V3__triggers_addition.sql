-- MySQL Script generated by MySQL Workbench
-- Tue Jul 27 22:56:13 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering


USE `cargo_transport_test`;

DELIMITER $$
USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`tender_BEFORE_INSERT`
BEFORE INSERT ON `cargo_transport_test`.`tender`
FOR EACH ROW
BEGIN
set NEW.`set_date` = SYSDATE();
END$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`route_BEFORE_INSERT`
BEFORE INSERT ON `cargo_transport_test`.`route`
FOR EACH ROW
BEGIN
IF NEW.`departure` = NEW.`destination`
THEN signal sqlstate '45000'
set message_text = 'departure and destination cannot be same point';
END IF;
END$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`route_BEFORE_UPDATE`
BEFORE UPDATE ON `cargo_transport_test`.`route`
FOR EACH ROW
BEGIN
IF NEW.`departure` = NEW.`destination`
THEN signal sqlstate '45000'
set message_text = 'departure and destination cannot be same point';
END IF;
END$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`the_only_reference_insert`
BEFORE INSERT ON `cargo_transport_test`.`transport_order`
FOR EACH ROW
BEGIN
    IF NEW.customer_id IS NULL THEN
        IF NEW.tender_id IS NULL THEN
            signal sqlstate '45000'
                set message_text = 'Either user_id or tender_id has to be null!';
        end if;
    end if;
    IF NEW.customer_id IS NOT NULL THEN
        IF NEW.tender_id IS NOT NULL THEN
            signal sqlstate '45000'
                set message_text = 'Either user_id or tender_id has to be null!';
        end if;
    end if;
end$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`the_only_reference_update`
BEFORE UPDATE ON `cargo_transport_test`.`transport_order`
FOR EACH ROW
BEGIN
    IF NEW.customer_id IS NULL THEN
        IF NEW.tender_id IS NULL THEN
            signal sqlstate '45000'
                set message_text = 'Either user_id or tender_id has to be null!';
        end if;
    end if;
    IF NEW.customer_id IS NOT NULL THEN
        IF NEW.tender_id IS NOT NULL THEN
            signal sqlstate '45000'
                set message_text = 'Either user_id or tender_id has to be null!';
        end if;
    end if;
end$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`transport_order_BEFORE_INSERT`
BEFORE INSERT ON `cargo_transport_test`.`transport_order`
FOR EACH ROW
BEGIN
set new.`issued` = CURRENT_TIMESTAMP();
END$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`bids_BEFORE_INSERT`
BEFORE INSERT ON `cargo_transport_test`.`bids`
FOR EACH ROW
BEGIN
set NEW.`made_at` = CURRENT_TIMESTAMP();
END$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`contract_BEFORE_INSERT`
BEFORE INSERT ON `cargo_transport_test`.`contract`
FOR EACH ROW
BEGIN
IF NEW.`customer_id` = NEW.`tsp_id`
THEN signal sqlstate '45000'
set message_text = 'customer and tsp have to be different users';
END IF;
END$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`contract_BEFORE_UPDATE`
BEFORE UPDATE ON `cargo_transport_test`.`contract`
FOR EACH ROW
BEGIN
IF NEW.`customer_id` = NEW.`tsp_id`
THEN signal sqlstate '45000'
set message_text = 'customer and tsp have to be different users';
END IF;
END$$

USE `cargo_transport_test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `cargo_transport_test`.`tsp_performance_BEFORE_INSERT`
BEFORE INSERT ON `cargo_transport_test`.`tsp_performance`
FOR EACH ROW
BEGIN
set NEW.`notified` = CURRENT_TIMESTAMP(); 
set NEW.`order_loading_date` = (select `loading_date` from `transport_order` where `transport_order`.`id` = NEW.`transport_order_id`); 
set NEW.`order_delivery_term` = (select `desired_delivery_term` from `transport_order` where `transport_order`.`id` = NEW.`transport_order_id`); 
set NEW.`order_loading_date` = (select `loading_date` from `transport_order` where `transport_order`.`id` = NEW.`transport_order_id`); 
set NEW.`order_line_id` = (select `line` from `transport_order` where `transport_order`.`id` = NEW.`transport_order_id`); 
set NEW.`order_tender_id` = (select `tender_id` from `transport_order` where `transport_order`.`id` = NEW.`transport_order_id`); 
set NEW.`order_customer_id` = (select `customer_id` from `transport_order` where `transport_order`.`id` = NEW.`transport_order_id`); 
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
