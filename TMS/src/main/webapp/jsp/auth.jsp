<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
</head>
<body>
	<%@include file="internatiolization.jsp"%>
	<h1><fmt:message key="auth" bundle="${bndle}" /></h1>
	<fmt:message key="userLoginPlaceHolder" bundle="${bndle}" var="ul" />
	<fmt:message key="userPasswordPlaceHolder" bundle="${bndle}" var="up" />

	<form action="${pageContext.request.contextPath}/" method="post">
		<input type="hidden" name="commandName" value="loginUser" /> 
		<label for="lgn">Your login, please :</label> 
		<input id="lgn" type="text" placeholder="${ul}" name="Login" /> 
		<label for="pswrd">Your password, please :</label>
		<input id="pswrd" type="text" placeholder="${up}" name="Password" />
		<button type="submit"> <fmt:message key="login" bundle="${bndle}" /></button>
	</form>

	<br>
	<c:if test="${empty sessionScope.user}">
		<a href="${pageContext.request.contextPath}/reg"><fmt:message key="registration" bundle="${bndle}" /></a>
	</c:if>
	<c:if test="${requestScope.loginSuccess}">
	    <br>
		<fmt:message key="LogStatus" bundle="${bndle}" var="logged" />
		<c:out value="${logged} ${sessionScope.user.name}" />
	</c:if>
	<c:if test="${requestScope.loginFailure}">
		<br>
		<fmt:message key="loginFailure" bundle="${bndle}" />
	</c:if>
		<c:out value="${warning}" />
	<br>
	<c:choose>
		<c:when test="${not empty sessionScope.user}">
			<a href="${pageContext.request.contextPath}/"><fmt:message key="startInvitation" bundle="${bndle}" /></a>
		</c:when>
		<c:otherwise>
			<a href="${pageContext.request.contextPath}/"><fmt:message key="startPageLink" bundle="${bndle}" /></a>
		</c:otherwise>
	</c:choose>
</body>
</html>