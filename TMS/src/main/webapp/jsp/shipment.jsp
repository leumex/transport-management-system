<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ym" uri="mytags"%>
<html>
<head>
<title>Shipment # ${order_id}</title>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/js/shipment.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/shipment.css" />
</head>
<body onload="troubleshootingDisplayTermsPopup();">
	<%@include file="internatiolization.jsp"%>
	<%@page import="by.minsk.training.entity.TspPerformance.TspResponse"%>
	<%@page import="by.minsk.training.ApplicationContext"%>
	<%@page import="by.minsk.training.command.pagination.PaginationIndices"%>
	<%@page import="by.minsk.training.user.UserEntity.Roles"%>
	<%@page import="by.minsk.training.entity.TransportOrder.State"%>
	<c:set var="transportOrderService" value="${sessionScope.applicationContext.getBean('TransportOrderService')}" />
	<c:set var="routeService" value="${sessionScope.applicationContext.getBean('RouteService')}" />
	<c:set var="truckloadService" value="${sessionScope.applicationContext.getBean('TruckloadService')}" />
	<c:set var="spotOrderService" value="${sessionScope.applicationContext.getBean('SpotOrderService')}" />
	<c:set var="bidsService" value="${sessionScope.applicationContext.getBean('BidsService')}" />
	<c:set var="tspPerformanceService" value="${sessionScope.applicationContext.getBean('TspPerformanceService')}" />
	<c:set var="order_id" value="${not empty param.id ? param.id : sessionScope.order_id}" scope="session" />
	<h1>
		<fmt:message key="shipmentDetailsTitle" bundle="${bndle}" />
		<c:out value="${param.id}" />
	</h1>
	<c:set var="order" value="${transportOrderService.findOrder(order_id)}" />
	<c:set var="assignment" value="${transportOrderService.getLatestAssignment(user, order)}" />
	<c:set var="rendered_order" value="${user.role eq 'TRANSPORT_SERVICE_PROVIDER' ? transportOrderService.restoreOrder(assignment) : order}" />

	<!-- DETAILED LAYOUT OF THE ORDER START  -->
	<table>
		<tr>
			<td><c:out value="Id: ${rendered_order.id}" /></td>
			<td><c:out value="Date: ${rendered_order.issued}" /></td>
			<td><c:out value="Loading date: ${rendered_order.loadingDate}" /></td>
		</tr>
		<tr>
			<td><c:out value="Loading address: ${routeService.deliverStartAddress(rendered_order.line.route)}" /></td>
			<td><c:out value="Unloading address: ${routeService.deliverDestinationAddress(rendered_order.line.route)}" /></td>
			<td><c:out value="Truckload: ${truckloadService.deliverRepresentation(rendered_order.line.truckload)}" /></td>
		</tr>
		<tr>
			<td><c:out value="Delivery term: ${rendered_order.deliveryTerm} days" /></td>
			<td><c:out value="State: ${rendered_order.state.toString()}" /></td>
			<td><c:out value="Scope: ${rendered_order.tender != null ? rendered_order.tender.title.concat(' from ')
.concat(rendered_order.tender.releaseDate.toString()) : rendered_order.state eq 'CREATED' ? 'no scope' : 'spot'}" /></td>
		</tr>
		<tr>
			<c:if test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
				<td><c:out value="Customer: ${rendered_order.tender != null ? rendered_order.tender.customer.name : rendered_order.customer.name}" />
				</td>
			</c:if>
			<c:if
				test="${rendered_order.state ne 'CREATED' || rendered_order.state ne 'ANNOUNCED' || rendered_order.state ne 'WITHDRAWN'}">
				<td><c:out
						value="Rate: ${transportOrderService.defineOrderRate(order)}" /></td>
				<td><c:out
						value="Assigned to ${assignment.tsp.name} on ${assignment.notified}; current tsp response ${assignment.tspResponse}" /></td>
			</c:if>
		</tr>
	</table>
	<!-- DETAILED LAYOUT OF THE ORDER END  -->

	<!-- COMMON FORM OF THE PAGE START  -->
	<form action="${pageContext.request.contextPath}/" id="shipment_form"
		method="post">
		<input type="hidden" name="local_url" value="/shipments/shipment" />
		<input type="hidden" name="commandName" id="command_name" /> 
		<input type="hidden" name="order_id" value="${order_id}" />
	</form>
	<!-- COMMON FORM OF THE PAGE END -->

	<!-- ASSIGNMENTS STORY SECTION START -->
	<c:if test="${user.role ne 'TRANSPORT_SERVICE_PROVIDER'}">
		<c:set var="former_assignments" value="${tspPerformanceService.getOrderAssignments(order)}" />
		<c:if test="${former_assignments ne null && former_assignments.size() > 1 && assignment ne null && former_assignments.remove(assignment)}">
			<h5>Assignments story</h5>
		</c:if>
		<c:forEach var="a" items="${former_assignments}"></c:forEach>
		<c:out
			value="${a.notified.concat(' ').concat(a.tsp.name).concat(' responded at ').concat(a.responseTime).concat(', eventually: ').concat(a.tspResponse)}" />
		<br>
	</c:if>
	<!-- ASSIGNMENTS STORY SECTION END -->

	<!-- ORDER EVENTS TRACKING SECTION START -->
	<!-- Are taken from latest assignment. 
Is available for CUSTOMER and ADMIN users when order has state not in (CREATED, ANNOUNCED, ASSIGNED)
Is available for TSP users when order has state in (ACCEPTED, IN_ACTION, COMPLETED) 
and WITHDRAWN - when correspondent assignment.orderFootprint.State is in (CANCELLED, INTERRUPTED) -->
	<c:if
		test="${assignment ne null && transportOrderService.trackingEventsToRender(user, assignment)}">
		<table>
			<tr>
				<td>Truck tractor: <c:choose>
						<c:when test="${user.role eq 'CUSTOMER'}">
							<c:out value="${assignment.truckPlates.split('/')[0]}" />
						</c:when>
						<c:when
							test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER' && (order.state eq 'COMPLETED' or order.state eq 'WITHDRAWN')}">
							<c:out value="${assignment.truckPlates.split('/')[0]}" />
						</c:when>
						<c:otherwise>
							<input type="text" form="shipment_form" name="tractor"
								value="${assignment.truckPlates.split('/')[0]}" />
							<br>
							<c:out value="${wrongTractor}" />
						</c:otherwise>
					</c:choose>
				</td>
				<td>Semitrailer: <c:choose>
						<c:when test="${user.role eq 'CUSTOMER'}">
							<c:out value="${assignment.truckPlates.split('/')[1]}" />
						</c:when>
						<c:when
							test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER' && (order.state eq 'COMPLETED' or order.state eq 'WITHDRAWN')}">
							<c:out value="${assignment.truckPlates.split('/')[1]}" />
						</c:when>
						<c:otherwise>
							<input type="text" form="shipment_form" name="semitrailer"
								value="${assignment.truckPlates.split('/')[1]}" />
							<br>
							<c:out value="${wrongSemitrailer}" />
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<td>Arrived for loading: <c:choose>
						<c:when test="${user.role eq 'CUSTOMER'}">
							<c:out value="${assignment.arrivedForUnloading}" />
						</c:when>
						<c:when
							test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER' && (order.state eq 'COMPLETED' || order.state eq 'WITHDRAW')}">
							<c:out value="${assignment.arrivedForUnloading}" />
						</c:when>
						<c:otherwise>
							<input type="datetime" form="shipment_form"
								name="loading_arrival" value="${assignment.arrivedForLoading}" />
							<br>
							<c:out value="${wrongLoading_arrival}" />
						</c:otherwise>
					</c:choose>
				</td>
				<td>Left loading: <c:choose>
						<c:when test="${user.role eq 'CUSTOMER'}">
							<c:out value="${assignment.leftLoading}" />
						</c:when>
						<c:when
							test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER' && (order.state eq 'COMPLETED' || order.state eq 'WITHDRAWN')}">
							<c:out value="${assignment.leftLoading}" />
						</c:when>
						<c:otherwise>
							<input type="datetime" form="shipment_form"
								name="loading_departure" value="${assignment.leftLoading}" />
							<br>
							<c:out value="${wrongLoading_departure}" />
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<td>Arrived for unloading: <c:choose>
						<c:when test="${user.role eq 'CUSTOMER'}">
							<c:out value="${assignment.arrivedForUnloading}" />
						</c:when>
						<c:when
							test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER' && (order.state eq 'COMPLETED' || order.state eq 'WITHDRAWN')}">
							<c:out value="${assignment.arrivedForUnloading}" />
						</c:when>
						<c:otherwise>
							<input type="datetime" form="shipment_form"
								name="unloading_arrival"
								value="${assignment.arrivedForUnloading}" />
							<br>
							<c:out value="${wrongUnloading_arrival}" />
						</c:otherwise>
					</c:choose>
				</td>
				<td>Left unloading: <c:choose>
						<c:when test="${user.role eq 'CUSTOMER'}">
							<c:out value="${assignment.leftUnloading}" />
						</c:when>
						<c:when
							test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER' && (order.state eq 'COMPLETED' || order.state eq 'WITHDRAWN')}">
							<c:out value="${assignment.leftUnloading}" />
						</c:when>
						<c:otherwise>
							<input type="datetime" form="shipment_form"
								name="unloading_departure" value="${assignment.leftUnloading}" />
							<br>
							<c:out value="${wrongUnloading_departure}" />
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
		<br>
		<button type="button" onclick="trackingUpdate();">Save</button>
	</c:if>
	<!-- ORDER EVENTS TRACKING SECTION END -->

	<!-- 	LOADING DATE AND DELIVERY TERM AMENDMENT START -->
	<c:if
		test="${user.role eq 'CUSTOMER' && (order.state eq 'CREATED' || order.state eq 'WITHDRAWN')}">
		<button type="button" onclick="openNewOrderTerms();">Revise
			order terms</button>
	</c:if>
	<div class="separate_window" id="orderTermsPopup">
		<form action="${pageContext.request.contextPath}/"
			id="termsRevizePopup" method="post">
			<input type="hidden" name="commandName" id="popupCommand" /> 
			<input type="hidden" name="local_url" value="/shipments/shipment" /> 
			<input type="hidden" name="termsRevizePopupFlag" id="termsRevizePopupFlag" value="${param.termsRevizePopupFlag}" /> 
			<label for="new_loading_date">New loading date: </label> 
			<input type="date" name="new_loading_date" id="new_loading_date"
				value="${empty param.new_loading_date ? order.loadingDate : param.new_loading_date}" />
			<label for="new_delivery_term">New delivery term: </label> 
			<input type="text" name="new_delivery_term" id="new_delivery_term"
				value="${empty param.new_delivery_term ? order.deliveryTerm : param.new_delivery_term}" />
			<input type="hidden" name="orderId" value="${order_id}" />
			<button type="button" onclick="submitNewOrderTerms();">Save</button>
		</form>
		<br>
		<c:out value="${wrongLoadingDate}" />
		<br>
		<c:out value="${wrongDeliveryTerm}" />
		<br>
		<c:out value="${wrongOrderId}" />
		<br>
		<c:out value="${updateFailure}" />
		<button type="button" class="cancel" onclick="closePopup();">close</button>
	</div>
	<!-- 	LOADING DATE AND DELIVERY TERM AMENDMENT END -->

	<!-- 	ORDER HANDLING SECTION START -->
	<c:choose>
		<c:when
			test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER' && order.state eq 'ASSIGNED' && assignment.tspResponse eq 'OUTSTANDING'}">
			<button type="button" onclick="acceptOrder();">Accept transport order</button>
			<br>
			<button type="button" onclick="rejectOrder();">Reject transport order</button>
		</c:when>
		<c:when
			test="${user.role eq 'CUSTOMER' && (order.state ne 'CREATED' || order.state ne 'ANNOUNCED' || 
		order.state ne 'WITHDRAWN' || order.state ne 'COMPLETED')}">
			<button type="button" onclick="recallTheOrder();">recall order</button>
		</c:when>
		<c:when
			test="${user.role eq 'CUSTOMER' && (order.state eq 'CREATED' || order.state eq 'WITHDRAWN')}">
			<c:if
				test="${order.tender != null && order.tender.tenderEnd.isAfter(order.loadingDate)}">
Select the tsp to assign the order to: <select name="assignee" form="shipment_form">
					<option selected disabled>select tsp</option>
					<c:forEach var="tlquery" items="${tenderService.retrieveLineWinners(order.tender, order.line, sessionScope.user)}">
						<c:set var="tsp" value="${tlquery.participation.tsp}" />
						<option value="${tsp.id}">
						<c:out  value="${tsp.name.concat(' ').concat(ToolSet.round((tlq.awardedShare*100), 2)).concat(' %')}" /></option>
					</c:forEach>
				</select>
				<button type="button" onclick="assignTenderOrder();">assign order</button>
				<c:out value="${wrongTspId}" />
				<c:out value="${wrongOrderId}" />
				<c:out value="${systemMistake}" />
			</c:if>
			<c:if test="${order.tender.tenderEnd.isBefore(order.loadingDate)}">Tender 
				<c:out value="${tender.id.concat('is over on ').concat(order.tender.tenderEnd)}" />
			</c:if>
			<!-- 	ORDER HANDLING SECTION END -->

			<!-- SPOT AUCTION SETUP AND ABORT SECTION START -->
			<c:set var="spotOrder" value="${spotOrderService.getSpotOrder(order)}" />
			<c:choose>
				<c:when test="${spotOrder == null}">
					<button type="button" onclick="releaseSpotAuction();">release spot auction</button>
					<c:out value="${spotReleaseFailure}" />
				</c:when>
				<c:otherwise>
					<c:if test="${spotOrderService.hasOpenSpotAuction(order)}">
						<button type="button" onclick="abortSpotAuction();">cancel spot auction</button>
						<c:out value="${spotAbortFailure}" />
					</c:if>
				</c:otherwise>
			</c:choose>
		</c:when>
	</c:choose>
	<!-- SPOT AUCTION SETUP AND ABORT SECTION END -->

	<!-- SPOT BIDS AND ASSIGNMENT BLOCK START -->
	<c:set var="spotBids"
		value="${sessionScope.spotBids == null ? (spotOrder ne null ? spotOrderService.obtainRelativeBids(spotOrder) : null) : sessionScope.spotBids}" />

	<c:if test="${spotOrder ne null}">
		<c:if test="${spotOrderService.hasOpenSpotAuction(order)}">Spot deadline: <c:out value="${spotOrder.spotDeadline}" />
		</c:if>
		<c:choose>
			<c:when test="${user.role ne Roles.TRANSPORT_SERVICE_PROVIDER}">
				<ym:paging source="${spotOrderService.obtainRelativeBids(spotOrder)}"
					size="15" indexName="${PaginationIndices.BIDS_INDEX.provideIndex()}">
					<table>
						<tr>
							<c:if test="${spotOrderService.hasOpenSpotAuction(order)}">
								<th>Id</th>
							</c:if>
							<th>Bid moment</th>
							<th>Bid provider</th>
							<th><a href="#" onclick="sortByRate();">Rate</a></th>
							<th>Line-relative tender nomination</th>
							<th>Assignment</th>
						</tr>
						<c:forEach var="offer" items="${bunch}">
							<tr>
								<c:if
									test="${spotOrderService.hasOpenSpotAuction(order) || user.role eq 'ADMIN'}">
									<td><input type="radio" form="shipment_form" name="bid_id" value="${bid.id}"></td>
								</c:if>
								<td><c:out value="${offer.madeAt}" /></td>
								<td><c:out value="${offer.tsp.name}" /></td>
								<td><c:out value="${offer.bid}" /></td>
								<td><c:forEach var="query" items="${tenderService.checkValidLineTenderObligations(order.line, order.customer, tsp)}">
									   <c:out value="${query.tenderLine.tender.name.concat(' from ').concat(
query.tenderLine.tender.releaseDate).concat('; rate is ').concat(query.rate).concat('; line share is ')
.concat(ToolSet.round((query.awardedShare)*100, 2)).concat('%')}" />
										<br>
									</c:forEach></td>
							</tr>
							<tr>
								<c:out value="${spotOrderService.reflectSpotOrderAssignment(spotOrder, offer.tsp)}" />
							</tr>
						</c:forEach>
					</table>
				</ym:paging>
				<c:if
					test="${user.role eq 'CUSTOMER' && spotOrderService.hasOpenSpotAuction(order) 
				&& spotOrderService.obtainRelativeBids(spotOrder).size() > 0}">
					<button type="button" onclick="assignOverSpot();">bestow the order</button>
				</c:if>
				<!-- BID ELIMINATION IF THERE'S NO BOUND ASSIGNMENT, START -->
				<c:if test="${user.role eq 'ADMIN'}">
					<button type="button" onclick="deleteBid();">Delete bid</button>
					<c:out value="${bidEliminationError}" />
				</c:if>
				<!-- BID ELIMINATION IF THERE'S NO BOUND ASSIGNMENT, END -->
			</c:when>
			<c:when test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
				<!-- BID PROVISION FORM START -->
Spot deadline: <c:out value="${spotOrder.spotDeadline}" />
				<c:set var="tspBid" value="${bidsService.retrieveBid(spotOrder, user)}" />
				<c:set var="tspSpotAssignment" value="${tspBid ne null ? tspPerformanceService.getSpotOrderAssignment(tspBid) : null}" />
Your bid: <c:if test="${spotOrder.spotDeadline.isAfter(LocalDateTime.now()) && tspSpotAssignment eq null}">
					<input type="text" form="shipment_form" id="tsp_bid" name="tsp_bid" value="${tspBid.bid}" />
					<input type="hidden" name="orderId" form="shipment_form" value="${sessionScope.order_id}" />
					<c:if test="${tspBid ne null}">
						<c:out value=" from ${tspBid.madeAt}" />
					</c:if>
					<c:out value="${wrongBid}" />
					<c:out value="${bidOutOfTime}" />
					<button type="button" onclick="submitBid();">submit bid</button>
					<c:if test="${tspBid ne null}">
						<input type="hidden" form="shipment_form" name="bid_id" value="${tspBid.id}" />
						<button type="button" onclick="deleteBid();">drop the bid</button>
					</c:if>
				</c:if>
				<c:if test="${spotOrder.spotDeadline.isBefore(LocalDateTime.now())}">
					<c:out value="${tspBid.bid += (tspBid ne null ? (' from ' += (tspBid.madeAt += (tspSpotAssignment ne null ? ' - has won' : ''))) : '')}" />
				</c:if>
				<c:out value="${bidEliminationError}" />
				<!-- BID PROVISION FORM END -->
			</c:when>
		</c:choose>
	</c:if>
	<!-- SPOT BIDS AND ASSIGNMENT BLOCK END -->

	<a href="${pageContext.request.contextPath}/shipments"><fmt:message key="shipmentsLink" bundle="${bndle}" /></a>
	<a href="${pageContext.request.contextPath}/"><fmt:message key="startPageLink" bundle="${bndle}" /></a>
</body>
</html>
