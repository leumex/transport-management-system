<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ym" uri="mytags"%>
<html>
<head>
<title>Spot queries</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/spot.js"></script>
</head>
<body>
	<%@include file="internatiolization.jsp"%>
	<%@page import="by.minsk.training.command.pagination.PaginationIndices"%>
	<h1>
		<fmt:message key="spotLink" bundle="${bndle}" />
		<c:out value=" ${param.shipmentId}" />
	</h1>
	<c:set var="spotOrderService" value="${sessionScope.applicationContext.getBean('SpotOrderService')}" />
	<c:set var="routeService" value="${applicationContext.getBean('RouteService')}" />
	<c:set var="truckloadService" value="${applicationContext.getBean('TruckloadService')}" />
	<c:set var="bidsService" value="${applicationContext.getBean('BidsService')}" />
	<c:set var="spots" value="${spotOrderService.retrieveTspAvailableSpotAuctions(sessionScope.user)}" />
	<c:choose>
		<c:when test="${spots == null || spots.isEmpty()}">
			<c:out value="There's no available spot auction currently" />
		</c:when>
		<c:otherwise>
			<form action="${pageContext.request.contextPath}/" id="spot_form"
				method="post">
				<input type="hidden" name="local_url" value="/spot"/>
				<input type="hidden" name="commandName" id="commandName"/>
				<ym:paging source="${spots}" size="5"
					indexName="${PaginationIndices.SPOTS_INDEX.provideIndex()}">

					<table>
						<tr>
							<th>Id</th>
							<th>Transport Order</th>
							<th>Loading Date</th>
							<th>Route</th>
							<th>Cargo</th>
							<th>Deadline</th>
							<th>Your rate</th>
							<th></th>
						</tr>
						<c:forEach var="spotContest" items="${bunch}">
							<tr>
								<td><input type="radio" id="orderId" name="orderId"
									value="${spotContest.transportOrder.id}" /><label
									for="orderId"><c:out value="${spotContest.id}" /></label></td>
								<td><c:out value="${spotContest.transportOrder.id}" /></td>
								<td><c:out value="${spotContest.transportOrder.loadingDate}" /></td>
								<td><c:out value="${routeService.deliverRepresentation(spotContest.transportOrder.line.route)}" /></td>
								<td><c:out value="${truckloadService.deliverRepresentation(spotContest.transportOrder.line.truckload)}" /></td>
								<td><c:out value="${spotContest.spotDeadline}" /></td>
								<c:set var="rate" value="${bidsService.retrieveBid(spotContest, sessionScope.user)}" />
								<td><input type="text" id="userRate" name="tsp_bid" value="${rate.bid}" /></td>
								<td><a href="#" onclick="bidSave();">save/update bid</a><br>
									<a href="#" onclick="bidDrop();">drop bid</a></td>
							</tr>
						</c:forEach>
					</table>
				</ym:paging>
			</form>
		</c:otherwise>
	</c:choose>

	<h5>includes table with TOs that are available on spot and
		functions to be executed over TO: bids proposal, TO opening (link to
		shipment rendering page) - to TSP user</h5>
	<a href="${pageContext.request.contextPath}/"><fmt:message
			key="startPageLink" bundle="${bndle}" /></a>
</body>
</html>
