<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page isErrorPage="true"%>
<html>
<head>
<title>Oops...</title>
</head>
<body>
	<h3>
		<c:out
			value="${pageContext.errorData.requestURI} ${pageContext.request.queryString} has not been executed successfully" />
	</h3>
	Error code: ${pageContext.errorData.statusCode}
	<br> Error message: ${pageContext.exception.message}
	<br>
	<a href="${pageContext.request.contextPath}/">To main page</a>
	<br>
	<script>
		document.write('<a href="' + document.referrer + '">Go back</a>');
	</script>
</body>
</html>
