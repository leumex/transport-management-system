<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ym" uri="mytags"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/style.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/layout.js"></script>
<title>TMS 2020(c)</title>
</head>
<body>
	<c:if test="${not empty param.lang && param.lang ne applicationScope.language}">
		<c:set var="language" value="${param.lang}" scope="application" />
		<c:redirect context="${pageContext.request.contextPath}" url="/" />
	</c:if>
	<%@include file="internatiolization.jsp"%>
	<%@page import="by.minsk.training.ApplicationConstants"%>
	<%@page import="by.minsk.training.ApplicationContext"%>
	<%@page import="by.minsk.training.user.UserEntity.Roles" %>
	<%@page import="by.minsk.training.command.pagination.PaginationIndices" %>
	<c:set var="applicationContext" scope="session" value="${ApplicationContext.getInstance()}"/>
	<c:set var="notificationsTracker" value="${applicationContext.getBean('NotificationsTracker')}"/>
	<c:set var="transportOrderService" value="${applicationContext.getBean('TransportOrderService')}"/>
	<h1>
		<fmt:message key="startTitle" bundle="${bndle}" />
	</h1>
	<br>
	<div id="upper_right_corner">
		<c:if test="${empty user}">
			<a href="${pageContext.request.contextPath}/auth"><fmt:message key="login" bundle="${bndle}" /></a>
			<a href="${pageContext.request.contextPath}/reg"><fmt:message key="registration" bundle="${bndle}" /></a>
			<fmt:message key="langChange" bundle="${bndle}" var="chL" scope="application" />
		</c:if>
		<c:if test="${not empty user}">
			<fmt:message key="LogStatus" bundle="${bndle}" />
			<c:out value="${sessionScope.user.name}" />
			<form id="logout" action="${pageContext.request.contextPath}/" method="post">
				<input type="hidden" name="commandName" value="logoutUser">
				<input type="submit" value="logout" />
			</form>
		</c:if>
		<form action="${pageContext.request.contextPath}/" method="get">
			<select name="lang">
				<option disabled selected><fmt:message key="languageSelection" bundle="${bndle}" /></option>
				<option value="English">EN</option>
				<option value="Dutch">NL</option>
				<option value="German">DE</option>
				<option value="Russian">RU</option>
			</select> <input type="submit" value="${chL}" />
		</form>
	</div>
	<c:if test="${not empty user}">
		<a href="${pageContext.request.contextPath}/shipments"><fmt:message key="shipmentsLink" bundle="${bndle}" /></a>
		<a href="${pageContext.request.contextPath}/tenders"><fmt:message key="tendersLink" bundle="${bndle}" /></a>
		<c:if test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
			<a href="${pageContext.request.contextPath}/spot"><fmt:message key="spotLink" bundle="${bndle}" /></a>
		</c:if>
		<a href="${pageContext.request.contextPath}/report"><fmt:message key="reportLink" bundle="${bndle}" /></a>
		<br>
		<h5>Transport orders in focus</h5>
		<ym:paging source="${transportOrderService.findTopicalOrders(user)}" size="10" indexName="${PaginationIndices.HOT_ORDERS.provideIndex()}">
		<table>
			<tr>
				<th>Id</th>
				<th>Tender</th>
				<c:if test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
					<th>Customer</th>
				</c:if>
				<th>State</th>
				<th>Issued</th>
				<th>Loading date</th>
				<th>Source city</th>
				<th>Source country</th>
				<th>Destination city</th>
				<th>Destination country</th>
				<th>Goods</th>
				<th>Weight</th>
			</tr>
			<c:forEach var="order" items="${bunch}">
				<tr>
					<td><c:out value="${order.id}" /></td>
					<td><c:choose>
							<c:when test="${order.tender != null}">
								<c:out value="${order.tender.title}" />
							</c:when>
							<c:otherwise>
								<c:out value="spot" />
							</c:otherwise>
						</c:choose></td>
					<c:if test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
						<td><c:choose>
								<c:when test="${order.tender != null}">
									<c:out value="${order.tender.customer.name}" />
								</c:when>
								<c:otherwise>
									<c:out value="${order.customer.name}" />
								</c:otherwise>
							</c:choose></td>
					</c:if>
					<td><c:out value="${order.state}" /></td>
					<td><c:out value="${order.issued}" /></td>
					<td><c:out value="${order.loadingDate}" /></td>
					<td><c:out value="${order.line.route.start.locality.name}" /></td>
					<td><c:out value="${order.line.route.start.locality.country.name}" /></td>
					<td><c:out value="${order.line.route.destination.locality.name}" /></td>
					<td><c:out value="${order.line.route.destination.locality.country.name}" /></td>
					<td><c:out value="${order.line.truckload.name}" /></td>
					<td><c:out value="${order.line.truckload.weight}" /></td>
				</tr>
			</c:forEach>
		</table>
		</ym:paging>
		<br>
 
		<ym:paging source="${notificationsTracker.updateNotifications(user)}" size="10" indexName="${PaginationIndices.NOTIFICATIONS_INDEX.provideIndex()}">
		   <c:if test="${bunch.size() gt 0}">
		       <h3>Actual notifications</h3>
		   </c:if>
		<table>
			<tr>
				<th>Notification</th>
				<th>Description</th>
				<th>Reference</th>
			</tr>
			<c:forEach var="n" items="${bunch}">
				<tr>
					<td><c:out value="${n.type}" /></td>
					<td><c:out value="${n.description}" /></td>
					<td><c:choose>
							<c:when test="${n.type.targetObject == 'transportOrder'}">
								<a href="${pageContext.request.contextPath}/shipments/shipment?id=${n.referencedObject}"><c:out
										value="shipment ${n.referencedObject}" /></a>
							</c:when>
							<c:when test="${n.type.targetObject=='tenderContest'}">
								<a href="${pageContext.request.contextPath}/tenders/tender/contest?id=${n.referencedObject}"><c:out
										value="tender ${n.referencedObject} contest" /></a>
							</c:when>
							<c:when test="${n.type.targetObject=='spotContest'}">
								<a href="${pageContext.request.contextPath}/spot?id=${n.referencedObject}"><c:out
										value="shipment ${n.referencedObject} spot contest" /></a>
							</c:when>
						</c:choose></td>
				</tr>
			</c:forEach>
		</table>
		</ym:paging>
	</c:if>
	<c:if test="${empty user}">
		<h3>
			<fmt:message key="greeting" bundle="${bndle}" />
		</h3>
		<br>
		<p>
			<fmt:message key="toProceed" bundle="${bndle}" />
			<a href="${pageContext.request.contextPath}/auth"><fmt:message key="login" bundle="${bndle}" /></a>
		</p>
	</c:if>	
</body>
</html>