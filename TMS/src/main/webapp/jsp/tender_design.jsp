<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ym" uri="mytags"%>
<html>
<c:set var="local_url" scope="session" value="/tenders/create"/>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/tender_design.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/tender_design.js"></script>
<title>New tender</title>
</head>
<body
	onload="displayLine('renderLineId'); displayAddressForm('newAddressCountry'); displayTroubleshootingAddressForm(); displayTroubleshootingCountryForm(); displayTroubleshootingLocalityForm();">

	<jsp:include page="../jsp/popUps.jsp"/>
	<%@include file="internatiolization.jsp"%>
	<%@ page import="by.minsk.training.entity.Truckload.TruckType"%>
	<%@ page import="by.minsk.training.command.pagination.PaginationIndices" %>

	<c:set var="countryService" value="${sessionScope.applicationContext.getBean('CountryService')}" />
	<c:set var="localityService" value="${sessionScope.applicationContext.getBean('LocalityService')}" />
	<c:set var="addressService" value="${sessionScope.applicationContext.getBean('AddressService')}" />
	<c:set var="truckloadService" value="${sessionScope.applicationContext.getBean('TruckloadService')}" />
	<c:set var="routeService" value="${sessionScope.applicationContext.getBean('RouteService')}" />
	<c:set var="lineService" value="${sessionScope.applicationContext.getBean('LineService')}" />

	<h1>
		<fmt:message key="newTender" bundle="${bndle}" />
	</h1>
	
	<!-- NEW TENDER FORM BEGINNING -->
	<h3>Tender design form</h3>
	<c:set var="newTenderTitle" scope="session" value="${empty param.tenderTitle ? sessionScope.newTenderTitle : param.tenderTitle}" />
	<c:set var="newTenderTripsQuantity" scope="session" value="${empty param.tripsQuantity ? sessionScope.newTenderTripsQuantity : param.tripsQuantity}" />
	<c:set var="newTenderStart" scope="session" value="${empty param.tenderStart ? sessionScope.newTenderStart : param.tenderStart}" />
	<c:set var="newTenderEnd" scope="session" value="${empty param.tenderEnd ? sessionScope.newTenderEnd : param.tenderEnd}" />

	<form name="newTenderForm" id="newTenderForm" action="${pageContext.request.contextPath}/" method="post">
		<input type="hidden" name="local_url" value="/tenders/create" /> 
		<input type="hidden" name="commandName" id="saveNewTender" value="saveNewTender" />
		<table>
			<tr>
				<td><label for="tenderTitle">Title: </label>
				    <input type="text" name="tenderTitle" id="tenderTitle" value="${sessionScope.newTenderTitle}"
					placeholder="title of new tender" onchange="newTenderFormFieldsUpload();" /> <br>
					<div class="alarm">
						<c:out value="${wrongNewTenderTitle}" />
					</div></td>
				<td><label for="">Quantity of trips: </label>
				    <input type="text" name="tripsQuantity" id="tripsQuantity" value="${sessionScope.newTenderTripsQuantity}"
					placeholder="approx. quantity of new tender" onchange="newTenderFormFieldsUpload();" /> <br>
					<div class="alarm">
						<c:out value="${wrongNewTenderTripsQuantity}" />
					</div></td>
				<td><label for="tenderStart">Tender begins: </label>
				    <input type="date" name="tenderStart" id="tenderStart" value="${sessionScope.newTenderStart}"
					placeholder="date of tender start" onchange="newTenderFormFieldsUpload();" /> <br>
					<div class="alarm">
						<c:out value="${wrongNewTenderStart}" />
					</div></td>
				<td><label for="tenderEnd">Tender ends: </label>
				    <input type="date" name="tenderEnd" id="tenderEnd" value="${sessionScope.newTenderEnd}" 
				    placeholder="date of tender end" onchange="newTenderFormFieldsUpload();" /> <br>
					<div class="alarm">
						<c:out value="${wrongNewTenderEnd}" />
					</div></td>
			</tr>
		</table>
	</form>

	<c:set var="newTenderNewLines" scope="session" value="${lineService.getUserRelativeLines(user)}"/>
	<c:set var="newTenderNewLinesToRender" scope="session"
		value="${empty sessionScope.newTenderNewLinesToRender ? (param.prefix2 eq 'newLine' and param.commandName eq 'filterLines' ? 
		sessionScope.newTenderNewLinesToRender : newTenderNewLines) : sessionScope.newTenderNewLinesToRender}"/>
	<c:set var="newTenderNewLinesToRender" scope="session" value="${not empty sessionScope.newTenderNewLinesComparator ? (
	    param.prefix2 eq 'newLine' and (param.commandName eq 'filterLines' or param.commandName eq 'resetLinesFilter' or param.commandName eq 'sortLines') ? 
	    lineService.sort(sessionScope.newTenderNewLinesToRender, sessionScope.newTenderNewLinesComparator) : 
	    sessionScope.newTenderNewLinesToRender) : sessionScope.newTenderNewLinesToRender}"/>
	    
	<c:set var="newTenderSelectedLinesToRender" scope="session"
		value="${empty sessionScope.newTenderSelectedLinesToRender ? (param.prefix2 eq 'selectedLine' and param.commandName eq 'filterLines' ? 
		sessionScope.newTenderSelectedLinesToRender : newTenderSelectedLines) : sessionScope.newTenderSelectedLinesToRender}"/>
	<c:set var="newTenderSelectedLinesToRender" scope="session" value="${not empty sessionScope.newTenderSelectedLinesComparator ? (
	    param.prefix2 eq 'selectedLine' and (param.commandName eq 'filterLines' or param.commandName eq 'resetLinesFilter' or param.commandName eq 'sortLines') ? 
	    lineService.sort(sessionScope.newTenderSelectedLinesToRender, sessionScope.newTenderSelectedLinesComparator) : 
	    sessionScope.newTenderSelectedLinesToRender) : sessionScope.newTenderSelectedLinesToRender}"/>
	<!-- NEW TENDER FORM END -->

	<!-- LINES FILTER / NEW LINE DESIGN FORM BEGINNING-->
	<h4>Lines filter and design</h4>
	<c:set var="newTenderNewLinePickupCountry"
		value="${empty param.newLinePickupCountry ? sessionScope.newTenderNewLinePickupCountry : param.newLinePickupCountry}"
		scope="session" />
	<c:set var="newTenderNewLinePickupLocality"
		value="${empty param.newLinePickupLocality ? sessionScope.newTenderNewLinePickupLocality : param.newLinePickupLocality}"
		scope="session" />
	<c:set var="newTenderNewLinePickupAddress"
		value="${empty param.newLinePickupAddress ? sessionScope.newTenderNewLinePickupAddress : param.newLinePickupAddress}"
		scope="session" />
	<c:set var="newTenderNewLineDeliveryCountry"
		value="${empty param.newLineDeliveryCountry ? sessionScope.newTenderNewLineDeliveryCountry : param.newLineDeliveryCountry}"
		scope="session" />
	<c:set var="newTenderNewLineDeliveryLocality"
		value="${empty param.newLineDeliveryLocality ? sessionScope.newTenderNewLineDeliveryLocality : param.newLineDeliveryLocality}"
		scope="session" />
	<c:set var="newTenderNewLineDeliveryAddress"
		value="${empty param.newLineDeliveryAddress ? sessionScope.newTenderNewLineDeliveryAddress : param.newLineDeliveryAddress}"
		scope="session" />
	<c:set var="newTenderNewLineCargoName"
		value="${empty param.newLineCargoName ? sessionScope.newTenderNewLineCargoName : param.newLineCargoName}"
		scope="session" />
	<c:set var="newTenderNewLineLoadUnits"
		value="${empty requestScope.newLineLoadUnits ? 
		(empty param.newLineLoadUnits ? sessionScope.newTenderNewLineLoadUnits : param.newLineLoadUnits) 
		: requestScope.newLineLoadUnits}" scope="session" />
	<c:set var="newTenderNewLineCargoWeight"
		value="${empty requestScope.newLineCargoWeight ? 
		(empty param.newLineCargoWeight ? sessionScope.newTenderNewLineCargoWeight : param.newLineCargoWeight) 
		: requestScope.newLineCargoWeight}" scope="session" />
	<c:set var="newTenderNewLineCargoTruckType"
		value="${empty param.newLineCargoTruckType ? sessionScope.newTenderNewLineCargoTruckType : param.newLineCargoTruckType}"
		scope="session" />
	<c:set var="newTenderNewLineRouteConditions"
		value="${empty param.newLineRouteConditions ? sessionScope.newTenderNewLineRouteConditions : param.newLineRouteConditions}"
		scope="session" />

	<form name="lineDefinition" id="lineDefinition" action="${pageContext.request.contextPath}/" method="post">
		<input type="hidden" name="local_url" value="/tenders/create" /> 
		<input type="hidden" name="prefix1" value= "newTenderNewLine" />
		<input type="hidden" name="prefix2" value= "newLine" />
		<input type="hidden" name="renderLineId" id="renderLineId" /> 
		<input type="hidden" name="addressCountry" id="addressCountry" />
		<input type="hidden" name="sortArg" id="newSortArg" /> 
		<input type="hidden" name="commandName" id="lineDefinitionCommand" value="saveNewLine" /> 
		<table>
			<tr>
				<td><label for="newLinePickupCountry">Country of pickup: </label>
				<select name="newLinePickupCountry" id="newLinePickupCountry" onchange="filterLines();">
						<option disabled selected>select country of pickup</option>
						<c:forEach var="pickupCountry" items="${countryService.getAll()}">
							<option value="${pickupCountry.id}"
								${sessionScope.newTenderNewLinePickupCountry == pickupCountry.id ? 'selected':''}>
								<c:out value="${pickupCountry.name}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openCountryForm()">new</button><br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongPickupCountry}" />
					</div></td>
				<td><label for="newLinePickupLocality">Locality of pickup: </label>
				<select name="newLinePickupLocality" id="newLinePickupLocality" onchange="filterLines();">
						<option disabled selected>select locality of pickup</option>
						<c:forEach var="pickupLocality"
							items="${localityService.getByCountry(sessionScope.newTenderNewLinePickupCountry)}">
							<option value="${pickupLocality.id}"
								${sessionScope.newTenderNewLinePickupLocality == pickupLocality.id ? 'selected':''}>
								<c:out value="${pickupLocality.name}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openLocalityForm()">new</button><br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongPickupLocality}" />
					</div></td>
				<td><label for="newLinePickupAddress">Address of pickup: </label>
				<select name="newLinePickupAddress" id="newLinePickupAddress" onchange="filterLines();">
						<option disabled selected>select address of pickup</option>
						<c:forEach var="pickupAddress"
							items="${addressService.getByLocality(sessionScope.newTenderNewLinePickupLocality)}">
							<option value="${pickupAddress.id}"
								${sessionScope.newTenderNewLinePickupAddress == pickupAddress.id ? 'selected':''}>
								<c:out value="${pickupAddress.details}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openAddressForm()">new</button> <br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongPickupAddress}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="newLineDeliveryCountry">Country of delivery: </label>
				<select name="newLineDeliveryCountry" id="newLineDeliveryCountry" onchange="filterLines();">
						<option disabled selected>select country of delivery</option>
						<c:forEach var="deliveryCountry"
							items="${countryService.getAll()}">
							<option value="${deliveryCountry.id}"
								${sessionScope.newTenderNewLineDeliveryCountry == deliveryCountry.id ? 'selected':''}>
								<c:out value="${deliveryCountry.name}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openCountryForm()">new</button><br>
					<div class="alarm">
						<c:out
							value="${newTenderNewLineWrongDeliveryCountry}" />
					</div></td>
				<td><label for="newLineDeliveryLocality">Locality of delivery: </label>
				<select id="newLineDeliveryLocality" name="newLineDeliveryLocality" onchange="filterLines();">
						<option disabled selected>select delivery
							locality</option>
						<c:forEach var="deliveryLocality"
							items="${localityService.getByCountry(sessionScope.newTenderNewLineDeliveryCountry)}">
							<option value="${deliveryLocality.id}"
								${sessionScope.newTenderNewLineDeliveryLocality == deliveryLocality.id ? 'selected' : ''}>
								<c:out value="${deliveryLocality.name}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openLocalityForm()">new</button> <br>
					<div class="alarm">
						<c:out
							value="${newTenderNewLineWrongDeliveryLocality}" />
					</div></td>
				<td><label for="newLineDeliveryAddress">Address of delivery</label>
				<select name="newLineDeliveryAddress" id="newLineDeliveryAddress" onchange="filterLines();">
						<option disabled selected>select delivery address</option>
						<c:forEach var="deliveryAddress"
							items="${addressService.getByLocality(sessionScope.newTenderNewLineDeliveryLocality)}">
							<option value="${deliveryAddress.id}"
								${sessionScope.newTenderNewLineDeliveryAddress == deliveryAddress.id ? 'selected' : ''}>
								<c:out value="${deliveryAddress.details}" /></option>
						</c:forEach>
				</select>
					<button type="button" onclick="openAddressForm()">new</button> <br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongDeliveryAddress}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="newLineCargoName">Cargo: </label>
				<input type="text" name="newLineCargoName" id="newLineCargoName"
					value="${sessionScope.newTenderNewLineCargoName}" onchange="filterLines();" />
					<br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongCargoName}" />
					</div></td>
				<td><label for="newLineLoadUnits">Number of load units to pickup: </label>
				<input type="text" name="newLineLoadUnits" id="newLineLoadUnits" value="${sessionScope.newTenderNewLineLoadUnits}"
					onchange="filterLines();" /><br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongLoadUnits}" />
					</div></td>
				<td><label for="newLineCargoWeight">Cargo weight, kg: </label>
					<input type="text" name="newLineCargoWeight" id="newLineCargoWeight" value="${sessionScope.newTenderNewLineCargoWeight}"
					onchange="filterLines();" /><br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongCargoWeight}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="newLineCargoTruckType">Required truck  type: </label>
				<select name="newLineCargoTruckType" id="newLineCargoTruckType" onchange="filterLines();">
						<option disabled selected>select truck type</option>
						<c:forEach var="typeOfTruck" items="${truckloadService.getTruckTypes()}">
							<option value="${typeOfTruck}"
								${sessionScope.newTenderNewLineCargoTruckType == typeOfTruck ? 'selected' : ''}>
								<c:out value="${typeOfTruck}" /></option>
						</c:forEach>
				</select><br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongCargoTruckType}" />
					</div></td>
				<td><label for="newLineRouteConditions">Specific transport conditions: </label>
				<select name="newLineRouteConditions" id="newLineRouteConditions" onchange="filterLines();">
						<option disabled selected>select conditions</option>
						<c:forEach var="condition" items="${routeService.getConditions()}">
							<option value="${condition}"
								${sessionScope.newTenderNewLineRouteConditions == condition ? 'selected' : ''}>
								<c:out value="${condition}" />
							</option>
						</c:forEach>
				</select><br>
					<div class="alarm">
						<c:out value="${newTenderNewLineWrongRouteConditions}" />
					</div></td>
				<td><button type="submit" value="submit">Save New Line</button>
					<button onclick="resetFilter();">Drop Filter</button></td>
			</tr>
		</table>
	<!-- LINES FILER / NEW LINE DESIGN FORM END-->

	<!-- FILTERED LIST OF AVAILABLE LINES / LINES SORTING / LINES SELECTION FORM BEGINNING -->
	<h4>Lines selection form</h4>
	 <ym:paging source="${newTenderNewLinesToRender}" size="5" indexName="${PaginationIndices.FILTERED_LINES.provideIndex()}">
		<table>
			<tr>
				<th><a href="#" onclick="sortById();">Id</a></th>
				<th><a href="#" onclick="sortByStartCountry();">Country of Departure</a></th>
				<th><a href="#" onclick="sortByStartCity();">City of Departure</a></th>
				<th><a href="#" onclick="sortByEndCountry();">Country of Arrival</a></th>
				<th><a href="#" onclick="sortByEndCity();">City of Arrival</a></th>
				<th><a href="#" onclick="sortByConditions();">Transport Conditions</a></th>
				<th><a href="#" onclick="sortByLoad();">Truckload</a></th>
				<th><a href="#" onclick="sortByWeight();">Weight</a></th>
				<th><a href="#" onclick="sortByLoadUnits();">Loading Units</a></th>
				<th><a href="#" onclick="sortByTruckType();">Truck Type</a></th>
			</tr>
			<c:forEach var="existingLine" items="${bunch}" varStatus="loop">
				<tr>
					<td><input type="radio" name="lineToAddInNewTender" id="lineId${loop.index}" value="${existingLine.id}" /> 
					<label for="lineId${loop.index}"><a href="#" onclick="openLineRenderPopup(${existingLine.id})"> 
						<c:out value="${existingLine.id}" /></a> </label></td>
					<td><c:out value="${existingLine.route.start.locality.country.name}" /></td>
					<td><c:out value="${existingLine.route.start.locality.name}" /></td>
					<td><c:out value="${existingLine.route.destination.locality.country.name}" /></td>
					<td><c:out value="${existingLine.route.destination.locality.name}" /></td>
					<td><c:out value="${existingLine.route.conditions.toString()}" /></td>
					<td><c:out value="${existingLine.truckload.name}" /></td>
					<td><c:out value="${existingLine.truckload.weight}" /></td>
					<td><c:out value="${existingLine.truckload.loadUnits}" /></td>
					<td><c:out value="${existingLine.truckload.truckType.toSqlValue()}" /></td>
				</tr>
			</c:forEach>
		</table>
     </ym:paging>
		<br>
					<div class="alarm">
						<c:out value="${lineDoubleSelection}" />
						<c:out value="${newTenderNewLineEmptyId}"/>
					</div>
		<button type="submit" onclick="selectForNewTender();">Add to new tender</button>
	</form>
	<!-- FILTERED LIST OF AVAILABLE LINES / LINES SORTING / LINES SELECTION FORM END -->

	<!-- RENDERING OF LINES SET TO BE INCLUDED IN NEW TENDER & FORM OF LINE REMOVAL FROM THE SET BEGINNING -->

	<h4>New tender lines</h4>
	<c:set var="newTenderSelectedLinePickupCountry"
		value="${empty param.selectedLinePickupCountry ? sessionScope.newTenderSelectedLinePickupCountry : param.selectedLinePickupCountry}"
		scope="session" />
	<c:set var="newTenderSelectedLinePickupLocality"
		value="${empty param.selectedLinePickupLocality ? sessionScope.newTenderSelectedLinePickupLocality : param.selectedLinePickupLocality}"
		scope="session" />
	<c:set var="newTenderSelectedLinePickupAddress"
		value="${empty param.selectedLinePickupAddress ? sessionScope.newTenderSelectedLinePickupAddress : param.selectedLinePickupAddress}"
		scope="session" />
	<c:set var="newTenderSelectedLineDeliveryCountry"
		value="${empty param.selectedLineDeliveryCountry ? sessionScope.newTenderSelectedLineDeliveryCountry : param.selectedLineDeliveryCountry}"
		scope="session" />
	<c:set var="newTenderSelectedLineDeliveryLocality"
		value="${empty param.selectedLineDeliveryLocality ? sessionScope.newTenderSelectedLineDeliveryLocality : param.selectedLineDeliveryLocality}"
		scope="session" />
	<c:set var="newTenderSelectedLineDeliveryAddress"
		value="${empty param.selectedLineDeliveryAddress ? sessionScope.newTenderSelectedLineDeliveryAddress : param.selectedLineDeliveryAddress}"
		scope="session" />
	<c:set var="newTenderSelectedLineCargoName"
		value="${empty param.selectedLineCargoName ? sessionScope.newTenderSelectedLineCargoName : param.selectedLineCargoName}"
		scope="session" />
	<c:set var="newTenderSelectedLineLoadUnits"
		value="${empty requestScope.selectedLineLoadUnits ? 
		(empty param.selectedLineLoadUnits ? sessionScope.newTenderSelectedLineLoadUnits : param.selectedLineLoadUnits) 
		: requestScope.selectedLineLoadUnits}" scope="session" />
	<c:set var="newTenderSelectedLineCargoWeight"
		value="${empty requestScope.selectedLineCargoWeight ? 
		(empty param.selectedLineCargoWeight ? sessionScope.newTenderSelectedLineCargoWeight : param.selectedLineCargoWeight) 
		: requestScope.selectedLineCargoWeight}" scope="session" />
	<c:set var="newTenderSelectedLineCargoTruckType"
		value="${empty param.selectedLineCargoTruckType ? sessionScope.newTenderSelectedLineCargoTruckType : param.selectedLineCargoTruckType}"
		scope="session" />
	<c:set var="newTenderSelectedLineRouteConditions"
		value="${empty param.selectedLineRouteConditions ? sessionScope.newTenderSelectedLineRouteConditions : param.selectedLineRouteConditions}"
		scope="session" />

	<form name="selectedLinesForm" id="selectedLinesForm" action="${pageContext.request.contextPath}/" method="post">
		<input type="hidden" name="local_url" value="/tenders/create" /> 
		<input type="hidden" name="prefix1" value= "newTenderSelectedLine" />
		<input type="hidden" name="prefix2" value="selectedLine" />
		<input type="hidden" name="sortArg" id="selectedSortArg" />
		<input type="hidden" name="commandName" id="selectedLinesCommandName" value="filterLines" /> 
		<table>
			<tr>
				<td><label for="selectedLinePickupCountry">Country of pickup: </label>
				<select name="selectedLinePickupCountry" id="selectedLinePickupCountry" onchange="filterSelectedLines();">
						<option disabled selected>select country of pickup</option>
						<c:forEach var="pickupCountry" items="${countryService.getAll()}">
							<option value="${pickupCountry.id}"
								${sessionScope.newTenderSelectedLinePickupCountry == pickupCountry.id ? 'selected' : ''}>
								<c:out value="${pickupCountry.name}" />
							</option>
						</c:forEach>
				</select>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongPickupCountry}" />
					</div></td>
				<td><label for="selectedLinePickupLocality">Locality of pickup: </label>
				<select name="selectedLinePickupLocality" id="selectedLinePickupLocality" onchange="filterSelectedLines();">
						<option disabled selected>select locality of pickup</option>
						<c:forEach var="pickupLocality"
							items="${localityService.getByCountry(sessionScope.newTenderSelectedLinePickupCountry)}">
							<option value="${pickupLocality.id}"
								${sessionScope.newTenderSelectedLinePickupLocality == pickupLocality.id ? 'selected' : ''}>
								<c:out value="${pickupLocality.name}" />
							</option>
						</c:forEach>
				</select>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongPickupLocality}" />
					</div></td>
				<td><label for="selectedLinePickupAddress">Address of
						pickup: </label><select name="selectedLinePickupAddress"
					id="selectedLinePickupAddress" onchange="filterSelectedLines();">
						<option disabled selected>select address of pickup</option>
						<c:forEach var="pickupAddress"
							items="${addressService.getByLocality(sessionScope.newTenderSelectedLinePickupLocality)}">
							<option value="${pickupAddress.id}"
								${sessionScope.newTenderSelectedLinePickupAddress == pickupAddress.id ? 'selected' : ''}>
								<c:out value="${pickupAddress.details}" /></option>
						</c:forEach>
				</select>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongPickupAddress}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="selectedLineDeliveryCountry">Country of delivery: </label> 
				<select name="selectedLineDeliveryCountry" id="selectedLineDeliveryCountry" onchange="filterSelectedLines();">
						<option disabled selected>select country of delivery</option>
						<c:forEach var="deliveryCountry" items="${countryService.getAll()}">
							<option value="${deliveryCountry.id}" 
							${sessionScope.newTenderSelectedLineDeliveryCountry == deliveryCountry.id ? 'selected':''}><c:out
									value="${deliveryCountry.name}" /></option>
						</c:forEach>
				</select>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongDeliveryCountry}" />
					</div></td>
				<td><label for="selectedLineDeliveryLocality">Locality
						of delivery: </label><select id="selectedLineDeliveryLocality"
					name="selectedLineDeliveryLocality"
					onchange="filterSelectedLines();">
						<option disabled selected>select delivery locality</option>
						<c:forEach var="deliveryLocality"
							items="${localityService.getByCountry(sessionScope.newTenderSelectedLineDeliveryCountry)}">
							<option value="${deliveryLocality.id}"
								${sessionScope.newTenderSelectedLineDeliveryLocality == deliveryLocality.id ? 'selected' : ''}>
								<c:out value="${deliveryLocality.name}" />
							</option>
						</c:forEach>
				</select>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongDeliveryLocality}" />
					</div></td>
				<td><label for="selectedLineDeliveryAddress">Address of
						delivery</label><select name="selectedLineDeliveryAddress"
					id="selectedLineDeliveryAddress" onchange="filterSelectedLines();">
						<option hidden disabled selected value>select delivery
							address</option>
						<c:forEach var="deliveryAddress"
							items="${addressService.getByLocality(sessionScope.newTenderSelectedLineDeliveryLocality)}">
							<option value="${deliveryAddress.id}"
								${sessionScope.newTenderSelectedLineDeliveryAddress == deliveryAddress.id?'selected':''}>
								<c:out value="${deliveryAddress.details}" />
							</option>
						</c:forEach>
				</select>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongDeliveryAddress}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="selectedLineCargoName">Cargo: </label> <input
					type="text" name="selectedLineCargoName" id="selectedLineCargoName"
					value="${sessionScope.newTenderSelectedLineCargoName}"
					onchange="filterSelectedLines();" /> <br>
					<div class="alarm">
						<c:out
							value="${newTenderSelectedLineWrongCargoName}" />
					</div></td>
				<td><label for="selectedLineLoadUnits">Number of load
						units to pickup: </label> <input type="text" name="selectedLineLoadUnits"
					id="selectedLineLoadUnits"
					value="${sessionScope.newTenderSelectedLineLoadUnits}"
					onchange="filterSelectedLines();" /><br>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongLoadUnits}" />
					</div></td>
				<td><label for="selectedLineCargoWeight">Cargo weight, kg: </label> 
				<input type="text" name="selectedLineCargoWeight"
					id="selectedLineCargoWeight"
					value="${sessionScope.newTenderSelectedLineCargoWeight}"
					onchange="filterSelectedLines();" /><br>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongCargoWeight}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="selectedLineCargoTruckType">Required truck type: </label> 
				<select name="selectedLineCargoTruckType" id="selectedLineCargoTruckType" onchange="filterSelectedLines();">
						<option disabled selected>select truck type</option>
						<c:forEach var="typeOfTruck"
							items="${truckloadService.getTruckTypes()}">
							<option value="${typeOfTruck}"
								${sessionScope.newTenderSelectedLineCargoTruckType == typeOfTruck ? 'selected' : ''}>
								<c:out value="${typeOfTruck}" />
							</option>
						</c:forEach>
				</select><br>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongCargoTruckType}" />
					</div></td>
				<td><label for="selectedLineRouteConditions">Specific
						transport conditions: </label><select name="selectedLineRouteConditions"
					id="selectedLineRouteConditions" onchange="filterSelectedLines();">
						<option disabled selected>select conditions</option>
						<c:forEach var="condition" items="${routeService.getConditions()}">
							<option value="${condition}"
								${sessionScope.newTenderSelectedLineRouteConditions == condition ? 'selected' : ''}><c:out
									value="${condition}" /></option>
						</c:forEach>
				</select><br>
					<div class="alarm">
						<c:out value="${newTenderSelectedLineWrongRouteConditions}" />
					</div></td>
				<td><button onclick="resetSelectedLinesFilter();">Drop Filter</button></td>
			</tr>
		</table>

			<ym:paging source="${newTenderSelectedLinesToRender}" size="5" indexName="${PaginationIndices.SELECTED_LINES.provideIndex()}">
		<table>
			<tr>
				<th><a href="#" onclick="selectedSortById();">Id</a></th>
				<th><a href="#" onclick="selectedSortByStartCountry();">Country of Departure</a></th>
				<th><a href="#" onclick="selectedSortByStartCity();">City of Departure</a></th>
				<th><a href="#" onclick="selectedSortByEndCountry();">Country of Arrival</a></th>
				<th><a href="#" onclick="selectedSortByEndCity();">City of Arrival</a></th>
				<th><a href="#" onclick="selectedSortByConditions();">Transport Conditions</a></th>
				<th><a href="#" onclick="selectedSortByLoad();">Truckload</a></th>
				<th><a href="#" onclick="selectedSortByWeight();">Weight</a></th>
				<th><a href="#" onclick="selectedSortByLoadUnits();">Loading Units</a></th>
				<th><a href="#" onclick="selectedSortByTruckType();">Truck Type</a></th>
			</tr>
			<c:forEach var="existingLine" items="${bunch}" varStatus="loop">
				<tr>
					<td><input type="radio" name="lineToRemoveFromNewTender" id="lineId${loop.index}" value="${existingLine.id}" /> 
					<a href="#" onclick="openLineRenderPopup(${existingLine.id})">
						<c:out value="${existingLine.id}" /></a></td>
					<td><c:out value="${existingLine.route.start.locality.country.name}" /></td>
					<td><c:out value="${existingLine.route.start.locality.name}" /></td>
					<td><c:out value="${existingLine.route.destination.locality.country.name}" /></td>
					<td><c:out value="${existingLine.route.destination.locality.name}" /></td>
					<td><c:out value="${existingLine.route.conditions.toString()}" /></td>
					<td><c:out value="${existingLine.truckload.name}" /></td>
					<td><c:out value="${existingLine.truckload.weight}" /></td>
					<td><c:out value="${existingLine.truckload.loadUnits}" /></td>
					<td><c:out value="${existingLine.truckload.truckType.toSqlValue()}" /></td>
				</tr>
			</c:forEach>
		</table>
		</ym:paging>
		<br>	
		<button type="button" onclick="removeFromSelectedLines();">remove from new tender</button>
	</form>
	<div class="alarm">
	    <c:out value="${wrongLineToRemoveFromNewTender}"/>
	    <c:out value="${newTenderSelectedLinesEmptySet}"/>
	</div>

	<!-- RENDERING OF LINES SET TO BE INCLUDED IN NEW TENDER & FORM OF LINE REMOVAL FROM THE SET END -->

	<br>
	<div class="alarm">
		<c:out value="${wrongLinesSet}" />
	</div>
	<button type="submit" form="newTenderForm">submit new tender</button>
	<br>
	<c:if test="${sessionScope.newTenderId != null}">
	    <br>
		<a href="${pageContext.request.contextPath}/tenders/tender?id=${requestScope.newTenderId}">
			Check New Tender <c:out value="${requestScope.newTenderId}" /></a>
		<br>
	</c:if>
	<br>
	<a href="${pageContext.request.contextPath}/tenders"><fmt:message key="backToTenders" bundle="${bndle}" /></a>
</body>
</html>
