<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ym" uri="mytags"%>
<%@page import="by.minsk.training.ApplicationContext"%>
<%@page import="by.minsk.training.entity.Tender.State"%>
<%@page import="by.minsk.training.user.UserEntity.Roles"%>
<%@page import="by.minsk.training.ToolSet"%>
<%@page import="by.minsk.training.command.pagination.PaginationIndices"%>
<html>
<head>
<title>Tender</title>
<meta charset="utf-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/tenders.js"></script>
</head>
<body>
	<%@include file="internatiolization.jsp"%>
	<c:set var="appContext" value="${ApplicationContext.getInstance()}" />
	<c:set var="tenderService" value="${appContext.getBean('TenderService')}" />
	<c:set var="renderedTender" scope="session" value="${tenderService.get(param.id)}" />
	<c:set var="lineService" value="${appContext.getBean('LineService')}" />
	<c:set var="tenderParticipationService" value="${appContext.getBean('TenderParticipationService')}" />
	<c:set var="tenderId" value="${empty param.id ? sessionScope.renderedTenderId : param.id}" />
	<h1>
		<fmt:message key="tenderDetailsTitle" bundle="${bndle}" />
		<c:out value=" ${tenderId}" />
	</h1>
	<c:set var="renderedTender" value="${tenderService.get(tenderId)}" />
	<h3>
		Tender
		<c:out value="${renderedTender.id.toString().concat(' ').concat(renderedTender.title)}" />
	</h3>
	<h4>Tender details</h4>
	<ym:paging source="${tenderService.getLines(renderedTender)}" size="10"
		indexName="${PaginationIndices.TENDER_LINES.provideIndex()}">
		<table>
			<tr>
				<td>Title:</td>
				<td><c:out value="${renderedTender.title}" /></td>
			</tr>
			<tr>
				<td>State:</td>
				<td><c:out value="${renderedTender.state.printOut()}" /></td>
			</tr>
			<tr>
				<td>Set on:</td>
				<td><c:out value="${renderedTender.setDate}" /></td>
			</tr>
			<c:if test="${sessionScope.user.role ne 'CUSTOMER'}">
				<tr>
					<td>Customer:</td>
					<td><a
						href="${pageContext.request.contextPath}/report/company?id=${renderedTender.customer.id}">
							<c:out value="${renderedTender.customer.name}" />
					</a></td>
				</tr>
			</c:if>
			<tr>
				<td>Release date:</td>
				<td><c:out
						value="${renderedTender.state eq 'CREATED' ? 'yet to be released' : renderedState.releaseDate}" /></td>
			</tr>
			<c:if test="${renderedTender.state eq 'RELEASED'}">
				<tr>
					<td>Competition deadline:</td>
					<td><c:out value="${renderedTender.competitionDeadline}" /></td>
				</tr>
			</c:if>
			<tr>
				<td>Tender start:</td>
				<td><c:out value="${renderedTender.tenderStart}" /></td>
			</tr>
			<tr>
				<td>Tender end:</td>
				<td><c:out value="${renderedTender.tenderEnd}" /></td>
			</tr>
			<tr>
				<td>Estimated amount of trips:</td>
				<td><c:out value="${renderedTender.estimatedQuantity}" /></td>
			</tr>
		</table>
		<h4>Lines set</h4>
		<table>
			<tr>
				<th>Id</th>
				<th>Route</th>
				<th>Cargo Title</th>
				<c:if test="${tenderService.isContracted(renderedTender)}">
					<th>Quantity of Transport Orders</th>
					<th>Awards</th>
				</c:if>
				<c:if test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
					<th>Rate</th>
					<th>Orders Assigned</th>
					<th>Orders Accepted</th>
				</c:if>
			</tr>
			<c:forEach var="line" items="${bunch}">
				<tr>
					<td><c:out value="${line.id}" /></td>
					<td><c:out value="${line.route.start.locality.name.concat(', ').concat(line.route.start.locality.country.name).concat(' - ')
				.concat(line.route.destination.locality.name).concat(', ').concat(line.route.destination.locality.country.name)}" /></td>
					<td><c:out value="${line.truckload.name}" /></td>
					<c:if test="${tenderService.isContracted(renderedTender)}">
						<td><c:out value="${tenderService.countPerLineOrders(renderedTender, line, sessionScope.user)}" /></td>
						<c:choose>
							<c:when test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
								<td><c:out value="${ToolSet.round((tenderParticipationService.findAward(renderedTender, line, sessionScope.user))*100, 2).concat('%')}" /></td>
							</c:when>
							<c:otherwise>
								<td><ym:paging source="${tenderService.retrieveLineWinners(renderedTender, line, sessionScope.user)}"
										size="5" indexName="${PaginationIndices.LINE_AWARDS.provideIndex()}">
										<c:forEach var="tlquery" items="${bunch}">
											<c:out value="${tlquery.participation.tsp.name.concat(' ').concat(ToolSet.round(tlquery.awardedShare*100, 2)).concat('%')}" />
											<br>
										</c:forEach>
									</ym:paging></td>
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if
						test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
						<td><c:out value="${tenderParticipationService.findRate(renderedTender, line, sessionScope.user)}" /></td>
						<td><c:out value="${tenderParticipationService.countTenderLineTspAssignedOrders(renderedTender,line,sessionScope.user)}" /></td>
						<td><c:out value="${tenderParticipationService.countTenderLineTspAcceptedOrders(renderedTender,line,sessionScope.user)}" /></td>
					</c:if>
				</tr>
			</c:forEach>
		</table>
	</ym:paging>
	<c:if test="${sessionScope.user.role == 'CUSTOMER'}">
		<h5>Tender current KPI values</h5>
		<c:set var="kpi" value="${tenderService.calculateTenderKPI(renderedTender)}" />
		<!-- Quantity of executed transport orders -->
	% of assigned TOs acceptance <c:out value="${kpi.assignedTOsAccepted}" />
		<br>
	% of in-time arrivals for loading <c:out value="${kpi.intimePickups}" />
		<br>
	% of transport order execution success <c:out value="${kpi.acceptedTOsExecution}" />
		<br>
	% of delivery terms compliance <c:out value="${kpi.deliveryTermCompliance}" />
		<br>

		<c:choose>
			<c:when test="${renderedTender.state =='CONTRACTED'}">
				<a href="${pageContext.request.contextPath}/shipments/create"><fmt:message key="shipmentDesignLink" bundle="${bndle}" /></a>
			</c:when>
			<c:when test="${renderedTender.state == 'CREATED'}">
				<h5>Tender release form</h5>
				<c:set var="tenderCarriers" scope="session" value="${empty sessionScope.tenderCarriers ? userService.findContractedTsps(sessionScope.user) : sessionScope.tenderCarriers}" />
				<!-- tender release form implementation -->
				<form id="tenderReleaseForm" action="${pageContext.request.contextPath}/" method="post">
					<input type="hidden" name="commandName" /> <input type="hidden"
						name="local_url" value="/tenders/tender" /> <input type="text"
						name="id" value="${tenderId}" /> <label for="contestDeadline">Set
						tender competition deadline: </label> <input type="date"
						name="contestDeadline" id="contestDeadline" required /> <label
						for="tspToExclude">Check carriers list to receive the
						tender contest invitation: </label><br> <select id="tspToExclude"
						name="tspToExclude">
						<option hidden disabled selected value>select carrier to
							exclude from the list</option>
						<c:forEach var="tsp" items="${tenderCarriers}">
							<option value="${tsp.id}"><c:out
									value="${tsp.name.concat(', id ').concat(tsp.id)}" /></option>
						</c:forEach>
					</select>
					<c:if test="${not empty sessionScope.availableTsps}">
						<br>
						<label for="tspToInclude">Select carriers to be added to
							tender release' recipients: </label>
						<br>
						<select id="tspToInclude" name="tspToInclude">
							<option hidden disabled selected value>select carrier</option>
							<c:forEach var="tsp1" items="${sessionScope.availableTsps}">
								<option value="${tsp1.id}"><c:out
										value="${tsp1.name.concat(', id ').concat(tsp1.id)}" /></option>
							</c:forEach>
						</select>
					</c:if>
				</form>
				<br>
				<button type=button onclick="reviseTenderTsps();">revise
					carriers list</button>
				<br>
				<button type="button" onclick="releaseNewTender();">release
					tender</button>
			</c:when>
			<c:when test="${renderedTender.state=='RELEASED'}">
			Tender competition deadline: <c:out
					value="${renderedTender.competitionDeadline}" />
				<br>
				<a href="${pageContext.request.contextPath}/tenders/contest/tender"><fmt:message
						key="tenderContestLink" bundle="${bndle}" /></a>
			</c:when>
		</c:choose>
	</c:if>
	<c:if test="${sessionScope.user.role == 'TRANSPORT_SERVICE_PROVIDER'}">
		<h5>Not yet completed transport orders</h5>
		<!-- user's ongoing TOs ('assigned', 'accepted', 'in_action') that refers to the tender, including TO opening function (link to shipment rendering page) - to TSP users -->
		<ym:paging
			source="${tenderService.findTspTenderUncompletedAssignments(renderedTender, sessionScope.user)}"
			size="10"
			indexName="${PaginationIndices.UNCOMPLETED_ORDERS.provideIndex()}">
			<table>
				<tr>
					<th>Id</th>
					<th>Route</th>
					<th>Loading date</th>
					<th>Truckload</th>
					<th>State</th>
					<th>Truck plates</th>
					<th>Arrived for loading</th>
					<th>Left loading</th>
					<th>Arrived at unloading</th>
					<th>Left unloading</th>
				</tr>
				<c:forEach var="assignment" items="${bunch}">
					<tr>
						<td><a
							href="${pageContext.request.contextPath}/shipments/shipment?id=${assignment.transportOrder.id}"><c:out
									value="${assignment.transportOrder.id}" /></a></td>
						<td><c:out
								value="${assignment.transportOrder.line.route.start.name.concat(', ')
					.concat(assignment.transportOrder.line.route.start.country.name)
					.concat(' - ').concat(assignment.transportOrder.line.route.destination.name).concat(', ')
					.concat(assignment.transportOrder.line.route.destination.country.name)}" /></td>
						<td><c:out value="${assignment.orderFootprint.loadingDate}" /></td>
						<td><c:out
								value="${assignment.transportOrder.line.truckload.name.concat(', ')
					.concat(assignment.transportOrder.line.truckload.weight).concat('kg')}" /></td>
						<td><c:out value="${assignment.orderFootprint.order_state}" /></td>
						<td><c:out value="${assignment.truckPlates}" /></td>
						<td><c:out value="${assignment.arrivedForLoading}" /></td>
						<td><c:out value="${assignment.leftLoading}" /></td>
						<td><c:out value="${assignment.arrivedForUnloading}" /></td>
						<td><c:out value="${assignment.leftUnloading}" /></td>
					</tr>
				</c:forEach>
			</table>
		</ym:paging>
		<br>
Customer of the tender : <a
			href="${pageContext.request.contextPath}/report/company?id=${renderedTender.customer.id}"><c:out
				value="${renderedTender.customer.name}" /></a>
	</c:if>
	<br>

	<a href="${pageContext.request.contextPath}/tenders"><fmt:message
			key="tendersLink" bundle="${bndle}" /></a>
	<br>
	<a href="${pageContext.request.contextPath}/"><fmt:message
			key="startPageLink" bundle="${bndle}" /></a>
</body>
</html>
