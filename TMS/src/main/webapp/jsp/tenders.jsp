<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ym" uri="mytags"%>
<html>
<head>
<title>Tenders</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/tenders.js"></script>
</head>
<body>
	<%@include file="internatiolization.jsp"%>
	<%@page import="by.minsk.training.command.pagination.PaginationIndices"%>
	<%@page import="by.minsk.training.user.UserEntity.Roles"%>
	<%@page import="by.minsk.training.entity.Tender.State"%>
	<h1>
		<fmt:message key="tendersLink" bundle="${bndle}" />
	</h1>
	<br>
	<c:set var="tenderService" scope="page" value="${sessionScope.applicationContext.getBean('TenderService')}" />
	<c:set var="tendersToRender" scope="session" 
	value="${empty sessionScope.tendersToRender ? tenderService.gatherTenders(sessionScope.user) : sessionScope.tendersToRender}" />
	<form name="tenderForm" id="tenderForm"
		action="${pageContext.request.contextPath}/" method="post">
		<input type="hidden" name="commandName" id="command" /> 
		<input type="hidden" name="local_url" value="/tenders"/>
		<input type="hidden" name="comparingParameter" id="comparingParameter" />
		<input type="hidden" name="tendersSet" value="tendersToRender"/>
	</form>
	<ym:paging source="${tendersToRender}" size="5" indexName="${PaginationIndices.TENDERS_INDEX.provideIndex()}">
		<table>
			<tr>
				<th><a href="#" onclick="sortById();">Id</a><br> <input
					type="text" name="id_field" placeholder="find by id"
					form="tenderForm" value="${param.id_field}" oninput="filter();" />
				</th>
				<th><a href="#" onclick="sortByTitle();">Title</a><br> <input
					type="text" name="title_field" placeholder="find by title"
					form="tenderForm" value="${param.title_field}" oninput="filter();" />
				</th>
				<c:if test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
					<th><a href="#" onclick="sortByReleaseDate();">Date</a><br>
						<label for="releasedate_from">from: </label> <input type="date"
						name="releasedate_from" id="releasedate_from" form="tenderForm"
						value="{requestScope.releasedate_from}" oninput="filter();" /> <label
						for="releasedate_to"> to: </label> <input type="date"
						name="releasedate_to" id="releasedate_to" form="tenderForm"
						value="{requestScope.releasedate_to}" oninput="filter();" /></th>
				</c:if>
				<c:if test="${sessionScope.user.role ne 'CUSTOMER'}">
					<th><a href="#" onclick="sortByCustomer();">Customer</a><br>
						<input type="text" name="customer" placeholder="customer "
						form="tenderForm" value="{requestScope.customer}"
						oninput="filter();" /></th>
				</c:if>
				<c:if
					test="${sessionScope.user.role ne 'TRANSPORT_SERVICE_PROVIDER'}">
					<th><a href="#" onclick="sortBySetDate();">Date</a><br> <label
						for="setDate_from">from: </label> <input type="date"
						name="setDate_from" id="setDate_from" form="tenderForm"
						value="{requestScope.setDate_from}" oninput="filter();" /> <label
						for="setDate_to"> to: </label> <input type="date"
						name="setDate_to" id="setDate_to" form="tenderForm"
						value="{requestScope.setDate_to}" oninput="filter();" /></th>
			   </c:if>
					<th><a href="#" onclick="sortByState();">State</a><br> <input
						type="text" name="state_field" placeholder="find by state"
						form="tenderForm" value="${requestScope.state_field}"
						oninput="filter();" /></th>
				<th><a href="#" onclick="sortByDeadline();">Competition deadline</a><br> 
				    <label for="deadline_from">from: </label> <input
					type="date" name="deadline_from" id="deadline_from"
					form="tenderForm" value="{requestScope.deadline_from}"
					oninput="filter();" /> <label for="deadline_to"> to: </label> <input
					type="date" name="deadline_to" id="deadline_to" form="tenderForm"
					value="{requestScope.deadline_to}" oninput="filter();" /></th>
				<th><a href="#" onclick="sortByTenderStart();">Tender Start</a><br>
					<label for="tenderstart_from">from: </label> <input type="date"
					name="tenderstart_from" id="tenderstart_from" form="tenderForm"
					value="{requestScope.tenderstart_from}" oninput="filter();" /> <label
					for="tenderstart_to"> to: </label> <input type="date"
					name="tenderstart_to" placeholder="tenderstart_to"
					form="tenderForm" value="{requestScope.tenderstart_to}"
					oninput="filter();" /></th>
				<th><a href="#" onclick="sortByTenderEnd();">Tender End</a><br>
					<label for="tendersend_from">from: </label> <input type="date"
					name="tendersend_from" id="tendersend_from" form="tenderForm"
					value="{requestScope.tenderend_from}" oninput="filter();" /> <label
					for="tenderend_to"> to: </label> <input type="date"
					name="tenderend_to" id="tenderend_to" form="tenderForm"
					value="{requestScope.tenderend_to}" oninput="filter();" /></th>
				<th><a href="#" onclick="sortByShipmentsQuantity();">Shipments quantity</a><br></th>
			</tr>
			<c:forEach var="tender" items="${bunch}">
				<tr>
					<td><a href="${pageContext.request.contextPath}/tenders/tender?id=${tender.id}"><c:out value="${tender.id}" /></a></td>
					<td><c:out value="${tender.title}" /></td>
					<c:if
						test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
						<td><c:out value="${tender.releaseDate}" /></td>
					</c:if>
					<c:if test="${sessionScope.user.role ne 'CUSTOMER'}">
						<td><c:out value="${tender.customer.name}" /></td>
					</c:if>
					<c:if
						test="${sessionScope.user.role ne 'TRANSPORT_SERVICE_PROVIDER'}">
						<td><c:out value="${tender.setDate}" /></td>
						<td><c:out value="${tender.state.printOut()}" /></td>
					</c:if>
					<td><c:out value="${tender.state eq 'CREATED' ? 'competition is not set yet' :
						(tender.state eq 'RELEASED' ? tender.competitionDeadline : 'competition is over')}" /></td>
					<td><c:out value="${tender.tenderStart}" /></td>
					<td><c:out value="${tender.tenderEnd}" /></td>
					<td><c:out value="${tenderService.countOrders(tender, sessionScope.user)}" /></td>
				</tr>
			</c:forEach>
		</table>
	</ym:paging>
	<br>
	<button type="button" onclick="tendersReset();">Reset Selection</button>
	<br>
	<c:if test="${user.role eq 'CUSTOMER'}">
		<a href="${pageContext.request.contextPath}/tenders/create"><fmt:message key="tenderDesignLink" bundle="${bndle}" /></a>
	</c:if>
	<br>
	<a href="${pageContext.request.contextPath}/"><fmt:message key="startPageLink" bundle="${bndle}" /></a>
</body>
</html>