<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ym" uri="mytags"%>
<html>
	<c:set var="local_url" scope="session" value="/shipments/create"/>
	<c:set var="countryService" value="${applicationContext.getBean('CountryService')}" />
	<c:set var="localityService" value="${applicationContext.getBean('LocalityService')}" />
	<c:set var="addressService" value="${applicationContext.getBean('AddressService')}" />
	<c:set var="lineService" value="${applicationContext.getBean('LineService')}" />
	<c:set var="tenderService" value="${applicationContext.getBean('TenderService')}" />
	<c:set var="transportOrderService" value="${applicationContext.getBean('TransportOrderService')}" />
	<c:set var="truckloadService" value="${applicationContext.getBean('TruckloadService')}"/>
	<c:set var="routeService" value="${applicationContext.getBean('RouteService')}"/>
	
    <c:if test="${param.commandName eq 'dropTender'}">
		<c:remove var="newShipmentTender" scope="session" />
		<c:remove var="newShipmentNewLinesToRender" scope="session"/>
		<c:set var="newShipmentNewLines" scope="session" value="${lineService.getUserRelativeLines(user)}" />
		<jsp:forward page="../">
			<jsp:param name="commandName" value="filterLines" />
		</jsp:forward>
	</c:if>
<head>
<title>New shipment</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/shipment_design.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/shipment_design.js"></script>
</head>
<body>
    <jsp:include page="../jsp/popUps.jsp"/>
	<%@include file="internatiolization.jsp"%>
	<%@ page import="by.minsk.training.command.pagination.PaginationIndices"%>

	<h1>
		<fmt:message key="shipmentDesignTitle" bundle="${bndle}" />
	</h1>
	<!-- TENDER SELECTION SECTION START -->
	<h3>Tender selection</h3>
	<form action="${pageContext.request.contextPath}/" id="tenders" action="post">
		<input type="hidden" name="local_url" value="/shipments/create" /> 
		<input type="hidden" name="commandName" id="commandName" /> 
		<input type="hidden" name="comparingParameter" id="comparingParameter" /> 
		<input type="hidden" name="tendersSet" value="userTendersToRender" />
		<input type="hidden" name="prefix1" value="newShipmentNewLine" /> 
		<input type="hidden" name="prefix2" value="newLine" /> 
	</form>
	<c:set var="userTendersToRender" value="${empty sessionScope.userTendersToRender ? (param.commandName eq 'sortTenders' or param.commandName 
	eq 'tendersFilter' ? sessionScope.userTendersToRender : tenderService.selectActiveTenders(user)) : sessionScope.userTendersToRender}" scope="session" />
	<div>
		<ym:paging source="${userTendersToRender}" size="5" indexName="${paginationIndices.AVAILABLE_TENDERS}">
			<table>
				<tr>
					<th><a href="#" onclick="sortById();">Id</a><br> 
					    <input type="text" form="tenders" name="id_field" value="${not empty param.tender_id ? param.tender_id : param.id_field}"
						id="tenderIdFilter" onchange="filter();" /></th>
					<th><a href="#" onclick="sortByTitle();">Title</a><br> 
					    <input type="text" form="tenders" name="title_field" id="tenderTitleFilter" onchange="filter();" /></th>
					<th><a href="#" onclick="sortByDate();">Date</a><br> <label for="setDateFromFilter">Set from </label> 
					    <input type="text" form="tenders" name="setDate_from" id="setDateFromFilter" onchange="filter();" /> 
					    <label for="setDateToFilter">Set by </label> 
					    <input type="text" form="tenders" name="setDate_to" id="setDateToFilter" onchange="filter();" /></th>
					<th><a href="#" onclick="sortByTenderStart();">Tender terms</a><br> Tender starts between 
					    <input type="text" form="tenders" name="tenderstart_from" id="tenderStartDateFilter" onchange="filter();" /> and 
					    <input type="text" form="tenders" name="tenderstart_to" id="tenderEndDateFilter" onchange="filter();" /><br> Tender ends between 
					    <input type="text" form="tenders" name="tenderend_from" id="tenderStartDateFilter" onchange="filter();" /> and 
					    <input type="text" form="tenders" name="tenderend_to" id="tenderEndDateFilter" onchange="filter();" /></th>
				</tr>
				<c:forEach var="userTender" items="${bunch}" varStatus="loop">
					<tr>
						<th><input type="radio" form="tenders" name="tender_id" id="tenderId${loop.index}" value="${userTender.id}" /> 
						<label for="tenderId${loop.index}"> <c:out value="${userTender.id}" /></label></th>
						<th><c:out value="${userTender.title}" /></th>
						<th><c:out value="${userTender.setDate}" /></th>
						<th><c:out value="${userTender.tenderStart.toString().concat(' - ').concat(userTender.tenderEnd)}" /></th>
					</tr>
				</c:forEach>
			</table>
		</ym:paging>
		<button type="button" onclick="dropFilter();">Cancel filter</button>
		<button type="button" onclick="applyForNewShipment();">Select tender</button>
		<button type="button" onclick="cancelTenderSelection();">Drop selected tender</button>
		<c:out value="${tenderNotSelected}" />
	</div>
	<!-- TENDER SELECTION SECTION END -->

	<!-- AVAILABLE LINES SECTION START -->
	<h3>Line definition</h3>
	<c:set var="newShipmentNewLinePickupCountry"
		value="${empty param.newLinePickupCountry ? sessionScope.newShipmentNewLinePickupCountry : param.newLinePickupCountry}" scope="session" />
	<c:set var="newShipmentNewLinePickupLocality"
		value="${empty param.newLinePickupLocality ? sessionScope.newShipmentNewLinePickupLocality : param.newLinePickupLocality}" scope="session" />
	<c:set var="newShipmentNewLinePickupAddress"
		value="${empty param.newLinePickupAddress ? sessionScope.newShipmentNewLinePickupAddress : param.newLinePickupAddress}" scope="session" />
	<c:set var="newShipmentNewLineDeliveryCountry"
		value="${empty param.newLineDeliveryCountry ? sessionScope.newShipmentNewLineDeliveryCountry : param.newLineDeliveryCountry}" scope="session" />
	<c:set var="newShipmentNewLineDeliveryLocality"
		value="${empty param.newLineDeliveryLocality ? sessionScope.newShipmentNewLineDeliveryLocality : param.newLineDeliveryLocality}" scope="session" />
	<c:set var="newShipmentNewLineDeliveryAddress"
		value="${empty param.newLineDeliveryAddress ? sessionScope.newShipmentNewLineDeliveryAddress : param.newLineDeliveryAddress}" scope="session" />
	<c:set var="newShipmentNewLineCargoName"
		value="${empty param.newLineCargoName ? sessionScope.newShipmentNewLineCargoName : param.newLineCargoName}" scope="session" />
	<c:set var="newShipmentNewLineLoadUnits" value="${empty requestScope.newLineLoadUnits ? (empty param.newLineLoadUnits ? 
	    sessionScope.newShipmentNewLineLoadUnits : param.newLineLoadUnits) : requestScope.newLineLoadUnits}" scope="session" />
	<c:set var="newShipmentNewLineCargoWeight" value="${empty requestScope.newLineCargoWeight ? (empty param.newLineCargoWeight ? 
	    sessionScope.newShipmentNewLineCargoWeight : param.newLineCargoWeight) : requestScope.newLineCargoWeight}" scope="session" />
	<c:set var="newShipmentNewLineCargoTruckType"
		value="${empty param.newLineCargoTruckType ? sessionScope.newShipmentNewLineCargoTruckType : param.newLineCargoTruckType}" scope="session" />
	<c:set var="newShipmentNewLineRouteConditions"
		value="${empty param.newLineRouteConditions ? sessionScope.newShipmentNewLineRouteConditions : param.newLineRouteConditions}" scope="session" />

	<form name="lineDefinition" id="lineDefinition" action="${pageContext.request.contextPath}/" method="post">
		<input type="hidden" name="local_url" value="/shipments/create" /> 
		<input type="hidden" name="prefix1" value="newShipmentNewLine" /> 
		<input type="hidden" name="prefix2" value="newLine" /> 
		<input type="hidden" name="renderLineId" id="renderLineId" value="${param.renderLineId}"/> 
		<input type="hidden" name="addressCountry" id="addressCountry" value="${param.addressCountry}" /> 
		<input type="hidden" name="sortArg" id="lineSortArg" /> 
		<input type="hidden" name="commandName" id="lineDefinitionCommand" value="saveNewLine" />
		<table>
			<tr>
				<td><label for="newLinePickupCountry">Country of pickup: </label> 
				    <select name="newLinePickupCountry" id="newLinePickupCountry" onchange="filterLines();">
						<option disabled selected>select country of pickup</option>
						<c:forEach var="pickupCountry" items="${countryService.getAll()}">
							<option value="${pickupCountry.id}" ${sessionScope.newShipmentNewLinePickupCountry == pickupCountry.id ? 'selected' : ''}>
								<c:out value="${pickupCountry.name}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openCountryForm()">new</button> <br>
					<div class="alarm"> <c:out value="${newShipmentNewLineWrongPickupCountry}" /></div></td>
				<td><label for="newLinePickupLocality">Locality of pickup: </label> <select name="newLinePickupLocality" id="newLinePickupLocality" 
				onchange="filterLines();">
						<option disabled selected>select locality of pickup</option>
						<c:forEach var="pickupLocality" items="${localityService.getByCountry(sessionScope.newShipmentNewLinePickupCountry)}">
							<option value="${pickupLocality.id}" ${sessionScope.newShipmentNewLinePickupLocality == pickupLocality.id ? 'selected':''}>
								<c:out value="${pickupLocality.name}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openLocalityForm()">new</button> <br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongPickupLocality}" />
					</div></td>
				<td><label for="newLinePickupAddress">Address of pickup: </label> 
				<select name="newLinePickupAddress" id="newLinePickupAddress" onchange="filterLines();">
						<option disabled selected>select address of pickup</option>
						<c:forEach var="pickupAddress" items="${addressService.getByLocality(sessionScope.newShipmentNewLinePickupLocality)}">
							<option value="${pickupAddress.id}"
								${sessionScope.newShipmentNewLinePickupAddress == pickupAddress.id ? 'selected':''}>
								<c:out value="${pickupAddress.details}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openAddressForm()">new</button> <br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongPickupAddress}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="newLineDeliveryCountry">Country of delivery: </label> 
				    <select name="newLineDeliveryCountry" id="newLineDeliveryCountry" onchange="filterLines();">
						<option disabled selected>select country of delivery</option>
						  <c:forEach var="deliveryCountry" items="${countryService.getAll()}">
							<option value="${deliveryCountry.id}"
								${sessionScope.newShipmentNewLineDeliveryCountry == deliveryCountry.id ? 'selected':''}>
								<c:out value="${deliveryCountry.name}" />
							</option>
						  </c:forEach>
				    </select>
					<button type="button" onclick="openCountryForm()">new</button> <br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongDeliveryCountry}" />
					</div></td>
				<td><label for="newLineDeliveryLocality">Locality of delivery: </label> 
				    <select id="newLineDeliveryLocality" name="newLineDeliveryLocality" onchange="filterLines();">
						<option disabled selected>select delivery locality</option>
						<c:forEach var="deliveryLocality" items="${localityService.getByCountry(sessionScope.newShipmentNewLineDeliveryCountry)}">
							<option value="${deliveryLocality.id}"
								${sessionScope.newShipmentNewLineDeliveryLocality == deliveryLocality.id ? 'selected' : ''}>
								<c:out value="${deliveryLocality.name}" />
							</option>
						</c:forEach>
				</select>
					<button type="button" onclick="openLocalityForm()">new</button> <br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongDeliveryLocality}" />
					</div></td>
				<td><label for="newLineDeliveryAddress">Address of delivery</label> 
				<select name="newLineDeliveryAddress" id="newLineDeliveryAddress" onchange="filterLines();">
						<option disabled selected>select delivery address</option>
						<c:forEach var="deliveryAddress"
							items="${addressService.getByLocality(sessionScope.newShipmentNewLineDeliveryLocality)}">
							<option value="${deliveryAddress.id}" ${sessionScope.newShipmentNewLineDeliveryAddress == deliveryAddress.id ? 'selected' : ''}>
								<c:out value="${deliveryAddress.details}" /></option>
						</c:forEach>
				</select>
					<button type="button" onclick="openAddressForm()">new</button> <br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongDeliveryAddress}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="newLineCargoName">Cargo: </label> 
				    <input type="text" name="newLineCargoName" id="newLineCargoName" value="${sessionScope.newShipmentNewLineCargoName}" onchange="filterLines();" /> <br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongCargoName}" />
					</div></td>
				<td><label for="newLineLoadUnits">Number of load units to pickup: </label> 
				<input type="text" name="newLineLoadUnits" id="newLineLoadUnits" value="${sessionScope.newShipmentNewLineLoadUnits}" onchange="filterLines();" /><br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongLoadUnits}" />
					</div></td>
				<td><label for="newLineCargoWeight">Cargo weight, kg: </label>
					<input type="text" name="newLineCargoWeight" id="newLineCargoWeight" value="${sessionScope.newShipmentNewLineCargoWeight}" onchange="filterLines();" /><br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongCargoWeight}" />
					</div></td>
			</tr>
			<tr>
				<td><label for="newLineCargoTruckType">Required truck type: </label> 
				    <select name="newLineCargoTruckType" id="newLineCargoTruckType" onchange="filterLines();">
						<option disabled selected>select truck type</option>
						<c:forEach var="typeOfTruck"
							items="${truckloadService.getTruckTypes()}">
							<option value="${typeOfTruck}"
								${sessionScope.newShipmentNewLineCargoTruckType == typeOfTruck ? 'selected' : ''}>
								<c:out value="${typeOfTruck}" /></option>
						</c:forEach>
				</select><br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongCargoTruckType}" />
					</div></td>
				<td><label for="newLineRouteConditions">Specific transport conditions: </label> 
				    <select name="newLineRouteConditions" id="newLineRouteConditions" onchange="filterLines();">
						<option disabled selected>select conditions</option>
						<c:forEach var="condition" items="${routeService.getConditions()}">
							<option value="${condition}"
								${sessionScope.newShipmentNewLineRouteConditions == condition ? 'selected' : ''}>
								<c:out value="${condition}" />
							</option>
						</c:forEach>
				</select><br>
					<div class="alarm">
						<c:out value="${newShipmentNewLineWrongRouteConditions}" />
					</div></td>
				<td><button type="submit" value="submit">Save New Line</button>
					<button onclick="resetFilter();">Drop Filter</button></td>
			</tr>
		</table>
		<c:set var="newShipmentNewLines" scope="session" value="${not empty param.tender_id ? 
		lineService.getByTender(param.tender_id) : (not empty sessionScope.newShipmentTender ? 
		lineService.getByTender(newShipmentTender.id) : lineService.getUserRelativeLines(user))}" />
		<c:set var="newShipmentNewLinesToRender" scope="session" value="${not empty param.tender_id ? newShipmentNewLines : 
		(empty sessionScope.newShipmentNewLinesToRender ? (param.commandName ne 'filterLines' 
		&& param.commandName ne 'sortLines' ? newShipmentNewLines : sessionScope.newShipmentNewLinesToRender) : sessionScope.newShipmentNewLinesToRender)}" />

		<c:set var="newShipmentNewLinesToRender" scope="session" value="${empty newShipmentNewLinesComparator ? sessionScope.newShipmentNewLinesToRender : 
		((param.commandName eq 'filterLines' or param.commandName eq 'sortLines' or param.commandName eq 'resetLinesFilter') ? 
		lineService.sort(sessionScope.newShipmentNewLinesToRender, sessionScope.newShipmentNewLinesComparator) : sessionScope.newShipmentNewLinesToRender)}" />
		<h4>Lines selection form</h4>
		<ym:paging source="${newShipmentNewLinesToRender}" size="5" indexName="${PaginationIndices.AVAILABLE_LINES.provideIndex()}">
			<table>
				<tr>
					<th><a href="#" onclick="sortLinesById();">Id</a></th>
					<th><a href="#" onclick="sortByStartCountry();">Country of Departure</a></th>
					<th><a href="#" onclick="sortByStartCity();">City of Departure</a></th>
					<th><a href="#" onclick="sortByEndCountry();">Country of Arrival</a></th>
					<th><a href="#" onclick="sortByEndCity();">City of Arrival</a></th>
					<th><a href="#" onclick="sortByConditions();">Transport Conditions</a></th>
					<th><a href="#" onclick="sortByLoad();">Truckload</a></th>
					<th><a href="#" onclick="sortByWeight();">Weight</a></th>
					<th><a href="#" onclick="sortByLoadUnits();">Loading Units</a></th>
					<th><a href="#" onclick="sortByTruckType();">Truck Type</a></th>
				</tr>
				<c:forEach var="existingLine" items="${bunch}" varStatus="loop">
					<tr>
						<td><input type="radio" name="lineToUtilize" id="lineId${loop.index}" value="${existingLine.id}" /> 
						<label for="lineId${loop.index}"><a href="#" onclick="openLineRenderPopup(${existingLine.id})"> 
							<c:out value="${existingLine.id}" /></a></label></td>
						<td><c:out value="${existingLine.route.start.locality.country.name}" /></td>
						<td><c:out value="${existingLine.route.start.locality.name}" /></td>
						<td><c:out value="${existingLine.route.destination.locality.country.name}" /></td>
						<td><c:out value="${existingLine.route.destination.locality.name}" /></td>
						<td><c:out value="${existingLine.route.conditions.toString()}" /></td>
						<td><c:out value="${existingLine.truckload.name}" /></td>
						<td><c:out value="${existingLine.truckload.weight}" /></td>
						<td><c:out value="${existingLine.truckload.loadUnits}" /></td>
						<td><c:out value="${existingLine.truckload.truckType.toSqlValue()}" /></td>
					</tr>
				</c:forEach>
			</table>
		</ym:paging>
		<br>
		<c:choose>
			<c:when test="${empty newShipmentNewLines}">
				<button type="button" onclick="createLine();">Save new line</button>
			</c:when>
			<c:otherwise>
				<button type="button" onclick="selectForNewShipment();">Select line</button>
				<button type="button" onclick="dropSelectedLine();">Drop selected line</button>
			</c:otherwise>
		</c:choose>
	</form>
	<!-- AVAILABLE LINES SECTION END -->

	<!-- SHIPMENT DETAILS INPUT SECTION START -->
	<c:set var="newShipmentTender" scope="session"
		value="${empty param.tender_id ? sessionScope.newShipmentTender : tenderService.get(param.tender_id)}" />
	<c:set var="newShipmentLine" scope="session"
		value="${empty param.lineToUtilize ? sessionScope.newShipmentLine : lineService.get(param.lineToUtilize)}" />
	<c:if test="${not empty param.tender_id or param.commandName eq 'dropLine'}">
		<c:remove var="newShipmentLine" scope="session" />
	</c:if>
	<!-- SHIPMENT DETAILS INPUT SECTION END -->

	<!-- NEW SHIPMENT DESIGN SECTION START -->	
	<c:if test="${not empty newShipmentLine}">
	<h3>Your new order details: </h3>
	<table>
      <tr>
        <td>Loading date: </td>
	    <td><input type="date" name="loadingDate" id="loadingDate" form="tenders" 
	    value="${not empty param.loadingDate ? param.loadingDate : sessionScope.newShipmentLoadingDate}" onchange="registerNewShipmentTerms();"/>
	    <c:out value=" ${invalidLoadingDate}" /></td>
        <td>Delivery term: </td>
        <td><input type="text" name="deliveryTerm" id="deliveryTerm" form="tenders" 
        value="${not empty param.deliveryTerm ? param.deliveryTerm : sessionScope.newShipmentDeliveryTerm}" onchange="registerNewShipmentTerms();" />
	    <c:out value=" ${invalidDeliveryTerm}" /></td>
	  </tr>
      <tr>
        <td>Scope: </td>
        <td><c:out value="${not empty newShipmentTender ? 'tender '.concat(newShipmentTender.id).concat(' ').concat(newShipmentTender.title) : 'spot'}"/></td>
        <td>Truck type: </td>
        <td><c:out value="${newShipmentLine.truckload.truckType}"/></td>
      </tr>
      <tr>
        <td>Place of loading: </td>
        <td><c:out value="${newShipmentLine.route.start.details.concat(' ').concat(newShipmentLine.route.start.locality.name)
	        .concat(', ').concat(newShipmentLine.route.start.locality.country.name)}"/></td>
        <td>Place of delivery: </td>
        <td><c:out value="${newShipmentLine.route.destination.details.concat(' ').concat(newShipmentLine.route.destination.locality.name)
        .concat(', ').concat(newShipmentLine.route.destination.locality.country.name)}"/></td>
      </tr>
      <tr>
        <td>Cargo: </td>
        <td><c:out value="${newShipmentLine.truckload.name.concat(' ').concat(newShipmentLine.truckload.loadUnits).concat(' units, ')
	    .concat(newShipmentLine.truckload.weight).concat(' kg')}"/></td>
        <td>Transit conditions: </td>
        <td><c:out value="${newShipmentLine.route.conditions}"/></td>
      </tr>
    </table>
		<br><button type="button" onclick="shimpentSubmit();">Submit new order</button>
	</c:if>
	<!-- NEW SHIPMENT DESIGN SECTION END -->
	<c:if test="${not empty newShipmentId}">
		<form action="${pageContext.request.contextPath}/shipments/shipment" method="get">
			<input type="hidden" name="id" value="${newShipmentId}" />
			<fmt:message var="view" key="viewNewShipment" bundle="${bndle}" />
			<input type="submit" value="view" />
		</form>
	</c:if>
	<br><a href="${pageContext.request.contextPath}/shipments"><fmt:message key="shipmentsLink" bundle="${bndle}" /></a>
</body>
</html>
