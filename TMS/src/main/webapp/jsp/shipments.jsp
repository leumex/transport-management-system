<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ym" uri="mytags"%>
<html>
<head>
<title>Shipments</title>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/js/shipments.js"></script>
</head>
<body>
	<%@include file="internatiolization.jsp"%>
	<%@page import="by.minsk.training.entity.TspPerformance.TspResponse"%>
	<%@page import="by.minsk.training.ApplicationContext"%>
	<%@page import="by.minsk.training.command.pagination.PaginationIndices"%>
	<%@page import="by.minsk.training.user.UserEntity.Roles"%>
	<%@page import="by.minsk.training.entity.TransportOrder.State"%>

	<c:set var="transportOrderService" value="${sessionScope.applicationContext.getBean('TransportOrderService')}" />
	<c:set var="routeService" value="${sessionScope.applicationContext.getBean('RouteService')}" />
	<c:set var="truckloadService" value="${sessionScope.applicationContext.getBean('TruckloadService')}" />
	<c:set var="spotOrderService" value="${sessionScope.applicationContext.getBean('SpotOrderService')}" />
	<h1>
		<fmt:message key="shipmentsLink" bundle="${bndle}" />
	</h1>
	<br>

	<!-- definition of shipments to be rendered -->
	<c:set var="user_shipments" scope="session" 
	value="${empty sessionScope.user_shipments ? transportOrderService.gatherOrders(sessionScope.user) : sessionScope.user_shipments}" />

	<!-- transport orders table with content dependent on the user role -->
	<ym:paging source="${user_shipments}" indexName="${PaginationIndices.SHIPMENTS_CONTENT.provideIndex()}" size="15">
		<form action="${pageContext.request.contextPath}/" method="post" id="shipments_form">
			<input type="hidden" name="local_url" value="/shipments" /> 
			<input type="hidden" name="commandName" id="command" /> 
			<input type="hidden" name="comparingParameter" id="comparingParameter" />
			<table>
				<tr>
					<td><c:out value="${wrong_order_id}" /><br>
					    <c:out value="${invalid_order_state}" /></td>
					<td><button type="button" onclick="resetOrdersFilter();">Drop Filter</button></td>
					<td><c:if test="${sessionScope.user.role ne 'CUSTOMER'}">
							<button type="submit" onclick="acceptTransportOrder();">Accept Order</button>
						</c:if></td>
					<td><c:if test="${sessionScope.user.role ne 'CUSTOMER'}">
							<button type="submit" onclick="rejectTransportOrder();">Reject Order</button>
						</c:if></td>
					<td><c:if
							test="${sessionScope.user.role ne 'TRANSPORT_SERVICE_PROVIDER'}">
							<button type="submit" onclick="recallTransportOrder();">Recall Order</button>
						</c:if></td>
					<c:if test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
						<td></td>
					</c:if>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<th><a href="#" onclick="sortById();">Id</a><br>
					<input type="text" name="id_filter" value="${param.id_filter}" onchange="filter();" /> <br>
					<c:out value="${wrong_id_filter}" /></th>
					<th><a href="#" onclick="sortByDate();">Date</a><br>
					<input type="text" name="date_filter" value="${param.date_filter}" onchange="filter();" /><br>
					<c:out value="${wrong_date_filter}" /></th>
					<th><a href="#" onclick="sortByLoadingDate();">Loading Date</a><br>
					<input type="text" name="pickup_date_filter" value="${param.pickup_date_filter}" onchange="filter();" /><br>
					<c:out value="${wrong_pickup_date_filter}" /></th>
					<th><a href="#" onclick="sortByRoute();">Route</a><br>
					<input type="text" name="route_filter" value="${param.route_filter}" onchange="filter();" /></th>
					<th><a href="#" onclick="sortByTruckload();">Truckload</a><br>
					<input type="text" name="truckload_filter" value="${param.truckload_filter}" onchange="filter();" /></th>
					<th><a href="#" onclick="sortByCompany();"> <c:choose>
								<c:when test="${user.role eq 'CUSTOMER'}">Carrier</c:when>
								<c:when test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">Customer</c:when>
								<c:otherwise>Customer / Carrier</c:otherwise>
							</c:choose>
					</a><br>
					<input type="text" name="company_filter" value="${param.company_filter}" onchange="filter();" /></th>
					<th><a href="#" onclick="sortByScope();">Scope</a><br>
					<input type="text" name="scope_filter" value="${param.scope_filter}" onchange="filter();" /></th>
					<th><a href="#" onclick="sortByState();">State</a><br>
					<input type="text" name="state_filter" value="${param.state_filter}" onchange="filter();" /></th>
					<th>Rate<br>
					<input type="text" name="rate_filter" value="${param.rate_filter}" onchange="filter();" /><br>
					<c:out value="${wrong_rate_filter}" /></th>
					<th>Truck reg.#<br>
					<input type="text" name="plates_filter" value="${param.plates_filter}" onchange="filter();" /></th>
				</tr>
				<c:forEach var="order" items="${bunch}">
					<tr>
						<td><label for="order_id"> <a href="${pageContext.request.contextPath}/shipments/shipment?id=${order.id}">
						        <c:out value="${order.id}" /></a>
						    </label><input type="radio" name="order_id" id="order_id" value="${order.id}" /></td>
						<td><c:out value="${order.issued}" /></td>
						<td><c:out value="${order.loadingDate}" /></td>
						<td><c:out value="${routeService.deliverRepresentation(order.line.route)}" /></td>
						<td><c:out value="${truckloadService.deliverRepresentation(order.line.truckload)}" /></td>
						<td><c:choose> 
								<c:when test="${user.role eq 'CUSTOMER'}">
									<c:out value="${order.state eq TransportOrder.State.CREATED ? '' : 
									(not empty order.customer ? (spotOrderService.hasOpenSpotAuction(order) ? 'spot' : (transportOrderService.getLatestAssignment(user, order).tsp.name)) :
									transportOrderService.getLatestAssignment(user, order).tsp.name)}" />
								</c:when>
								<c:when test="${user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
									<c:out value="${not empty order.customer ? order.customer.name : order.tender.customer.name}" />
								</c:when>
								<c:otherwise>
									<c:out value="${(not empty order.customer ? order.customer.name : order.tender.customer.name).concat(' / ')
		        					.concat(not empty order.customer ? (spotOrderService.hasOpenSpotAuction(order) ? 'spot' : (transportOrderService.getLatestAssignment(user, order).tsp.name)) :
									transportOrderService.getLatestAssignment(user, order).tsp.name)}" />
								</c:otherwise>
							</c:choose></td>
						<td><c:out value="${not empty order.customer ? (order.state ne State.CREATED ? 'spot' : 'no scope') : order.tender.id.toString().concat(' ').concat(order.tender.title)}" /></td>
						<td><c:out value="${order.state.toString().concat(not empty transportOrderService.getLatestAssignment(user, order) ? 
									' / '.concat(transportOrderService.getLatestAssignment(user, order).tspResponse.toString()) : '')}" /></td>
						<!-- Rate column. 
		In case order belongs to a tender - the rate is defined using latest correspondent tsp_performance 
		or it is blank if there's no tsp_performances so far (Order.state is CREATED);
		Since order has been sent to spot - order no longer belongs to a tender.
		In case order belongs to a spot scope, and spot auction is open - the field is blank;
		in case order belongs to a spot, and there're correspondent tspPerformances, then the rate is defined using the latest correspondent tsp_performance record	
		-->
						<c:set var="rate" value="${transportOrderService.defineOrderRate(user, order)}" />
						<td><c:out value="${rate == 0 ? '' : rate}" /></td>
						<td><c:out value="${not empty transportOrderService.getLatestAssignment(user, order) ? transportOrderService.getLatestAssignment(user, order).truckPlates : ''}" /></td>
					</tr>
				</c:forEach>
			</table>
		</form>
	</ym:paging>
	<br>
	<a href="${pageContext.request.contextPath}/">to home page</a>
</body>
</html>
