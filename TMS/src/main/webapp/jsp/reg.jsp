<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<%@include file="internatiolization.jsp" %>
<h1><fmt:message key="startTitle" bundle="${bndle}"/></h1>

<h1><fmt:message key="registration" bundle="${bndle}"/></h1>
<form action="${pageContext.request.contextPath}/" method="post">
    <input type="hidden" name="commandName" value="registerSaveUser">
    <fmt:message key="namePlaceHolder" bundle="${bndle}" var="namePh"/>
    <fmt:message key="name" bundle="${bndle}"/>:
    <input type="text" placeholder="${namePh}" name="name" value="${param.name}" required><br>

    <fmt:message key="role" bundle="${bndle}"/>:<br>
    <input type="radio" name="role" value="customer" required><fmt:message key="customer" bundle="${bndle}"/><br>
    <input type="radio" name="role" value="carrier"><fmt:message key="carrier" bundle="${bndle}"/><br>
    <input type="radio" name="role" value="admin"><fmt:message key="admin" bundle="${bndle}"/><br>

    <fmt:message key="userLoginName" bundle="${bndle}"/>:
    <fmt:message key="userLoginPlaceHolder" bundle="${bndle}" var="loginPh"/>
    <input type="text" placeholder="${loginPh}" name="login" value="${param.login}"  required> <br>

    <fmt:message key="password" bundle="${bndle}"/>:
    <fmt:message key="userPasswordPlaceHolder" bundle="${bndle}" var="pswdPh"/>
    <input type="text" placeholder="${pswdPh}" name="password" required><br>

    <button type="submit"><fmt:message key="register" bundle="${bndle}"/></button>

</form>
<c:if test="${not empty requestScope.incorrect_login}">
<fmt:message key="${requestScope.incorrect_login}" bundle="${bndle}"/>
</c:if>
<c:if test="${not empty requestScope.poor_password}">
<fmt:message key="${requestScope.poor_password}" bundle="${bndle}"/>
</c:if>
<c:if test="${requestScope.registerSuccess}">
    <fmt:message key="registrationSuccess" bundle="${bndle}" var="regSuccess"/>
    <c:out value="${regSuccess}"/>
</c:if>
<br>
<a href="${pageContext.request.contextPath}/"><fmt:message key="startPageLink" bundle="${bndle}"/></a>

</body>
</html>
