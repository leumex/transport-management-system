<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title>Company</title>
</head>
<body>
	<%@include file="internatiolization.jsp"%>
	<h1>
		<fmt:message key="company" bundle="${bndle}" />
	</h1>
	<%@page import="by.minsk.training.ApplicationConstants"%>
	<%@page import="by.minsk.training.ApplicationContext"%>
	<%@page import="by.minsk.training.user.UserEntity.Roles"%>
	<c:set var="appContext" scope="page" value="${ApplicationContext.getInstance()}" />
	<c:set var="userService" scope="page" value="${appContext.getBean('UserService')}" />
	<c:set var="transportOrderService" scope="page" value="${appContext.getBean('TransportOrderService')}" />
	<c:set var="tenderService" scope="page" value="${appContext.getBean('TenderService')}" />
	<c:set var="tenderParticipationService" scope="page" value="${appContext.getBean('TenderParticipationService')}" />
	<c:set var="bidsService" scope="page" value="${appContext.getBean('BidsService')}" />
	<c:set var="tspPerformanceService" value="${appContext.getBean('TspPerformanceService')}" />
	<c:set var="spotOrderService" value="${appContext.getBean('SpotOrderService')}" />
	<c:set var="user" scope="page" value="${sessionScope.user}" />
	<!-- companies search form, always available -->
	<form action="${pageContext.request.contextPath}/" method="get" id="searchForm">
		<input type="hidden" name="commandName" value="findCompany" /> 
		<input type="hidden" name="local_url" value="/report/company" /> 
		<label for="companySearchInput">Enter company's title or id: </label> <input type="text" name="usersInput" id="companySearchInput" placeholder="search company" /> 
		<input type="submit" value="search" name="search" />
	</form>
	<!-- ------------------------search results---------------------------- -->
	<c:if test="${not empty requestScope.foundCompanies}">
	Found companies:
	<c:forEach var="company" items="${requestScope.foundCompanies}">
	<a href="${pagContext.request.contextPath}/report/company?id=${company.id}"><c:out value="${company.id.concat(' ').concat(company.name)}"/></a>
	</c:forEach>
	</c:if>
	<c:out value="${requestScope.emptyResult}"/>
	<!-- ------depending on presence of company' identifying parameters / attributes , also depending on user's role and the role of the rendered company - different content is displayed-------- -->
	<c:choose>
		<c:when
			test="${not empty sessionScope.renderedCompany or not empty param.id}">
			<c:set var="renderedCompany" scope="session" value="${not empty sessionScope.renderedCompany ? sessionScope.renderedCompany : userService.findById(param.id)}" />
			<h3>
				<c:if test="${renderedCompany.id eq sessionScope.user.id}">
					<c:out value="You have been registered in the system as " />
				</c:if>
				<c:out value="${renderedCompany.name}" />
			</h3>
							Id: <c:out value="${renderedComany.id}" />
			<br>
							Name: <c:out value="${renderedCompany.name}" />
			<br>
							Role: <c:out value="${renderedCompany.role.printOut()}" />
			<br>
							Registered on: <c:out value="${renderedCompany.registrationDate}" />
			<br>
			<c:choose>
				<c:when test="${sessionScope.user.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
					<c:choose>
						<c:when test="${renderedCompany.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
							<!-- ----------------------------------------user is tsp, company is tsp-------------------------------------------------- -->
							Transport orders executed: <c:out value="${transportOrderService.getTspEverExecutedOrdersNumber(renderedCompany)}" />
							<c:if test="${renderedCompany.id eq sessionScope.user.id}">
								<!-- overall KPI (if user finds himself only) -->
								<h4>Own general KPI</h4>
								<c:set var="tspKPI"
									value="${transportOrderService.calculateTSPGeneralKPI(sessionScope.user)}" />
								<table>
									<tr>
										<th>% of assigned TOs acceptance</th>
										<th>% of in-time arrivals for loading</th>
										<th>% of transport order execution success</th>
										<th>% of delivery terms compliance</th>
									</tr>
									<tr>
										<td><c:out value="${tspKPI.assignedTOsAccepted}" /></td>
										<td><c:out value="${tspKPI.inTimePickups}" /></td>
										<td><c:out value="${tspKPI.acceptedTOsExecution}" /></td>
										<td><c:out value="${tspKPI.deliveryTermCompliance}" /></td>
									</tr>
								</table>
							</c:if>
						</c:when>
						<c:when test="${renderedCompany.role eq 'CUSTOMER'}">
							<!-- -----------------------------------------user is tsp, company is customer------------------------------------------------- -->
							<br>
							Number of carried out tenders: <c:out value="${tenderService.countCarriedOutTenders(renderedCompany)}" />
							<br>
							Number of released and completed transport orders: <c:out value="${transportOrderService.getCustomerEverExecutedOrdersNumber(renderedCompany)}" />
							<c:set var="contract" value="${userService.findContract(sessionScope.user, renderedCompany)}" />
							<c:choose>
								<c:when test="${empty contract}">
									<form action="${pageContext.request.contextPath}/" method="post">
										<input type="hidden" name="commandName" value="concludeContract" /> 
										<input type="hidden" name="local_url" value="/report/company" /> 
										<label for="singedDate">Contact date: </label><input type="date" name="signedDate" id="signedDate" /> 
										<input type="submit" value="register new contract" name="register" />
									</form>
								</c:when>
								<c:otherwise>
							Contract # <c:out value="${contract.id}" />
							Contract sign date: <c:out value="${contract.signed}" />
								</c:otherwise>
							</c:choose>
							<h4>
								Your participation in the tenders of <c:out value="${renderedCompany.name}" />
							</h4>
							<table>
								<tr>
									<th>Tender</th>
									<th>Terms</th>
									<th>Quantity of executed transport orders</th>
									<th>% of assigned TOs acceptance</th>
									<th>% of in-time arrivals for loading</th>
									<th>% of transport order execution success</th>
									<th>% of delivery terms compliance</th>
								</tr>
								<c:forEach var="participation"
									items="${tenderParticipationService.summarizeTspToCustomerTenderResponse(sessionScope.user, renderedCompany)}">
									<tr>
										<c:set var="tender" value="${tenderService.get(participation.tender.id)}" />
										<td><a href="${pageContext.request.contextPath}/tenders/tender?id=${tender.id}"><c:out
													value="${tender.id.concat(' ').concat(tender.title)}" /></a></td>
										<td><c:out value="${tender.tenderStart.concat(' - ').concat(tender.tenderEnd)}" /></td>
										<td><c:out value="${tenderService.countTspExecutedOrdersInTender(tender, sessionScope.user)}" /></td>
										<c:set var="indicators" value="${tenderService.calculateTspTenderKPI(tender,sessionScope.user)}" />
										<td><c:out value="${indicators.assignedTOsAccepted}" /></td>
										<td><c:out value="${indicators.inTimePickups}" /></td>
										<td><c:out value="${indicators.acceptedTOsExecution}" /></td>
										<td><c:out value="${indicators.deliveryTermCompliance}" /></td>
									</tr>
								</c:forEach>
							</table>
							<h4>
								Your performance stats of spot orders execution received from
								<c:out value="${renderedCompany.name}" />
							</h4>
							<table>
								<tr>
									<th>Id</th>
									<th>Route</th>
									<th>Cargo</th>
									<th>Rate</th>
									<th>Truck plates</th>
									<th>Arrived at loading</th>
									<th>Left loading</th>
									<th>Arrived at destination</th>
									<th>Left destination</th>
								</tr>
								<c:forEach var="loopBid" items="${bidsService.findTspSuccessfulBids(renderedCompany, sessionScope.user)}">
									<c:set var="orderRoute" value="${loopBid.spotOrder.transportOrder.line.route.start.name.concat(', ').concat(loopBid.spotOrder.transportOrder.line.route.start.country.name).concat(' - ')
					.concat(loopBid.spotOrder.transportOrder.line.route.destination.name).concat(', ').concat(loopBid.spotOrder.transportOrder.line.route.destination.country.name)}" />
									<c:set var="performance" value="${tspPerformanceService.getSpotOrderImplementedAssignment(loopBid.spotOrder.transportOrder)}" />
									<tr>
										<td><a href="${pageContext.request.contextPath}/shipments/shipment?id=${loopBid.spotOrder.transportOrder.id}"></a></td>
										<td><c:out value="${orderRoute}" /></td>
										<td><c:out value="${loopBid.spotOrder.transportOrder.line.truckload.name.concat(', ')
									.concat(loopBid.spotOrder.transportOrder.line.truckload.weight).concat(' kg')}" /></td>
										<td><c:out value="${loopBid.bid}" />
										<td><c:out value="${performance.truckPlates}" /></td>
										<td><c:out value="${performance.arrivedForLoading}" /></td>
										<td><c:out value="${performance.leftLoading}" /></td>
										<td><c:out value="${performance.arrivedForUnloading}" /></td>
										<td><c:out value="${performance.leftUnloading}" /></td>
									</tr>
								</c:forEach>
							</table>
							<br>
							<c:set var="spotIndicators" value="${spotOrderService.calculateTspSpotOrdersExecutionKPI(sessionScope.user)}" />
							<table>
								<tr>
									<th>Quantity of executed transport orders</th>
									<th>% of assigned TOs acceptance</th>
									<th>% of in-time arrivals for loading</th>
									<th>% of transport order execution success</th>
									<th>% of delivery terms compliance</th>
								</tr>
								<tr>
									<td><c:out value="${spotOrderService.countTspSpotExecutedOrders(sessionScope.user)}" /></td>
									<td><c:out value="${spotIndicators.assignedTOsAccepted}" /></td>
									<td><c:out value="${spotIndicators.inTimePickups}" /></td>
									<td><c:out value="${spotIndicators.acceptedTOsExecution}" /></td>
									<td><c:out value="${spotIndicators.deliveryTermCompliance}" /></td>
								</tr>
							</table>
						</c:when>
					</c:choose>
				</c:when>
				<c:when test="${sessionScope.user.role eq 'CUSTOMER'}">
					<c:choose>
						<c:when
							test="${renderedCompany.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
							<!-- ----------------------------------user is customer, company is tsp---------------------------------------------- -->
							<c:set var="contract"
								value="${userService.findContract(renderedCompany, sessionScope.user)}" />
							Number of transport orders executed by the company: <c:out
								value="${transportOrderService.getTspEverExecutedOrdersNumber(renderedCompany)}" />
							<c:choose>
								<c:when test="${empty contract}">
									<form action="${pageContext.request.contextPath}/" method="post">
										<input type="hidden" name="commandName" value="concludeContract" /> 
										<input type="hidden" name="local_url" value="/report/company" /> 
										<label for="singedDate">Contract date: </label><input type="date" name="signedDate" id="signedDate" /> 
										<input type="submit" value="register new contract" name="register" />
									</form>
								</c:when>
								<c:otherwise>
							Contract # <c:out value="${contract.id}" />
							Contract sign date: <c:out value="${contract.signed}" />
								</c:otherwise>
							</c:choose>
							<h4>
								<c:out value="${renderedCompany.name}" /> participation in your tenders
							</h4>
							<table>
								<tr>
									<th>Tender</th>
									<th>Terms</th>
									<th>Quantity of executed transport orders</th>
									<th>% of assigned TOs acceptance</th>
									<th>% of in-time arrivals for loading</th>
									<th>% of transport order execution success</th>
									<th>% of delivery terms compliance</th>
								</tr>
								<c:forEach var="participation" items="${tenderParticipationService.summarizeTspToCustomerTenderResponse(renderedCompany, sessionScope.user)}">
									<tr>
										<c:set var="tender" value="${tenderService.get(participation.tender.id)}" />
										<td><a href="${pageContext.request.contextPath}/tenders/tender?id=${tender.id}">
										<c:out value="${tender.id.concat(' ').concat(tender.title)}" /></a></td>
										<td><c:out value="${tender.tenderStart.concat(' - ').concat(tender.tenderEnd)}" /></td>
										<td><c:out value="${tenderService.countTspExecutedOrdersInTender(tender, renderedCompany)}" /></td>
										<c:set var="indicators" value="${tenderService.calculateTspTenderKPI(tender,renderedCompany)}" />
										<td><c:out value="${indicators.assignedTOsAccepted}" /></td>
										<td><c:out value="${indicators.inTimePickups}" /></td>
										<td><c:out value="${indicators.acceptedTOsExecution}" /></td>
										<td><c:out value="${indicators.deliveryTermCompliance}" /></td>
									</tr>
								</c:forEach>
							</table>
							<h4>
								<c:out value="${renderedCompany.name}" /> performance of your spot orders execution
							</h4>
							<table>
								<tr>
									<th>Id</th>
									<th>Route</th>
									<th>Cargo</th>
									<th>Rate</th>
									<th>Truck plates</th>
									<th>Arrived at loading</th>
									<th>Left loading</th>
									<th>Arrived at destination</th>
									<th>Left destination</th>
								</tr>
								<c:forEach var="loopBid"
									items="${bidsService.findTspSuccessfulBids(renderedCompany)}">
									<c:set var="orderRoute"
										value="${loopBid.spotOrder.transportOrder.line.route.start.name.concat(', ').concat(loopBid.spotOrder.transportOrder.line.route.start.country.name).concat(' - ')
					.concat(loopBid.spotOrder.transportOrder.line.route.destination.name).concat(', ').concat(loopBid.spotOrder.transportOrder.line.route.destination.country.name)}" />
									<c:set var="performance"
										value="${tspPerformanceService.getSpotOrderImplementedAssignment(loopBid.spotOrder.transportOrder)}" />
									<td><a
										href="${pageContext.request.contextPath}/shipments/shipment?id=${loopBid.spotOrder.transportOrder.id}"></a></td>
									<td><c:out value="${orderRoute}" /></td>
									<td><c:out
											value="${loopBid.spotOrder.transportOrder.line.truckload.name.concat(', ').concat(loopBid.spotOrder.transportOrder.line.truckload.weight).concat(' kg')}" /></td>
									<td><c:out value="${loopBid.bid}" />
									<td><c:out value="${performance.truckPlates}" /></td>
									<td><c:out value="${performance.arrivedForLoading}" /></td>
									<td><c:out value="${performance.leftLoading}" /></td>
									<td><c:out value="${performance.arrivedForUnloading}" /></td>
									<td><c:out value="${performance.leftUnloading}" /></td>
								</c:forEach>
							</table>
							<br>
							<c:set var="spotIndicators"
								value="${spotOrderService.calculateTspSpotOrdersExecutionKPI(renderedCompany)}" />
							<table>
								<tr>
									<th>Quantity of executed transport orders</th>
									<th>% of assigned TOs acceptance</th>
									<th>% of in-time arrivals for loading</th>
									<th>% of transport order execution success</th>
									<th>% of delivery terms compliance</th>
								</tr>
								<tr>
									<td><c:out
											value="${spotOrderService.countTspSpotExecutedOrders(renderedCompany)}" /></td>
									<td><c:out value="${spotIndicators.assignedTOsAccepted}" /></td>
									<td><c:out value="${spotIndicators.inTimePickups}" /></td>
									<td><c:out value="${spotIndicators.acceptedTOsExecution}" /></td>
									<td><c:out
											value="${spotIndicators.deliveryTermCompliance}" /></td>
								</tr>
							</table>
						</c:when>
						<c:when test="${renderedCompany.role eq 'CUSTOMER'}">
							<!-- -----------------------------------------user is customer, company is customer--------------------------------------- -->
							Number of released and completed transport orders: <c:out
								value="${transportOrderService.getCustomerEverExecutedOrdersNumber(renderedCompany)}" />
						</c:when>
					</c:choose>
				</c:when>
				<c:when test="${sessionScope.user.role eq 'ADMIN'}">
					<c:choose>
						<c:when
							test="${renderedCompany.role eq 'TRANSPORT_SERVICE_PROVIDER'}">
							<!-- ---------------------------------user is admin, company is tsp-------------------------------------- -->
							Number of trips ever executed by the company: <c:out
								value="${transportOrderService.getTspEverExecutedOrdersNumber(renderedCompany)}" />
							<h4>
								<c:out value="${renderedCompany.name}" />
								tenders participation
							</h4>
							<table>
								<tr>
									<th>Tender</th>
									<th>Customer</th>
									<th>Terms</th>
									<th>Quantity of executed transport orders</th>
									<th>% of assigned TOs acceptance</th>
									<th>% of in-time arrivals for loading</th>
									<th>% of transport order execution success</th>
									<th>% of delivery terms compliance</th>
								</tr>
								<c:forEach var="participation"
									items="${tenderParticipationService.findTspTenderParticipations(renderedCompany)}">
									<tr>
										<c:set var="tender"
											value="${tenderService.get(participation.tender.id)}" />
										<td><a
											href="${pageContext.request.contextPath}/tenders/tender?id=${tender.id}"><c:out
													value="${tender.id.concat(' ').concat(tender.title)}" /></a></td>
										<td><c:out value="${tender.customer.name}" />
										<td><c:out
												value="${tender.tenderStart.concat(' - ').concat(tender.tenderEnd)}" /></td>
										<td><c:out
												value="${tenderService.countTspExecutedOrdersInTender(tender, sessionScope.user)}" /></td>
										<c:set var="indicators"
											value="${tenderService.calculateTspTenderKPI(tender,sessionScope.user)}" />
										<td><c:out value="${indicators.assignedTOsAccepted}" /></td>
										<td><c:out value="${indicators.inTimePickups}" /></td>
										<td><c:out value="${indicators.acceptedTOsExecution}" /></td>
										<td><c:out value="${indicators.deliveryTermCompliance}" /></td>
									</tr>
								</c:forEach>
							</table>
							<h4>
								<c:out value="${renderedCompany.name}" />
								performance at spot orders execution
							</h4>
							<table>
								<tr>
									<th>Id</th>
									<th>Customer</th>
									<th>Route</th>
									<th>Cargo</th>
									<th>Rate</th>
									<th>Truck plates</th>
									<th>Arrived at loading</th>
									<th>Left loading</th>
									<th>Arrived at destination</th>
									<th>Left destination</th>
								</tr>
								<c:forEach var="loopBid"
									items="${bidsService.findTspSuccessfulBids(renderedCompany)}">
									<c:set var="orderRoute"
										value="${loopBid.spotOrder.transportOrder.line.route.start.name
									.concat(', ').concat(loopBid.spotOrder.transportOrder.line.route.start.country.name).concat(' - ')
				                	.concat(loopBid.spotOrder.transportOrder.line.route.destination.name).concat(', ')
					                .concat(loopBid.spotOrder.transportOrder.line.route.destination.country.name)}" />
									<c:set var="performance"
										value="${tspPerformanceService.getSpotOrderImplementedAssignment(loopBid.spotOrder.transportOrder)}" />
									<tr>
										<td><a
											href="${pageContext.request.contextPath}/shipments/shipment?id=${loopBid.spotOrder.transportOrder.id}"></a></td>
										<td><c:out
												value="${loopBid.spotOrder.transportOrder.customer.name}" /></td>
										<td><c:out value="${orderRoute}" /></td>
										<td><c:out
												value="${loopBid.spotOrder.transportOrder.line.truckload.name.concat(', ').concat(loopBid.spotOrder.transportOrder.line.truckload.weight).concat(' kg')}" /></td>
										<td><c:out value="${loopBid.bid}" />
										<td><c:out value="${performance.truckPlates}" /></td>
										<td><c:out value="${performance.arrivedForLoading}" /></td>
										<td><c:out value="${performance.leftLoading}" /></td>
										<td><c:out value="${performance.arrivedForUnloading}" /></td>
										<td><c:out value="${performance.leftUnloading}" /></td>
									</tr>
								</c:forEach>
							</table>
							<br>
							<c:set var="spotIndicators"
								value="${spotOrderService.calculateTspSpotOrdersExecutionKPI(renderedCompany)}" />
							<h4>Spot orders execution KPI</h4>
							<table>
								<tr>
									<th>Quantity of executed transport orders</th>
									<th>% of assigned TOs acceptance</th>
									<th>% of in-time arrivals for loading</th>
									<th>% of transport order execution success</th>
									<th>% of delivery terms compliance</th>
								</tr>
								<tr>
									<td><c:out
											value="${spotOrderService.countTspSpotExecutedOrders(sessionScope.user)}" /></td>
									<td><c:out value="${spotIndicators.assignedTOsAccepted}" /></td>
									<td><c:out value="${spotIndicators.inTimePickups}" /></td>
									<td><c:out value="${spotIndicators.acceptedTOsExecution}" /></td>
									<td><c:out
											value="${spotIndicators.deliveryTermCompliance}" /></td>
								</tr>
							</table>
						</c:when>
						<c:when test="${renderedCompany.role eq 'CUSTOMER'}">
							<!-- -------------------------------user is admin, company is customer---------------------------------------- -->						
	Number of tenders carried out: <c:out value="${tenderService.countCarriedOutTenders(renderedCompany)}" />
							<br>
    Number of tender transport orders fulfilled by anyone <c:out value="${tenderService.countImplementedTenderOrders(renderedCompany)}" />
							<br>
    Number of spot transport orders fulfilled by anyone <c:out value="${tenderService.countImplementedSpotOrders(renderedCompany)}" />
							<br>
							<h4>
								<c:out value="${renderedCountry.name}" /> tenders list
							</h4>
							<table>
								<tr>
									<th>Tender</th>
									<th>Tender terms</th>
									<th>Tender state</th>
								</tr>
								<c:forEach var="tender" items="${tenderService.findCustomerBoundTenders(renderedCompany)}">
									<tr>
										<td><a href="${pageContext.request.contextPath}/tenders/tender?id=${tender.id}">
										<c:out value="${tender.id.concat(' ').concat(tender.name)}" /></a></td>
										<td><c:out value="${tender.tenderStart.concat(' - ').concat(tender.tenderEnd)}" /></td>
										<td><c:out value="${tender.state.printOut()}" /></td>
									</tr>
								</c:forEach>
							</table>
						</c:when>
					</c:choose>
				</c:when>
			</c:choose>
		</c:when>
		<c:when test="${not empty requestScope.foundCompanies}">
			<h3>Found:</h3>
			<table>
				<tr>
					<th>Id</th>
					<th>Title</th>
					<th>Role</th>
				</tr>
				<c:forEach var="company" items="${requestScope.foundCompanies}">
					<tr>
						<td>${company.id}</td>
						<td><a
							href="${pageContext.request.contextPath}/report/company?id=${company.id}">${company.name}</a></td>
						<td>${company.role.printOut()}</td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
	</c:choose>
	<br>
	<a href = "${pageContext.request.contextPath}/report">To reports page</a>
	<br>
	<a href="${pageContext.request.contextPath}/">To start page</a>
</body>
</html>
