<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tender contest</title>
</head>
<body>
<%@include file="internatiolization.jsp" %>
<h1><fmt:message key="tenderDetailsTitle" bundle="${bndle}"/> <c:out value=" ${param.tenderId}"/></h1>
<h5>

    includes tender competition control view
    ( lines list/table and once selected - the other form on the page shows all line queries with daytime they have been given,
    value, TSP-user name (link to TSP user) , already selected by queries 1stly rate value 2ndly delivery term value ) - to CUSTOMER users

    includes line volume share award function (is not available once tender became 'contracted') - to CUSTOMER users

</h5>

<a href="${pageContext.request.contextPath}/tenders/tender"><fmt:message key="tenderDetailsTitle" bundle="${bndle}"/></a>
<a href="${pageContext.request.contextPath}/"><fmt:message key="startPageLink" bundle="${bndle}"/></a>
</body>
</html>
