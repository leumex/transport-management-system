<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>PopUp forms</title>
</head>
<body onload="displayLine('renderLineId'); displayAddressForm();">
    <c:set var="countryService" value="${applicationContext.getBean('CountryService')}" />
	<c:set var="localityService" value="${applicationContext.getBean('LocalityService')}" />
	<c:set var="addressService" value="${applicationContext.getBean('AddressService')}" />
	<c:set var="lineService" value="${applicationContext.getBean('LineService')}"/>
<!-- NEW COUNTRY DESIGN FORM BEGINNING -->
	<div class="separate_window" id="newCountryPopup">
		<h4>New Country Form</h4>
		<form id="newCountry" action="${pageContext.request.contextPath}/" method="post" class="form-container">
			<input type="hidden" name="commandName" id="countryAdd" value="addCountry" />
			<input type="text" name="country" id="country" placeholder="type new country here " value="${param.country}" required />
			<button type="submit">Save</button>
			<br>
			<button type="button" class="cancel" onclick="closeCountryForm()">close</button>
		</form>
		<br>
		<div class="alarm">
			<c:out value="${requestScope.wrongCountry}" />
		</div>
	</div>
	<!-- NEW COUNTRY DESIGN FORM END -->

	<!-- NEW LOCALITY DESIGN FORM BEGINNING -->
	<div class="separate_window" id="newLocalityPopup">
		<h4>New Locality Form</h4>
		<form id="newLocality" action="${pageContext.request.contextPath}/" method="post" class="form-container">
			<input type="hidden" name="commandName" value="addLocality" /> 
			<select name="localityCountry" id="localityCountry" required>
				<option disabled selected>select country</option>
				<c:forEach var="country" items="${countryService.getAll()}">
					<option value="${country.id}" ${param.localityCountry == country.id ? 'selected' : ''}><c:out value="${country.name}" /></option>
				</c:forEach>
			</select> 
			<input type="text" name="locality" id="locality" placeholder="type new locality here " required />
			<button type="submit">Save</button>
			<br>
			<div class="alarm">
				<c:out value="${requestScope.wrongLocalityCountry}" />
				<c:out value="${requestScope.wrongLocality}" />
			</div>
			<button type="button" class="cancel" onclick="closeLocalityForm()">close</button>
		</form>
	</div>
	<!-- NEW LOCALITY DESIGN FORM END -->

	<!-- NEW ADDRESS DESIGN FORM BEGINNING -->
	<div class="separate_window" id="newAddressPopup">
		<h4>New Address Form</h4>
		<form id="newAddress" action="${pageContext.request.contextPath}/" method="post" class="form-container">
			<input type="hidden" name="commandName" value="addAddress" /> 
			<select name="addressCountry" id="newAddressCountry" required onchange="specifyLocalitiesSet();">
				<option disabled selected>select country</option>
				<c:forEach var="country" items="${countryService.getAll()}">
					<option value="${country.id}" ${param.addressCountry eq country.id ? 'selected' : ''}><c:out value="${country.name}" /></option>
				</c:forEach>
			</select> 
			<select name="addressLocality" id="addressLocality" required>
				<option disabled selected>select locality</option>
				<c:forEach var="locality" items="${localityService.getByCountry(param.addressCountry)}">
					<option value="${locality.id}" ${param.addressLocality == locality.id ? 'selected':''}><c:out value="${locality.name}" /></option>
				</c:forEach>
			</select> 
			<input type="text" name="address" id="address" placeholder="type new address here " value="${param.address}" required />
			<button type="submit">Save</button>
		<br>
		<div class="alarm">
			<c:out value="${requestScope.wrongAddressCountry}" /><br>
			<c:out value="${requestScope.wrongAddressLocality}" /><br>
			<c:out value="${requestScope.wrongAddress}" />
			<c:out value="${requestScope.address}" />
		</div>
		<button type="button" class="cancel" onclick="closeAddressForm()">close</button>
		</form>
	</div>
	<!-- NEW ADDRESS DESIGN FORM END -->

	<!-- LINE RENDER POPUP BEGINNING -->
	<div class="separate_window" id="lineRenderPopup">
		<c:set var="lineToRender" value="${lineService.get(param.renderLineId)}" />
		<h4>LINE <c:out value="${lineToRender.id}" /></h4>
		Place of loading:
		<c:out value="${lineToRender.route.start.details.concat(', ').concat(lineToRender.route.start.locality.name).concat(', ').concat(lineToRender.route.start.locality.country.name)}" />
		<br> Place of delivery:
		<c:out value="${lineToRender.route.destination.details.concat(', ').concat(lineToRender.route.destination.locality.name).concat(', ').concat(lineToRender.route.destination.locality.country.name)}" />
		<br> Goods to transport: <c:out value="${lineToRender.truckload.name}" />
		<br> Required truck type: <c:out value="${lineToRender.truckload.truckType.toSqlValue()}" />
		<br> Required transport conditions: <c:out value="${lineToRender.route.conditions.toString()}" />
		<br> Goods weight: <c:out value="${lineToRender.truckload.weight}" />
		<br> Loading items: <c:out value="${lineToRender.truckload.loadUnits}" />
		<br> <button type="button" class="cancel" onclick="closeLineRenderPopup()">close</button>
	</div>
	<!-- LINE RENDER POPUP END -->
</body>
</html>