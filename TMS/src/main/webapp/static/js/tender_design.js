function newTenderFormFieldsUpload() {
	var s = document.getElementById("newTenderForm").action;
	document.getElementById("newTenderForm").action = s + "tenders/create";
	document.getElementById("newTenderForm").submit();
}

function specifyLocalitiesSet() {
	var s = document.getElementById("lineDefinition").action;
	document.getElementById("lineDefinition").action = s + "tenders/create";
	document.getElementById("addressCountry").value = document.getElementById("newAddressCountry").value;
	document.getElementById("lineDefinition").submit();
}

function filterLines() {
	document.getElementById("lineDefinitionCommand").value = "filterLines";
	document.getElementById("lineDefinition").submit();
}

function filterSelectedLines() {
	document.getElementById("selectedLinesForm").submit();
}

function resetSelectedLinesFilter() {
	document.getElementById("selectedLinesCommandName").value = "resetLinesFilter";
	document.getElementById("selectedLinesForm").submit();
}

function resetFilter() {
	document.getElementById("lineDefinitionCommand").value = "resetLinesFilter";
	document.getElementById("lineDefinition").submit();
}

function openCountryForm() {
	document.getElementById("newCountryPopup").style.display = "block";
}

function closeCountryForm() {
	document.getElementById("newCountryPopup").style.display = "none";
}

function openLocalityForm() {
	document.getElementById("newLocalityPopup").style.display = "block";
}

function closeLocalityForm() {
	document.getElementById("newLocalityPopup").style.display = "none";
}

function openAddressForm() {
	document.getElementById("newAddressPopup").style.display = "block";
}

function closeAddressForm() {
	document.getElementById("newAddressPopup").style.display = "none";
}

function closeLineRenderPopup() {
	document.getElementById("lineRenderPopup").style.display = "none";
}

function openLineRenderPopup(id) {
	var s = document.getElementById("newTenderForm").action;
	document.getElementById("lineDefinition").action = s + "tenders/create";
	document.getElementById("renderLineId").value = id;
	document.getElementById("lineDefinition").submit();
}

function displayLine(name) {
	console.log('displayLine(name) is invoked');
	if (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]+)')
			.test(location.search))
		document.getElementById("lineRenderPopup").style.display = "block";
}

function displayTroubleshootingAddressForm() {
	if (document.getElementById("address").value.length > 1) {
		document.getElementById("newAddressPopup").style.display = "block";
	}

}

function displayTroubleshootingCountryForm() {
	if (document.getElementById("country").value.length > 1) {
		document.getElementById("newCountryPopup").style.display = "block";
	}
}

function displayTroubleshootingLocalityForm() {
	if (document.getElementById("locality").value.length > 1) {
		document.getElementById("newLocalityPopup").style.display = "block";
	}
}

function displayAddressForm(name) {
	console.log('displayAddressForm(' + name + ') is invoked');
	var s = document.getElementById(name).value;
	if (s.length > 0 && s != "select country")
		document.getElementById("newAddressPopup").style.display = "block";
}

function sortById() {
	document.getElementById("newSortArg").value = "id";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByStartCountry() {
	document.getElementById("newSortArg").value = "startCountry";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByStartCity() {
	document.getElementById("newSortArg").value = "startLocality";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}
function sortByEndCountry() {
	document.getElementById("newSortArg").value = "endCountry";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}
function sortByEndCity() {
	document.getElementById("newSortArg").value = "endLocality";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}
function sortByConditions() {
	document.getElementById("newSortArg").value = "conditions";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}
function sortByLoad() {
	document.getElementById("newSortArg").value = "load";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}
function sortByWeight() {
	document.getElementById("newSortArg").value = "weight";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}
function sortByLoadUnits() {
	document.getElementById("newSortArg").value = "loadUnits";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineSelectionForm").submit();
}
function sortByTruckType() {
	document.getElementById("newSortArg").value = "truckType";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function selectForNewTender() {
	document.getElementById("lineDefinitionCommand").value = "tenderIncludeLine";
	document.getElementById("lineDefinition").submit();
}

/*-=-*/

function selectedSortById() {
	document.getElementById("selectedSortArg").value = "id";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}

function selectedSortByStartCountry() {
	document.getElementById("selectedSortArg").value = "startCountry";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}

function selectedSortByStartCity() {
	document.getElementById("selectedSortArg").value = "startLocality";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}
function selectedSortByEndCountry() {
	document.getElementById("selectedSortArg").value = "endCountry";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}
function selectedSortByEndCity() {
	document.getElementById("selectedSortArg").value = "endLocality";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}
function selectedSortByConditions() {
	document.getElementById("selectedSortArg").value = "conditions";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}
function selectedSortByLoad() {
	document.getElementById("selectedSortArg").value = "load";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}
function selectedSortByWeight() {
	document.getElementById("selectedSortArg").value = "weight";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}
function selectedSortByLoadUnits() {
	document.getElementById("selectedSortArg").value = "loadUnits";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}
function selectedSortByTruckType() {
	document.getElementById("selectedSortArg").value = "truckType";
	document.getElementById("selectedLinesCommandName").value = "sortLines";
	document.getElementById("selectedLinesForm").submit();
}

function removeFromSelectedLines(){
	document.getElementById("selectedLinesCommandName").value="removeNewTenderLine";
	document.getElementById("selectedLinesForm").submit();
	
}