function sortById(){
	document.getElementById("comparingParameter").value = "id";
	document.getElementById("commandName").value = "sortTenders";
	document.getElementById("tenders").submit();
}

function sortByTitle(){
	document.getElementById("comparingParameter").value = "title";
	document.getElementById("commandName").value = "sortTenders";
	document.getElementById("tenders").submit();
}

function sortByDate(){
	document.getElementById("comparingParameter").value = "set_date";
	document.getElementById("commandName").value = "sortTenders";
	document.getElementById("tenders").submit();
}

function sortByTenderStart(){
	document.getElementById("comparingParameter").value = "tender_start";
	document.getElementById("commandName").value = "sortTenders";
	document.getElementById("tenders").submit();
}

function filter(){
	document.getElementById("commandName").value = "tendersFilter";
	document.getElementById("tenders").submit();
}

function dropFilter(){
	document.getElementById("commandName").value = "resetTendersFilter";
	document.getElementById("tenders").submit();
}

function sortLinesById() {
	document.getElementById("lineSortArg").value = "id";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByStartCountry() {
	document.getElementById("lineSortArg").value = "startCountry";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByStartCity() {
	document.getElementById("lineSortArg").value = "startLocality";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByEndCountry() {
	document.getElementById("lineSortArg").value = "endCountry";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByEndCity() {
	document.getElementById("lineSortArg").value = "endLocality";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByConditions() {
	document.getElementById("lineSortArg").value = "conditions";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByLoad() {
	document.getElementById("lineSortArg").value = "load";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByWeight() {
	document.getElementById("lineSortArg").value = "weight";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByLoadUnits() {
	document.getElementById("lineSortArg").value = "loadUnits";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function sortByTruckType() {
	document.getElementById("lineSortArg").value = "truckType";
	document.getElementById("lineDefinitionCommand").value = "sortLines";
	document.getElementById("lineDefinition").submit();
}

function filterLines() {
	document.getElementById("lineDefinitionCommand").value = "filterLines";
	document.getElementById("lineDefinition").submit();
}

function resetFilter() {
	document.getElementById("lineDefinitionCommand").value = "resetLinesFilter";
	document.getElementById("lineDefinition").submit();
}

function applyForNewShipment(){
	document.getElementById("commandName").value = "applyTender";
	document.getElementById("tenders").submit();
}

function createLine(){
	document.getElementById("lineDefinitionCommand").value = "saveNewLine";
	document.getElementById("lineDefinition").submit();
}

function selectForNewShipment(){
	var s = document.getElementById("lineDefinition").action;
	document.getElementById("lineDefinition").action = s + "shipments/create";
	document.getElementById("lineDefinition").submit();
}

function cancelTenderSelection(){
	var s = document.getElementById("tenders").action;
	document.getElementById("tenders").action = s + "shipments/create";
	document.getElementById("commandName").value = "dropTender";
	document.getElementById("tenders").submit();
}

function dropSelectedLine(){
	var s = document.getElementById("lineDefinition").action;
	document.getElementById("lineDefinition").action = s + "shipments/create";
	document.getElementById("lineDefinitionCommand").value = "dropLine";
	document.getElementById("lineDefinition").submit();
}

function shimpentSubmit(){
	document.getElementById("commandName").value="saveNewShipment";
	document.getElementById("tenders").submit();	
}

function openCountryForm() {
	document.getElementById("newCountryPopup").style.display = "block";
}

function closeCountryForm() {
	document.getElementById("newCountryPopup").style.display = "none";
}

function specifyLocalitiesSet() {
	var s = document.getElementById("lineDefinition").action;
	document.getElementById("lineDefinition").action = s + "shipments/create";
	document.getElementById("addressCountry").value = document.getElementById("newAddressCountry").value;
	document.getElementById("lineDefinition").submit();
}

function openLocalityForm() {
	document.getElementById("newLocalityPopup").style.display = "block";
}

function closeLocalityForm() {
	document.getElementById("newLocalityPopup").style.display = "none";
}

function openAddressForm() {
	document.getElementById("newAddressPopup").style.display = "block";
}

function closeAddressForm() {
	document.getElementById("addressCountry").value = 0;
	document.getElementById("newAddressPopup").style.display = "none";
}

function closeLineRenderPopup() {
	document.getElementById("renderLineId").value = 0;
	document.getElementById("lineRenderPopup").style.display = "none";
}

function openLineRenderPopup(id) {
	var s = document.getElementById("lineDefinition").action;
	document.getElementById("lineDefinition").action = s + "shipments/create";
	document.getElementById("renderLineId").value = id;
	document.getElementById("lineDefinition").submit();
}

function displayLine(name) {
	console.log('displayLine('+ name +') is invoked');
	if (document.getElementById("renderLineId").value != 0)
		document.getElementById("lineRenderPopup").style.display = "block";
}

function displayTroubleshootingAddressForm() {
	if (document.getElementById("address").value.length > 1) {
		document.getElementById("newAddressPopup").style.display = "block";
	}
}

function displayTroubleshootingCountryForm() {
	if (document.getElementById("country").value.length > 1) {
		document.getElementById("newCountryPopup").style.display = "block";
	}
}

function displayTroubleshootingLocalityForm() {
	if (document.getElementById("locality").value.length > 1) {
		document.getElementById("newLocalityPopup").style.display = "block";
	}
}

function displayAddressForm() {
	console.log('displayAddressForm is invoked');
	if (document.getElementById("addressCountry").value != 0)
		document.getElementById("newAddressPopup").style.display = "block";
}

function registerNewShipmentTerms(){
	var s = document.getElementById("tenders").action;
	document.getElementById("tenders").action = s + "shipments/create";
	document.getElementById("tenders").submit();
}