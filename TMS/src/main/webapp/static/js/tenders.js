function filter(){
	document.getElementById("command").value='tendersFilter';
	document.getElementById("tenderForm").submit();
}

function sortById(){
	document.getElementById("comparingParameter").value = 'id';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function sortByReleaseDate(){
	document.getElementById("comparingParameter").value = 'release_date';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function sortByTitle(){
	document.getElementById("comparingParameter").value='title';
	document.getElementById("command").value  = "sortTenders";
	document.getElementById("tenderForm").submit();
}

function sortByCustomer(){
	document.getElementById("comparingParameter").value = 'customer';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function sortBySetDate(){
	document.getElementById("comparingParameter").value = 'set_date';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function sortByState(){
	document.getElementById("comparingParameter").value = 'state';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function sortByDeadline(){
	document.getElementById("comparingParameter").value = 'deadline';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function sortByTenderStart(){
	document.getElementById("comparingParameter").value = 'tender_start';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function reviseTenderTsps(){
	document.getElementById("commandName").value='reviseTenderList';
	document.getElementById("tenderReleaseForm").submit();
}

function releaseNewTender(){
	document.getElementById("commandName").value='reviseTenderList';
	document.getElementById("releaseTender").submit();
}

function sortByTenderEnd(){
	document.getElementById("comparingParameter").value = 'tender_end';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function sortByShipmentsQuantity(){
	document.getElementById("comparingParameter").value = 'shipments_quantity';
	document.getElementById("command").value = 'sortTenders';
	document.getElementById("tenderForm").submit();
}

function tendersReset(){
	document.getElementById("command").value = 'resetTendersFilter';
	document.getElementById("tenderForm").submit();
}

