function saveBid() {
	document.getElementById("command_name").value = "saveBid";
	document.getElementById("shipment_form").submit();
}

function removeBid() {
	document.getElementById("command_name").value = "removeBid";
	document.getElementById("shipment_form").submit();
}

function trackingUpdate() {
	document.getElementById("command_name").value = "updateTrackingDetails";
	document.getElementById("shipment_form").submit();
}

function openNewOrderTerms() {
	document.getElementById("orderTermsPopup").style.display = "block";
}

function submitNewOrderTerms() {
	document.getElementById("command_name").value = "reviseOrderTerms";
	document.getElementById("termsRevizePopupFlag").value="sign";
	document.getElementById("termsRevizePopup").submit();
}

function closePopup() {
	document.getElementById("orderTermsPopup").style.display = "none";
}

function troubleshootingDisplayTermsPopup() {
	var popup = '${termsRevizePopupFlag}';
	if (popup.value.length > 1) {
		document.getElementById("orderTermsPopup").style.display = "block";
	}
}
	
function acceptOrder(){
	document.getElementById("command_name").value="acceptOrder";
	document.getElementById("shipment_form").submit();
}

function rejectOrder(){
	document.getElementById("command_name").value="rejectOrder";
	document.getElementById("shipment_form").submit();
}

function assignTenderOrder(){
	document.getElementById("command_name").value="assignTenderOrder";
	document.getElementById("shipment_form").submit();
}

function recallTheOrder(){
	document.getElementById("command_name").value = "orderRecall";
	document.getElementById("shipment_form").submit();
}

function releaseSpotAuction(){
	document.getElementById("command_name").value = "setSpotAuction";
	document.getElementById("shipment_form").submit();
}

function abortSpotAuction(){
	document.getElementById("command_name").value = "cancelSpotAuction";
	document.getElementById("shipment_form").submit();	
}

function assignOverSpot(){
	document.getElementById("command_name").value="orderSpotAssign";
	document.getElementById("shipment_form").submit();
}

function sortByRate(){
	document.getElementById("command_name").value="sortBids";
	document.getElementById("shipment_form").submit();
}

function deleteBid(){
	document.getElementById("command_name").value="deleteBid";
	document.getElementById("shipment_form").submit();
}

function submitBid(){
	document.getElementById("command_name").value="addBid";
	document.getElementById("shipment_form").submit();
}
