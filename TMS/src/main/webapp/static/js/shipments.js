function sortById() {
	document.getElemenyById("comparingParameter").value = 'id';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("shipments_form").submit();
}

function sortByDate() {
	document.getElemenyById("comparingParameter").value = 'date';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("shipments_form").submit();
}

function sortByLoadingDate() {
	document.getElemenyById("comparingParameter").value = 'loadingDate';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("shipments_form").submit();
}

function sortByState() {
	document.getElemenyById("comparingParameter").value = 'state';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("shipments_form").submit();
}

function sortByTruckload() {
	document.getElemenyById("comparingParameter").value = 'truckload';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("shipments_form").submit();
}

function sortByCompany() {
	document.getElemenyById("comparingParameter").value = 'company';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("shipments_form").submit();
}

function sortByScope() {
	document.getElemenyById("comparingParameter").value = 'scope';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("shipments_form").submit();
}

function sortByRoute() {
	document.getElemenyById("comparingParameter").value = 'route';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("shipments_form").submit();
}

function resetOrdersFilter() {
	document.getElementById("command").value = 'resetOrdersFilter';
	document.getElemenyById("shipments_form").submit();
}

function filter() {
	document.getElementById("command").value = 'ordersFilter';
	document.getElementById("shipments_form").submit();
}

function acceptTransportOrder() {
	document.getElementById("command").value = 'acceptOrder';
	document.getElementById("shipments_form").submit();
}

function rejectTransportOrder() {
	document.getElementById("command").value = 'rejectOrder';
	document.getElementById("shipments_form").submit();
}

function recallTransportOrder() {
	document.getElementById("command").value = 'orderRecall';
	document.getElementById("shipments_form").submit();
}

function deleteTransortOrder() {
	document.getElementById("command").value = 'orderDeleteCommand';
	document.getElementById("shipments_form").submit();
}

