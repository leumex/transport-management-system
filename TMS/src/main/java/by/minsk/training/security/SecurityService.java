package by.minsk.training.security;

import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.localization.BundleProvider;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserService;

@Bean
public class SecurityService {

    private UserService userService;
    private BundleProvider bundleProvider;

    public SecurityService(UserService userService, BundleProvider bundleProvider) {
        this.userService = userService;
        this.bundleProvider = bundleProvider;
    }

    private static final Logger logger = LogManager.getLogger(SecurityService.class);
    private Map<String, UserEntity> authorizedUsers = new ConcurrentHashMap<>();

    public boolean logIn(HttpServletRequest req, String login, String password) throws AuthorizedException {
        HttpSession session = req.getSession();
        String currentLanguage = (String) req.getServletContext().getAttribute("language");
        ResourceBundle bundle;
        try {
            bundle = bundleProvider.getBundle(currentLanguage);
        } catch (NullPointerException | MissingResourceException e) {
            throw new AuthorizedException("Failed to define appropriate locale", e);
        }
        if (session.getAttribute("user") != null) {
            req.setAttribute("warning", bundle.getString("warning"));
            return false;
        }
        UserEntity entity = new UserEntity();
        entity.setLogin(login);
        entity.setPassword(password);
        boolean loggedIn = userService.loginUser(entity);
        if (loggedIn & !authorizedUsers.containsValue(entity)) {
        	entity = userService.findUser(login);
        	logger.debug("User " + entity.getName() + ", id = "+ entity.getId() +  " has been logged on");
            entity = userService.findUser(login);
            authorizedUsers.put(session.getId(), entity);
            session.setAttribute("user", entity);
        } else if (loggedIn) {
            session.invalidate();
            throw new AuthorizedException("Someone has already logged in with these credentials!");
        }
        return loggedIn;
    }

    public void logOut(HttpServletRequest req) throws AuthorizedException {
        HttpSession session = req.getSession(false);
        logger.debug(session == null ? "session is null" : "session refers to HttpSession valid instance");
        if (session != null) {
            session.removeAttribute("user");
            authorizedUsers.remove(session.getId());
            session.invalidate();
            logger.debug("SecurityService#logout has successfully completed execution!");
        } else {
            throw new AuthorizedException("No alive session is accompanying the request");
        }
    }

    public UserEntity getCurrentUser(HttpServletRequest req) throws AuthorizedException {
        UserEntity user;
        try {
            String id = req.getSession().getId();
            user = authorizedUsers.get(id);
        } catch (IllegalStateException e) {
            throw new AuthorizedException("No alive session accompanying the request", e);
        }
        return user;
    }
}
