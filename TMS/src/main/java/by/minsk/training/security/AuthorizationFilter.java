package by.minsk.training.security;

import java.io.IOException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.ApplicationContext;
import by.minsk.training.localization.BundleProvider;

@WebFilter("/*")
public class AuthorizationFilter implements Filter {

    private static final Logger logger = LogManager.getLogger(AuthorizationFilter.class);
    private static final String[] freeFromLoginURIes = {"/", "/auth", "/reg"};

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        logger.debug("doFilter method has been invoked..");
        HttpServletRequest req = (HttpServletRequest) request;
        BundleProvider bundleProvider = ApplicationContext.getInstance().getBean(BundleProvider.class);
        String currentLanguage = (String) req.getServletContext().getAttribute("language");
        ResourceBundle bundle;
        try {
            bundle = bundleProvider.getBundle(currentLanguage);
        } catch (NullPointerException | MissingResourceException e) {
            throw new ServletException("Failed to define appropriate locale", e);
        }
        String path = req.getRequestURI().substring(req.getContextPath().length());
        logger.debug("incoming request has path: " + path);
        HttpSession session = req.getSession(false);
        boolean isLoggedIn = (session != null) && (session.getAttribute("user") != null);
        boolean isHomePage = path.equals(freeFromLoginURIes[0]);
        boolean isLogging = path.startsWith(freeFromLoginURIes[1]) || path.startsWith(freeFromLoginURIes[2]);

        if (isLoggedIn || isHomePage || isLogging) {
            chain.doFilter(request, response);
        } else {
            req.setAttribute("notAuthorized", bundle.getString("notAuthorized"));
            req.getRequestDispatcher("/login").forward(req, (HttpServletResponse) response);
        }
    }

    @Override
    public void destroy() {
    }
}
