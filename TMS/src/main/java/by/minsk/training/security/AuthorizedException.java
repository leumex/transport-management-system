package by.minsk.training.security;

public class AuthorizedException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3897534096764530838L;

	public AuthorizedException(String message){
        super(message);
    }

    public AuthorizedException(String message, Exception e){
        super(message, e);
    }
}
