package by.minsk.training;

/*import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
*/
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import by.minsk.training.command.AddressSaveCommand;
import by.minsk.training.command.BidDeleteCommand;
import by.minsk.training.command.BidSaveCommand;
import by.minsk.training.command.BidsSortCommand;
import by.minsk.training.command.CountrySaveCommand;
import by.minsk.training.command.LineSaveCommand;
import by.minsk.training.command.LinesFilterCommand;
import by.minsk.training.command.LinesSortCommand;
import by.minsk.training.command.LocalitySaveCommand;
import by.minsk.training.command.LoginUserCommand;
import by.minsk.training.command.LogoutUserCommand;
import by.minsk.training.command.LookForCompanyCommand;
import by.minsk.training.command.OrderAcceptCommand;
import by.minsk.training.command.OrderAssignOverTenderCommand;
import by.minsk.training.command.OrderRecallCommand;
import by.minsk.training.command.OrderReviseCommand;
import by.minsk.training.command.OrderSaveCommand;
import by.minsk.training.command.OrdersFilterCommand;
import by.minsk.training.command.OrdersSortCommand;
import by.minsk.training.command.RegisterUserSaveCommand;
import by.minsk.training.command.ReleaseTenderCommand;
import by.minsk.training.command.RemoveNewTenderLineCommand;
import by.minsk.training.command.ResetLinesFilterCommand;
import by.minsk.training.command.ResetOrdersFilterCommand;
import by.minsk.training.command.ResetTendersFilterCommand;
import by.minsk.training.command.SelectLineForNewTenderCommand;
import by.minsk.training.command.SpotRecallCommand;
import by.minsk.training.command.SpotReleaseCommand;
import by.minsk.training.command.TenderApplyCommand;
import by.minsk.training.command.TenderSaveCommand;
import by.minsk.training.command.TenderTspListReviseCommand;
import by.minsk.training.command.TendersFilterCommand;
import by.minsk.training.command.TendersSortCommand;
import by.minsk.training.command.pagination.TableDemonstrator;
import by.minsk.training.command.validation.AddressValidator;
import by.minsk.training.command.validation.BidValidator;
import by.minsk.training.command.validation.CountryValidator;
import by.minsk.training.command.validation.LineValidator;
import by.minsk.training.command.validation.LocalityValidator;
import by.minsk.training.command.validation.NewTenderLinePullOutValidator;
import by.minsk.training.command.validation.NewTenderLineSelectValidator;
import by.minsk.training.command.validation.OrderAcceptanceValidator;
import by.minsk.training.command.validation.OrderRecallValidator;
import by.minsk.training.command.validation.OrderReviseTermsValidator;
import by.minsk.training.command.validation.OrderSubmitValidator;
import by.minsk.training.command.validation.OrdersFilterValidator;
import by.minsk.training.command.validation.RegValidator;
import by.minsk.training.command.validation.TenderOrderAssignValidator;
import by.minsk.training.command.validation.TenderValidator;
import by.minsk.training.core.BeanRegistry;
import by.minsk.training.core.BeanRegistryImpl;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.dao.ConnectionManagerImpl;
import by.minsk.training.dao.DataSource;
import by.minsk.training.dao.DataSourceImpl;
import by.minsk.training.dao.TransactionInterceptor;
import by.minsk.training.dao.TransactionManager;
import by.minsk.training.dao.TransactionManagerImpl;
import by.minsk.training.dao.basic.AddressDAOImpl;
import by.minsk.training.dao.basic.BidDAOImpl;
import by.minsk.training.dao.basic.ContractDAOImpl;
import by.minsk.training.dao.basic.CountryDAOImpl;
import by.minsk.training.dao.basic.LineDAOImpl;
import by.minsk.training.dao.basic.LocalityDAOImpl;
import by.minsk.training.dao.basic.RouteDAOImpl;
import by.minsk.training.dao.basic.SpotDAOImpl;
import by.minsk.training.dao.basic.TenderDAOImpl;
import by.minsk.training.dao.basic.TenderLineDAOImpl;
import by.minsk.training.dao.basic.TenderLineQueryDAOImpl;
import by.minsk.training.dao.basic.TenderParticipationDAOImpl;
import by.minsk.training.dao.basic.TransportOrderDAOImpl;
import by.minsk.training.dao.basic.TruckloadDAOImpl;
import by.minsk.training.dao.basic.TspPerformanceDAOImpl;
import by.minsk.training.localization.BundleProvider;
import by.minsk.training.security.SecurityService;
import by.minsk.training.service.AddressServiceImpl;
import by.minsk.training.service.BidsServiceImpl;
import by.minsk.training.service.CountryServiceImpl;
import by.minsk.training.service.LineServiceImpl;
import by.minsk.training.service.LocalityServiceImpl;
import by.minsk.training.service.RouteServiceImpl;
import by.minsk.training.service.SpotOrderServiceImpl;
import by.minsk.training.service.TenderParticipationServiceImpl;
import by.minsk.training.service.TenderServiceImpl;
import by.minsk.training.service.TransportOrderServiceImpl;
import by.minsk.training.service.TruckloadServiceImpl;
import by.minsk.training.service.TspPerformanceServiceImpl;
import by.minsk.training.service.notification.NotificationScanner;
import by.minsk.training.service.notification.TenderNotificationsExtractor;
import by.minsk.training.service.notification.TransportOrderNotificationsExtractor;
import by.minsk.training.user.UserDaoImpl;
import by.minsk.training.user.UserServiceImpl;

public class ApplicationContext implements BeanRegistry {
    private final static AtomicBoolean INITIALIZED = new AtomicBoolean(false);
    private final static Lock INITIALIZE_LOCK = new ReentrantLock();
	/*
	 * private static final Logger logger =
	 * LogManager.getLogger(ApplicationContext.class);
	 */
    private static ApplicationContext INSTANCE;

    private BeanRegistry beanRegistry = new BeanRegistryImpl();

    private ApplicationContext() {
    }

    public static void initialize() {
        INITIALIZE_LOCK.lock();
        try {
            if (INSTANCE != null && INITIALIZED.get()) {
                throw new IllegalStateException("Context has been already initialized");
            } else {
                ApplicationContext context = new ApplicationContext();
                context.init();
                INSTANCE = context;
                INITIALIZED.set(true);
            }
        } finally {
            INITIALIZE_LOCK.unlock();
        }
    }

    public static ApplicationContext getInstance() {
        if (INSTANCE != null && INITIALIZED.get()) {
            return INSTANCE;
        } else {
            throw new IllegalStateException("Context has not been yet initialized!");
        }
    }

    @Override
    public <T> void registerBean(T bean) {
        this.beanRegistry.registerBean(bean);
    }

    @Override
    public <T> void registerBean(Class<T> beanClass) {
        this.beanRegistry.registerBean(beanClass);
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        return this.beanRegistry.getBean(beanClass);
    }

    @Override
    public <T> T getBean(String name) {
        return this.beanRegistry.getBean(name);
    }

    @Override
    public <T> boolean removeBean(T bean) {
        return this.beanRegistry.removeBean(bean);
    }

    @Override
    public void destroy() {
        DataSource dataSource = this.getBean(DataSource.class);
        dataSource.close();
        beanRegistry.destroy();
        INITIALIZED.set(false);
    }

    private void init() {
        registerDataSource();
        registerClasses();
    }

    private void registerDataSource() {
        DataSource dataSource = DataSourceImpl.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(dataSource);
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager, dataSource);
        TransactionInterceptor transactionInterceptor = new TransactionInterceptor(transactionManager);
        registerBean(dataSource);
        registerBean(transactionManager);
        registerBean(connectionManager);
        registerBean(transactionInterceptor);
    }

    private void registerClasses() {
        registerBean(UserDaoImpl.class);
        registerBean(UserServiceImpl.class);
        registerBean(RegValidator.class);
        registerBean(RegisterUserSaveCommand.class);
        registerBean(LoginUserCommand.class);
        registerBean(SecurityService.class);
        registerBean(LogoutUserCommand.class);
        registerBean(BundleProvider.class);
        registerBean(CountryDAOImpl.class);
        registerBean(AddressDAOImpl.class);
        registerBean(LocalityDAOImpl.class);
        registerBean(RouteDAOImpl.class);
        registerBean(LineDAOImpl.class);
        registerBean(TruckloadDAOImpl.class);
        registerBean(TransportOrderDAOImpl.class);
        registerBean(TenderDAOImpl.class);
        registerBean(TableDemonstrator.class);
        registerBean(BidDAOImpl.class);
        registerBean(ContractDAOImpl.class);
        registerBean(SpotDAOImpl.class);
        registerBean(TenderLineDAOImpl.class);
        registerBean(TenderLineQueryDAOImpl.class);
        registerBean(TenderParticipationDAOImpl.class);
        registerBean(TspPerformanceDAOImpl.class);
        registerBean(BidsServiceImpl.class);
        registerBean(SpotOrderServiceImpl.class);
        registerBean(TransportOrderServiceImpl.class);
        registerBean(LineServiceImpl.class);
        registerBean(LineValidator.class);
        registerBean(TenderServiceImpl.class);
        registerBean(RouteServiceImpl.class);
        registerBean(TspPerformanceServiceImpl.class);
        registerBean(OrdersSortCommand.class);
        registerBean(OrdersFilterCommand.class);
        registerBean(OrderRecallValidator.class);
        registerBean(OrderRecallCommand.class);
        registerBean(ResetOrdersFilterCommand.class);
        registerBean(TendersSortCommand.class);
        registerBean(TendersFilterCommand.class);
        registerBean(ResetTendersFilterCommand.class);   
        registerBean(CountryServiceImpl.class);
        registerBean(AddressServiceImpl.class);
        registerBean(LocalityServiceImpl.class);
        registerBean(TruckloadServiceImpl.class);
        registerBean(LinesFilterCommand.class);
        registerBean(AddressSaveCommand.class);
        registerBean(LocalitySaveCommand.class); 
        registerBean(CountrySaveCommand.class);
        registerBean(AddressValidator.class);
        registerBean(CountryValidator.class);
        registerBean(LocalityValidator.class);
        registerBean(TenderValidator.class);
        registerBean(LineSaveCommand.class);
        registerBean(ResetLinesFilterCommand.class);
        registerBean(NewTenderLineSelectValidator.class);
        registerBean(SelectLineForNewTenderCommand.class);
        registerBean(NewTenderLinePullOutValidator.class);
        registerBean(RemoveNewTenderLineCommand.class);
        registerBean(LinesSortCommand.class);
        registerBean(TenderSaveCommand.class);
        registerBean(TenderParticipationServiceImpl.class);
        registerBean(TenderTspListReviseCommand.class);
        registerBean(ReleaseTenderCommand.class);
        registerBean(LookForCompanyCommand.class);
        registerBean(OrdersFilterValidator.class);
        registerBean(OrderAcceptCommand.class);
        registerBean(OrderAcceptanceValidator.class);
        registerBean(OrderReviseTermsValidator.class);
        registerBean(OrderReviseCommand.class);
        registerBean(TenderOrderAssignValidator.class);
        registerBean(OrderAssignOverTenderCommand.class);
        registerBean(SpotReleaseCommand.class);
        registerBean(SpotRecallCommand.class);
        registerBean(BidsSortCommand.class);
        registerBean(BidDeleteCommand.class); 
        registerBean(BidSaveCommand.class);
        registerBean(BidValidator.class);
        registerBean(NotificationScanner.class);
        registerBean(TenderNotificationsExtractor.class);
        registerBean(TransportOrderNotificationsExtractor.class);
        registerBean(OrderSubmitValidator.class);
        registerBean(OrderSaveCommand.class);
        registerBean(TenderApplyCommand.class);
    }
}
