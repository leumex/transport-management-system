package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Country;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class CountryDAOImpl implements CountryDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into country (name) values(?);";
	private static final String UPDATE_QUERY = "update country set name = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from country where id = ?;";
	private static final String ID_SELECT_QUERY = "select name from country where id = ?;";
	private static final String ALL_SELECT_QUERY = "select id, name from country;";

	@Override
	public Long save(Country entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setString(1, entity.getName());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(Country entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setString(1, entity.getName());
			updateStatement.setLong(2, entity.getId());
			int rowCount = updateStatement.executeUpdate();
			return rowCount > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(Country entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			int rowCount = deleteStatement.executeUpdate();
			return rowCount > 0 ? true : false;
		}
	}

	@Override
	public Country getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			Country country = new Country();
			country.setId(id);
			while (rs.next()) {
				country.setName(rs.getString(1));
			}
			rs.close();
			return country;
		}
	}

	@Override
	public Collection<Country> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			Collection<Country> collection = new HashSet<>();
			while (rs.next()) {
				Country country = new Country();
				country.setId(rs.getLong(1));
				country.setName(rs.getString(2));
				collection.add(country);
			}
			rs.close();
			return collection;
		}
	}
}
