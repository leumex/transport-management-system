package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Locality;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class AddressDAOImpl implements AddressDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into address (locality_id, details) values (?, ?);";
	private static final String UPDATE_QUERY = "update address state locality_id = ?, details = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from address where id = ?;";
	private static final String ID_SELECT_QUERY = "select a.details, a.locality_id, l.name, l.country_id, c.name "
			+ "from address a inner join locality l on l.id = a.locality_id "
			+ "inner join country c on c.id = l.country_id where a.id = ?;";
	private static final String ALL_SELECT_QUERY = "select a.id, a.details, a.locality_id, l.name, l.country_id, c.name "
			+ "from address a inner join locality l on l.id = a.locality_id "
			+ "inner join country c on c.id = l.country_id;";
	private static final String SELECT_BY_LOCALITY = "select a.id, a.details, a.locality_id, l.name, l.country_id, c.name "
			+ "from address a inner join locality l on l.id = a.locality_id "
			+ "inner join country c on c.id = l.country_id where a.locality_id = ?;";

	@Override
	public Set<Address> findByLocalityId(Long localityId) throws SQLException {
		if (localityId == null) {
			return null;
		}
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(SELECT_BY_LOCALITY)) {
			selectStatement.setLong(1, localityId);
			ResultSet rs = selectStatement.executeQuery();
			Set<Address> addresses = new HashSet<>();
			while (rs.next()) {
				Address address = new Address();
				address.setId(rs.getLong(1));
				address.setDetails(rs.getString(2));
				Locality locality = new Locality();
				locality.setId(rs.getLong(3));
				locality.setName(rs.getString(4));
				Country country = new Country();
				country.setId(rs.getLong(5));
				country.setName(rs.getString(6));
				locality.setCountry(country);
				address.setLocality(locality);
				addresses.add(address);
			}
			return addresses;
		}
	}

	@Override
	public Long save(Address entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setLong(1, entity.getLocality().getId());
			insertStatement.setString(2, entity.getDetails());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			entity.setId(id);
			rs.close();
			return id;
		}
	}

	@Override
	public boolean update(Address entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setLong(1, entity.getLocality().getId());
			updateStatement.setString(2, entity.getDetails());
			updateStatement.setLong(3, entity.getId());
			int rowCount = updateStatement.executeUpdate();
			return rowCount > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(Address entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			int rowCount = deleteStatement.executeUpdate();
			return rowCount > 0 ? true : false;
		}
	}

	@Override
	public Address getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			if (!rs.next()) {
				return null;
			}
			Address address = new Address();
			int count = 0;
			do {
				int i = 1;
				address.setId(id);
				address.setDetails(rs.getString(i++));
				Locality locality = new Locality();
				locality.setId(rs.getLong(i++));
				locality.setName(rs.getString(i++));
				Country country = new Country();
				country.setId(rs.getLong(i++));
				country.setName(rs.getString(i++));
				locality.setCountry(country);
				address.setLocality(locality);
				count++;
			} while (rs.next());
			rs.close();
			if (count > 1) {
				throw new SQLException("There's more than 1 Address record with given id");
			}
			return address;
		}
	}

	@Override
	public Collection<Address> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			Set<Address> list = new HashSet<>();
			ResultSet rs = selectStatement.executeQuery();
			while (rs.next()) {
				int i = 1;
				Address address = new Address();
				address.setId(rs.getLong(i++));
				address.setDetails(rs.getString(i++));
				Locality locality = new Locality();
				locality.setId(rs.getLong(i++));
				locality.setName(rs.getString(i++));
				Country country = new Country();
				country.setId(rs.getLong(i++));
				country.setName(rs.getString(i++));
				locality.setCountry(country);
				address.setLocality(locality);
				list.add(address);
			}
			rs.close();
			return list;
		}
	}
}
