package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLineQuery;

public interface TenderLineQueryDAO extends CRUDDao<TenderLineQuery, Long> {

	Set<TenderLineQuery> getByTender(Tender tender) throws SQLException;
	
	TenderLineQuery getTenderLineQuery (long tenderId, long lineId, long tspId) throws SQLException;

}
