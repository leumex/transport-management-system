package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.SpotTransportOrder;

public interface SpotDAO extends CRUDDao<SpotTransportOrder, Long> {
	
	Set<SpotTransportOrder> getTspSpotExecutedOrders (long tspId) throws SQLException;

	SpotTransportOrder getByOrder(long transportOrderId) throws SQLException;
	
	/* returns a set of ongoing spot transport orders, where 
	 * -customer is contracted with the tsp
	 * -bound transportOrder order has state ANNOUNCED
	 * -the tsp has no out of tender assignments of the bound order
	 * */
	Set<SpotTransportOrder> getTspOpenSpotOrders (long tspId) throws SQLException;

}
