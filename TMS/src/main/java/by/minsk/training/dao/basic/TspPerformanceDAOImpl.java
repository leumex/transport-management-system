package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Locality;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.Truckload;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.entity.TspPerformance.OrderFootprint;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TspPerformanceDAOImpl implements TspPerformanceDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into tsp_performance (left_unloading, left_loading, arrived_for_unloading, "
			+ "arrived_for_loading, truck_plates, response_time, comment, notified, tsp_response, transport_order_id, tsp_id values (?,?,?,?,?,?,?,?,?,?,?);";
	private static final String UPDATE_QUERY = "update tsp_performance set left_unloading = ?, left_loading = ?, arrived_for_unloading = ?, arrived_for_loading = ?, "
			+ "truck_plates = ?, response_time = ?, comment = ?, notified = ?, tsp_response = ?, transport_order_id = ?, tsp_id = ?, order_state = ? where shipment_id = ?;";
	private static final String DELETE_QUERY = "delete  from tsp_performance where shipment_id = ?;";
	private static final String ID_SELECT_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, tpf.truck_plates, "
			+ "tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, tpf.order_delivery_term, "
			+ "tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, ld.name, ld.weight, ld.load_units, "
			+ "ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tpf.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tpf.order_customer_id = cus.id left join tender tdr on tdr.id = tpf.order_tender_id "
			+ "left join user tcus on tdr.customer_id = tcus.id " + "union "
			+ "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, tpf.truck_plates, tpf.arrived_for_loading, "
			+ "tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, tpf.order_delivery_term, tpf.order_line_id, "
			+ "tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tspu.id, tspu.role, tspu.name, tspu.registration_date, tro.id, tro.issued, "
			+ "tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, "
			+ "rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, "
			+ "ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id inner join tender tdr on tdr.id = tro.tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where tpf.shipment_id = ?;";
	private static final String ALL_SELECT_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, tpf.truck_plates, "
			+ "tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, tpf.order_delivery_term, "
			+ "tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, ld.name, ld.weight, ld.load_units, "
			+ "ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tpf.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tpf.order_customer_id = cus.id left join tender tdr on tdr.id = tpf.order_tender_id "
			+ "left join user tcus on tdr.customer_id = tcus.id " + "union "
			+ "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, tpf.truck_plates, tpf.arrived_for_loading, "
			+ "tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, tpf.order_delivery_term, tpf.order_line_id, "
			+ "tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tspu.id, tspu.role, tspu.name, tspu.registration_date, tro.id, tro.issued, "
			+ "tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, "
			+ "rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, "
			+ "ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tpf.order_customer_id = cus.id inner join tender tdr on tdr.id = tpf.order_tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id;";
	private static final String CUSTOMER_SELECT_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, "
			+ "tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, "
			+ "tpf.order_delivery_term, tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, "
			+ "tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, "
			+ "ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, "
			+ "ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, "
			+ "tdr.id, tdr.title, tdr.set_date, tdr.release_date, tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, "
			+ "tdr.state, tcus.id, tcus.role, tcus.name, tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tpf.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id inner join address start on rt.departure = start.id "
			+ "inner join locality lcl1 on start.locality_id = lcl1.id inner join country ctr1 on lcl1.country_id = ctr1.id "
			+ "inner join address finish on rt.destination = finish.id inner join locality lcl2 on finish.locality_id = lcl2.id "
			+ "inner join country ctr2 on ctr2.id = lcl2.country_id inner join user cus on tpf.order_customer_id = cus.id "
			+ "left join tender tdr on tdr.id = tpf.order_tender_id left join user tcus on tdr.customer_id = tcus.id "
			+ "where tpf.order_customer_id = ? or tdr.customer_id = ? "
			+ "union select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, tpf.truck_plates, tpf.arrived_for_loading, "
			+ "tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, tpf.order_delivery_term, tpf.order_line_id, "
			+ "tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, "
			+ "tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, "
			+ "rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, "
			+ "ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tpf.order_customer_id = cus.id inner join tender tdr on tdr.id = tpf.order_tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where tpf.order_customer_id = ? or tdr.customer_id = ?;";
	private static final String ORDER_SELECT_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, "
			+ "tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, "
			+ "tpf.order_delivery_term, tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tspu.id, tspu.role, "
			+ "tspu.name, tspu.registration_date, tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, "
			+ "ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, "
			+ "ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, "
			+ "tdr.id, tdr.title, tdr.set_date, tdr.release_date, tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, "
			+ "tdr.state, tcus.id, tcus.role, tcus.name, tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tpf.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tpf.order_customer_id = cus.id left join tender tdr on tdr.id = tpf.order_tender_id "
			+ "left join user tcus on tdr.customer_id = tcus.id where tro.id = ? " + "union "
			+ "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, "
			+ "tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, "
			+ "tpf.order_delivery_term, tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tspu.id, tspu.role, "
			+ "tspu.name, tspu.registration_date, tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, "
			+ "ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, "
			+ "ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, "
			+ "tdr.id, tdr.title, tdr.set_date, tdr.release_date, tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, "
			+ "tdr.state, tcus.id, tcus.role, tcus.name, tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id inner join tender tdr on tdr.id = tro.tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where tro.id = ?;";
	private static final String TSP_SELECT_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, tpf.truck_plates, "
			+ "tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, tpf.order_delivery_term, "
			+ "tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, ld.name, ld.weight, ld.load_units, "
			+ "ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tpf.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tpf.order_customer_id = cus.id left join tender tdr on tdr.id = tpf.order_tender_id "
			+ "left join user tcus on tdr.customer_id = tcus.id  where tspu.id = ? " + "union "
			+ "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, tpf.truck_plates, tpf.arrived_for_loading, "
			+ "tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, tpf.order_delivery_term, tpf.order_line_id, "
			+ "tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tspu.id, tspu.role, tspu.name, tspu.registration_date, tro.id, tro.issued, "
			+ "tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, "
			+ "rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, "
			+ "ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id inner join tender tdr on tdr.id = tro.tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where tspu.id = ?;";
	private static final String TSP_ACCEPTED_ASSIGNMENTS_SELECT_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, "
			+ "tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, "
			+ "tpf.order_delivery_term, tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, ld.name, ld.weight, ld.load_units, "
			+ "ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, "
			+ "lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tpf.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tpf.order_customer_id = cus.id left join tender tdr on tdr.id = tpf.order_tender_id "
			+ "left join user tcus on tdr.customer_id = tcus.id where tpf.tsp_id = ? and ((tpf.order_state in ('ACCEPTED', 'IN_ACTION', 'COMPLETED') "
			+ "and tpf.tsp_response = 'ACCEPTED') or (tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')) or "
			+ "(tpf.order_state = 'WITHDRAWN' and tpf.tsp_response = 'ACCEPTED')) "
			+ "union select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, tpf.truck_plates, tpf.arrived_for_loading, "
			+ "tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, tpf.order_delivery_term, tpf.order_line_id, "
			+ "tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tspu.id, tspu.role, tspu.name, tspu.registration_date, tro.id, tro.issued, "
			+ "tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, "
			+ "rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, "
			+ "ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id inner join tender tdr on tdr.id = tro.tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where tpf.tsp_id = ? and ((tpf.order_state in ('ACCEPTED', 'IN_ACTION', 'COMPLETED') "
			+ "and tpf.tsp_response = 'ACCEPTED') or (tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')) or "
			+ "(tpf.order_state = 'WITHDRAWN' and tpf.tsp_response = 'ACCEPTED'));";
	private static final String TSP_SPOT_WON_SELECT_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, "
			+ "tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, "
			+ "tpf.order_delivery_term, tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tspu.id, tspu.role, "
			+ "tspu.name, tspu.registration_date, tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, "
			+ "ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, "
			+ "finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date "
			+ "from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id inner join transport_order tro on tpf.transport_order_id = tro.id "
			+ "inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id inner join spot_orders sp on sp.transport_order_id = tro.id "
			+ "inner join bids b on b.spot_order_id = sp.id where b.made_at <= tpf.notified and tpf.order_tender_id = null and "
			+ "tspu.id = b.tsp_id and tspu.id = ?;";
	private static final String TSP_SPOT_EXECUTED_SELECT_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, "
			+ "tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tspu.id, tspu.role, tspu.name, "
			+ "tspu.registration_date, tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, "
			+ "ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date from tsp_performance tpf "
			+ "inner join user tspu on tspu.id = tpf.tsp_id inner join transport_order tro on tpf.transport_order_id = tro.id "
			+ "inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id inner join spot_orders sp on sp.transport_order_id = tro.id "
			+ "inner join bids b on b.spot_order_id = sp.id where b.made_at <= tpf.notified and tro.tender_id = null and tspu.id = ? "
			+ "and ((tpf.order_state in ('IN_ACTION', 'COMPLETED') and tpf.tsp_response = 'ACCEPTED') "
			+ "or (tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')));";
	private static final String TENDER_ACCEPTED_ASSIGNMENTS_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, "
			+ "tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, "
			+ "tpf.order_delivery_term, tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, ld.name, ld.weight, ld.load_units, "
			+ "ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tpf.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tpf.order_customer_id = cus.id inner join tender tdr on tdr.id = tpf.order_tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where ((tpf.order_state in ('ACCEPTED', 'IN_ACTION', 'COMPLETED') "
			+ "and tpf.tsp_response = 'ACCEPTED') or (tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')) "
			+ "or (tpf.order_state = 'WITHDRAWN' and tpf.tsp_response = 'ACCEPTED')) and exists "
			+ "(select trpn.id from tender_participation trpn where trpn.tsp_id = tpf.tsp_id and trpn.tender_id = tpf.order_tender_id) and tpf.order_tender_id = ?;";
	private static final String TSP_UNCOMPLETED_ASSIGNMENTS_QUERY = "select tpf.shipment_id, tpf.tsp_response, tpf.notified, tpf.comment, tpf.response_time, "
			+ "tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, tpf.order_loading_date, "
			+ "tpf.order_delivery_term, tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, ld.name, ld.weight, ld.load_units, "
			+ "ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tpf.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tpf.order_customer_id = cus.id left join tender tdr on tdr.id = tpf.order_tender_id "
			+ "left join user tcus on tdr.customer_id = tcus.id where tpf.tsp_id = ? and ((tpf.tsp_response = 'OUTSTANDING' and "
			+ "tpf.order_state in ('ASSIGNED', 'CANCELLED')) or (tpf.tsp_response = 'ACCEPTED' and "
			+ "tpf.order_state in ('ACCEPTED', 'IN_ACTION', 'CANCELLED', 'INTERRUPTED'))) "
			+ "union select tpf.shipment_id, tpf.tsp_response, tpf.notified, "
			+ "tpf.comment, tpf.response_time, tpf.truck_plates, tpf.arrived_for_loading, tpf.arrived_for_unloading, tpf.left_loading, tpf.left_unloading, "
			+ "tpf.order_loading_date, tpf.order_delivery_term, tpf.order_line_id, tpf.order_tender_id, tpf.order_customer_id, tpf.order_state, "
			+ "tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state,  tspu.id, tspu.role, tspu.name, tspu.registration_date, ln.id, ld.id, "
			+ "ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, "
			+ "ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, "
			+ "tdr.title, tdr.set_date, tdr.release_date, tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, "
			+ "tcus.id, tcus.role, tcus.name, tcus.registration_date from tsp_performance tpf inner join user tspu on tspu.id = tpf.tsp_id "
			+ "inner join transport_order tro on tpf.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id inner join tender tdr on tdr.id = tro.tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where tpf.tsp_id = ? and "
			+ "((tpf.tsp_response = 'OUTSTANDING' and tpf.order_state in ('ASSIGNED', 'CANCELLED')) or "
			+ "(tpf.tsp_response = 'ACCEPTED' and tpf.order_state in ('ACCEPTED', 'IN_ACTION', 'CANCELLED', 'INTERRUPTED')));";

	@Override
	public Set<TspPerformance> selectTspSpotsAssignmentsExecuted(long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_SPOT_EXECUTED_SELECT_QUERY)) {
			selectStatement.setLong(1, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrievePerformance(rs);
		}
	}

	@Override
	public Set<TspPerformance> getByCustomer(long customerId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(CUSTOMER_SELECT_QUERY)) {
			selectStatement.setLong(1, customerId);
			selectStatement.setLong(2, customerId);
			selectStatement.setLong(3, customerId);
			selectStatement.setLong(4, customerId);
			ResultSet rs = selectStatement.executeQuery();
			return retrievePerformance(rs);
		}
	}

	@Override
	public Set<TspPerformance> getTenderAcceptedAssignments(long tenderId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement prepareStatement = connection.prepareStatement(TENDER_ACCEPTED_ASSIGNMENTS_QUERY)) {
			prepareStatement.setLong(1, tenderId);
			ResultSet rs = prepareStatement.executeQuery();
			return retrievePerformance(rs);
		}
	}

	/* selects all assignments the tsp user has ever received */
	@Override
	public Set<TspPerformance> selectTspOrderAssingments(long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_SELECT_QUERY)) {
			selectStatement.setLong(1, tspId);
			selectStatement.setLong(2, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrievePerformance(rs);
		}
	}

	@Override
	public Set<TspPerformance> selectTspSpotOrdersAssignments(long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_SPOT_WON_SELECT_QUERY)) {
			selectStatement.setLong(1, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrievePerformance(rs);
		}
	}

	@Override
	public Set<TspPerformance> selectOrderReferringAssignments(long orderId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ORDER_SELECT_QUERY)) {
			selectStatement.setLong(1, orderId);
			selectStatement.setLong(2, orderId);
			ResultSet rs = selectStatement.executeQuery();
			return retrievePerformance(rs);
		}
	}

	@Override
	public Long save(TspPerformance entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setTimestamp(1, Timestamp.valueOf(entity.getLeftUnloading()));
			insertStatement.setTimestamp(2, Timestamp.valueOf(entity.getLeftLoading()));
			insertStatement.setTimestamp(3, Timestamp.valueOf(entity.getArrivedForUnloading()));
			insertStatement.setTimestamp(4, Timestamp.valueOf(entity.getArrivedForLoading()));
			insertStatement.setString(5, entity.getTruckPlates());
			insertStatement.setTimestamp(6, Timestamp.valueOf(entity.getResponseTime()));
			insertStatement.setString(7, entity.getComment());
			insertStatement.setTimestamp(8, Timestamp.valueOf(entity.getNotified()));
			insertStatement.setString(9, entity.getTspResponse().toString());
			insertStatement.setLong(10, entity.getTransportOrder().getId());
			insertStatement.setLong(11, entity.getTsp().getId());
			ResultSet rs = insertStatement.executeQuery();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setShipment_id(id);
			return id;
		}
	}

	@Override
	public boolean update(TspPerformance entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setTimestamp(1, Timestamp.valueOf(entity.getLeftUnloading()));
			updateStatement.setTimestamp(2, Timestamp.valueOf(entity.getLeftLoading()));
			updateStatement.setTimestamp(3, Timestamp.valueOf(entity.getArrivedForUnloading()));
			updateStatement.setTimestamp(4, Timestamp.valueOf(entity.getArrivedForLoading()));
			updateStatement.setString(5, entity.getTruckPlates());
			updateStatement.setTimestamp(6, Timestamp.valueOf(entity.getResponseTime()));
			updateStatement.setString(7, entity.getComment());
			updateStatement.setTimestamp(8, Timestamp.valueOf(entity.getNotified()));
			updateStatement.setString(9, entity.getTspResponse().toString());
			updateStatement.setLong(10, entity.getTransportOrder().getId());
			updateStatement.setLong(11, entity.getTsp().getId());
			updateStatement.setLong(12, entity.getShipment_id());
			int rowCount = updateStatement.executeUpdate();
			return rowCount > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(TspPerformance entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getShipment_id());
			int rowCount = deleteStatement.executeUpdate();
			return rowCount > 0 ? true : false;
		}
	}

	@Override
	public TspPerformance getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			selectStatement.setLong(2, id);
			ResultSet rs = selectStatement.executeQuery();
			TspPerformance performance = retrievePerformance(rs).stream().findFirst().orElse(null);
			return performance;
		}
	}

	@Override
	public Collection<TspPerformance> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			Set<TspPerformance> performances = retrievePerformance(rs).stream().collect(Collectors.toSet());
			return performances;
		}
	}

	private Set<TspPerformance> retrievePerformance(ResultSet rs) throws SQLException {
		Set<TspPerformance> shipments = new HashSet<>();
		while (rs.next()) {
			int i = 1;
			TspPerformance tspPerformance = new TspPerformance();
			tspPerformance.setShipment_id(rs.getLong(i++));
			tspPerformance.setTspResponse(rs.getString(i++) == null ? null : TspPerformance.TspResponse.valueOf(rs.getString(i-1)));
			tspPerformance.setNotified(
					rs.getString(i++) == null ? null : LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER));
			tspPerformance.setComment(rs.getString(i++));
			tspPerformance.setResponseTime(
					rs.getString(i++) == null ? null : LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER));
			tspPerformance.setTruckPlates(rs.getString(i++));
			tspPerformance.setArrivedForLoading(
					rs.getString(i++) == null ? null : LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER));
			tspPerformance.setArrivedForUnloading(
					rs.getString(i++) == null ? null : LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER));
			tspPerformance.setLeftLoading(
					rs.getString(i++) == null ? null : LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER));
			tspPerformance.setLeftUnloading(
					rs.getString(i++) == null ? null : LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER));
			LocalDate orderLoadingDate = (rs.getString(i++) == null ? null
					: LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER));
			int orderDeliveryTerm = rs.getInt(i++);
			long orderLineId = rs.getLong(i++);
			long orderTenderId = rs.getLong(i++);
			long orderCustomerId = rs.getLong(i++);
			OrderFootprint.State state = rs.getString(i++) == null ? null : OrderFootprint.State.valueOf(rs.getString(i-1));
			OrderFootprint footprint = new OrderFootprint(orderLoadingDate, orderDeliveryTerm, orderLineId,
					orderTenderId, orderCustomerId, state);
			tspPerformance.setOrderFootprint(footprint);
			TransportOrder order = new TransportOrder();
			order.setId(rs.getLong(i++));
			order.setIssued(
					rs.getString(i++) != null ? LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER) : null);
			order.setLoadingDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			order.setDeliveryTerm(rs.getInt(i++));
			String orderState = rs.getString(i++);
			orderState = (orderState.equalsIgnoreCase("INTERRUPTED") || orderState.equalsIgnoreCase("CANCELLED"))
					? "WITHDRAWN"
					: orderState;
			order.setState(orderState == null ? null : TransportOrder.State.valueOf(orderState));
			UserEntity tsp = new UserEntity();
			tsp.setId(rs.getLong(i++));
			tsp.setRole(rs.getString(i++) == null ? null : UserEntity.Roles.obtainRole(rs.getString(i-1)));
			tsp.setName(rs.getString(i++));
			tsp.setRegistrationDate(LocalDate.parse(rs.getString(i++) == null ? null : rs.getString(i - 1), DATE_FORMATTER));
			tspPerformance.setTsp(tsp);
			Line line = new Line();
			line.setId(rs.getLong(i++));
			Truckload load = new Truckload();
			load.setId(rs.getLong(i++));
			load.setName(rs.getString(i++));
			load.setWeight(rs.getDouble(i++));
			load.setLoadUnits(rs.getInt(i++));
			load.setTruckType(rs.getString(i++) == null ? null : Truckload.TruckType.getType(rs.getString(i-1)));
			line.setTruckload(load);
			Route route = new Route();
			route.setId(rs.getLong(i++));
			route.setConditions(rs.getString(i++) == null ? null : Route.Conditions.getCondition(rs.getString(i-1)));
			Address start = new Address();
			start.setId(rs.getLong(i++));
			start.setDetails(rs.getString(i++));
			Locality startLocality = new Locality();
			startLocality.setId(rs.getLong(i++));
			startLocality.setName(rs.getString(i++));
			Country startCountry = new Country();
			startCountry.setId(rs.getLong(i++));
			startCountry.setName(rs.getString(i++));
			startLocality.setCountry(startCountry);
			start.setLocality(startLocality);
			route.setStart(start);
			Address finish = new Address();
			finish.setId(rs.getLong(i++));
			finish.setDetails(rs.getString(i++));
			Locality endLocality = new Locality();
			endLocality.setId(rs.getLong(i++));
			endLocality.setName(rs.getString(i++));
			Country endCountry = new Country();
			endCountry.setId(rs.getLong(i++));
			endCountry.setName(rs.getString(i++));
			endLocality.setCountry(endCountry);
			finish.setLocality(endLocality);
			route.setDestination(finish);
			line.setRoute(route);
			order.setLine(line);
			long customerId = rs.getLong(i++);
			if (customerId == 0) {
				i = i + 3;
			} else {
				UserEntity customer = new UserEntity();
				customer.setId(customerId);
				customer.setRole(rs.getString(i++) == null ? null : UserEntity.Roles.obtainRole(rs.getString(i-1)));
				if (customer.getRole() != UserEntity.Roles.CUSTOMER) {
					throw new SQLException(
							"user " + customer.getId() + " is retrieved as transport order' " + order.getId()
									+ " customer, but it has other Role! (" + customer.getRole().printOut() + ")");
				}
				customer.setName(rs.getString(i++));
				customer.setRegistrationDate(
						rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
				order.setCustomer(customer);
				tspPerformance.setTransportOrder(order);
				shipments.add(tspPerformance);
				continue;
			}
			Tender tender = new Tender();
			tender.setId(rs.getLong(i++));
			tender.setTitle(rs.getString(i++));
			tender.setSetDate(rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setReleaseDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setCompetitionDeadline(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setTenderStart(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setTenderEnd(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setEstimatedQuantity(rs.getInt(i++));
			tender.setState(rs.getString(i++) == null ? null : Tender.State.valueOf(rs.getString(i-1).toUpperCase()));
			UserEntity customer = new UserEntity();
			customer.setId(rs.getLong(i++));
			customer.setRole(rs.getString(i++) == null ? null : UserEntity.Roles.obtainRole(rs.getString(i-1)));
			if (customer.getRole() != UserEntity.Roles.CUSTOMER) {
				throw new SQLException("user " + customer.getId() + " is retrieved as tenders' " + tender.getId()
						+ " customer, but it has other Role! (" + customer.getRole().printOut() + ")");
			}
			customer.setName(rs.getString(i++));
			customer.setRegistrationDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setCustomer(customer);
			order.setTender(tender);
			tspPerformance.setTransportOrder(order);
			shipments.add(tspPerformance);
		}
		rs.close();
		return shipments;
	}

	@Override
	public Set<TspPerformance> selectTspAcceptedAssignments(long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection
						.prepareStatement(TSP_ACCEPTED_ASSIGNMENTS_SELECT_QUERY)) {
			selectStatement.setLong(1, tspId);
			selectStatement.setLong(2, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrievePerformance(rs);
		}
	}

	@Override
	public Set<TspPerformance> getTspUncompletedAssignments(long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_UNCOMPLETED_ASSIGNMENTS_QUERY)) {
			selectStatement.setLong(1, tspId);
			selectStatement.setLong(2, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrievePerformance(rs);
		}
	}
}
