package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.Locality;

public interface LocalityDAO extends CRUDDao<Locality, Long> {
	
	Set<Locality> findByCountryId(Long countryId) throws SQLException;

}
