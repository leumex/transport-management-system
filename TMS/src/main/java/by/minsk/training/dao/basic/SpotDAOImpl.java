package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Locality;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.Truckload;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class SpotDAOImpl implements SpotDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into spot_orders (transport_order_id, spot_deadline) values (?,?);";
	private static final String UPDATE_QUERY = "update spot_orders set transport_order_id = ?, spot_deadline = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from spot_orders where id = ?;";
	private static final String ID_SELECT_QUERY = "select sp.id, sp.spot_deadline, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, "
			+ "rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date from spot_orders sp " 
			+ "inner join transport_order tro on sp.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id where sp.id = ?;";
	private static final String ALL_SELECT_QUERY = "select sp.id, sp.spot_deadline, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, "
			+ "rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date from spot_orders sp " 
			+ "inner join transport_order tro on sp.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id;";
	private static final String ORDER_SELECT_QUERY = "select sp.id, sp.spot_deadline, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, "
			+ "rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date from spot_orders sp " 
			+ "inner join transport_order tro on sp.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id where sp.transport_order_id = ?;";
	private static final String TSP_FULFILLED_SELECT_QUERY = "select sp.id, sp.spot_deadline, tro.id, tro.issued, tpf.order_loading_date, "
			+ "tpf.order_delivery_term, tpf.order_state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, "
			+ "rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date from spot_orders sp "
			+ "inner join transport_order tro on sp.transport_order_id = tro.id inner join tsp_performance tpf on tpf.transport_order_id = tro.id "
			+ "inner join line ln on tpf.order_line_id = ln.id inner join route rt on ln.route_line = rt.id "
			+ "inner join truckload ld on ln.truckload_id = ld.id inner join address start on rt.departure = start.id "
			+ "inner join locality lcl1 on start.locality_id = lcl1.id inner join country ctr1 on lcl1.country_id = ctr1.id "
			+ "inner join address finish on rt.destination = finish.id inner join locality lcl2 on finish.locality_id = lcl2.id "
			+ "inner join country ctr2 on ctr2.id = lcl2.country_id inner join user cus on tpf.order_customer_id = cus.id "
			+ "where tpf.tsp_id = ? and ((tpf.tsp_response = 'ACCEPTED' and tpf.order_state in ('IN_ACTION', 'COMPLETED')) or "
			+ "(tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')));";
	private static final String TSP_AVAILABLE_SELECT_QUERY = "select sp.id, sp.spot_deadline, tro.id, tro.issued, tro.loading_date, "
			+ "tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, "
			+ "start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, "
			+ "cus.id, cus.role, cus.name, cus.registration_date from spot_orders sp inner join transport_order tro on sp.transport_order_id = tro.id "
			+ "inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id where tro.state = 'ANNOUNCED' and not exists "
			+ "(select tp.shipment_id from tsp_performance tp where tp.order_tender_id = null and tp.order_customer_id = cus.id and tp.tsp_id = ?) "
			+ "and exists (select ct.id from contract ct where ct.customer_id = cus.id and ct.tsp_id = ?);";
	

	@Override
	public Set<SpotTransportOrder> getTspOpenSpotOrders(long tspId) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_AVAILABLE_SELECT_QUERY)){
			selectStatement.setLong(1, tspId);
			selectStatement.setLong(2, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveSpots(rs);
		}
	}
	
	@Override
	public Set<SpotTransportOrder> getTspSpotExecutedOrders(long tspId) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_FULFILLED_SELECT_QUERY)){
			selectStatement.setLong(1, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveSpots(rs);		
		}
	}
	
	@Override
	public SpotTransportOrder getByOrder(long transportOrderId) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ORDER_SELECT_QUERY)){
			selectStatement.setLong(1, transportOrderId);
			ResultSet rs = selectStatement.executeQuery();
			Set<SpotTransportOrder> results =  retrieveSpots(rs);
			if (results.size() > 1) {throw new SQLException("Single transport order can be sent on spot only once!");}
			Iterator<SpotTransportOrder> iterator = results.iterator();
			return iterator.hasNext() ? iterator.next() : null;
		}
	}
	
	@Override
	public Long save(SpotTransportOrder entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setLong(1, entity.getTransportOrder().getId());
			insertStatement.setTimestamp(2, Timestamp.valueOf(entity.getSpotDeadline()));
			ResultSet rs = insertStatement.executeQuery();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(SpotTransportOrder entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setLong(1, entity.getTransportOrder().getId());
			updateStatement.setTimestamp(2, Timestamp.valueOf(entity.getSpotDeadline()));
			updateStatement.setLong(3, entity.getId());
			return updateStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(SpotTransportOrder entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			return deleteStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public SpotTransportOrder getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			Set<SpotTransportOrder> spots = retrieveSpots(rs);
			return spots.stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<SpotTransportOrder> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			Set<SpotTransportOrder> spots = retrieveSpots(rs);
			return spots;
		}
	}

	private Set<SpotTransportOrder> retrieveSpots(ResultSet rs) throws SQLException {
		Set<SpotTransportOrder> spots = new HashSet<SpotTransportOrder>();
		while (rs.next()) {
			int i = 1;
			SpotTransportOrder spot = new SpotTransportOrder();
			spot.setId(rs.getLong(i++));
			spot.setSpotDeadline(rs.getString(i++) != null ? LocalDateTime.parse(rs.getString(i-1), DATETIME_FORMATTER):null);
			TransportOrder order = new TransportOrder();
			order.setId(rs.getLong(i++));
			order.setIssued(rs.getString(i++) != null ? LocalDateTime.parse(rs.getString(i-1)):null);
			order.setLoadingDate(rs.getString(i++) != null ? LocalDate.parse(rs.getString(i-1)):null);
			order.setDeliveryTerm(rs.getInt(i++));
			String orderState = rs.getString(i++);
			orderState = (orderState.equalsIgnoreCase("INTERRUPTED")||orderState.equalsIgnoreCase("CANCELLED"))?"WITHDRAWN":orderState;
			order.setState(TransportOrder.State.valueOf(orderState));
			Line line = new Line();
			line.setId(rs.getLong(i++));
			Truckload load = new Truckload();
			load.setId(rs.getLong(i++));
			load.setName(rs.getString(i++));
			load.setWeight(rs.getDouble(i++));
			load.setLoadUnits(rs.getInt(i++));
			load.setTruckType(Truckload.TruckType.getType(rs.getString(i++)));
			line.setTruckload(load);
			Route route = new Route();
			route.setId(rs.getLong(i++));
			route.setConditions(Route.Conditions.getCondition(rs.getString(i++)));
			Address start = new Address();
			start.setId(rs.getLong(i++));
			start.setDetails(rs.getString(i++));
			Locality startLocality = new Locality();
			startLocality.setId(rs.getLong(i++));
			startLocality.setName(rs.getString(i++));
			Country startCountry = new Country();
			startCountry.setId(rs.getLong(i++));
			startCountry.setName(rs.getString(i++));
			startLocality.setCountry(startCountry);
			start.setLocality(startLocality);
			route.setStart(start);
			Address finish = new Address();
			finish.setId(rs.getLong(i++));
			finish.setDetails(rs.getString(i++));
			Locality endLocality = new Locality();
			endLocality.setId(rs.getLong(i++));
			endLocality.setName(rs.getString(i++));
			Country endCountry = new Country();
			endCountry.setId(rs.getLong(i++));
			endCountry.setName(rs.getString(i++));
			endLocality.setCountry(endCountry);
			finish.setLocality(endLocality);
			route.setDestination(finish);
			line.setRoute(route);
			order.setLine(line);
			UserEntity customer = new UserEntity();
			customer.setId(rs.getLong(i++));
			customer.setRole(Roles.obtainRole(rs.getString(i++)));
			if (customer.getRole() != UserEntity.Roles.CUSTOMER) {
				throw new SQLException(
						"user " + customer.getId() + " is retrieved as transport order' " + order.getId()
								+ " customer, but it has other Role! (" + customer.getRole().printOut() + ")");
			}
			customer.setName(rs.getString(i++));
			customer.setRegistrationDate(rs.getString(i)!=null ? LocalDate.parse(rs.getString(i), DATE_FORMATTER):null);
			order.setCustomer(customer);
			spot.setTransportOrder(order);
			spots.add(spot);			
		}
		rs.close();
		return spots;
	}
}
