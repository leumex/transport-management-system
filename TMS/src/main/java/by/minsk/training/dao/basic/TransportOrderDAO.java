package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.TransportOrder;

public interface TransportOrderDAO extends CRUDDao<TransportOrder, Long> {

int countTenderLineOrders(long tenderId, long lineId) throws SQLException;

int countTspExecutedOrders(long tenderId, long lineId, long tspId) throws SQLException;

int countTspEverExecutedOrders(long tspId) throws SQLException;

int countCustomerEverExecutedOrders(long customerId) throws SQLException;

Set<TransportOrder> findTspExecutedTenderTransportOrders(long tenderId, long tspId) throws SQLException;

int getExecutedTenderOrdersNumber(Long id) throws SQLException;

int getExecutedSpotOrdersNumber(Long id) throws SQLException;

Set<TransportOrder> findTspTenderLineAssignedOrders(long tenderId, long lineId, long tspId) throws SQLException;

Set<TransportOrder> findTspTenderLineAcceptedOrders(long tenderId, long lineId, long tspId) throws SQLException;

int countTspEverReceivedOrders(long tspId) throws SQLException;

Set<TransportOrder> findUncompletedOrders (long customerId) throws SQLException;

}
