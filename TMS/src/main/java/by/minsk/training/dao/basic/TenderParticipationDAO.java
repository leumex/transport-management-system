package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.user.UserEntity;

public interface TenderParticipationDAO extends CRUDDao<TenderParticipation, Long> {

	TenderParticipation getParticipation(long tspId, long tenderId) throws SQLException;

	Set<TenderParticipation> getAllOfTspCustomerPair(long tspId, long customerId) throws SQLException;

	Set<TenderParticipation> getTspParticipations(Long id) throws SQLException;
}
