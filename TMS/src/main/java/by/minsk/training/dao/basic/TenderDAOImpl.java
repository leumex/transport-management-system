package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Tender;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TenderDAOImpl implements TenderDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into tender (state, estimated_quantity, tender_end, tender_start, competition_deadline, release_date, "
			+ "customer_id, title) values (?,?,?,?,?,?,?,?);";
	private static final String UPDATE_QUERY = "update tender set state = ?, estimated_quantity = ?, tender_end = ?, tender_start = ?, competition_deadline = ?, "
			+ "release_date = ?, set_date = ?, customer_id = ?, title = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from tender where id = ?;";
	private static final String ID_SELECT_QUERY = "select t.id, t.title, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, "
			+ "t.release_date, t.set_date, t.customer_id, c.name, c.registration_date, c.role from tender t inner join user c on t.customer_id = c.id "
			+ "where t.id = ?;";
	private static final String ALL_SELECT_QUERY = "select t.id, t.title, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, "
			+ "t.release_date, t.set_date, t.customer_id, c.name, c.registration_date, c.role from tender t inner join user c on t.customer_id = c.id;";
	private static final String CARRIED_OUT_TENDERS_COUNT_QUERY = "select distinct count(tr.id) from tender tr where state in ('contracted', 'in_action', 'completed') "
			+ "and tr.customer_id = ?;";
	private static final String BY_TSP_EXECUTED_TENDER_TRANSPORT_ORDERS_COUNT_QUERY = "select distinct count(tpf.transport_order_id) from tsp_performance tpf "
			+ "where ((tpf.tsp_response = 'ACCEPTED' and tpf.order_state in ('IN_ACTION', 'COMPLETED')) or "
			+ "(tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN'))) and tpf.order_tender_id = ? and tpf.tsp_id = ?;";
	private static final String TO_TSP_ASSINGED_TENDER_TRANSPORT_ORDERS_COUNT_QUERY = "select distinct count(tro.id) from transport_order tro "
			+ "inner join tsp_performance tp on tp.transport_order_id = tro.id "
			+ "where exists (select trpn.id from tender_participation trpn where trpn.tsp_id = tp.tsp_id and trpn.tender_id = tp.order_tender_id) "
			+ "and tp.order_state != 'CANCELLED' and tp.order_tender_id = ? and tp.tsp_id = ?;";
	private static final String BY_TSP_ACCEPTED_TENDER_TRANSPORT_ORDERS_COUNT_QUERY = "select distinct count(tpf.transport_order_id) "
			+ "from tsp_performance tpf where ((tpf.order_state in ('ACCEPTED', 'IN_ACTION', 'COMPLETED') and tpf.tsp_response = 'ACCEPTED') or "
			+ "(tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')) or "
			+ "(tpf.order_state = 'WITHDRAWN' and tpf.tsp_response = 'ACCEPTED')) and tpf.order_tender_id = ? and tpf.tsp_id = ?;";
	private static final String BY_CUSTOMER_SELECT_QUERY = "select t.id, t.title, t.state, t.estimated_quantity, t.tender_end, t.tender_start, "
			+ "t.competition_deadline, t.release_date, t.set_date, t.customer_id, c.name, c.registration_date, c.role from tender t "
			+ "inner join user c on t.customer_id = c.id where c.id = ?;";
	private static final String TENDER_ASSIGNED_ORDERS_COUNT = "select distinct count(tro.id) from transport_order tro "
			+ "inner join tsp_performance tp on tp.transport_order_id = tro.id "
			+ "where exists (select trpn.id from tender_participation trpn where trpn.tsp_id = tp.tsp_id and trpn.tender_id = tp.order_tender_id) "
			+ "and tp.order_state != 'CANCELLED' and tp.order_tender_id = ?;";
	private static final String TENDER_ACCEPTED_ORDERS_COUNT = "select distinct count(tpf.transport_order_id) from tsp_performance tpf where "
			+ "((tpf.order_state in ('ACCEPTED', 'IN_ACTION', 'COMPLETED') and tpf.tsp_response = 'ACCEPTED') or "
			+ "(tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')) or "
			+ "(tpf.order_state = 'WITHDRAWN' and tpf.tsp_response = 'ACCEPTED')) and tpf.order_tender_id = ?;";

	@Override
	public int getAssignedOrdersNumber(long tenderId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection.prepareStatement(TENDER_ASSIGNED_ORDERS_COUNT)) {
			countStatement.setLong(1, tenderId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
		}
	}
	
	@Override
	public int getAcceptedOrdersNumber (long tenderId) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection.prepareStatement(TENDER_ACCEPTED_ORDERS_COUNT)){
			countStatement.setLong(1, tenderId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
		}
	}

	@Override
	public Set<Tender> getCustomerTenders(long customerId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(BY_CUSTOMER_SELECT_QUERY)) {
			selectStatement.setLong(1, customerId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveTenders(rs);
		}
	}

	@Override
	public int getTspAssignedTenderTransportOrders(long tenderId, long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection
						.prepareStatement(TO_TSP_ASSINGED_TENDER_TRANSPORT_ORDERS_COUNT_QUERY)) {
			countStatement.setLong(1, tenderId);
			countStatement.setLong(2, tspId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
		}
	}

	@Override
	public int getTspAcceptedTenderTransportOrders(long tenderId, long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection
						.prepareStatement(BY_TSP_ACCEPTED_TENDER_TRANSPORT_ORDERS_COUNT_QUERY)) {
			countStatement.setLong(1, tenderId);
			countStatement.setLong(2, tspId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
		}
	}

	@Override
	public int getTspExecutedTenderTransportOrders(long tenderId, long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection
						.prepareStatement(BY_TSP_EXECUTED_TENDER_TRANSPORT_ORDERS_COUNT_QUERY)) {
			countStatement.setLong(1, tenderId);
			countStatement.setLong(2, tspId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
		}
	}

	@Override
	public Long save(Tender entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setString(1, entity.getState().toString().toLowerCase());
			insertStatement.setInt(2, entity.getEstimatedQuantity());
			if (entity.getTenderEnd() == null) {
				insertStatement.setNull(3, 91);
			} else {
				insertStatement.setString(3, DATE_FORMATTER.format(entity.getTenderEnd()));
			}
			if (entity.getTenderStart() == null) {
				insertStatement.setNull(4, 91);
			} else {
				insertStatement.setString(4, DATE_FORMATTER.format(entity.getTenderStart()));
			}
			if (entity.getCompetitionDeadline() == null) {
				insertStatement.setNull(5, 91);
			} else {
				insertStatement.setString(5, DATE_FORMATTER.format(entity.getCompetitionDeadline()));
			}
			if (entity.getReleaseDate() == null) {
				insertStatement.setNull(6, 91);
			} else {
				insertStatement.setString(6, DATE_FORMATTER.format(entity.getReleaseDate()));
			}
			insertStatement.setLong(7, entity.getCustomer().getId());
			insertStatement.setString(8, entity.getTitle());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(Tender entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			boolean flag = false;
			updateStatement.setString(1, entity.getState().toString().toLowerCase());
			updateStatement.setInt(2, entity.getEstimatedQuantity());
			if (entity.getTenderEnd() == null) {
				updateStatement.setNull(3, 91);
			} else {
				updateStatement.setString(3, DATE_FORMATTER.format(entity.getTenderEnd()));
			}
			if (entity.getTenderStart() == null) {
				updateStatement.setNull(4, 91);
			} else {
				updateStatement.setString(4, DATE_FORMATTER.format(entity.getTenderStart()));
			}
			if (entity.getCompetitionDeadline() == null) {
				updateStatement.setNull(5, 91);
			} else {
				updateStatement.setString(5, DATE_FORMATTER.format(entity.getCompetitionDeadline()));
			}
			if (entity.getReleaseDate() == null) {
				updateStatement.setNull(6, 91);
			} else {
				updateStatement.setString(6, DATE_FORMATTER.format(entity.getReleaseDate()));
			}
			updateStatement.setLong(8, entity.getCustomer().getId());
			updateStatement.setString(9, entity.getTitle());
			updateStatement.setLong(10, entity.getId());
			int rowsCount = updateStatement.executeUpdate();
			if (rowsCount > 0) {
				flag = true;
			}
			return flag;
		}
	}

	@Override
	public boolean delete(Tender entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			boolean flag = false;
			deleteStatement.setLong(1, entity.getId());
			int rowsCount = deleteStatement.executeUpdate();
			if (rowsCount > 0) {
				flag = true;
			}
			return flag;
		}
	}

	@Override
	public Tender getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveTenders(rs).stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<Tender> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			return retrieveTenders(rs);
		}
	}

	@Override
	public int getCarriedOutTendersNumber(long customerId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection.prepareStatement(CARRIED_OUT_TENDERS_COUNT_QUERY)) {
			countStatement.setLong(1, customerId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			rs.close();
			return result;
		}
	}

	private Set<Tender> retrieveTenders(ResultSet rs) throws SQLException {
		Set<Tender> tenders = new HashSet<>();
		while (rs.next()) {
			Tender tender = new Tender();
			UserEntity customer = new UserEntity();
			int i = 1;
			tender.setId(rs.getLong(i++));
			tender.setTitle(rs.getString(i++));
			tender.setState(Tender.State.valueOf(rs.getString(i++).toUpperCase()));
			tender.setEstimatedQuantity(rs.getInt(i++));
			tender.setTenderEnd(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setTenderStart(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setCompetitionDeadline(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setReleaseDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setSetDate(rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			customer.setId((Long) rs.getLong(i++));
			customer.setName(rs.getString(i++));
			customer.setRegistrationDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			customer.setRole(UserEntity.Roles.obtainRole(rs.getString(i).trim().toLowerCase()));
			tender.setCustomer(customer);
			tenders.add(tender);
		}
		rs.close();
		return tenders;
	}
}
