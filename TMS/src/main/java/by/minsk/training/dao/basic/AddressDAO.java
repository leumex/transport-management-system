package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.Address;

public interface AddressDAO extends CRUDDao<Address, Long> {
	
	Set<Address> findByLocalityId(Long localityId) throws SQLException;

}
