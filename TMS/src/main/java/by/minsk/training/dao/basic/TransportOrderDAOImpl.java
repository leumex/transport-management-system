package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Locality;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.Truckload;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TransportOrderDAOImpl implements TransportOrderDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into transport_order (state, customer_id, tender_id, line, desired_delivery_term, loading_date) values (?,?,?,?,?,?);";
	private static final String UPDATE_QUERY = "update transport_order set state = ?, customer_id = ?, tender_id = ?, line = ?, desired_delivery_term = ?, loading_date = ? "
			+ "where id = ?;";
	private static final String DELETE_QUERY = "delete from transport_order where id = ?;";
	private static final String ID_SELECT_QUERY = "select tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, "
			+ "ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, "
			+ "lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, "
			+ "tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, tcus.registration_date "
			+ "from transport_order tro inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id left join tender tdr on tdr.id = tro.tender_id left join user tcus on tdr.customer_id = tcus.id "
			+ "where tro.id = ? "
			+ "union "
			+ "select tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, "
			+ "rt.id,  rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, "
			+ "ctr2.id, ctr2.name,  cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, tdr.competition_deadline, "
			+ "tdr.tender_start,  tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, tcus.registration_date from transport_order tro "
			+ "inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id  inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id  inner join tender tdr on tdr.id = tro.tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where tro.id = ?;";
	private static final String ALL_SELECT_QUERY = "select tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, "
			+ "ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, "
			+ "tdr.release_date, tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from transport_order tro inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id "
			+ "inner join truckload ld on ln.truckload_id = ld.id inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id inner join locality lcl2 on finish.locality_id = lcl2.id "
			+ "inner join country ctr2 on ctr2.id = lcl2.country_id inner join user cus on tro.customer_id = cus.id left join tender tdr on tdr.id = tro.tender_id "
			+ "left join user tcus on tdr.customer_id = tcus.id " + "union "
			+ "select tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, "
			+ "rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, "
			+ "cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, tdr.competition_deadline, tdr.tender_start, "
			+ "tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, tcus.registration_date from transport_order tro "
			+ "inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id inner join country ctr1 on lcl1.country_id = ctr1.id "
			+ "inner join address finish on rt.destination = finish.id inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id inner join tender tdr on tdr.id = tro.tender_id inner join user tcus on tdr.customer_id = tcus.id;";
	private static final String TENDER_PER_LINE_ORDERS_COUNT = "select count(tro.id) from transport_order tro "
			+ "inner join tender tr on tro.tender_id = tr.id inner join line l on tro.line = l.id "
			+ "where exists (select 1 from tender_has_line thl where l.id = thl.line_id) and tro.state in ('IN_ACTION', 'COMPLETED') "
			+ "and tr.id = ? and l.id = ?;";
	private static final String TENDER_PER_LINE_EXECUTED_BY_TSP_ORDERS_COUNT = "select distinct count(tro.id) from transport_order tro "
			+ "inner join tsp_performance tpf on tpf.transport_order_id = tro.id inner join tender tr on tro.tender_id = tr.id "
			+ "inner join tender_has_line thl on thl.tender_id = tr.id where ((tpf.tsp_response = 'ACCEPTED' and "
			+ "tpf.order_state in ('IN_ACTION', 'COMPLETED')) or (tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN'))) "
			+ "and tr.id = ? and thl.line_id = ? and tpf.tsp_id = ?;";
	private static final String TOTAL_TSP_EXECUTED_ORDERS_COUNT = "select distinct count(tpf.transport_order_id) from tsp_performance tpf "
			+ "where ((tpf.tsp_response = 'ACCEPTED' and tpf.order_state in ('IN_ACTION', 'COMPLETED')) or "
			+ "(tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN'))) and tpf.tsp_id = ?;";
	private static final String TOTAL_TSP_RECEIVED_ORDERS_COUNT = "select distinct count(tpf.transport_order_id) from tsp_performance tpf "
			+ "where tpf.order_state  != 'CANCELLED' and tpf.tsp_id = ?;";
	private static final String TOTAL_CUSTOMER_EXECUTED_ORDERS_COUNT = "select distinct count(tpf.transport_order_id) from tsp_performance tpf "
			+ "left join tender tr on tr.id = tpf.order_tender_id where (tr.customer_id = ? or tpf.order_customer_id = ?) and "
			+ "((tpf.tsp_response = 'ACCEPTED' and tpf.order_state in ('IN_ACTION', 'COMPLETED')) or "
			+ "(tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')));";
	private static final String CUSTOMER_TENDERS_EXECUTED_ORDERS_COUNT = "select distinct count(tpf.transport_order_id) from tsp_performance tpf "
			+ "where tpf.order_tender_id != null and ((tpf.tsp_response = 'ACCEPTED' and tpf.order_state in ('IN_ACTION', 'COMPLETED')) or "
			+ "(tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')));";
	private static final String CUSTOMER_SPOT_EXECUTED_ORDERS_COUNT = "select distinct count(tpf.transport_order_id) from tsp_performance tpf "
			+ "where tpf.order_customer_id != null and ((tpf.tsp_response = 'ACCEPTED' and tpf.order_state in ('IN_ACTION', 'COMPLETED')) or "
			+ "(tpf.order_state = 'INTERRUPTED' and tpf.tsp_response in ('ACCEPTED', 'WITHDRAWN')));";
	private static final String TSP_TENDER_EXECUTED_ORDERS_SELECT_QUERY = "select tro.id, tro.issued, tp.order_loading_date, tp.order_delivery_term, "
			+ "tp.order_state, tp.order_line_id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, "
			+ "start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, "
			+ "cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, tdr.competition_deadline, "
			+ "tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, tcus.registration_date "
			+ "from transport_order tro inner join tsp_performance tp on tp.transport_order_id = tro.id inner join line ln on tp.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id inner join tender tdr on tdr.id = tp.order_customer_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where ((tp.tsp_response = 'ACCEPTED' and tp.order_state in ('IN_ACTION', 'COMPLETED')) "
			+ "or (tp.order_state = 'INTERRUPTED' and tp.tsp_response in ('ACCEPTED', 'WITHDRAWN'))) and "
			+ "exists (select trpn.id from tender_participation trpn where trpn.tsp_id = tp.tsp_id and trpn.tender_id = tp.order_tender_id) "
			+ "and tp.order_tender_id = ? and tro.tender_id = tp.order_tender_id and tp.tsp_id = ?;";
	private static final String TSP_TENDER_LINE_ACCEPTED_ORDERS_SELECT_QUERY = "select tro.id, tro.issued, tp.order_loading_date, tp.order_delivery_term, tp.order_state, "
			+ "ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, "
			+ "finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, "
			+ "tdr.release_date, tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from transport_order tro inner join tsp_performance tp on tp.transport_order_id = tro.id inner join line ln on tp.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id inner join address start on rt.departure = start.id "
			+ "inner join locality lcl1 on start.locality_id = lcl1.id inner join country ctr1 on lcl1.country_id = ctr1.id "
			+ "inner join address finish on rt.destination = finish.id inner join locality lcl2 on finish.locality_id = lcl2.id "
			+ "inner join country ctr2 on ctr2.id = lcl2.country_id left join user cus on tp.order_customer_id = cus.id inner join tender tdr on tdr.id = tp.order_tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where exists (select trpn.id from tender_participation trpn "
			+ "where trpn.tsp_id = tp.tsp_id and trpn.tender_id = tp.order_tender_id) and exists (select thl.id from tender_has_line thl "
			+ "where thl.tender_id = tdr.id and thl.line_id = ln.id) and ((tp.order_state in ('ACCEPTED', 'IN_ACTION', 'COMPLETED') and tp.tsp_response = 'ACCEPTED') or "
			+ "(tp.order_state = 'INTERRUPTED' and tp.tsp_response in ('ACCEPTED', 'WITHDRAWN')) or (tp.order_state = 'WITHDRAWN' and tp.tsp_response = 'ACCEPTED')) "
			+ "and tro.tender_id = ? and ln.id = ? and tp.tsp_id = ?;";
	private static final String TSP_TENDER_LINE_ASSIGNED_ORDERS_SELECT_QUERY = "select tro.id, tro.issued, tp.order_loading_date, tp.order_delivery_term, tp.order_state, "
			+ "ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, "
			+ "finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, "
			+ "tdr.release_date, tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from transport_order tro inner join tsp_performance tp on tp.transport_order_id = tro.id inner join line ln on tp.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id inner join address start on rt.departure = start.id "
			+ "inner join locality lcl1 on start.locality_id = lcl1.id inner join country ctr1 on lcl1.country_id = ctr1.id "
			+ "inner join address finish on rt.destination = finish.id inner join locality lcl2 on finish.locality_id = lcl2.id "
			+ "inner join country ctr2 on ctr2.id = lcl2.country_id left join user cus on tp.order_customer_id = cus.id inner join tender tdr on tdr.id = tp.order_tender_id "
			+ "inner join user tcus on tdr.customer_id = tcus.id where exists (select trpn.id from tender_participation trpn "
			+ "where trpn.tsp_id = tp.tsp_id and trpn.tender_id = tp.order_tender_id) and exists (select thl.id from tender_has_line thl "
			+ "where thl.tender_id = tdr.id and thl.line_id = ln.id) and tp.order_state != 'CANCELLED' and tro.tender_id = ? and ln.id = ? and tp.tsp_id = ?;";
	private static final String CUSTOMER_UNCOMPLETED_ORDERS_SELECT_QUERY = "select tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, "
			+ "ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, "
			+ "tdr.release_date, tdr.competition_deadline, tdr.tender_start, tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, "
			+ "tcus.registration_date from transport_order tro inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id "
			+ "inner join truckload ld on ln.truckload_id = ld.id inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id inner join locality lcl2 on finish.locality_id = lcl2.id "
			+ "inner join country ctr2 on ctr2.id = lcl2.country_id inner join user cus on tro.customer_id = cus.id left join tender tdr on tdr.id = tro.tender_id "
			+ "left join user tcus on tdr.customer_id = tcus.id where tro.state != 'COMPLETED' and cus.id = ? " + "union "
			+ "select tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, "
			+ "rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, "
			+ "cus.id, cus.role, cus.name, cus.registration_date, tdr.id, tdr.title, tdr.set_date, tdr.release_date, tdr.competition_deadline, tdr.tender_start, "
			+ "tdr.tender_end, tdr.estimated_quantity, tdr.state, tcus.id, tcus.role, tcus.name, tcus.registration_date from transport_order tro "
			+ "inner join line ln on tro.line = ln.id inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id inner join country ctr1 on lcl1.country_id = ctr1.id "
			+ "inner join address finish on rt.destination = finish.id inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "left join user cus on tro.customer_id = cus.id inner join tender tdr on tdr.id = tro.tender_id inner join user tcus on tdr.customer_id = tcus.id"
			+ " where tro.state != 'COMPLETED' and tcus.id = ?;";
	
	
	@Override
	public Set<TransportOrder> findTspTenderLineAssignedOrders(long tenderId, long lineId, long tspId)
			throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection
						.prepareStatement(TSP_TENDER_LINE_ASSIGNED_ORDERS_SELECT_QUERY)) {
			selectStatement.setLong(1, tenderId);
			selectStatement.setLong(2, lineId);
			selectStatement.setLong(3, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveOrders(rs);
		}
	}

	@Override
	public Set<TransportOrder> findTspTenderLineAcceptedOrders(long tenderId, long lineId, long tspId)
			throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection
						.prepareStatement(TSP_TENDER_LINE_ACCEPTED_ORDERS_SELECT_QUERY)) {
			selectStatement.setLong(1, tenderId);
			selectStatement.setLong(2, lineId);
			selectStatement.setLong(3, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveOrders(rs);
		}
	}

	@Override
	public int getExecutedSpotOrdersNumber(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection.prepareStatement(CUSTOMER_SPOT_EXECUTED_ORDERS_COUNT)) {
			countStatement.setLong(1, id);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
		}
	}

	@Override
	public int getExecutedTenderOrdersNumber(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection
						.prepareStatement(CUSTOMER_TENDERS_EXECUTED_ORDERS_COUNT)) {
			countStatement.setLong(1, id);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
		}
	}

	@Override
	public Set<TransportOrder> findTspExecutedTenderTransportOrders(long tenderId, long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection
						.prepareStatement(TSP_TENDER_EXECUTED_ORDERS_SELECT_QUERY)) {
			selectStatement.setLong(1, tenderId);
			selectStatement.setLong(2, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveOrders(rs);
		}
	}

	/*
	 * returns number of transport orders a customer has ever ordered, when
	 * execution has at least started
	 */
	@Override
	public int countCustomerEverExecutedOrders(long customerId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection.prepareStatement(TOTAL_CUSTOMER_EXECUTED_ORDERS_COUNT)) {
			countStatement.setLong(1, customerId);
			countStatement.setLong(2, customerId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			rs.close();
			return result;
		}
	}

	/*
	 * returns number of transport orders a tsp user has ever executed - those that
	 * are registered in the app's db
	 */
	@Override
	public int countTspEverExecutedOrders(long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection.prepareStatement(TOTAL_TSP_EXECUTED_ORDERS_COUNT)) {
			countStatement.setLong(1, tspId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			rs.close();
			return result;
		}
	}

	@Override
	public Long save(TransportOrder entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setString(1, entity.getState().toString());
			if (entity.getCustomer() != null) {
				insertStatement.setLong(2, entity.getCustomer().getId());
				insertStatement.setNull(3, java.sql.Types.INTEGER);
			} else {
				insertStatement.setNull(2, java.sql.Types.INTEGER);
				insertStatement.setLong(3, entity.getTender().getId());
			}
			insertStatement.setLong(4, entity.getLine().getId());
			insertStatement.setInt(5, entity.getDeliveryTerm());
			insertStatement.setDate(5, Date.valueOf(entity.getLoadingDate()));
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(TransportOrder entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			boolean flag = false;
			updateStatement.setString(1, entity.getState().toString());
			if (entity.getCustomer() == null) {
				updateStatement.setNull(2, java.sql.Types.INTEGER);
				updateStatement.setLong(3, entity.getTender().getId());
			} else {
				updateStatement.setLong(2, entity.getCustomer().getId());
				updateStatement.setNull(3, java.sql.Types.INTEGER);
			}
			updateStatement.setLong(4, entity.getLine().getId());
			updateStatement.setInt(5, entity.getDeliveryTerm());
			updateStatement.setDate(6, Date.valueOf(entity.getLoadingDate()));
			updateStatement.setLong(7, entity.getId());
			int rowsUpdated = updateStatement.executeUpdate();
			if (rowsUpdated > 0) {
				flag = true;
			}
			return flag;
		}
	}

	@Override
	public boolean delete(TransportOrder entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			boolean flag = false;
			deleteStatement.setLong(1, entity.getId());
			int rowsUpdated = deleteStatement.executeUpdate();
			if (rowsUpdated > 0) {
				flag = true;
			}
			return flag;
		}
	}

	@Override
	public TransportOrder getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			selectStatement.setLong(2, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveOrders(rs).stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<TransportOrder> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			return retrieveOrders(rs);
		}
	}

	/*
	 * counts transport orders that have actually got executed within specified
	 * tender based on specified tender's line
	 */
	@Override
	public int countTenderLineOrders(long tenderId, long lineId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection.prepareStatement(TENDER_PER_LINE_ORDERS_COUNT)) {
			countStatement.setLong(1, tenderId);
			countStatement.setLong(2, lineId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			rs.close();
			return result;
		}
	}

	@Override
	public int countTspExecutedOrders(long tenderId, long lineId, long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection
						.prepareStatement(TENDER_PER_LINE_EXECUTED_BY_TSP_ORDERS_COUNT)) {
			countStatement.setLong(1, tenderId);
			countStatement.setLong(2, lineId);
			countStatement.setLong(3, tspId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			rs.close();
			return result;
		}
	}

	private Set<TransportOrder> retrieveOrders(ResultSet rs) throws SQLException {
		Set<TransportOrder> list = new HashSet<>();
		while (rs.next()) {
			int i = 1;
			TransportOrder order = new TransportOrder();
			order.setId(rs.getLong(i++));
			order.setIssued(
					rs.getString(i++) != null ? LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER) : null);
			order.setLoadingDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			order.setDeliveryTerm(rs.getInt(i++));
			String state = rs.getString(i++);
			state = (state.equalsIgnoreCase("INTERRUPTED") || state.equalsIgnoreCase("CANCELLED")) ? "WITHDRAWN"
					: state;
			order.setState(TransportOrder.State.valueOf(state));
			Line line = new Line();
			line.setId(rs.getLong(i++));
			Truckload load = new Truckload();
			load.setId(rs.getLong(i++));
			load.setName(rs.getString(i++));
			load.setWeight(rs.getDouble(i++));
			load.setLoadUnits(rs.getInt(i++));
			load.setTruckType(Truckload.TruckType.getType(rs.getString(i++)));
			line.setTruckload(load);
			Route route = new Route();
			route.setId(rs.getLong(i++));
			route.setConditions(Route.Conditions.getCondition(rs.getString(i++)));
			Address start = new Address();
			start.setId(rs.getLong(i++));
			start.setDetails(rs.getString(i++));
			Locality startLocality = new Locality();
			startLocality.setId(rs.getLong(i++));
			startLocality.setName(rs.getString(i++));
			Country startCountry = new Country();
			startCountry.setId(rs.getLong(i++));
			startCountry.setName(rs.getString(i++));
			startLocality.setCountry(startCountry);
			start.setLocality(startLocality);
			route.setStart(start);
			Address finish = new Address();
			finish.setId(rs.getLong(i++));
			finish.setDetails(rs.getString(i++));
			Locality endLocality = new Locality();
			endLocality.setId(rs.getLong(i++));
			endLocality.setName(rs.getString(i++));
			Country endCountry = new Country();
			endCountry.setId(rs.getLong(i++));
			endCountry.setName(rs.getString(i++));
			endLocality.setCountry(endCountry);
			finish.setLocality(endLocality);
			route.setDestination(finish);
			line.setRoute(route);
			order.setLine(line);
			long customerId = rs.getLong(i++);
			if (customerId == 0) {
				i = i + 3;
			} else {
				UserEntity customer = new UserEntity();
				customer.setId(customerId);
				customer.setRole(UserEntity.Roles.obtainRole(rs.getString(i++)));
				if (customer.getRole() != UserEntity.Roles.CUSTOMER) {
					throw new SQLException(
							"user " + customer.getId() + " is retrieved as transport order' " + order.getId()
									+ " customer, but it has other Role! (" + customer.getRole().printOut() + ")");
				}
				customer.setName(rs.getString(i++));
				customer.setRegistrationDate(
						rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
				order.setCustomer(customer);
				list.add(order);
				continue;
			}
			Tender tender = new Tender();
			tender.setId(rs.getLong(i++));
			tender.setTitle(rs.getString(i++));
			tender.setSetDate(rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setReleaseDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setCompetitionDeadline(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setTenderStart(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setTenderEnd(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setEstimatedQuantity(rs.getInt(i++));
			tender.setState(Tender.State.valueOf(rs.getString(i++).toUpperCase()));
			UserEntity customer = new UserEntity();
			customer.setId(rs.getLong(i++));
			customer.setRole(UserEntity.Roles.obtainRole(rs.getString(i++)));
			if (customer.getRole() != UserEntity.Roles.CUSTOMER) {
				throw new SQLException("user " + customer.getId() + " is retrieved as tenders' " + tender.getId()
						+ " customer, but it has other Role! (" + customer.getRole().printOut() + ")");
			}
			customer.setName(rs.getString(i++));
			customer.setRegistrationDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			tender.setCustomer(customer);
			order.setTender(tender);
			list.add(order);
		}
		rs.close();
		return list;
	}

	@Override
	public int countTspEverReceivedOrders(long tspId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement countStatement = connection.prepareStatement(TOTAL_TSP_RECEIVED_ORDERS_COUNT)) {
			countStatement.setLong(1, tspId);
			ResultSet rs = countStatement.executeQuery();
			int result = 0;
			while (rs.next()) {
				result = rs.getInt(1);
			}
			rs.close();
			return result;
		}
	}

	@Override
	public Set<TransportOrder> findUncompletedOrders(long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(CUSTOMER_UNCOMPLETED_ORDERS_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			selectStatement.setLong(2, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveOrders(rs);
		}
	}
}
