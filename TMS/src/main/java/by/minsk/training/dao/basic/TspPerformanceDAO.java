package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.TspPerformance;

public interface TspPerformanceDAO extends CRUDDao<TspPerformance, Long> {

	/*
	 * returns set of transport order assignments of a particular transport order to
	 * a particular tsp. There can be several records in some circumstances
	 */
	Set<TspPerformance> selectOrderReferringAssignments(long orderId) throws SQLException;
	
	/*
	 * returns a set of transport order assignments received by  a particular tsp as a winner of
	 * spot order contest
	 */
	Set<TspPerformance> selectTspSpotOrdersAssignments(long tspId) throws SQLException;

	/*
	 * returns a set of transport order assignments to a particular tsp that eventually have been
	 * executed by him
	 */
	Set<TspPerformance> selectTspSpotsAssignmentsExecuted(long tspId) throws SQLException;

	/* returns all assignments a particular tsp has ever received within the app */
	Set<TspPerformance> selectTspOrderAssingments(long tspId) throws SQLException;

	Set<TspPerformance> getTenderAcceptedAssignments(long tenderId) throws SQLException;
	
	Set<TspPerformance> selectTspAcceptedAssignments(long tspId) throws SQLException;
	
	Set<TspPerformance> getTspUncompletedAssignments(long tspId) throws SQLException;

	Set<TspPerformance> getByCustomer(long customerId) throws SQLException;
}
