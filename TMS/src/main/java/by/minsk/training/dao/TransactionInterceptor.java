package by.minsk.training.dao;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Arrays;

import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanInterceptor;
import by.minsk.training.core.Interceptor;

@Bean
@Interceptor(clazz = TransactionSupport.class)
public class TransactionInterceptor implements BeanInterceptor {
    private TransactionManager transactionManager;

    public TransactionInterceptor(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    @Override
    public void before(Object proxy, Object service, Method method, Object[] args) {
        if (isMethodHasTransaction(service, method)) {
            try {
                transactionManager.beginTransaction();
            } catch (SQLException e) {
                throw new IllegalStateException("Failed to begin transaction", e);
            }
        }
    }

    @Override
    public void success(Object proxy, Object service, Method method, Object[] args) {
        if (isMethodHasTransaction(service, method)) {
            try {
                transactionManager.commitTransaction();
            } catch (SQLException e) {
                throw new IllegalStateException("Failed to commit transaction", e);
            }
        }
    }

    @Override
    public void fail(Object proxy, Object service, Method method, Object[] args) {
        if (isMethodHasTransaction(service, method)) {
            try {
                transactionManager.rollbackTransaction();
            } catch (SQLException e) {
                throw new IllegalStateException("Failed to rollback transaction", e);
            }
        }
    }

    private boolean isMethodHasTransaction(Object service, Method method) {
        boolean methodHasTransaction = method.getAnnotation(Transactional.class) != null;
        if (!methodHasTransaction) {
            methodHasTransaction = Arrays.stream(service.getClass().getDeclaredMethods())
                    .filter(serviceMethod -> serviceMethod.getName().equals(method.getName()))
                    .anyMatch(serviceMethod -> serviceMethod.getAnnotation(Transactional.class) != null);
        }
        return methodHasTransaction;
    }
}
