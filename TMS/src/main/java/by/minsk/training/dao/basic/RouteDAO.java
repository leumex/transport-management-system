package by.minsk.training.dao.basic;

import by.minsk.training.entity.Route;

public interface RouteDAO extends CRUDDao<Route, Long> {

}
