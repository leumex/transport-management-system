package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

public interface CRUDDao<ENTITY, KEY> {
	
	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("uuuu-MM-dd");
	public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
	
	KEY save(ENTITY entity) throws SQLException;

	boolean update(ENTITY entity) throws SQLException;

	boolean delete(ENTITY entity) throws SQLException;

	ENTITY getById(KEY id) throws SQLException;

	Collection<ENTITY> findAll() throws SQLException;
}
