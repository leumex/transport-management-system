package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.Line;

public interface LineDAO extends CRUDDao<Line, Long> {

	Set<Line> findbyCustomer(Long id) throws SQLException;
	
	Set<Line> findUnusedLines() throws SQLException;
	
	Set<Line> findByTender (Long id) throws SQLException;

}
