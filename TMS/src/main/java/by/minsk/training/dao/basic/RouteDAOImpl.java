package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Locality;
import by.minsk.training.entity.Route;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class RouteDAOImpl implements RouteDAO {
	
	private static final Logger logger = LogManager.getLogger(RouteDAOImpl.class);

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into route (transit_conditions, departure, destination) values (?,?,?);";
	private static final String UPDATE_QUERY = "update route set destination = ?, departure = ?, transit_conditions = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from route where id = ?;";
	private static final String ID_SELECT_QUERY = "select r.destination, r.departure, r.transit_conditions, r.id, a2.details, "
			+ "a2.locality_id, a1.details, a1.locality_id, l2.name, l2.country_id, l1.name, l1.country_id, "
			+ "c2.name, c1.name from route r inner join address a2 on r.destination = a2.id inner join address a1 on r.departure = a1.id "
			+ "inner join locality l2 on a2.locality_id = l2.id inner join locality l1 on a1.locality_id = l1.id "
			+ "inner join country c2 on l2.country_id = c2.id inner join country c1 on l1.country_id = c1.id where id = ?;";
	private static final String ALL_SELECT_QUERY = "select r.destination, r.departure, r.transit_conditions, r.id, a2.details, "
			+ "a2.locality_id, a1.details, a1.locality_id, l2.name, l2.country_id, l1.name, l1.country_id, "
			+ "c2.name, c1.name from route r inner join address a2 on r.destination = a2.id inner join address a1 on r.departure = a1.id "
			+ "inner join locality l2 on a2.locality_id = l2.id inner join locality l1 on a1.locality_id = l1.id "
			+ "inner join country c2 on l2.country_id = c2.id inner join country c1 on l1.country_id = c1.id;";

	@Override
	public Long save(Route entity) throws SQLException {
		logger.debug("RouteDAO#save() method is invoked with the parameter " + entity.toString());
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setString(1, entity.getConditions().toString());
			insertStatement.setLong(2, entity.getStart().getId());
			insertStatement.setLong(3, entity.getDestination().getId());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			entity.setId(id);
			rs.close();
			return id;
		}
	}

	@Override
	public boolean update(Route entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setLong(1, entity.getDestination().getId());
			updateStatement.setLong(2, entity.getStart().getId());
			updateStatement.setString(3, entity.getConditions().toString());
			updateStatement.setLong(4, entity.getId());
			return updateStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(Route entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			return deleteStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public Route getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			List<Route> routes = retrieveRoutes(rs);
			return routes.stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<Route> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			List<Route> routes = retrieveRoutes(rs);
			return routes;
		}
	}

	private List<Route> retrieveRoutes(ResultSet rs) throws SQLException {
		List<Route> routes = new ArrayList<>();
		while (rs.next()) {
			Route route = new Route();
			Address destination = new Address();
			Address start = new Address();
			Locality startLocality = new Locality();
			Locality destinationLocality = new Locality();
			Country startCountry = new Country();
			Country destinationCountry = new Country();
			int i = 1;
			destination.setId(rs.getLong(i++));
			start.setId(rs.getLong(i++));
			route.setConditions(Route.Conditions.getCondition(rs.getString(i++)));
			route.setId(rs.getLong(i++));
			destination.setDetails(rs.getString(i++));
			destinationLocality.setId(rs.getLong(i++));
			start.setDetails(rs.getString(i++));
			startLocality.setId(rs.getLong(i++));
			destinationLocality.setName(rs.getString(i++));
			destinationCountry.setId(rs.getLong(i++));
			startLocality.setName(rs.getString(i++));
			startCountry.setId(rs.getLong(i++));
			destinationCountry.setName(rs.getString(i++));
			startCountry.setName(rs.getString(i));
			destinationLocality.setCountry(destinationCountry);
			startLocality.setCountry(startCountry);
			destination.setLocality(destinationLocality);
			start.setLocality(startLocality);
			route.setDestination(destination);
			route.setStart(start);
			routes.add(route);
		}
		rs.close();
		return routes;
	}

}
