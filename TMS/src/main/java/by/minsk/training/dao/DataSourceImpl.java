package by.minsk.training.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Objects;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.BaseConnectionPool;
import by.minsk.training.core.Bean;
import by.minsk.training.core.ConnectionPool;

@Bean
public class DataSourceImpl implements DataSource {

	private Properties properties = properties();

	private final String dbUrl = properties.getProperty("jdbcUrl");
	private final String driverClass = properties.getProperty("driverClass");
	private final String user = properties.getProperty("user");
	private final String password = properties.getProperty("password");
	private final int poolSize = Integer.parseInt(properties.getProperty("maximumPoolSize"));
	private final ConnectionPool pool = new BaseConnectionPool(dbUrl, driverClass, user, password, poolSize);
	private static final Logger logger = LogManager.getLogger("DataSourceImpl.class");

	private static class DSCreator {
		private static DataSourceImpl dataSource = new DataSourceImpl();
	}

	private DataSourceImpl() {
	}

	public static DataSource getInstance() {
		return DSCreator.dataSource;
	}

	private Properties properties() {
		Properties properties = new Properties();
		try {
			InputStream inputStream = Objects
					.requireNonNull(getClass().getClassLoader().getResource("datasource.properties")).openStream();
			properties.load(inputStream);
		} catch (IOException e) {
			logger.error(
					"Datasource properties file may be incorrect. Error while initializing datasource parameters.");
			throw new IllegalStateException("Check datasource.properties file", e);
		}
		return properties;
	}

	@Override
	public Connection getConnection() {
		return pool.getConnection();
	}

	@Override
	public void close() {
		pool.close();
	}
}
