package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Locality;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLine;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.entity.Truckload;
import by.minsk.training.entity.Route.Conditions;
import by.minsk.training.entity.Truckload.TruckType;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TenderLineQueryDAOImpl implements TenderLineQueryDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into tender_line_query (tender_participation_id, tender_has_line_id, awarded_share, "
			+ "delivery_term, rate) values (?,?,?,?,?);";
	private static final String UPDATE_QUERY = "update tender_line_query set tender_participation_id = ?, tender_has_line_id = ?, "
			+ "awarded_share = ?, delivery_term = ?, rate = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from tender_line_query where id = ?;";
	private static final String ID_SELECT_QUERY = "select tlq.id, tlq.rate, tlq.delivery_term, tlq.awarded_share, thl.id, t.id, t.title, "
			+ "t.set_date, t.release_date, t.competition_deadline, t.tender_start, t.tender_end, t.estimated_quantity, t.state, cus.id, "
			+ "cus.name, cus.role, cus.registration_date, l.id, r.id, r.transit_conditions, start.id, start.details, end.id, end.details, "
			+ "l1.id, l1.name, l2.id, l2.name, c1.id, c1.name, c2.id, c2.name, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, "
			+ "tp.id, tp.invitation_date, tp.feedback_date, tsp.id, tsp.name, tsp.role, tsp.registration_date from tender_line_query tlq "
			+ "inner join tender_has_line thl on tlq.tender_has_Line_id = thl.id inner join tender t on thl.tender_id = t.id "
			+ "inner join user cus on t.customer_id = cus.id inner join line l on thl.line_id = l.id inner join route r on l.route_line = r.id "
			+ "inner join address start on start.id = r.departure inner join address end on end.id = r.destination "
			+ "inner join locality l1 on l1.id = start.locality_id inner join locality l2 on l2.id = end.locality_id "
			+ "inner join country c1 on l1.country_id = c1.id inner join country c2 on l2.country_id = c2.id "
			+ "inner join truckload ld on l.truckload_id = ld.id inner join tender_participation tp on tlq.tender_participation_id = tp.id "
			+ "inner join user tsp on tp.tsp_id = tsp.id where tp.tender_id = t.id and tlq.id = ?;";
	private static final String ALL_SELECT_QUERY = "select tlq.id, tlq.rate, tlq.delivery_term, tlq.awarded_share, thl.id, t.id, t.title, "
			+ "t.set_date, t.release_date, t.competition_deadline, t.tender_start, t.tender_end, t.estimated_quantity, t.state, cus.id, "
			+ "cus.name, cus.role, cus.registration_date, l.id, r.id, r.transit_conditions, start.id, start.details, end.id, end.details, "
			+ "l1.id, l1.name, l2.id, l2.name, c1.id, c1.name, c2.id, c2.name, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, "
			+ "tp.id, tp.invitation_date, tp.feedback_date, tsp.id, tsp.name, tsp.role, tsp.registration_date from tender_line_query tlq "
			+ "inner join tender_has_line thl on tlq.tender_has_Line_id = thl.id inner join tender t on thl.tender_id = t.id "
			+ "inner join user cus on t.customer_id = cus.id inner join line l on thl.line_id = l.id inner join route r on l.route_line = r.id "
			+ "inner join address start on start.id = r.departure inner join address end on end.id = r.destination "
			+ "inner join locality l1 on l1.id = start.locality_id inner join locality l2 on l2.id = end.locality_id "
			+ "inner join country c1 on l1.country_id = c1.id inner join country c2 on l2.country_id = c2.id "
			+ "inner join truckload ld on l.truckload_id = ld.id inner join tender_participation tp on tlq.tender_participation_id = tp.id "
			+ "inner join user tsp on tp.tsp_id = tsp.id where tp.tender_id = t.id;";
	private static final String TENDER_SELECT_QUERY = "select tlq.id, tlq.rate, tlq.delivery_term, tlq.awarded_share, thl.id, t.id, t.title, "
			+ "t.set_date, t.release_date, t.competition_deadline, t.tender_start, t.tender_end, t.estimated_quantity, t.state, cus.id, "
			+ "cus.name, cus.role, cus.registration_date, l.id, r.id, r.transit_conditions, start.id, start.details, end.id, end.details, "
			+ "l1.id, l1.name, l2.id, l2.name, c1.id, c1.name, c2.id, c2.name, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, "
			+ "tp.id, tp.invitation_date, tp.feedback_date, tsp.id, tsp.name, tsp.role, tsp.registration_date from tender_line_query tlq "
			+ "inner join tender_has_line thl on tlq.tender_has_Line_id = thl.id inner join tender t on thl.tender_id = t.id "
			+ "inner join user cus on t.customer_id = cus.id inner join line l on thl.line_id = l.id inner join route r on l.route_line = r.id "
			+ "inner join address start on start.id = r.departure inner join address end on end.id = r.destination "
			+ "inner join locality l1 on l1.id = start.locality_id inner join locality l2 on l2.id = end.locality_id "
			+ "inner join country c1 on l1.country_id = c1.id inner join country c2 on l2.country_id = c2.id "
			+ "inner join truckload ld on l.truckload_id = ld.id inner join tender_participation tp on tlq.tender_participation_id = tp.id "
			+ "inner join user tsp on tp.tsp_id = tsp.id where tp.tender_id = t.id and t.id = ?;";	
	private static final String TENDER_LINE_SELECT_QUERY = "select tlq.id, tlq.rate, tlq.delivery_term, tlq.awarded_share, thl.id, t.id, t.title, "
			+ "t.set_date, t.release_date, t.competition_deadline, t.tender_start, t.tender_end, t.estimated_quantity, t.state, cus.id, "
			+ "cus.name, cus.role, cus.registration_date, l.id, r.id, r.transit_conditions, start.id, start.details, end.id, end.details, "
			+ "l1.id, l1.name, l2.id, l2.name, c1.id, c1.name, c2.id, c2.name, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, "
			+ "tp.id, tp.invitation_date, tp.feedback_date, tsp.id, tsp.name, tsp.role, tsp.registration_date from tender_line_query tlq "
			+ "inner join tender_has_line thl on tlq.tender_has_Line_id = thl.id inner join tender t on thl.tender_id = t.id "
			+ "inner join user cus on t.customer_id = cus.id inner join line l on thl.line_id = l.id inner join route r on l.route_line = r.id "
			+ "inner join address start on start.id = r.departure inner join address end on end.id = r.destination "
			+ "inner join locality l1 on l1.id = start.locality_id inner join locality l2 on l2.id = end.locality_id "
			+ "inner join country c1 on l1.country_id = c1.id inner join country c2 on l2.country_id = c2.id "
			+ "inner join truckload ld on l.truckload_id = ld.id inner join tender_participation tp on tlq.tender_participation_id = tp.id "
			+ "inner join user tsp on tp.tsp_id = tsp.id where tp.tender_id = t.id and t.id = ? and l.id = ? and tsp.id = ?;";

	@Override
	public Long save(TenderLineQuery entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			int i = 1;
			insertStatement.setLong(i++, entity.getParticipation().getId());
			insertStatement.setLong(i++, entity.getTenderLine().getId());
			insertStatement.setDouble(i++, entity.getAwardedShare());
			insertStatement.setInt(i++, entity.getDeliveryTerm());
			insertStatement.setDouble(i, entity.getRate());
			ResultSet rs = insertStatement.executeQuery();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(TenderLineQuery entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			int i = 1;
			updateStatement.setLong(i++, entity.getParticipation().getId());
			updateStatement.setLong(i++, entity.getTenderLine().getId());
			updateStatement.setDouble(i++, entity.getAwardedShare());
			updateStatement.setInt(i++, entity.getDeliveryTerm());
			updateStatement.setDouble(i++, entity.getRate());
			updateStatement.setLong(i, entity.getId());
			return updateStatement.executeUpdate() > 0 ? true : false;
		}
	}
	

	@Override
	public TenderLineQuery getTenderLineQuery (long tenderId, long lineId, long tspId) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(TENDER_LINE_SELECT_QUERY)){
			preparedStatement.setLong(1,  tenderId);
			preparedStatement.setLong(2, lineId);
			preparedStatement.setLong(3, tspId);
			ResultSet rs = preparedStatement.executeQuery();
			return retrieveQueries(rs).stream().findFirst().orElse(null);
		}
	}	

	@Override
	public boolean delete(TenderLineQuery entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			return deleteStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public TenderLineQuery getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			Set<TenderLineQuery> queries = retrieveQueries(rs);
			return queries.stream().findFirst().orElse(null);
		}
	}
	

	@Override
	public Set<TenderLineQuery> getByTender(Tender tender) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TENDER_SELECT_QUERY)) {
			selectStatement.setLong(1, tender.getId());
			ResultSet rs = selectStatement.executeQuery();
			return retrieveQueries(rs);
		}
	}

	@Override
	public Collection<TenderLineQuery> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			Set<TenderLineQuery> queries = retrieveQueries(rs);
			return queries;
		}
	}

	private Set<TenderLineQuery> retrieveQueries(ResultSet rs) throws SQLException {
		Set<TenderLineQuery> queries = new HashSet<>();
		while (rs.next()) {
			int i = 1;
			TenderLineQuery tenderLineQuery = new TenderLineQuery();
			tenderLineQuery.setId(rs.getLong(i++));
			tenderLineQuery.setRate(rs.getDouble(i++));
			tenderLineQuery.setDeliveryTerm(rs.getInt(i++));
			tenderLineQuery.setAwardedShare(rs.getDouble(i++));
			TenderLine tenderLine = new TenderLine();
			tenderLine.setId(rs.getLong(i++));
			Tender tender = new Tender();
			tender.setId(rs.getLong(i++));
			tender.setSetDate(rs.getString(i++) == null? null:LocalDate.parse(rs.getString(i-1), DATE_FORMATTER));
			tender.setReleaseDate(rs.getString(i++) == null? null:LocalDate.parse(rs.getString(i-1), DATE_FORMATTER));
			tender.setCompetitionDeadline(rs.getString(i++) == null? null:LocalDate.parse(rs.getString(i-1), DATE_FORMATTER));
			tender.setTenderStart(rs.getString(i++) == null? null:LocalDate.parse(rs.getString(i-1), DATE_FORMATTER));
			tender.setTenderEnd(rs.getString(i++) == null? null:LocalDate.parse(rs.getString(i-1), DATE_FORMATTER));
			tender.setEstimatedQuantity(rs.getInt(i++));
			tender.setState(Tender.State.valueOf(rs.getString(i++).toUpperCase()));
			UserEntity customer = new UserEntity();
			customer.setId(rs.getLong(i++));
			customer.setName(rs.getString(i++));
			customer.setRole(Roles.obtainRole(rs.getString(i++)));
			customer.setRegistrationDate(rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i-1),DATE_FORMATTER));
			tender.setCustomer(customer);
			tenderLine.setTender(tender);
			Line line = new Line();
			line.setId(rs.getLong(i++));
			Route route = new Route();
			route.setId(rs.getLong(i++));
			route.setConditions(Conditions.getCondition(rs.getString(i++)));
			Address start = new Address();
			start.setId(rs.getLong(i++));
			start.setDetails(rs.getString(i++));
			Address end = new Address();
			end.setId(rs.getLong(i++));
			end.setDetails(rs.getString(i++));
			Locality startLocality = new Locality();
			startLocality.setId(rs.getLong(i++));
			startLocality.setName(rs.getString(i++));
			Locality endLocality = new Locality();
			endLocality.setId(rs.getLong(i++));
			endLocality.setName(rs.getString(i++));
			Country startCountry = new Country();
			startCountry.setId(rs.getLong(i++));
			startCountry.setName(rs.getString(i++));
			Country endCountry = new Country();
			endCountry.setId(rs.getLong(i++));
			endCountry.setName(rs.getString(i++));
			startLocality.setCountry(startCountry);
			endLocality.setCountry(endCountry);
			start.setLocality(startLocality);
			end.setLocality(endLocality);
			route.setStart(start);
			route.setDestination(end);
			Truckload load = new Truckload();
			load.setId(rs.getLong(i++));
			load.setName(rs.getString(i++));
			load.setWeight(rs.getDouble(i++));
			load.setLoadUnits(rs.getInt(i++));
			load.setTruckType(TruckType.getType(rs.getString(i++)));
			line.setRoute(route);
			line.setTruckload(load);
			tenderLine.setLine(line);
			tenderLineQuery.setTenderLine(tenderLine);
			TenderParticipation tenderParticipation = new TenderParticipation();
			tenderParticipation.setId(rs.getLong(i++));
			tenderParticipation.setInvitation(rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i-1), DATE_FORMATTER));
			tenderParticipation.setFeedback(rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i-1), DATE_FORMATTER));
			UserEntity tsp = new UserEntity();
			tsp.setId(rs.getLong(i++));
			tsp.setName(rs.getString(i++));
			tsp.setRole(Roles.obtainRole(rs.getString(i++)));
			tsp.setRegistrationDate(rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i++), DATE_FORMATTER));
			tenderParticipation.setTsp(tsp);
			tenderParticipation.setTender(tender);
			tenderLineQuery.setParticipation(tenderParticipation);
			queries.add(tenderLineQuery);			
		}
		rs.close();
		return queries;
	}
}
