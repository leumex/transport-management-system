package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.Tender;

public interface TenderDAO extends CRUDDao<Tender, Long> {
	
	int getCarriedOutTendersNumber(long customerId) throws SQLException;

	int getTspExecutedTenderTransportOrders(long tenderId, long tspId) throws SQLException;
	
	int getTspAssignedTenderTransportOrders(long tenderId, long tspId) throws SQLException;
	
	int getTspAcceptedTenderTransportOrders (long tenderId, long tspId) throws SQLException;

	Set<Tender> getCustomerTenders(long customerId) throws SQLException;

	int getAssignedOrdersNumber(long tenderId) throws SQLException;

	int getAcceptedOrdersNumber(long tenderId) throws SQLException;

}
