package by.minsk.training.dao.basic;

import java.sql.SQLException;

import by.minsk.training.entity.Contract;

public interface ContractDAO extends CRUDDao<Contract, Long> {
	
	Contract getContract (long tspId, long customerId) throws SQLException;

}
