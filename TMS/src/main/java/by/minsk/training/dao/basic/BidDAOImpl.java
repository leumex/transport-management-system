
package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Locality;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.Truckload;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class BidDAOImpl implements BidDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into bids (tsp_id, bid, spot_order_id) values (?, ?, ?);";
	private static final String UPDATE_QUERY = "update bids set tsp_id = ? , spot_order_id = ?, made_at = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from bids where id = ?;";
	private static final String ID_SELECT_QUERY = "select b.id, b.bid, b.made_at, tspu.id, tspu.role, tspu.name, tspu.registration_date, "
			+ "sp.id, sp.spot_deadline, tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, "
			+ "ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date from bids b "
			+ "inner join user tspu on b.tsp_id = tspu.id inner join spot_orders sp on b.spot_order_id = sp.id "
			+ "inner join transport_order tro on sp.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id where b.id = ?;";
	private static final String ALL_SELECT_QUERY = "select b.id, b.bid, b.made_at, tspu.id, tspu.role, tspu.name, tspu.registration_date, "
			+ "sp.id, sp.spot_deadline, tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, "
			+ "ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date from bids b "
			+ "inner join user tspu on b.tsp_id = tspu.id inner join spot_orders sp on b.spot_order_id = sp.id "
			+ "inner join transport_order tro on sp.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id;";
	private static final String SPOT_ORDER_SELECT_QUERY = "select b.id, b.bid, b.made_at, tspu.id, tspu.role, tspu.name, tspu.registration_date, "
			+ "sp.id, sp.spot_deadline, tro.id, tro.issued, tro.loading_date, tro.desired_delivery_term, tro.state, ln.id, ld.id, ld.name, ld.weight, "
			+ "ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date from bids b "
			+ "inner join user tspu on b.tsp_id = tspu.id inner join spot_orders sp on b.spot_order_id = sp.id "
			+ "inner join transport_order tro on sp.transport_order_id = tro.id inner join line ln on tro.line = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id where sp.id = ?;";
	private static final String TSP_SUCCESS_SELECT_QUERY = "select b.id, b.bid, b.made_at, tspu.id, tspu.role, tspu.name, "
			+ "tspu.registration_date, sp.id, sp.spot_deadline, tro.id, tro.issued, tp.order_loading_date, tp.order_delivery_term, "
			+ "tp.order_state, ln.id, ld.id, ld.name, ld.weight, ld.load_units, ld.truck_type, rt.id, rt.transit_conditions, "
			+ "start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, finish.details, lcl2.id, lcl2.name, "
			+ "ctr2.id, ctr2.name, cus.id, cus.role, cus.name, cus.registration_date "
			+ "from bids b inner join user tspu on b.tsp_id = tspu.id inner join spot_orders sp on b.spot_order_id = sp.id "
			+ "inner join transport_order tro on sp.transport_order_id = tro.id "
			+ "inner join tsp_performance tp on tro.id = tp.transport_order_id inner join line ln on tp.order_line_id = ln.id "
			+ "inner join route rt on ln.route_line = rt.id inner join truckload ld on ln.truckload_id = ld.id "
			+ "inner join address start on rt.departure = start.id inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on rt.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id "
			+ "inner join user cus on tro.customer_id = cus.id where tp.tsp_id = b.tsp_id and tp.order_tender_id = null "
			+ "and tp.tsp_response = 'ACCEPTED' and tp.order_state in ('IN_ACTION', 'COMPLETED') and tro.customer_id = ? and b.tsp_id = ?;";
	

	/*
	 * selection of bids that have led to the order assignment to a bid maker, when
	 * the assignment has been subsequently accepted by the tsp, and order execution
	 * has at least started
	 */
	@Override
	public Set<Bid> getTspSuccessBids (long customerId, long tspId) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_SUCCESS_SELECT_QUERY)){
			selectStatement.setLong(1, customerId);
			selectStatement.setLong(2, tspId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveBids(rs);
		}
	}
		
	@Override
	public Set<Bid> getBySpotOrder(long spotOrderId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(SPOT_ORDER_SELECT_QUERY)) {
			selectStatement.setLong(1, spotOrderId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveBids(rs);
		}
	}

	@Override
	public Long save(Bid entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setLong(1, entity.getTsp().getId());
			insertStatement.setDouble(2, entity.getBid());
			insertStatement.setLong(3, entity.getSpotOrder().getId());
			ResultSet rs = insertStatement.executeQuery();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			entity.setId(id);
			rs.close();
			return id;
		}
	}

	@Override
	public boolean update(Bid entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setLong(1, entity.getTsp().getId());
			updateStatement.setLong(2, entity.getSpotOrder().getId());
			updateStatement.setTimestamp(3, Timestamp.valueOf(entity.getMadeAt()));
			updateStatement.setLong(4, entity.getId());
			int rowsCount = updateStatement.executeUpdate();
			return rowsCount > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(Bid entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			int rowCount = deleteStatement.executeUpdate();
			return rowCount > 0 ? true : false;

		}
	}

	@Override
	public Bid getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveBids(rs).stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<Bid> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			return retrieveBids(rs);
		}
	}
 
	private Set<Bid> retrieveBids(ResultSet rs) throws SQLException {
		Set<Bid> list = new TreeSet<>((b1, b2) -> b1.getMadeAt().compareTo(b2.getMadeAt()) == 0 ? 1 : b1.getMadeAt().compareTo(b2.getMadeAt()));
		while (rs.next()) {
			int i = 1;
			Bid bid = new Bid();
			bid.setId(rs.getLong(i++));
			bid.setBid(rs.getDouble(i++));
			bid.setMadeAt(
					rs.getString(i++) != null ? LocalDateTime.parse(rs.getString(i - 1), DATETIME_FORMATTER) : null);
			UserEntity tsp = new UserEntity();
			tsp.setId(rs.getLong(i++));
			tsp.setRole(Roles.obtainRole(rs.getString(i++)));
			tsp.setName(rs.getString(i++));
			tsp.setRegistrationDate(
					rs.getString(i++) != null ? LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER) : null);
			bid.setTsp(tsp);
			SpotTransportOrder spot = new SpotTransportOrder();
			spot.setId(rs.getLong(i++));
			spot.setSpotDeadline(rs.getString(i++) != null ? LocalDateTime.parse(rs.getString(i-1), DATETIME_FORMATTER):null);
			TransportOrder order = new TransportOrder();
			order.setId(rs.getLong(i++));
			order.setIssued(rs.getString(i++) != null ? LocalDateTime.parse(rs.getString(i-1)):null);
			order.setLoadingDate(rs.getString(i++) != null ? LocalDate.parse(rs.getString(i-1)):null);
			order.setDeliveryTerm(rs.getInt(i++));
			String state = (rs.getString(i++));
			state = (state.equalsIgnoreCase("INTERRUPTED")||state.equalsIgnoreCase("CANCELLED"))?"WITHDRAWN":state;
			order.setState(TransportOrder.State.valueOf(state));
			Line line = new Line();
			line.setId(rs.getLong(i++));
			Truckload load = new Truckload();
			load.setId(rs.getLong(i++));
			load.setName(rs.getString(i++));
			load.setWeight(rs.getDouble(i++));
			load.setLoadUnits(rs.getInt(i++));
			load.setTruckType(Truckload.TruckType.getType(rs.getString(i++)));
			line.setTruckload(load);
			Route route = new Route();
			route.setId(rs.getLong(i++));
			route.setConditions(Route.Conditions.getCondition(rs.getString(i++)));
			Address start = new Address();
			start.setId(rs.getLong(i++));
			start.setDetails(rs.getString(i++));
			Locality startLocality = new Locality();
			startLocality.setId(rs.getLong(i++));
			startLocality.setName(rs.getString(i++));
			Country startCountry = new Country();
			startCountry.setId(rs.getLong(i++));
			startCountry.setName(rs.getString(i++));
			startLocality.setCountry(startCountry);
			start.setLocality(startLocality);
			route.setStart(start);
			Address finish = new Address();
			finish.setId(rs.getLong(i++));
			finish.setDetails(rs.getString(i++));
			Locality endLocality = new Locality();
			endLocality.setId(rs.getLong(i++));
			endLocality.setName(rs.getString(i++));
			Country endCountry = new Country();
			endCountry.setId(rs.getLong(i++));
			endCountry.setName(rs.getString(i++));
			endLocality.setCountry(endCountry);
			finish.setLocality(endLocality);
			route.setDestination(finish);
			line.setRoute(route);
			order.setLine(line);
			UserEntity customer = new UserEntity();
			customer.setId(rs.getLong(i++));
			customer.setRole(Roles.obtainRole(rs.getString(i++)));
			if (customer.getRole() != UserEntity.Roles.CUSTOMER) {
				throw new SQLException(
						"user " + customer.getId() + " is retrieved as transport order' " + order.getId()
								+ " customer, but it has other Role! (" + customer.getRole().printOut() + ")");
			}
			customer.setName(rs.getString(i++));
			customer.setRegistrationDate(rs.getString(i)!=null ? LocalDate.parse(rs.getString(i), DATE_FORMATTER):null);
			order.setCustomer(customer);
			spot.setTransportOrder(order);
			bid.setSpotOrder(spot);
			list.add(bid);
		}
		rs.close();
		return list;
	}
}
