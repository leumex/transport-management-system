package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.Bid;

public interface BidDAO extends CRUDDao<Bid, Long> {
	
	Set<Bid> getBySpotOrder (long spotOrderId) throws SQLException;

	Set<Bid> getTspSuccessBids(long customerId, long tspId) throws SQLException;

}
