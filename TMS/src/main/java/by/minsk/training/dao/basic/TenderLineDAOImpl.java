package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Locality;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.Route.Conditions;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLine;
import by.minsk.training.entity.Truckload;
import by.minsk.training.entity.Truckload.TruckType;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TenderLineDAOImpl implements TenderLineDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into tender_has_line (line_id, tender_id) values (?,?);";
	private static final String UPDATE_QUERY = "update tender_has_line set line_id = ?, tender_id = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from tender_has_line where id = ?;";
	private static final String ID_SELECT_QUERY = "select thl.id, t.id, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, "
			+ "t.release_date, t.set_date, t.title, cus.id, cus.name, cus.registration_date, cus.role, l.id, ld.id, ld.name, "
			+ "ld.weight, ld.load_units, ld.truck_type, r.id, r.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name "
			+ "from tender_has_line thl inner join tender t on t.id = thl.tender_id inner join line l on l.id = thl.line_id "
			+ "inner join user cus on cus.id = t.customer_id inner join truckload ld on ld.id = l.truckload_id inner join route r on r.id = l.route_line "
			+ "inner join address start on start.id = r.departure inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on r.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id where thl.id = ?;";
	private static final String ALL_SELECT_QUERY = "select thl.id, t.id, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, "
			+ "t.release_date, t.set_date, t.title, cus.id, cus.name, cus.registration_date, cus.role, l.id, ld.id, ld.name, "
			+ "ld.weight, ld.load_units, ld.truck_type, r.id, r.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name "
			+ "from tender_has_line thl inner join tender t on t.id = thl.tender_id inner join line l on l.id = thl.line_id "
			+ "inner join user cus on cus.id = t.customer_id inner join truckload ld on ld.id = l.truckload_id inner join route r on r.id = l.route_line "
			+ "inner join address start on start.id = r.departure inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on r.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id;";
	private static final String TENDER_SELECT_QUERY = "select thl.id, t.id, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, "
			+ "t.release_date, t.set_date, t.title, cus.id, cus.name, cus.registration_date, cus.role, l.id, ld.id, ld.name, "
			+ "ld.weight, ld.load_units, ld.truck_type, r.id, r.transit_conditions, start.id, start.details, lcl1.id, lcl1.name, ctr1.id, ctr1.name, finish.id, "
			+ "finish.details, lcl2.id, lcl2.name, ctr2.id, ctr2.name "
			+ "from tender_has_line thl inner join tender t on t.id = thl.tender_id inner join line l on l.id = thl.line_id "
			+ "inner join user cus on cus.id = t.customer_id inner join truckload ld on ld.id = l.truckload_id inner join route r on r.id = l.route_line "
			+ "inner join address start on start.id = r.departure inner join locality lcl1 on start.locality_id = lcl1.id "
			+ "inner join country ctr1 on lcl1.country_id = ctr1.id inner join address finish on r.destination = finish.id "
			+ "inner join locality lcl2 on finish.locality_id = lcl2.id inner join country ctr2 on ctr2.id = lcl2.country_id where t.id = ?;";

	@Override
	public Set<TenderLine> getByTender(long tenderId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TENDER_SELECT_QUERY)) {
			selectStatement.setLong(1, tenderId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveTenderLines(rs);
		}
	}

	@Override
	public Long save(TenderLine entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setLong(1, entity.getLine().getId());
			insertStatement.setLong(2, entity.getTender().getId());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(TenderLine entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setLong(1, entity.getLine().getId());
			updateStatement.setLong(2, entity.getTender().getId());
			updateStatement.setLong(3, entity.getId());
			return updateStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(TenderLine entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			return deleteStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public TenderLine getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			Set<TenderLine> tenderLines = retrieveTenderLines(rs);
			return tenderLines.stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<TenderLine> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			Set<TenderLine> tenderLines = retrieveTenderLines(rs);
			return tenderLines;
		}
	}

	private Set<TenderLine> retrieveTenderLines(ResultSet rs) throws SQLException {
		Set<TenderLine> tenderLines = new HashSet<>();
		Tender tender = null;
		UserEntity customer = null;
		while (rs.next()) {
			int i = 1;
			TenderLine tenderLine = new TenderLine();
			tenderLine.setId(rs.getLong(i++));
			if (tender == null) {
				tender = new Tender();
				customer = new UserEntity();
				tender.setId(rs.getLong(i++));
				tender.setState(Tender.State.valueOf(rs.getString(i++)));
				tender.setEstimatedQuantity(rs.getInt(i++));
				tender.setTenderEnd(
						rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER));
				tender.setTenderStart(
						rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER));
				tender.setCompetitionDeadline(
						rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER));
				tender.setReleaseDate(
						rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER));
				tender.setSetDate(
						rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER));
				tender.setTitle(rs.getString(i++));
				customer.setId(rs.getLong(i++));
				customer.setName(rs.getString(i++));
				customer.setRegistrationDate(
						rs.getString(i++) == null ? null : LocalDate.parse(rs.getString(i - 1), DATE_FORMATTER));
				UserEntity.Roles role = Roles.obtainRole(rs.getString(i++));
				if (role != Roles.CUSTOMER) {
					throw new SQLException("Tender references to a user with not a CUSTOMER role!");
				}
				customer.setRole(role);
				tender.setCustomer(customer);
			} else {
				i = i + 13;
			}
			Line line = new Line();
			Truckload truckload = new Truckload();
			Route route = new Route();
			line.setId(rs.getLong(i++));
			truckload.setId(rs.getLong(i++));
			truckload.setName(rs.getString(i++));
			truckload.setWeight(rs.getDouble(i++));
			truckload.setLoadUnits(rs.getInt(i++));
			truckload.setTruckType(TruckType.getType(rs.getString(i++)));
			line.setTruckload(truckload);
			route.setId(rs.getLong(i++));
			route.setConditions(Conditions.getCondition(rs.getString(i++)));
			Address start = new Address();
			start.setId(rs.getLong(i++));
			start.setDetails(rs.getString(i++));
			Locality startLocality = new Locality();
			startLocality.setId(rs.getLong(i++));
			startLocality.setName(rs.getString(i++));
			Country startCountry = new Country();
			startCountry.setId(rs.getLong(i++));
			startCountry.setName(rs.getString(i++));
			startLocality.setCountry(startCountry);
			start.setLocality(startLocality);
			route.setStart(start);
			Address end = new Address();
			end.setId(rs.getLong(i++));
			end.setDetails(rs.getString(i++));
			Locality endLocality = new Locality();
			endLocality.setId(rs.getLong(i++));
			endLocality.setName(rs.getString(i++));
			Country endCountry = new Country();
			endCountry.setId(rs.getLong(i++));
			endCountry.setName(rs.getString(i++));
			endLocality.setCountry(endCountry);
			end.setLocality(endLocality);
			route.setDestination(end);
			line.setRoute(route);
			line.setTruckload(truckload);
			tenderLine.setLine(line);
			tenderLine.setTender(tender);
			tenderLines.add(tenderLine);
		}
		rs.close();
		return tenderLines;
	}
}
