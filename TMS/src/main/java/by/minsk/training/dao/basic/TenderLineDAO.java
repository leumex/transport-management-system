package by.minsk.training.dao.basic;

import java.sql.SQLException;
import java.util.Set;

import by.minsk.training.entity.TenderLine;

public interface TenderLineDAO extends CRUDDao<TenderLine, Long> {
	
		Set<TenderLine> getByTender(long tenderId) throws SQLException;

}
