package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Locality;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class LocalityDAOImpl implements LocalityDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into locality (country_id, name) values (?,?);";
	private static final String UPDATE_QUERY = "update locality set country_id = ?, name = ? where id =?;";
	private static final String DELETE_QUERY = "delete from locality where id = ?;";
	private static final String ID_SELECT_QUERY = "select c.name, c.id, l.id, l.name from locality l "
			+ "inner join country c on c.id = l.country_id where l.id = ?;";
	private static final String ALL_SELECT_QUERY = "select c.name, c.id, l.id, l.name from locality l "
			+ "inner join country c on c.id = l.country_id;";
	private static final String BY_COUNTRY_SELECT_QUERY = "select c.name, c.id, l.id, l.name from locality l "
			+ "inner join country c on c.id = l.country_id where l.country_id = ?;";

	@Override
	public Set<Locality> findByCountryId(Long countryId) throws SQLException {
		if (countryId == null) {
			return null;
		}
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(BY_COUNTRY_SELECT_QUERY)) {
			selectStatement.setLong(1, countryId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveLocalities(rs);
		}
	}

	@Override
	public Long save(Locality entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setLong(1, entity.getCountry().getId());
			insertStatement.setString(2, entity.getName());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			entity.setId(id);
			rs.close();
			return id;
		}
	}

	@Override
	public boolean update(Locality entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setLong(1, entity.getCountry().getId());
			updateStatement.setString(2, entity.getName());
			updateStatement.setLong(3, entity.getId());
			return updateStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(Locality entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			return deleteStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public Locality getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			Set<Locality> localities = retrieveLocalities(rs);
			return localities.stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<Locality> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			Collection<Locality> localities = retrieveLocalities(rs);
			return localities;
		}
	}

	private Set<Locality> retrieveLocalities(ResultSet rs) throws SQLException {
		Set<Locality> localities = new HashSet<>();
		while (rs.next()) {
			int i = 1;
			Country country = new Country();
			country.setName(rs.getString(i++));
			country.setId(rs.getLong(i++));
			Locality locality = new Locality();
			locality.setCountry(country);
			locality.setId(rs.getLong(i++));
			locality.setName(rs.getString(i));
			localities.add(locality);
		}
		rs.close();
		return localities;
	}
}
