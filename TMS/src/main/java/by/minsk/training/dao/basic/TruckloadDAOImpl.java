package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Truckload;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TruckloadDAOImpl implements TruckloadDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into truckload (truck_type, load_units, weight, name) values (?,?,?,?);";
	private static final String UPDATE_QUERY = "update truckload set truck_type = ?, load_units = ?, weight = ?, name = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from truckload where id = ?;";
	private static final String ID_SELECT_QUERY = "select id, truck_type, load_units, weight, name from truckload where id = ?;";
	private static final String ALL_SELECT_QUERY = "select id, truck_type, load_units, weight, name from truckload;";

	@Override
	public Long save(Truckload entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setString(1, entity.getTruckType().toSqlValue());
			insertStatement.setInt(2, entity.getLoadUnits());
			insertStatement.setDouble(3, entity.getWeight());
			insertStatement.setString(4, entity.getName());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(Truckload entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setString(1, entity.getTruckType().toSqlValue());
			updateStatement.setInt(2, entity.getLoadUnits());
			updateStatement.setDouble(3, entity.getWeight());
			updateStatement.setString(4, entity.getName());
			updateStatement.setLong(5, entity.getId());
			return updateStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(Truckload entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			return deleteStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public Truckload getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			if (!rs.next()) {
				return null;
			}
			Truckload truckload = new Truckload();
			do {
				int i = 1;
				truckload.setId(rs.getLong(i++));
				truckload.setTruckType(Truckload.TruckType.valueOf(rs.getString(i++)));
				truckload.setLoadUnits(rs.getInt(i++));
				truckload.setWeight(rs.getDouble(i++));
				truckload.setName(rs.getString(i));
			} while (rs.next());
			rs.close();
			return truckload;
		}
	}

	@Override
	public Collection<Truckload> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			Set<Truckload> truckloads = new HashSet<>();
			while (rs.next()) {
				int i = 1;
				Truckload truckload = new Truckload();
				truckload.setId(rs.getLong(i++));
				truckload.setTruckType(Truckload.TruckType.getType(rs.getString(i++)));
				truckload.setLoadUnits(rs.getInt(i++));
				truckload.setWeight(rs.getDouble(i++));
				truckload.setName(rs.getString(i));
				truckloads.add(truckload);
			}
			rs.close();
			return truckloads;
		}
	}
}
