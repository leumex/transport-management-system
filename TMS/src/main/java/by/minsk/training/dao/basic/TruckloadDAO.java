package by.minsk.training.dao.basic;

import by.minsk.training.entity.Truckload;

public interface TruckloadDAO extends CRUDDao<Truckload, Long> {

}
