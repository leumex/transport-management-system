package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Locality;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.Truckload;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class LineDAOImpl implements LineDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into line (route_line, truckload_id) values (?,?);";
	private static final String UPDATE_QUERY = "update line set route_line = ? truckload_id = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from line where id = ?;";
	private static final String ID_SELECT_QUERY = "select r.id, r.transit_conditions, "
			+ "a1.details, a1.id, a2.details, a2.id, "
			+ "l1.name, l1.id, l2.name, l2.id,  c1.name, c1.id, c2.name, c2.id, "
			+ "t.id, t.truck_type, t.load_units, t.weight, t.name, l.id " + "from line l "
			+ "inner join route r on l.route_line = r.id " + "inner join address a1 on r.departure = a1.id "
			+ "inner join address a2 on r.destination = a2.id " + "inner join locality l1 on a1.locality_id = l1.id "
			+ "inner join locality l2 on a2.locality_id = l2.id " + "inner join country c1 on l1.country_id = c1.id "
			+ "inner join country c2 on l2.country_id = c2.id " + "inner join truckload t on truckload_id = t.id "
			+ "where l.id = ?;";
	private static final String ALL_SELECT_QUERY = "select r.id, r.transit_conditions, "
			+ "a1.details, a1.id, a2.details, a2.id, "
			+ "l1.name, l1.id, l2.name, l2.id,  c1.name, c1.id, c2.name, c2.id, "
			+ "t.id, t.truck_type, t.load_units, t.weight, t.name, ln.id " + "from line ln "
			+ "inner join route r on ln.route_line = r.id " + "inner join address a1 on r.departure = a1.id "
			+ "inner join address a2 on r.destination = a2.id " + "inner join locality l1 on a1.locality_id = l1.id "
			+ "inner join locality l2 on a2.locality_id = l2.id " + "inner join country c1 on l1.country_id = c1.id "
			+ "inner join country c2 on l2.country_id = c2.id " + "inner join truckload t on ln.truckload_id = t.id;";

	private static final String CUSTOMER_RELATIVE_SELECTION = "select r.id, r.transit_conditions, a1.details, "
			+ "a1.id, a2.details, a2.id, l1.name, l1.id, l2.name, l2.id, c1.name, c1.id, c2.name, c2.id, "
			+ "trl.id, trl.truck_type, trl.load_units, trl.weight, trl.name, l.id from line l "
			+ "inner join route r on r.id  = l.route_line inner join address a1 on a1.id = r.departure "
			+ "inner join address a2 on a2.id = r.destination inner join locality l1 on a1.locality_id = l1.id "
			+ "inner join locality l2 on a2.locality_id = l2.id inner join country c1 on c1.id = l1.country_id "
			+ "inner join country c2 on c2.id = l2.country_id inner join truckload trl on trl.id = l.truckload_id "
			+ "inner join tender_has_line thl on thl.line_id = l.id "
			+ "inner join tender tdr on thl.tender_id = tdr.id where tdr.customer_id = ? union "
			+ "select r.id, r.transit_conditions, a1.details, "
			+ "a1.id, a2.details, a2.id, l1.name, l1.id, l2.name, l2.id, c1.name, c1.id, c2.name, c2.id, "
			+ "trl.id, trl.truck_type, trl.load_units, trl.weight, trl.name, l.id from line l "
			+ "inner join route r on r.id  = l.route_line inner join address a1 on a1.id = r.departure "
			+ "inner join address a2 on a2.id = r.destination inner join locality l1 on a1.locality_id = l1.id "
			+ "inner join locality l2 on a2.locality_id = l2.id inner join country c1 on c1.id = l1.country_id "
			+ "inner join country c2 on c2.id = l2.country_id inner join truckload trl on trl.id = l.truckload_id "
			+ "inner join transport_order tror on tror.line = l.id where tror.customer_id = ?;";

	private static final String UNUSED_LINES_SELECTION = "select r.id as routeId, r.transit_conditions, a1.details, a1.id as startAddressId, "
			+ "a2.details, a2.id as destinationAddressId, l1.name, l1.id as startLocalityId, l2.name, l2.id as destinationLocalityId, "
			+ "c1.name, c1.id as startCountryId, c2.name, c2.id as destinationCountryId, trl.id as truckloadId, trl.truck_type, "
			+ "trl.load_units, trl.weight, trl.name, l.id as lineId from line l inner join route r on r.id  = l.route_line "
			+ "inner join address a1 on a1.id = r.departure inner join address a2 on a2.id = r.destination "
			+ "inner join locality l1 on a1.locality_id = l1.id inner join locality l2 on a2.locality_id = l2.id "
			+ "inner join country c1 on c1.id = l1.country_id inner join country c2 on c2.id = l2.country_id "
			+ "inner join truckload trl on trl.id = l.truckload_id left join transport_order tror on tror.line  = l.id "
			+ "left join tender_has_line thl on thl.line_id = l.id where tror.id is null and thl.id is null;";

	private static final String TENDER_RELATIVE_SELECTION = "select r.id, r.transit_conditions, a1.details, a1.id, "
			+ "a2.details, a2.id, l1.name, l1.id, l2.name, l2.id, c1.name, c1.id, c2.name, c2.id, trl.id, trl.truck_type, "
			+ "trl.load_units, trl.weight, trl.name, l.id from line l " + 
			"inner join route r on r.id  = l.route_line inner join address a1 on a1.id = r.departure " + 
			"inner join address a2 on a2.id = r.destination inner join locality l1 on a1.locality_id = l1.id " + 
			"inner join locality l2 on a2.locality_id = l2.id inner join country c1 on c1.id = l1.country_id " + 
			"inner join country c2 on c2.id = l2.country_id inner join truckload trl on trl.id = l.truckload_id " + 
			"inner join tender_has_line thl on thl.line_id = l.id inner join tender tdr on thl.tender_id = tdr.id where tdr.id = ?;";
	

	@Override
	public Set<Line> findByTender(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TENDER_RELATIVE_SELECTION)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveLines(rs);
		}
	}
	
	@Override
	public Set<Line> findUnusedLines() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(UNUSED_LINES_SELECTION)) {
			ResultSet rs = selectStatement.executeQuery();
			return retrieveLines(rs);
		}
	}

	@Override
	public Set<Line> findbyCustomer(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(CUSTOMER_RELATIVE_SELECTION)) {
			selectStatement.setLong(1, id);
			selectStatement.setLong(2, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveLines(rs);
		}
	}

	@Override
	public Long save(Line entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement saveStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			saveStatement.setLong(1, entity.getRoute().getId());
			saveStatement.setLong(2, entity.getTruckload().getId());
			saveStatement.executeUpdate();
			ResultSet rs = saveStatement.getGeneratedKeys();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(Line entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setLong(1, entity.getRoute().getId());
			updateStatement.setLong(2, entity.getTruckload().getId());
			boolean flag = false;
			int rowsCount = updateStatement.executeUpdate();
			if (rowsCount > 0) {
				flag = true;
			}
			return flag;
		}
	}

	@Override
	public boolean delete(Line entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			boolean flag = false;
			int rowsCount = deleteStatement.executeUpdate();
			if (rowsCount > 0) {
				flag = true;
			}
			return flag;
		}
	}

	@Override
	public Line getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveLines(rs).stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<Line> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			return retrieveLines(rs).stream().collect(Collectors.toList());
		}
	}

	private Set<Line> retrieveLines(ResultSet rs) throws SQLException {
		Set<Line> list = new HashSet<>();
		while (rs.next()) {
			int i = 1;
			Line line = new Line();
			Address start = new Address();
			Address destination = new Address();
			Country country1 = new Country();
			Country country2 = new Country();
			Route route = new Route();
			Locality locality1 = new Locality();
			Locality locality2 = new Locality();
			Truckload truckload = new Truckload();
			route.setId(rs.getLong(i++));
			route.setConditions(Route.Conditions.getCondition(rs.getString(i++)));
			start.setDetails(rs.getString(i++));
			start.setId(rs.getLong(i++));
			destination.setDetails(rs.getString(i++));
			destination.setId(rs.getLong(i++));
			locality1.setName(rs.getString(i++));
			locality1.setId(rs.getLong(i++));
			locality2.setName(rs.getString(i++));
			locality2.setId(rs.getLong(i++));
			country1.setName(rs.getString(i++));
			country1.setId(rs.getLong(i++));
			country2.setName(rs.getString(i++));
			country2.setId(rs.getLong(i++));
			truckload.setId(rs.getLong(i++));
			truckload.setTruckType(Truckload.TruckType.getType(rs.getString(i++)));
			truckload.setLoadUnits(rs.getInt(i++));
			truckload.setWeight(rs.getDouble(i++));
			truckload.setName(rs.getString(i++));
			line.setId(rs.getLong(i));
			line.setTruckload(truckload);
			locality1.setCountry(country1);
			locality2.setCountry(country2);
			start.setLocality(locality1);
			destination.setLocality(locality2);
			route.setStart(start);
			route.setDestination(destination);
			line.setRoute(route);
			list.add(line);
		}
		rs.close();
		return list;
	}
}
