package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Contract;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class ContractDAOImpl implements ContractDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into contract (tsp_id, customer_id, signed) values (?, ?, ?);";
	private static final String UPDATE_QUERY = "update contract set tsp_id = ?, customer_id = ?, signed = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from contract where id = ?;";
	private static final String ID_SELECT_QUERY = "select c.id, c.tsp_id, c.customer_id, c.signed, tsp.name, tsp.role, cus.name, cus.role"
			+ "from conctract c inner join user tsp on tsp.id = c.tsp_id inner join user cus on cus.id = c.customer_id "
			+ "where c.id = ?;";
	private static final String ALL_SELECT_QUERY = "select c.id, c.tsp_id, c.customer_id,  c.signed, tsp.name, tsp.role, cus.name, cus.role "
			+ "from contract c inner join user tsp on tsp.id = c.tsp_id inner join user cus on cus.id = c.customer_id;";
	private static final String PARTIES_SELECT_QUERY = "select c.id, c.tsp_id, c.customer_id,  c.signed, tsp.name, tsp.role, cus.name, cus.role "
			+ "from contract c inner join user tsp on tsp.id = c.tsp_id inner join user cus on cus.id = c.customer_id where c.tsp_id = ? and c.customer_id = ?;";

	@Override
	public Contract getContract(long tspId, long customerId) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(PARTIES_SELECT_QUERY)){
			selectStatement.setLong(1, tspId);
			selectStatement.setLong(2, customerId);
			ResultSet rs = selectStatement.executeQuery();
			Contract contract = retrieveContracts(rs).stream().findFirst()
					.orElse(null);
			return contract;
		}
	}	
	
	@Override
	public Long save(Contract entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setLong(1, entity.getTsp().getId());
			insertStatement.setLong(2, entity.getCustomer().getId());
			insertStatement.setDate(3, Date.valueOf(entity.getSigningDate()));
			ResultSet rs = insertStatement.executeQuery();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(Contract entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			updateStatement.setLong(1, entity.getTsp().getId());
			updateStatement.setLong(2, entity.getCustomer().getId());
			updateStatement.setDate(3, Date.valueOf(entity.getSigningDate()));
			updateStatement.setLong(4, entity.getId());
			int rowCount = updateStatement.executeUpdate();
			return rowCount > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(Contract entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			int rowCount = deleteStatement.executeUpdate();
			return rowCount > 0 ? true : false;
		}
	}

	@Override
	public Contract getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			Contract contract = retrieveContracts(rs).stream().findFirst()
					.orElse(null);
			return contract;
		}
	}

	@Override
	public Collection<Contract> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			List<Contract> contracts = retrieveContracts(rs);
			return contracts;
		}
	}

	private List<Contract> retrieveContracts(ResultSet rs) throws SQLException {
		List<Contract> list = new ArrayList<>();
		while (rs.next()) {
			int i=1;
			Contract contract = new Contract();
			contract.setId(rs.getLong(i++));
			Long tspId = rs.getLong(i++);
			Long customerId = rs.getLong(i++);
			contract.setSigningDate(rs.getDate(i++).toLocalDate());
			if (tspId.equals(customerId)) {
				throw new SQLException("Contract is not possible between parties when both are the same company!");
			}
			UserEntity tsp = new UserEntity();
			UserEntity customer = new UserEntity();
			tsp.setId(tspId);
			customer.setId(customerId);
			tsp.setName(rs.getString(i++));
			tsp.setRole(UserEntity.Roles.valueOf(rs.getString(i++)));
			customer.setName(rs.getString(i++));
			customer.setRole(UserEntity.Roles.valueOf(rs.getString(i)));
		}
		rs.close();
		return list;
	}
}
