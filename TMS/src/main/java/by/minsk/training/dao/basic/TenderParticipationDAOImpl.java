package by.minsk.training.dao.basic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TenderParticipationDAOImpl implements TenderParticipationDAO {

	private ConnectionManager connectionManager;

	private static final String SAVE_QUERY = "insert into tender_participation (tsp_id, tender_id, feedback_date, invitation_date) values (?,?,?,?);";
	private static final String UPDATE_QUERY = "update tender_participation set tsp_id = ?, tender_id = ?, feedback_date = ?, invitation_date = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from tender_participation where id = ?;";
	private static final String ID_SELECT_QUERY = "select tp.id, tp.tsp_id, tp.tender_id, tp.feedback_date, tp.invitation_date,  u.registration_date, "
			+ "u.name, u.role, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, t.release_date, "
			+ "t.set_date, t.customer_id, t.title from tender_participation tp inner join user u on tp.tsp_id = u.id "
			+ "inner join tender t on t.id = tp.tender_id where tp.id = ?;";
	private static final String ALL_SELECT_QUERY = "select tp.id, tp.tsp_id, tp.tender_id, tp.feedback_date, tp.invitation_date,  u.registration_date, "
			+ "u.name, u.role, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, t.release_date, "
			+ "t.set_date, t.customer_id, t.title from tender_participation tp inner join user u on tp.tsp_id = u.id "
			+ "inner join tender t on t.id = tp.tender_id;";
	private static final String TSP_TENDER_SELECT_QUERY = "select tp.id, tp.tsp_id, tp.tender_id, tp.feedback_date, tp.invitation_date,  u.registration_date, "
			+ "u.name, u.role, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, t.release_date, "
			+ "t.set_date, t.customer_id, t.title from tender_participation tp inner join user u on tp.tsp_id = u.id "
			+ "inner join tender t on t.id = tp.tender_id where tp.tsp_id = ? and tp.tender_id = ?;";
	private static final String TSP_CUSTOMER_SELECT_QUERY = "select tp.id, tp.tsp_id, tp.tender_id, tp.feedback_date, tp.invitation_date,  u.registration_date, "
			+ "u.name, u.role, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, t.release_date, t.set_date, t.customer_id, "
			+ "t.title from tender_participation tp inner join user u on tp.tsp_id = u.id inner join tender t on t.id = tp.tender_id "
			+ "where u.id = ? and t.customer_id =?;";
	private static final String TSP_SELECT_QUERY = "select tp.id, tp.tsp_id, tp.tender_id, tp.feedback_date, tp.invitation_date,  u.registration_date, "
			+ "u.name, u.role, t.state, t.estimated_quantity, t.tender_end, t.tender_start, t.competition_deadline, t.release_date, "
			+ "t.set_date, t.customer_id, t.title from tender_participation tp inner join user u on tp.tsp_id = u.id "
			+ "inner join tender t on t.id = tp.tender_id where tp.tsp_id = ?;";

	@Override
	public Set<TenderParticipation> getTspParticipations(Long id) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_SELECT_QUERY)){
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveParticipations(rs);
		}
	}
		
	@Override
	public Set<TenderParticipation> getAllOfTspCustomerPair(long tspId, long customerId) throws SQLException {
		try(Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_CUSTOMER_SELECT_QUERY)){
			selectStatement.setLong(1, tspId);
			selectStatement.setLong(2, customerId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveParticipations(rs);
		}
	}	
	
	@Override
	public TenderParticipation getParticipation(long tspId, long tenderId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(TSP_TENDER_SELECT_QUERY)) {
			selectStatement.setLong(1, tspId);
			selectStatement.setLong(2, tenderId);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveParticipations(rs).stream().findFirst().orElse(null);
		}
	}

	@Override
	public Long save(TenderParticipation entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(SAVE_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			insertStatement.setLong(1, entity.getTsp().getId());
			insertStatement.setLong(2, entity.getTender().getId());
			insertStatement.setDate(3, Date.valueOf(entity.getFeedback()));
			insertStatement.setDate(4, Date.valueOf(entity.getInvitation()));
			ResultSet rs = insertStatement.executeQuery();
			Long id = null;
			while (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			entity.setId(id);
			return id;
		}
	}

	@Override
	public boolean update(TenderParticipation entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY)) {
			int i = 1;
			updateStatement.setLong(i++, entity.getTsp().getId());
			updateStatement.setLong(i++, entity.getTender().getId());
			updateStatement.setDate(i++, Date.valueOf(entity.getFeedback()));
			updateStatement.setDate(i++, Date.valueOf(entity.getInvitation()));
			updateStatement.setLong(i, entity.getId());
			return updateStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public boolean delete(TenderParticipation entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY)) {
			deleteStatement.setLong(1, entity.getId());
			return deleteStatement.executeUpdate() > 0 ? true : false;
		}
	}

	@Override
	public TenderParticipation getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ID_SELECT_QUERY)) {
			selectStatement.setLong(1, id);
			ResultSet rs = selectStatement.executeQuery();
			return retrieveParticipations(rs).stream().findFirst().orElse(null);
		}
	}

	@Override
	public Collection<TenderParticipation> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(ALL_SELECT_QUERY)) {
			ResultSet rs = selectStatement.executeQuery();
			return retrieveParticipations(rs);
		}
	}

	private Set<TenderParticipation> retrieveParticipations(ResultSet rs) throws SQLException {
		Set<TenderParticipation> participations = new HashSet<>();
		while (rs.next()) {
			int i = 1;
			TenderParticipation participation = new TenderParticipation();
			UserEntity tsp = new UserEntity();
			Tender tender = new Tender();
			UserEntity tenderCustomer = new UserEntity();
			participation.setId(rs.getLong(i++));
			tsp.setId(rs.getLong(i++));
			participation.setFeedback(rs.getDate(i++).toLocalDate());
			participation.setInvitation(rs.getDate(i++).toLocalDate());
			tsp.setRegistrationDate(rs.getDate(i++).toLocalDate());
			tsp.setName(rs.getString(i++));
			tsp.setRole(UserEntity.Roles.valueOf(rs.getString(i++)));
			tender.setState(Tender.State.valueOf(rs.getString(i++)));
			tender.setEstimatedQuantity(rs.getInt(i++));
			tender.setTenderEnd(rs.getDate(i++).toLocalDate());
			tender.setTenderStart(rs.getDate(i++).toLocalDate());
			tender.setCompetitionDeadline(rs.getDate(i++).toLocalDate());
			tender.setReleaseDate(rs.getDate(i++).toLocalDate());
			tender.setSetDate(rs.getDate(i++).toLocalDate());
			tenderCustomer.setId(rs.getLong(i++));
			tender.setTitle(rs.getString(i));
			tender.setCustomer(tenderCustomer);
			participation.setTender(tender);
			participation.setTsp(tsp);
			participations.add(participation);
		}
		rs.close();
		return participations;
	}
}
