package by.minsk.training.service.notification;

import java.util.function.Function;

@FunctionalInterface
public interface HandlingExceptionFunction<T, R, E extends Throwable> {
	R apply(T r) throws E;

	static <T, R, E extends Throwable> Function <T, R> check(HandlingExceptionFunction<T,R,E> f) {
		return r -> {
			try {
				return f.apply(r);
			} catch (Throwable e) {
				throw new RuntimeException(e);
			}
		};
	}
}
