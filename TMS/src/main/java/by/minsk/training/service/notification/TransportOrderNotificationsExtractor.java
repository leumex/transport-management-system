package by.minsk.training.service.notification;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.Contract;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.entity.TspPerformance.OrderFootprint;
import by.minsk.training.entity.TspPerformance.OrderFootprint.State;
import by.minsk.training.entity.TspPerformance.TspResponse;
import by.minsk.training.service.BidsService;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.service.TspPerformanceService;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import by.minsk.training.user.UserService;
import lombok.AllArgsConstructor;

@Bean(name = "fromTransportOrdersExtractor")
@AllArgsConstructor
public class TransportOrderNotificationsExtractor implements NotificationExtractor {

	private BidsService bidsService;
	private TransportOrderService transportOrderService;
	private TspPerformanceService tspPerformanceService;
	private SpotOrderService spotOrderService;
	private UserService userService;

	@Override
	public Set<UserNotification> extractNotifications(UserEntity user) throws ServiceException {
		UserEntity.Roles role = user.getRole();
		Set<TspPerformance> assignments;
		Set<SpotTransportOrder> spots;
		if (role == Roles.TRANSPORT_SERVICE_PROVIDER) {
			assignments = tspPerformanceService.findUncompletedAssignments(user);
			spots = spotOrderService.retrieveTspAvailableSpotAuctions(user);
		} else if (role == Roles.CUSTOMER) {
			Set<TransportOrder> uncompletedOrders = transportOrderService.selectUncompletedOrder(user);
			assignments = uncompletedOrders.stream().map(order -> {
				try {
					return transportOrderService.getLatestAssignment(user, order);
				} catch (ServiceException e1) {
					throw new RuntimeException(e1);
				}
			}).filter(assignment -> assignment != null).collect(Collectors.toSet());
			spots = uncompletedOrders.stream().filter(order -> order.getState() == TransportOrder.State.ANNOUNCED)
					.map(order -> {
						try {
							return spotOrderService.getSpotOrder(order);
						} catch (ServiceException e) {
							throw new RuntimeException(e);
						}
					}).collect(Collectors.toSet());
		} else
			return null;
		Set<UserNotification> notifications = new HashSet<>();
		if (assignments != null && !assignments.isEmpty()) {
			notifications.addAll(
					assignments.stream().flatMap(assignment -> extractBasicNotifications(user, assignment).stream())
							.collect(Collectors.toSet()));
		}
		notifications.addAll(spots.stream().flatMap(spot -> {
			try {
				return extractSpotNotifications(spot, user).stream();
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}
		}).collect(Collectors.toSet()));
		return notifications;
	}

	private Set<UserNotification> extractBasicNotifications(UserEntity entity, TspPerformance assignment) {
		UserEntity.Roles role = entity.getRole();
		Set<UserNotification> notifications = new HashSet<>();
		if (role == UserEntity.Roles.CUSTOMER) {
			notifications.add(checkOrderDeclining(assignment));
			notifications.add(checkAssignmentResponse(assignment));
			notifications.add(checkTruckPlates(assignment));
			notifications.add(checkArrivalForLoading(assignment));
		} else if (role == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
			notifications.add(checkOrderRecall(assignment));
			notifications.add(checkNewOrderEmergence(assignment));
			notifications.add(checkTrackingEvents(assignment));
		}
		return notifications.stream().filter(n -> n != null).collect(Collectors.toSet());
	}

	/*
	 * collects bound notifications of a transport order that has been placed on
	 * spot auction, that are intended for current application user
	 */
	private Set<UserNotification> extractSpotNotifications(SpotTransportOrder spotOrder, UserEntity entity)
			throws ServiceException {
		Set<UserNotification> notifications = new HashSet<>();
		if (entity.getRole() == UserEntity.Roles.CUSTOMER) {
			notifications.add(checkSpotOrderDeadline(spotOrder, entity));
		} else if (entity.getRole() == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
			notifications.add(checkNewSpotOrder(spotOrder, entity));
		}
		return notifications.stream().filter(n -> n != null).collect(Collectors.toSet());
	}

	private UserNotification checkOrderDeclining(TspPerformance assignment) {
		TransportOrder order = assignment.getTransportOrder();
		UserEntity customer = order.getTender() == null ? order.getCustomer() : order.getTender().getCustomer();
		OrderFootprint.State orderFootprintState = assignment.getOrderFootprint().getState();
		return (order.getState() == TransportOrder.State.ASSIGNED & orderFootprintState == State.ASSIGNED
				&& assignment.getTspResponse() == TspPerformance.TspResponse.DECLINED)
						? new UserNotification(customer.getId(), Alert.TO_REJECTION, order.getId(),
								"Transport order has been declined by TSP")
						: null;
	}

	private UserNotification checkOrderRecall(TspPerformance assignment) {
		TransportOrder order = assignment.getTransportOrder();
		OrderFootprint.State orderFootprintState = assignment.getOrderFootprint().getState();
		return (assignment.getTspResponse() == TspResponse.ACCEPTED
				&& (orderFootprintState == State.CANCELLED || orderFootprintState == State.INTERRUPTED))
						? new UserNotification(assignment.getTsp().getId(), Alert.TO_RECALL, order.getId(),
								assignment.getComment())
						: null;
	}

	private UserNotification checkAssignmentResponse(TspPerformance assignment) {
		LocalDateTime presentTime = LocalDateTime.now();
		LocalDateTime assigmentTime = assignment.getNotified();
		Duration duration = Duration.between(assigmentTime, presentTime);
		long seconds = duration.getSeconds();
		long hours = seconds / 3600L;
		TransportOrder order = assignment.getTransportOrder();
		OrderFootprint.State orderFootprintState = assignment.getOrderFootprint().getState();
		UserNotification notification = (order.getState() == TransportOrder.State.ASSIGNED
				& orderFootprintState == State.ASSIGNED
				&& assignment.getTspResponse() == TspPerformance.TspResponse.OUTSTANDING && hours >= 2)
						? new UserNotification(order.getCustomer().getId(), Alert.TO_ASSIGNMENT_RESPONSE, order.getId(),
								"2 hours have passed since the order has been assigned to the TSP")
						: null;						
		return notification;
	}

	private UserNotification checkTruckPlates(TspPerformance assignment) {
		TransportOrder order = assignment.getTransportOrder();
		LocalDate loadingDate = order.getLoadingDate();
		LocalDateTime loadingTime = LocalDateTime.of(loadingDate, LocalTime.of(07, 00));
		LocalDateTime presentTime = LocalDateTime.now();
		Duration duration = Duration.between(presentTime, loadingTime);
		long hours = duration.getSeconds() / 3600;
		String truckPlates = assignment.getTruckPlates();
		OrderFootprint.State orderFootprintState = assignment.getOrderFootprint().getState();
		return (order.getState() == TransportOrder.State.ACCEPTED
				&& assignment.getTspResponse() == TspPerformance.TspResponse.ACCEPTED
				&& (orderFootprintState == State.ACCEPTED || orderFootprintState == State.IN_ACTION
						|| orderFootprintState == State.COMPLETED)
				&& hours <= 12 && (truckPlates == null | truckPlates.length() < 8))
						? new UserNotification(order.getCustomer().getId(), Alert.TRUCK_PLATES, order.getId(),
								"Truck plates are missing")
						: null;
	}

	private UserNotification checkArrivalForLoading(TspPerformance assignment) {
		TransportOrder order = assignment.getTransportOrder();
		OrderFootprint.State orderFootprintState = assignment.getOrderFootprint().getState();
		LocalDate loadingDate = order.getLoadingDate();
		LocalDateTime loadingTime = LocalDateTime.of(loadingDate, LocalTime.of(07, 00));
		LocalDateTime presentTime = LocalDateTime.now();
		Duration duration = Duration.between(loadingTime, presentTime);
		long hours = duration.getSeconds() / 3600;
		LocalDateTime arrival = assignment.getArrivedForLoading();
		return (order.getState() == TransportOrder.State.ACCEPTED & orderFootprintState == State.ACCEPTED
				&& assignment.getTspResponse() == TspResponse.ACCEPTED && hours >= 12 && arrival == null)
						? new UserNotification(order.getCustomer().getId(), Alert.ARRIVAL_FOR_LOADNING, order.getId(),
								"Truck arrival for loading has not been registered so far")
						: null;
	}

	private UserNotification checkNewOrderEmergence(TspPerformance assignment) {
		TransportOrder order = assignment.getTransportOrder();
		OrderFootprint.State orderFootprintState = assignment.getOrderFootprint().getState();
		return (order.getState() == TransportOrder.State.ASSIGNED & orderFootprintState == State.ASSIGNED
				&& assignment.getTspResponse() == TspPerformance.TspResponse.OUTSTANDING)
						? new UserNotification(assignment.getTsp().getId(), Alert.NEW_TO, order.getId(),
								"New order has been assigned to you")
						: null;
	}

	private UserNotification checkTrackingEvents(TspPerformance assignment) {
		TransportOrder order = assignment.getTransportOrder();
		OrderFootprint.State orderFootprintState = assignment.getOrderFootprint().getState();
		boolean orderIsExecuted = orderFootprintState == State.IN_ACTION
				& order.getState() == TransportOrder.State.IN_ACTION
				&& assignment.getTspResponse() == TspResponse.ACCEPTED;
		boolean leftLoadingEventAbsence = assignment.getLeftLoading() == null;
		LocalDate loadingDate = order.getLoadingDate();
		LocalDateTime loadingTime = LocalDateTime.of(loadingDate, LocalTime.of(07, 00));
		LocalDateTime presentTime = LocalDateTime.now();
		Duration duration = Duration.between(loadingTime, presentTime);
		long hours = duration.getSeconds() / 3600;
		boolean leftLoading72hrsAgo = hours >= 72;
		if (orderIsExecuted && leftLoadingEventAbsence && leftLoading72hrsAgo) {
			return new UserNotification(assignment.getTsp().getId(), Alert.TO_TRACKING, order.getId(),
					"Dispatch from loading point record is missing");
		} else {
			boolean arrivedUnloadingEventAbsence = assignment.getArrivedForUnloading() == null;
			LocalDate shouldHaveDelivered = loadingDate.plusDays(order.getDeliveryTerm() + 5);
			LocalDateTime scheduledDeliveyTime = LocalDateTime.of(shouldHaveDelivered, LocalTime.of(07, 00));
			duration = Duration.between(scheduledDeliveyTime, presentTime);
			hours = duration.getSeconds() / 3600;
			boolean shouldHaveDelivered72hrsAgo = hours >= 72;
			if (orderIsExecuted && arrivedUnloadingEventAbsence && shouldHaveDelivered72hrsAgo) {
				return new UserNotification(assignment.getTsp().getId(), Alert.TO_TRACKING, order.getId(),
						"Arrival at destination record is missing");
			} else {
				boolean leftUnloadingEventAbsence = assignment.getLeftUnloading() == null;
				return (orderIsExecuted && leftUnloadingEventAbsence && shouldHaveDelivered72hrsAgo)
						? new UserNotification(assignment.getTsp().getId(), Alert.TO_TRACKING, order.getId(),
								"Completed delivery at destination record is missing")
						: null;
			}
		}
	}

	private UserNotification checkSpotOrderDeadline(SpotTransportOrder spotOrder, UserEntity user)
			throws ServiceException {
		TransportOrder order = spotOrder.getTransportOrder();
		TspPerformance assignment = transportOrderService.getLatestAssignment(user, order);
		boolean isAssignedAfterSpotDeadline = assignment.getNotified().isAfter(spotOrder.getSpotDeadline())
				&& assignment.getOrderFootprint().getCustomerId() != 0;
		return (order.getState() == TransportOrder.State.ANNOUNCED
				& spotOrder.getSpotDeadline().isAfter(LocalDateTime.now()) && !isAssignedAfterSpotDeadline)
						? new UserNotification(order.getCustomer().getId(), Alert.SPOT_DEADLINE, order.getId(),
								"Spot contest is over, time to assign the order")
						: null;
	}

	private UserNotification checkNewSpotOrder(SpotTransportOrder spotOrder, UserEntity tsp) throws ServiceException {
		TransportOrder order = spotOrder.getTransportOrder();
		Contract contract = userService.findContract(tsp, order.getCustomer());
		Bid bid = bidsService.retrieveBid(spotOrder, tsp);
		return (order.getState() == TransportOrder.State.ANNOUNCED && contract == null && bid == null)
				? new UserNotification(tsp.getId(), Alert.NEW_SPOT_TO, order.getId(), "Check new spot order, please")
				: null;
	}
}
