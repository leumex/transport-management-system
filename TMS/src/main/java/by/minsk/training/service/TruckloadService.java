package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.Truckload;

public interface TruckloadService {
	
	Set<String> getTruckTypes();

	Set<Truckload> getAll() throws ServiceException;

	void persist(Truckload truckload) throws ServiceException;
	
	String deliverRepresentation (Truckload truckload);

}
