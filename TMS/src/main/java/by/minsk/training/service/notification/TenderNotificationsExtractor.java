package by.minsk.training.service.notification;

import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLine;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TenderService;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean(name = "fromTendersExtractor")
@AllArgsConstructor
public class TenderNotificationsExtractor implements NotificationExtractor {

	private static final Logger logger = LogManager.getLogger(TenderNotificationsExtractor.class);

	private TenderService tenderService;

	/*
	 * collects notifications bound to a tender that are intended for current
	 * application user
	 */
	@Override
	public Set<UserNotification> extractNotifications(UserEntity entity) throws ServiceException {
		UserEntity.Roles role = entity.getRole();
		if (role == Roles.ADMIN) {
			return null;
		}
		Set<Tender> newTenders = tenderService.detectNewReleasedTenders(entity);
		return newTenders != null ? newTenders.stream().map(tender -> {
			if (role == Roles.CUSTOMER) {
				try {
					return checkTenderDeadline(tender);
				} catch (ServiceException e) {
					logger.error(e.getMessage());
					throw new RuntimeException(e);
				}
			} else {
				try {
					return (checkNewTenderInvitation(tenderService.getTenderParticipation(tender, entity)));
				} catch (ServiceException e) {
					logger.error(e.getMessage());
					throw new RuntimeException(e);
				}
			}
		}).filter(n -> n != null).collect(Collectors.toSet()) : null;
	}

	private UserNotification checkNewTenderInvitation(TenderParticipation participation) throws ServiceException {
		Set<TenderLineQuery> lineQueries = tenderService.getQueries(participation);
		return (lineQueries == null || lineQueries.isEmpty())
				? new UserNotification(participation.getTsp().getId(), Alert.NEW_TENDER,
						participation.getTender().getId(), "new tender has been recently opened for quotation")
				: null;
	}

	private UserNotification checkTenderDeadline(Tender tender) throws ServiceException {
		Set<TenderLine> lines = tenderService.getTenderLines(tender);
		return lines.stream().peek(line -> {
			try {
				tenderService.bindLineQueries(line);
			} catch (ServiceException e) {
				logger.error(e.getMessage());
				throw new RuntimeException(e);
			}
		}).anyMatch(line -> {
			try {
				return tenderService.calculateAssignedLineVolume(line) < 100;
			} catch (ServiceException e) {
				logger.error(e.getMessage());
				throw new RuntimeException(e);
			}
		}) ? new UserNotification(tender.getCustomer().getId(), Alert.TENDER_COMPETITION_DEADLINE, tender.getId(),
				"there's unassigned volume of some tender line(s)") : null;
	}
}
