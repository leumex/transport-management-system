package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.Address;

public interface AddressService {
	Set<Address> getAll() throws ServiceException;

	Set<Address> getByLocality(String localityId) throws ServiceException;

	Address get(Long newLineDeliveryAddress) throws ServiceException;

	void persist(Address newAddress) throws ServiceException;
}
