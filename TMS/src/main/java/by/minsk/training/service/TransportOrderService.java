package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.ToolSet.KeyPerformanceIndicators;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.user.UserEntity;

public interface TransportOrderService {
 
	TransportOrder restoreOrder(TspPerformance performance) throws ServiceException;

	Set<TransportOrder> gatherOrders(UserEntity user) throws ServiceException;

	TspPerformance getLatestAssignment(UserEntity user, TransportOrder transportOrder) throws ServiceException;

	int getTspEverExecutedOrdersNumber(UserEntity tsp) throws ServiceException;

	int getCustomerEverExecutedOrdersNumber(UserEntity customer) throws ServiceException;

	KeyPerformanceIndicators calculateTSPGeneralKPI(UserEntity tsp) throws ServiceException;
	
	/*
	 * If transport order has state in (CREATED, ASSIGNED) - rate value is indefinite for any user; 
	 * if transport order has state WITHDRAWN - rate is indefinite for CUSTOMER or ADMIN user, but is defined by latest assignment - for TSP user;
	 * In any else case rate is determined by latest assignment and correspondent bid (if order is of a spot scope) or correspondent tender line query (if order is of a tender scope)
	 */
	double defineOrderRate (UserEntity user, TransportOrder order) throws ServiceException;	
	
	TransportOrder findOrder(long orderId) throws ServiceException;

	boolean сommitOrderAcceptance(long orderId, UserEntity user)throws ServiceException;

	boolean сommitOrderRejection(long orderId, UserEntity user)throws ServiceException;

	boolean commitOrderRecall(UserEntity user, long orderId) throws ServiceException;

	boolean trackingEventsToRender(UserEntity user, TspPerformance assignment);

	boolean updateOrder(TransportOrder order) throws ServiceException;
	
	Set<TransportOrder> selectUncompletedOrder(UserEntity customer) throws ServiceException;

    Set<TransportOrder> findTopicalOrders (UserEntity user) throws ServiceException;

	long persist(TransportOrder order) throws ServiceException;
}
