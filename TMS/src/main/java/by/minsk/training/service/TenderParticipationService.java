package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.Line;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.user.UserEntity;

public interface TenderParticipationService {
	
	double findRate(Tender tender, Line line, UserEntity tsp) throws ServiceException;

	TenderParticipation findParticipation(UserEntity tsp, Tender tender) throws ServiceException;
	/*
	 * returns all tender participations the Tsp has made on the Customer tender
	 * invitations
	 */
	Set<TenderParticipation> summarizeTspToCustomerTenderResponse(UserEntity tsp, UserEntity customer) throws ServiceException;

	/* returns tender participations of a particular tsp */
	Set<TenderParticipation> findTspTenderParticipations(UserEntity tsp) throws ServiceException;
	/*
	 * returns number of transport orders within a particular tender, of a particular
	 * line, received by a particular tsp
	 */
	int countTenderLineTspAssignedOrders(Tender tender, Line line, UserEntity tsp) throws ServiceException;
	
	/*
	 * returns number of transport orders within a particular tender, of a particular
	 * line, received and subsequently confirmed by a particular tsp
	 */
	int countTenderLineTspAcceptedOrders(Tender tender, Line line, UserEntity tsp) throws ServiceException;

	double findAward(Tender tender, Line line, UserEntity tsp) throws ServiceException;
	
	long sendTenderInvitation (Tender tender, UserEntity tsp) throws ServiceException;
}
