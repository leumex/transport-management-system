package by.minsk.training.service;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;

import by.minsk.training.entity.Line;
import by.minsk.training.entity.Route.Conditions;
import by.minsk.training.entity.Truckload.TruckType;
import by.minsk.training.user.UserEntity;

public interface LineService {
	/* returns set of lines that particular user has ever submitted */ 
	Set<Line> getUserRelativeLines (UserEntity user) throws ServiceException;

	Set<Line> filterByPickupCountry(Set<Line> foundLines, String newLinePickupCountry);

	Set<Line> filterByPickupAddress(Set<Line> foundLines, String newLinePickupAddress);

	Set<Line> filterByDeliveryCountry(Set<Line> foundLines, String newLineDeliveryCountry);

	Set<Line> filterByDeliveryLocality(Set<Line> foundLines, String newLineDeliveryLocality);

	Set<Line> filterByDeliveryAddress(Set<Line> foundLines, String newLineDeliveryAddress);

	Set<Line> filterByCargoName(Set<Line> foundLines, String newLineCargoName);

	Set<Line> filterByLoadUnits(Set<Line> foundLines, Integer newLineLoadUnits);

	Set<Line> filterByCargoWeight(Set<Line> foundLines, Double newLineCargoWeight);

	Set<Line> filterByTruckType(Set<Line> foundLines, TruckType newLineTruckType);

	Set<Line> filterByPickupLocality(Set<Line> foundLines, String newLinePickupLocality);

	Set<Line> filterByRouteConditions(Set<Line> foundLines, Conditions newLineRouteConditions);

	void persist(Line newLine) throws ServiceException;

	Line get(long id) throws ServiceException;
	
	Set<Line> getByTender (long tenderId) throws ServiceException;
	
	Set<Line> sort (Collection <Line> source, Comparator <Line> comparator);

}
