package by.minsk.training.service;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.ToolSet.KeyPerformanceIndicators;
import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.dao.basic.BidDAO;
import by.minsk.training.dao.basic.SpotDAO;
import by.minsk.training.dao.basic.TransportOrderDAO;
import by.minsk.training.dao.basic.TspPerformanceDAO;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.entity.TspPerformance.OrderFootprint;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean
@TransactionSupport
@AllArgsConstructor
public class SpotOrderServiceImpl implements SpotOrderService {

	private static final Logger logger = LogManager.getLogger(SpotOrderServiceImpl.class);
	private SpotDAO spotDAO;
	private TspPerformanceDAO tspPerformanceDAO;
	private TransportOrderDAO transportOrderDAO;
	private BidDAO bidDAO;

	/*
	 * returns true if there's a spot auction over the order, and the order has state CREATED or WITHDRAWN
	 */
	@Override
	public boolean hasOpenSpotAuction(TransportOrder order) throws ServiceException {
		if (order.getTender() != null)
			throw new ServiceException("method is invoked using transport order of some tender scope");
		try {
			SpotTransportOrder spotOrder = spotDAO.getByOrder(order.getId());
			if (spotOrder == null)
				return false;
			return (order.getState().equals(State.CREATED) || order.getState().equals(State.WITHDRAWN));
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/* Only one spot auction can be set over a particular transport order */
	@Override
	public SpotTransportOrder getSpotOrder(TransportOrder order) throws ServiceException {
		try {
			return spotDAO.getByOrder(order.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(
					"During retrieving spot correspondent to transport order id " + order.getId() + e);
		}
	}

	@Override
	public int countTspSpotExecutedOrders(UserEntity tsp) throws ServiceException {
		try {
			return spotDAO.getTspSpotExecutedOrders(tsp.getId()).size();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public KeyPerformanceIndicators calculateTspSpotOrdersExecutionKPI(UserEntity tsp) throws ServiceException {
		try {
			Set<TspPerformance> receivedSpots = tspPerformanceDAO.selectTspSpotOrdersAssignments(tsp.getId());
			int assignedSpotOrdersNumber = receivedSpots.stream()
					.filter(assignment -> assignment.getOrderFootprint().getState() != OrderFootprint.State.CANCELLED)
					.map(assignment -> assignment.getTransportOrder()).collect(Collectors.toSet()).size();
			Set<TspPerformance> acceptedSpotAssignments = receivedSpots.stream()
					.filter(assignment -> assignment.getTspResponse() == TspPerformance.TspResponse.ACCEPTED
							&& assignment.getOrderFootprint().getState() != OrderFootprint.State.CANCELLED
							&& assignment.getOrderFootprint().getCustomerId() != 0)
					.collect(Collectors.toSet());
			return KeyPerformanceIndicators.calculateKPI(assignedSpotOrdersNumber, acceptedSpotAssignments);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public boolean commitNewSpotAuction(TransportOrder order) throws ServiceException {
		UserEntity customer = order.getCustomer() == null ? order.getTender().getCustomer() : order.getCustomer();
		order.setState(State.ANNOUNCED);
		order.setTender(null);
		order.setCustomer(customer);
		SpotTransportOrder spotOrder = getSpotOrder(order);
		if (spotOrder == null) {
			spotOrder = new SpotTransportOrder();
			spotOrder.setTransportOrder(order);
			spotOrder.setSpotDeadline(LocalDateTime.now().plusHours(4));
			try {
				spotDAO.save(spotOrder);
				return transportOrderDAO.update(order);
			} catch (SQLException e) {
				logger.error(e.getMessage());
				throw new ServiceException(e);
			}
		}
		spotOrder.setSpotDeadline(LocalDateTime.now().plusHours(4));
		try {
			return spotDAO.update(spotOrder) & transportOrderDAO.update(order);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public boolean commitOngoingSpotAbort(TransportOrder order) throws ServiceException {
		order.setState(State.WITHDRAWN);
		try {
			SpotTransportOrder spotOrder = spotDAO.getByOrder(order.getId());
			spotOrder.setSpotDeadline(LocalDateTime.now());
			return spotDAO.update(spotOrder) & transportOrderDAO.update(order);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Bid> obtainRelativeBids(SpotTransportOrder order) throws ServiceException {
		try {
			Set<Bid> bids = bidDAO.getBySpotOrder(order.getId());
			if (bids.isEmpty()) {
				return null;
			} else {
				return bids;
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public String reflectSpotOrderAssignment(SpotTransportOrder order, UserEntity tsp) throws ServiceException {
		try {
			Bid bid = bidDAO.getBySpotOrder(order.getId()).stream().filter(b -> b.getTsp().getId() == tsp.getId())
					.findFirst().orElse(null);
			if (bid == null) {
				throw new ServiceException("If there's no bid, than the method should have not been called");
			}
			TspPerformance assignment = tspPerformanceDAO.selectOrderReferringAssignments(order.getId()).stream()
					.filter(a -> (a.getOrderFootprint().getTenderId() == 0 && a.getTsp().getId() == tsp.getId()))
					.findFirst().orElse(null);
			return assignment == null ? ""
					: new StringBuffer().append(assignment.getNotified()).append(", at rate ").append(bid.getBid())
							.append("; response provided at ").append(assignment.getResponseTime())
							.append("; eventual state is ").append(assignment.getTspResponse().toString()).toString();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<SpotTransportOrder> retrieveTspAvailableSpotAuctions(UserEntity tsp) throws ServiceException {
		if  (!tsp.getRole().equals(Roles.TRANSPORT_SERVICE_PROVIDER)) {
			throw new ServiceException("Method is invoked with UserEntity parameter other than transport service provider!");
		}
		try {
			return spotDAO.getTspOpenSpotOrders(tsp.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}	
}
