package by.minsk.training.service;

import java.sql.SQLException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.basic.AddressDAO;
import by.minsk.training.entity.Address;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class AddressServiceImpl implements AddressService {
	
	private static final Logger logger = LogManager.getLogger(AddressServiceImpl.class);

	private AddressDAO addressDAO;

	@Override
	public Address get(Long newLineDeliveryAddress) throws ServiceException {
		try {
			logger.debug("AddressService#get() is called with the parameter " + newLineDeliveryAddress);
			return addressDAO.getById(newLineDeliveryAddress);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Address> getAll() throws ServiceException {
		try {
			return (Set<Address>) addressDAO.findAll();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Address> getByLocality(String localityId) throws ServiceException {
		Long id;
		try {
			id = Long.parseLong(localityId);
		} catch (NumberFormatException e) {
			return null;
		}
		try {
			return  addressDAO.findByLocalityId(id);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public void persist(Address newAddress) throws ServiceException {
		try {
			addressDAO.save(newAddress);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
}
