package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.Bid;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.user.UserEntity;

public interface BidsService 
{
	
Bid retrieveBid (SpotTransportOrder order, UserEntity tsp) throws ServiceException;

/*
 * selection of bids that have led to the order assignment to a bid maker, when
 * the assignment has been subsequently accepted by the tsp, and order execution
 * has at least started
 */
Set<Bid> findTspSuccessfulBids (UserEntity customer, UserEntity tsp) throws ServiceException;

Set<Bid> findOrderBids(SpotTransportOrder order) throws ServiceException;

Bid find(long bidId) throws ServiceException;

boolean removeBid (Bid bid) throws ServiceException;

long commitNewBid(Bid bid) throws ServiceException;

}
