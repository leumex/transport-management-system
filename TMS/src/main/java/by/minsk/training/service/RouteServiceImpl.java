package by.minsk.training.service;

import java.sql.SQLException;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.dao.basic.RouteDAO;
import by.minsk.training.entity.Route;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
@TransactionSupport
public class RouteServiceImpl implements RouteService {

	private static final Logger logger = LogManager.getLogger(RouteServiceImpl.class);

	private RouteDAO routeDAO;

	@Override
	@Transactional
	public void persist(Route newRoute) throws ServiceException {
		try {
			Route savedRoute = routeDAO.findAll().stream()
					.filter(route -> route.getConditions() == newRoute.getConditions()
							& route.getStart().equals(newRoute.getStart())
							& route.getDestination().equals(newRoute.getDestination()))
					.findFirst().orElse(null);
			if (savedRoute != null) {
				newRoute.setId(savedRoute.getId());
			} else {
				logger.debug("no equal route is found , and new route is about to be persisted in database: " + newRoute.toString());
				routeDAO.save(newRoute);				
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Collection<String> getConditions() {
		return Route.Conditions.getValues();
	}

	@Override
	public String deliverRepresentation(Route route) {
		return new StringBuilder().append(route.getStart().getLocality().getName()).append(", ")
				.append(route.getStart().getLocality().getCountry().getName())
				.append(" - ").append(route.getDestination().getLocality().getName()).append(", ")
				.append(route.getDestination().getLocality().getCountry().getName()).toString();
	}

	@Override
	public String deliverStartAddress(Route route) {
		return new StringBuffer().append(route.getStart().getDetails()).append(", ")
				.append(route.getStart().getLocality().getName()).append(", ")
				.append(route.getStart().getLocality().getCountry().getName())
				.toString();
	}

	@Override
	public String deliverDestinationAddress(Route route) {
		return new StringBuffer().append(route.getDestination().getDetails()).append(", ")
				.append(route.getDestination().getLocality().getName()).append(", ")
				.append(route.getDestination().getLocality().getCountry().getName())
				.toString();
	}
}
