package by.minsk.training.service.notification;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserNotification {
	/* the notification receiver' id*/
	private long targetUser;
	private Alert type;
	/* TransportOrder entity id or Tender entity id - depending on the Alert type*/
	private long referencedObject;
	private String description;
}