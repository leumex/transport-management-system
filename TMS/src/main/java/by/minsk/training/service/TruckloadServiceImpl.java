package by.minsk.training.service;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.dao.basic.TruckloadDAO;
import by.minsk.training.entity.Truckload;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
@TransactionSupport
public class TruckloadServiceImpl implements TruckloadService {

	private static final Logger logger = LogManager.getLogger(TruckloadServiceImpl.class);

	private TruckloadDAO truckloadDAO;

	@Override
	public Set<String> getTruckTypes() {
		return Truckload.TruckType.getValues();
	}

	@Override
	public Set<Truckload> getAll() throws ServiceException {
		try {
			return new HashSet<>(truckloadDAO.findAll());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void persist(Truckload truckload) throws ServiceException {
		try {
			Truckload registeredTruckload = truckloadDAO.findAll().stream()
					.filter(load -> (load.getName().equals(truckload.getName()))
							& (load.getLoadUnits() == truckload.getLoadUnits())
							& (load.getWeight() == truckload.getWeight())
							& (load.getTruckType().equals(truckload.getTruckType())))
					.findFirst().orElse(null);
			if (registeredTruckload == null) {
				truckloadDAO.save(truckload);
			} else {
				truckload.setId(registeredTruckload.getId());
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public String deliverRepresentation(Truckload truckload) {
		return new StringBuilder().append(truckload.getName()).append(", ")
				.append(truckload.getWeight()).append("kg, ").append(truckload.getLoadUnits()).append("units")
				.toString();
	}
}
