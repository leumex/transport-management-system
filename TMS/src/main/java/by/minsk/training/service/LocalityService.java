package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.Locality;

public interface LocalityService {
	Set<Locality> getAll() throws ServiceException;

	Set<Locality> getByCountry(String countryId) throws ServiceException;

	Locality get(Long localityId) throws ServiceException;

	void persist(Locality locality) throws ServiceException;
}
