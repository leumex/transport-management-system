package by.minsk.training.service;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.ToolSet.KeyPerformanceIndicators;
import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.dao.basic.LineDAO;
import by.minsk.training.dao.basic.TenderDAO;
import by.minsk.training.dao.basic.TenderLineDAO;
import by.minsk.training.dao.basic.TenderLineQueryDAO;
import by.minsk.training.dao.basic.TenderParticipationDAO;
import by.minsk.training.dao.basic.TransportOrderDAO;
import by.minsk.training.dao.basic.TspPerformanceDAO;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.Tender.State;
import by.minsk.training.entity.TenderLine;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
@TransactionSupport
public class TenderServiceImpl implements TenderService {

	private static final Logger logger = LogManager.getLogger(TenderServiceImpl.class);

	private TenderDAO tenderDAO;
	private TenderParticipationDAO tenderParticipationDAO;
	private TenderLineDAO tenderLineDAO;
	private TenderLineQueryDAO tenderLineQueryDAO;
	private TransportOrderDAO transportOrderDAO;
	private TspPerformanceDAO tspPerformanceDAO;
	private LineDAO lineDAO;

	@Override
	@Transactional
	public Set<TenderLineQuery> checkValidLineTenderObligations(Line line, UserEntity customer, UserEntity tsp)
			throws ServiceException {
		try {
			Stream<Tender> customerTenders = tenderDAO.getCustomerTenders(customer.getId()).stream()
					.filter(tender -> tender.getTenderEnd().isAfter(LocalDate.now())
							&& tender.getState().equals(Tender.State.IN_ACTION));
			return customerTenders.map(tender -> {
				try {
					return tenderLineQueryDAO.getTenderLineQuery(tender.getId(), line.getId(), tsp.getId());
				} catch (SQLException e) {
					logger.error(e.getMessage());
					throw new RuntimeException(e);
				}
			}).filter(tlquery -> tlquery.getAwardedShare() > 0).collect(Collectors.toSet());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Tender> findCustomerBoundTenders(UserEntity customer) throws ServiceException {
		try {
			return tenderDAO.getCustomerTenders(customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Tender> selectActiveTenders(UserEntity customer) throws ServiceException {
		try {
			return tenderDAO.getCustomerTenders(customer.getId()).stream()
					.filter(tender -> tender.getState() == State.CONTRACTED || tender.getState() == State.IN_ACTION)
					.collect(Collectors.toSet());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public int countImplementedTenderOrders(UserEntity customer) throws ServiceException {
		try {
			return transportOrderDAO.getExecutedTenderOrdersNumber(customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public int countImplementedSpotOrders(UserEntity customer) throws ServiceException {
		try {
			return transportOrderDAO.getExecutedSpotOrdersNumber(customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Tender get(Long id) throws ServiceException {
		try {
			return tenderDAO.getById(id);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void persist(Tender tender, Set<Line> lines) throws ServiceException {
		try {
			tenderDAO.save(tender);
			lines.forEach(line -> {
				TenderLine tenderLine = new TenderLine();
				tenderLine.setTender(tender);
				tenderLine.setLine(line);
				try {
					tenderLineDAO.save(tenderLine);
				} catch (SQLException e) {
					logger.error(e.getMessage());
					throw new RuntimeException(e);
				}
			});
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<TenderLineQuery> getQueries(TenderParticipation participation) throws ServiceException {
		try {
			Set<TenderLine> lines = tenderLineDAO.findAll().stream()
					.filter(line -> line.getTender().getId() == participation.getTender().getId())
					.collect(Collectors.toSet());
			return tenderLineQueryDAO.findAll().stream()
					.filter(q -> q.getParticipation().getId() == participation.getId())
					.filter(q -> lines.stream().anyMatch(line -> q.getTenderLine().getId() == line.getId()))
					.collect(Collectors.toSet());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Tender> gatherTenders(UserEntity user) throws ServiceException {
		UserEntity.Roles role = user.getRole();
		try {
			if (role == UserEntity.Roles.ADMIN) {
				return (Set<Tender>) tenderDAO.findAll();
			} else if (role == UserEntity.Roles.CUSTOMER) {
				return tenderDAO.findAll().stream().filter(tender -> tender.getCustomer().getId() == user.getId())
						.collect(Collectors.toSet());
			} else if (role == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
				Stream<TenderParticipation> participations = tenderParticipationDAO.findAll().stream()
						.filter(participation -> participation.getTsp().getId().equals(user.getId()));
				return tenderDAO.findAll().stream()
						.filter(tender -> participations
								.anyMatch(participation -> participation.getTender().getId() == tender.getId()))
						.collect(Collectors.toSet());
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
		throw new ServiceException("User has incorrect Role value");
	}

	@Override
	public Set<TenderLine> getTenderLines(Tender tender) throws ServiceException {
		try {
			return tenderLineDAO.findAll().stream().filter(line -> line.getTender().getId() == tender.getId())
					.collect(Collectors.toSet());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public void bindLineQueries(TenderLine line) throws ServiceException {
		try {
			Set<TenderLineQuery> tenderQueries = tenderLineQueryDAO.getByTender(line.getTender()).stream()
					.filter(q -> q.getTenderLine().getId() == line.getId()).collect(Collectors.toSet());
			line.setLineQueries(tenderQueries);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public double calculateAssignedLineVolume(TenderLine line) throws ServiceException {
		try {
			return tenderLineQueryDAO.findAll().stream().filter(q -> q.getTenderLine().getId() == line.getId())
					.map(q -> q.getAwardedShare()).mapToDouble(d -> d.doubleValue()).sum();
		} catch (SQLException e) {
			logger.debug(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public TenderParticipation getTenderParticipation(Tender tender, UserEntity entity) throws ServiceException {
		try {
			Set<TenderParticipation> participations = tenderParticipationDAO.findAll().stream()
					.filter(participation -> participation.getTsp().getId() == entity.getId()
							& participation.getTender().getId() == tender.getId())
					.collect(Collectors.toSet());
			if (participations.size() > 1)
				throw new ServiceException(
						"There can be at most one TenderParticipation instance bound to a certain Tsp user and to  a certain tender!");
			return participations.stream().findFirst().orElse(null);
		} catch (SQLException e) {
			logger.debug(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/*
	 * if user is a customer, then all existing tender records are filtered by
	 * customer field and by tender state "RELEASED" value; if user is a tsp, then
	 * all existing tender records are filtered by State "RELEASED" value, then are
	 * checked on if there's a TspParticipation record referred to given tsp user
	 */
	@Override
	public Set<Tender> detectNewReleasedTenders(UserEntity entity) throws ServiceException {
		try {
			logger.debug("authorized user: " + entity.toString());
			UserEntity.Roles role = entity.getRole();
			if (role == UserEntity.Roles.CUSTOMER) {
				return tenderDAO.findAll().stream().filter(tender -> tender.getCustomer().getId() == entity.getId()
						& tender.getState() == Tender.State.RELEASED).collect(Collectors.toSet());
			} else if (role == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
				return tenderDAO.findAll().stream().filter(tender -> tender.getState() == Tender.State.RELEASED)
						.filter(tender -> {
							try {
								return tenderParticipationDAO.findAll().stream()
										.filter(participation -> participation.getTender().getId() == tender.getId())
										.anyMatch(participation -> entity.getId() == participation.getTsp().getId());
							} catch (SQLException e) {
								logger.error(e.getMessage());
								throw new RuntimeException(e);
							}
						}).collect(Collectors.toSet());
			} else
				return null;
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/*
	 * counts transport orders of the given tender scope under following terms :
	 * when tender has state 'CREATED', 'RELEASED' - return estimatedQuantity; when
	 * tender has any other state - return actual quantity of (orders distributed +
	 * orders assigned - in case of Customer user) (quantity of orders distributed +
	 * orders assigned - in case of Admin user) or (quantity of orders received ,
	 * both confirmed and declined - in case of Tsp user)
	 */

	@Override
	public int countOrders(Tender tender, UserEntity user) throws ServiceException {
		Tender.State state = tender.getState();
		if (state == Tender.State.CREATED || state == Tender.State.RELEASED) {
			return tender.getEstimatedQuantity();
		}
		UserEntity.Roles role = user.getRole();
		if (role == UserEntity.Roles.CUSTOMER || role == UserEntity.Roles.ADMIN) {
			try {
				return transportOrderDAO.findAll().stream()
						.filter(order -> order.getTender() != null && order.getTender().getId() == tender.getId())
						.filter(order -> {
							TransportOrder.State orderState = order.getState();
							return orderState == TransportOrder.State.ASSIGNED
									|| orderState == TransportOrder.State.ACCEPTED
									|| orderState == TransportOrder.State.IN_ACTION
									|| orderState == TransportOrder.State.COMPLETED;
						}).collect(Collectors.summingInt(order -> 1));
			} catch (SQLException e) {
				logger.error(e.getMessage());
				throw new ServiceException(e);
			}
		} else if (role == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
			try {
				return (int) tspPerformanceDAO.selectTspOrderAssingments(user.getId()).stream()
						.filter(assignment -> assignment.getOrderFootprint().getTenderId() == tender.getId()).count();
			} catch (SQLException e) {
				logger.error(e.getMessage());
				throw new ServiceException(e);
			}
		} else {
			throw new ServiceException("user has incorrect role value");
		}
	}

	@Override
	public boolean matchTitle(Tender tender, String title) {
		return tender.getTitle().toLowerCase().contains(title.toLowerCase());
	}

	@Override
	public boolean matchReleasePeriod(Tender tender, LocalDate releaseDateFrom, LocalDate releaseDateTo) {
		boolean isAfter = releaseDateFrom != null
				? tender.getReleaseDate().isAfter(releaseDateFrom) || tender.getReleaseDate().equals(releaseDateFrom)
				: true;
		boolean isBefore = releaseDateTo != null
				? tender.getReleaseDate().isBefore(releaseDateTo) || tender.getReleaseDate().equals(releaseDateTo)
				: true;
		return isAfter & isBefore;
	}

	@Override
	public boolean matchId(Tender tender, String id) {
		Long tenderId = Long.parseLong(id);
		return tender.getId() == tenderId;
	}

	@Override
	public boolean matchDeadlinePeriod(Tender tender, LocalDate deadlineFrom, LocalDate deadlineTo) {
		boolean isAfter = deadlineFrom != null
				? tender.getCompetitionDeadline().isAfter(deadlineFrom)
						|| tender.getCompetitionDeadline().equals(deadlineFrom)
				: true;
		boolean isBefore = deadlineTo != null
				? tender.getCompetitionDeadline().isBefore(deadlineTo)
						|| tender.getCompetitionDeadline().equals(deadlineTo)
				: true;
		return isAfter & isBefore;
	}

	@Override
	public boolean matchTenderStartPeriod(Tender tender, LocalDate tenderStartFrom, LocalDate tenderStartTo) {
		boolean isAfter = tenderStartFrom != null
				? tender.getTenderStart().isAfter(tenderStartFrom) || tender.getTenderStart().equals(tenderStartFrom)
				: true;
		boolean isBefore = tenderStartTo != null
				? tender.getTenderStart().isBefore(tenderStartTo) || tender.getTenderStart().equals(tenderStartTo)
				: true;
		return isAfter & isBefore;
	}

	@Override
	public boolean matchTenderEndPeriod(Tender tender, LocalDate tenderEndFrom, LocalDate tenderEndTo) {
		boolean isAfter = tenderEndFrom != null
				? tender.getTenderEnd().isAfter(tenderEndFrom) || tender.getTenderEnd().equals(tenderEndFrom)
				: true;
		boolean isBefore = tenderEndFrom != null
				? tender.getTenderEnd().isBefore(tenderEndFrom) || tender.getTenderEnd().equals(tenderEndFrom)
				: true;
		return isAfter & isBefore;
	}

	@Override
	public boolean matchState(Tender tender, String state) {
		return tender.getState().printOut().contains(state.toLowerCase());
	}

	@Override
	public boolean matchSetPeriod(Tender tender, LocalDate setDateFrom, LocalDate setDateTo) {
		boolean isAfter = setDateFrom != null
				? tender.getSetDate().isAfter(setDateFrom) || tender.getSetDate().equals(setDateFrom)
				: true;
		boolean isBefore = setDateTo != null
				? tender.getSetDate().isBefore(setDateTo) || tender.getSetDate().equals(setDateTo)
				: true;
		return isAfter & isBefore;
	}

	@Override
	public boolean matchCustomer(Tender tender, String customer) {
		return tender.getCustomer().getName().toLowerCase().contains(customer.toLowerCase());
	}

	@Override
	public Set<Line> getLines(Tender tender) throws ServiceException {
		long id = tender.getId();
		try {
			return lineDAO.findByTender(id);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public int countPerLineOrders(Tender tender, Line line, UserEntity user) throws ServiceException {
		if (tender.getState().equals(Tender.State.CREATED) || tender.getState().equals(Tender.State.RELEASED)) {
			logger.debug("Tender has not taken place yet");
			return 0;
		}
		try {
			if (user.getRole().equals(Roles.CUSTOMER) || user.getRole().equals(Roles.ADMIN)) {
				return transportOrderDAO.countTenderLineOrders(tender.getId(), line.getId());
			}
			if (user.getRole().equals(Roles.TRANSPORT_SERVICE_PROVIDER)) {
				return transportOrderDAO.countTspExecutedOrders(tender.getId(), line.getId(), user.getId());
			} else {
				throw new ServiceException("Method has been called by user without requested role!");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<TenderLineQuery> retrieveLineWinners(Tender tender, Line line, UserEntity user) throws ServiceException {
		if (tender.getState().equals(Tender.State.CREATED) || tender.getState().equals(Tender.State.RELEASED)) {
			logger.error("Tender competition is not completed yet, impossible to define line nominees!");
			throw new ServiceException("Tender winners are yet to be defined!");
		}
		try {
			Stream<TenderLineQuery> lineQueries = tenderLineQueryDAO.getByTender(tender).stream().filter(
					query -> (query.getTenderLine().getLine().getId() == line.getId() && query.getAwardedShare() > 0));
			Double lineAwardedVolume = lineQueries.collect(Collectors.summingDouble(l -> l.getAwardedShare()));
			if ((lineAwardedVolume < 0.99d) || (lineAwardedVolume > 1.01d)) {
				logger.error("Total share of all line's " + line.getId() + " awards within tender " + tender.getId()
						+ " is not equal 1!");
				throw new ServiceException(
						"Line awarded volume shares have to be double-checked, their sum is not equal 1!");
			}
			if (user.getRole().equals(UserEntity.Roles.CUSTOMER) || user.getRole().equals(UserEntity.Roles.ADMIN)) {
				return lineQueries.collect(Collectors.toSet());
			}
			if (user.getRole().equals(UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER)) {
				return lineQueries.filter(query -> query.getParticipation().getTsp().getId() == user.getId())
						.collect(Collectors.toSet()); // one item set or empty set
			} else {
				throw new ServiceException("Method has been called by user without requested role!");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean isContracted(Tender tender) {
		return !(tender.getState().equals(Tender.State.CREATED) || tender.getState().equals(Tender.State.RELEASED));
	}

	@Override
	public int countCarriedOutTenders(UserEntity customer) throws ServiceException {
		try {
			return tenderDAO.getCarriedOutTendersNumber(customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public int countTspExecutedOrdersInTender(Tender tender, UserEntity tsp) throws ServiceException {
		try {
			return tenderDAO.getTspExecutedTenderTransportOrders(tender.getId(), tsp.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public KeyPerformanceIndicators calculateTenderKPI(Tender tender) throws ServiceException {
		try {
			int tenderAssignedOrders = tenderDAO.getAssignedOrdersNumber(tender.getId());
			Set<TspPerformance> acceptedAssignments = tspPerformanceDAO.getTenderAcceptedAssignments(tender.getId());
			return KeyPerformanceIndicators.calculateKPI(tenderAssignedOrders, acceptedAssignments);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public KeyPerformanceIndicators calculateTspTenderKPI(Tender tender, UserEntity tsp) throws ServiceException {
		try {
			int tenderAssignedOrders = tenderDAO.getTspAssignedTenderTransportOrders(tender.getId(), tsp.getId());
			Set<TspPerformance> tenderAcceptedAssignments = tspPerformanceDAO
					.getTenderAcceptedAssignments(tender.getId()).stream()
					.filter(assignment -> assignment.getTsp().getId() == tsp.getId()).collect(Collectors.toSet());
			return KeyPerformanceIndicators.calculateKPI(tenderAssignedOrders, tenderAcceptedAssignments);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<TspPerformance> findTspTenderUncompletedAssignments(Tender tender, UserEntity tsp)
			throws ServiceException {
		try {
			long tenderId = tender.getId();
			long tspId = tsp.getId();
			return tspPerformanceDAO.getTspUncompletedAssignments(tspId).stream()
					.filter(assignment -> assignment.getOrderFootprint().getTenderId() == tenderId)
					.collect(Collectors.toSet());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public void confirmTenderRelease(Tender tender) throws ServiceException {
		try {
			tenderDAO.update(tender);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

}
