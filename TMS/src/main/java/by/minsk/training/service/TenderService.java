package by.minsk.training.service;

import java.time.LocalDate;
import java.util.Set;

import by.minsk.training.ToolSet;
import by.minsk.training.ToolSet.KeyPerformanceIndicators;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLine;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.user.UserEntity;

public interface TenderService {

	Set<TenderLineQuery> getQueries(TenderParticipation participation) throws ServiceException;

	Set<TenderLine> getTenderLines(Tender tender) throws ServiceException;
	
	Set<Line> getLines (Tender tender) throws ServiceException;

	void bindLineQueries(TenderLine line) throws ServiceException;

	double calculateAssignedLineVolume(TenderLine line) throws ServiceException;

	TenderParticipation getTenderParticipation(Tender tender, UserEntity entity) throws ServiceException;

	Set<Tender> detectNewReleasedTenders(UserEntity entity) throws ServiceException;

	/*
	 * counts transport orders of the given tender scope under following terms : when
	 * tender has state 'CREATED', 'RELEASED' - return estimatedQuantity; when
	 * tender has any other state - return actual quantity of (orders distributed +
	 * orders assigned - in case of Customer user) (quantity of orders distributed +
	 * orders assigned - in case of ADMIN user) or (quantity of orders received ,
	 * both confirmed and declined - in case of TSP user)
	 */
	int countOrders(Tender tender, UserEntity user) throws ServiceException;
	
	/* selects tenders to be rendered at tenders page */
	Set<Tender> gatherTenders (UserEntity user) throws ServiceException;
	
	Set<TenderLineQuery> retrieveLineWinners (Tender tender, Line line, UserEntity user) throws ServiceException;

	boolean matchTitle(Tender tender, String title);

	boolean matchReleasePeriod(Tender tender, LocalDate releaseDate_From, LocalDate releaseDate_To);

	boolean matchId(Tender tender, String id);

	boolean matchDeadlinePeriod(Tender tender, LocalDate deadline_From, LocalDate deadline_To);

	boolean matchTenderStartPeriod(Tender tender, LocalDate tenderStart_From, LocalDate tenderStart_To);

	boolean matchTenderEndPeriod(Tender tender, LocalDate tenderEnd_From, LocalDate tenderEnd_To);

	boolean matchState(Tender tender, String state);

	boolean matchSetPeriod(Tender tender, LocalDate setDate_From, LocalDate setDate_To);

	boolean matchCustomer(Tender tender, String customer);
	
	boolean isContracted (Tender tender);

	void persist(Tender tender, Set<Line> lines) throws ServiceException;
	
	Tender get(Long id) throws ServiceException;

	int countPerLineOrders(Tender tender, Line line, UserEntity user) throws ServiceException;
	
	int countCarriedOutTenders (UserEntity customer) throws ServiceException;
	
	int countImplementedTenderOrders (UserEntity customer) throws ServiceException;
	
	int countTspExecutedOrdersInTender (Tender tender, UserEntity tsp) throws ServiceException;
	
	ToolSet.KeyPerformanceIndicators calculateTspTenderKPI (Tender tender, UserEntity tsp) throws ServiceException;

	int countImplementedSpotOrders(UserEntity customer) throws ServiceException;

	Set<Tender> findCustomerBoundTenders(UserEntity customer) throws ServiceException;
	
	Set<Tender> selectActiveTenders(UserEntity customer) throws ServiceException;

	KeyPerformanceIndicators calculateTenderKPI(Tender tender) throws ServiceException;

	Set<TspPerformance> findTspTenderUncompletedAssignments(Tender tender, UserEntity tsp) throws ServiceException;

	void confirmTenderRelease(Tender tender) throws ServiceException;
	
	Set<TenderLineQuery> checkValidLineTenderObligations (Line line, UserEntity customer, UserEntity tsp) throws ServiceException;
	
}
