package by.minsk.training.service.notification;

import java.util.Set;

import by.minsk.training.service.ServiceException;
import by.minsk.training.user.UserEntity;

public interface NotificationsTracker {
	
	Set<UserNotification> updateNotifications (UserEntity user) throws ServiceException;

}
