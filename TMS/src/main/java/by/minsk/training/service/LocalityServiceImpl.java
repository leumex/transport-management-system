package by.minsk.training.service;

import java.sql.SQLException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.basic.LocalityDAO;
import by.minsk.training.entity.Locality;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class LocalityServiceImpl implements LocalityService {

	private static final Logger logger = LogManager.getLogger(LocalityServiceImpl.class);

	private LocalityDAO localityDAO;

	@Override
	public Set<Locality> getAll() throws ServiceException {
		try {
			return (Set<Locality>) localityDAO.findAll();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Locality> getByCountry(String countryId) throws ServiceException {
		Long id;
		try {
			id = Long.parseLong(countryId);
		} catch (NumberFormatException e) {
			return null;
		}
		try {
			return (Set<Locality>) localityDAO.findByCountryId(id);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Locality get(Long localityId) throws ServiceException {
		try {
			return localityDAO.getById(localityId);
		} catch (SQLException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void persist(Locality locality) throws ServiceException {
		try {
			localityDAO.save(locality);
		} catch (SQLException e) {
			throw new ServiceException(e);
		}
	}
}
