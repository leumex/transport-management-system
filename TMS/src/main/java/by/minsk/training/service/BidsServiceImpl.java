package by.minsk.training.service;

import java.sql.SQLException;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.basic.BidDAOImpl;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class BidsServiceImpl implements BidsService {

	private static final Logger logger = LogManager.getLogger(BidsServiceImpl.class);

	private BidDAOImpl bidDAO;

	/*
	 * return Bid value or null if given UserEntity tsp has not registered bids
	 * against given spot
	 */
	@Override
	public Bid retrieveBid(SpotTransportOrder order, UserEntity tsp) throws ServiceException {
		if (!tsp.getRole().equals(UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER)) {
			throw new ServiceException(
					"UserEntity entity other then Transport service provider has been used to obtain Bid entity");
		}
		Stream<Bid> bids;
		try {
			bids = bidDAO.findAll().stream().filter(bid -> bid.getTsp().getId() == tsp.getId())
					.filter(bid -> bid.getSpotOrder().getId() == order.getId());
		} catch (SQLException e) {
			throw new ServiceException(e);
		}
		if (bids.count() > 1)
			throw new ServiceException("There must be single record that references SpotTransportOrder record id = "
					+ +order.getId() + " and UserEntity tsp record id = " + tsp.getId());
		return bids.findFirst().orElseGet(() -> null);
	}

	@Override
	public Set<Bid> findTspSuccessfulBids(UserEntity customer, UserEntity tsp) throws ServiceException {
		try {
			return bidDAO.getTspSuccessBids(customer.getId(), tsp.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Bid> findOrderBids(SpotTransportOrder order) throws ServiceException {
		try {
			return bidDAO.getBySpotOrder(order.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Bid find(long bidId) throws ServiceException {
		try {
			return bidDAO.getById(bidId);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean removeBid(Bid bid) throws ServiceException {
		try {
			return bidDAO.delete(bid);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
	
	@Override
	public long commitNewBid (Bid bid) throws ServiceException {
		try {
			return bidDAO.save(bid);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
}
