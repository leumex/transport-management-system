package by.minsk.training.service;

import java.util.Collection;

import by.minsk.training.entity.Route;

public interface RouteService {

	void persist(Route route) throws ServiceException;
	Collection<String> getConditions();
	
	String deliverRepresentation (Route route);
	
	String deliverStartAddress (Route route);
	
	String deliverDestinationAddress (Route route);
}
