package by.minsk.training.service;

import java.sql.SQLException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.basic.CountryDAO;
import by.minsk.training.entity.Country;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class CountryServiceImpl implements CountryService {
	
	private static final Logger logger = LogManager.getLogger(CountryServiceImpl.class);

	private CountryDAO countryDAO;

	@Override
	public Set<Country> getAll() throws ServiceException {
		try {
			return (Set<Country>) countryDAO.findAll();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public void persist(Country newCountry) throws ServiceException {
		try {
			
			countryDAO.save(newCountry);
			logger.debug("CountyService#persist() method has been executed successfully with parameter " + newCountry);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Country get(Long newCountryId) throws ServiceException {
		try {
			return countryDAO.getById(newCountryId);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
}
