package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.Bid;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.user.UserEntity;

public interface TspPerformanceService {
	/*
	 * retrieves all tsp_response records from MySQL DB and binds them to
	 * correspondent transport order
	 */	
	Set<TspPerformance> retrieveAllOrderAssignments (TransportOrder transportOrder) throws ServiceException;
	
	TspPerformance getSpotOrderImplementedAssignment (TransportOrder transportOrder) throws ServiceException;
	
	TspPerformance getSpotOrderAssignment (Bid bid) throws ServiceException;
	
	void persist(TspPerformance assignment) throws ServiceException;
	
	long commitAssignment(TransportOrder order, UserEntity tsp) throws ServiceException;
	
	Set<TspPerformance> findUncompletedAssignments (UserEntity tsp) throws ServiceException;
	
	Set<TspPerformance> getOrderAssignments(TransportOrder order) throws ServiceException;
	
}