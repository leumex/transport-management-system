package by.minsk.training.service;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.dao.basic.TenderLineDAO;
import by.minsk.training.dao.basic.TenderLineQueryDAO;
import by.minsk.training.dao.basic.TenderParticipationDAO;
import by.minsk.training.dao.basic.TransportOrderDAO;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
@TransactionSupport
public class TenderParticipationServiceImpl implements TenderParticipationService {

	private static final Logger logger = LogManager.getLogger(TenderParticipationServiceImpl.class);
	private TenderLineQueryDAO tenderLineQueryDAO;
	private TenderParticipationDAO tenderParticipationDAO;
	private TenderLineDAO tenderLineDAO;
	private TransportOrderDAO transportOrderDAO;

	@Override
	public TenderParticipation findParticipation(UserEntity tsp, Tender tender) throws ServiceException {
		if (tsp.getRole() != UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
			throw new ServiceException(
					"Method is called with UserEntity parameter value other than Transort Service Provider!");
		}
		try {
			return tenderParticipationDAO.getParticipation(tsp.getId(), tender.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
	
	@Override
	public double findAward(Tender tender, Line line, UserEntity tsp) throws ServiceException {
		try {
			TenderLineQuery query = tenderLineQueryDAO.getTenderLineQuery(tender.getId(), line.getId(), tsp.getId());
			return query == null ? 0 : query.getAwardedShare();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public double findRate(Tender tender, Line line, UserEntity tsp) throws ServiceException {
		try {
			TenderLineQuery query = tenderLineQueryDAO.getTenderLineQuery(tender.getId(), line.getId(), tsp.getId());
			return query == null  ? 0 : query.getRate();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<TenderParticipation> summarizeTspToCustomerTenderResponse(UserEntity tsp, UserEntity customer)
			throws ServiceException {
		try {
			return tenderParticipationDAO.getAllOfTspCustomerPair(tsp.getId(), customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<TenderParticipation> findTspTenderParticipations(UserEntity tsp) throws ServiceException {
		try {
			return tenderParticipationDAO.getTspParticipations(tsp.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public int countTenderLineTspAssignedOrders(Tender tender, Line line, UserEntity tsp) throws ServiceException {
		try {
			if (!tenderLineDAO.getByTender(tender.getId()).stream()
					.anyMatch(tenderLine -> tenderLine.getLine().getId() == line.getId())) {
				throw new ServiceException("Given line (id = " + line.getId() + ") is not included into tender (id = "
						+ tender.getId() + ")");
			}
			return transportOrderDAO.findTspTenderLineAssignedOrders(tender.getId(), line.getId(), tsp.getId()).size();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public int countTenderLineTspAcceptedOrders(Tender tender, Line line, UserEntity tsp) throws ServiceException {
		try {
			if (!tenderLineDAO.getByTender(tender.getId()).stream()
					.anyMatch(tenderLine -> tenderLine.getLine().getId() == line.getId())) {
				throw new ServiceException("Given line (id = " + line.getId() + ") is not included into tender (id = "
						+ tender.getId() + ")");
			}
			return transportOrderDAO.findTspTenderLineAcceptedOrders(tender.getId(), line.getId(), tsp.getId()).size();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
	
	@Override
	public long sendTenderInvitation(Tender tender, UserEntity tsp) throws ServiceException {
		TenderParticipation participation = new TenderParticipation();
		participation.setTender(tender);
		participation.setTsp(tsp);
		participation.setInvitation(LocalDate.now());
		try {tenderParticipationDAO.save(participation);
		return participation.getId();} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}	
	}
}
