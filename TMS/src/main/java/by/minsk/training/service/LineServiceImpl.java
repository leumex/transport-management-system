package by.minsk.training.service;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.dao.basic.LineDAO;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.Truckload.TruckType;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
@TransactionSupport
public class LineServiceImpl implements LineService {

	private static final Logger logger = LogManager.getLogger(LineServiceImpl.class);

	private LineDAO lineDAO;

	@Transactional
	@Override
	public Set<Line> getUserRelativeLines(UserEntity user) throws ServiceException {
		Long id = user.getId();
		try {
			Set<Line> lines = lineDAO.findUnusedLines();
			lines.addAll(lineDAO.findbyCustomer(id));
			return lines;
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Line> filterByPickupCountry(Set<Line> foundLines, String newLinePickupCountry) {
		if (newLinePickupCountry == null) {
			return foundLines;
		}
		long id = Long.parseLong(newLinePickupCountry.trim());
		return foundLines.stream().filter(line -> line.getRoute().getStart().getLocality().getCountry().getId() == id)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByPickupAddress(Set<Line> foundLines, String newLinePickupAddress) {
		if (newLinePickupAddress == null) {
			return foundLines;
		}
		Long id = Long.parseLong(newLinePickupAddress.trim());
		return foundLines.stream().filter(line -> line.getRoute().getStart().getId() == id).collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByDeliveryCountry(Set<Line> foundLines, String newLineDeliveryCountry) {
		if (newLineDeliveryCountry == null) {
			return foundLines;
		}
		Long id = Long.parseLong(newLineDeliveryCountry.trim());
		return foundLines.stream()
				.filter(line -> line.getRoute().getDestination().getLocality().getCountry().getId() == id)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByDeliveryLocality(Set<Line> foundLines, String newLineDeliveryLocality) {
		if (newLineDeliveryLocality == null) {
			return foundLines;
		}
		Long id = Long.parseLong(newLineDeliveryLocality.trim());
		return foundLines.stream().filter(line -> line.getRoute().getDestination().getLocality().getId() == id)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByDeliveryAddress(Set<Line> foundLines, String newLineDeliveryAddress) {
		if (newLineDeliveryAddress == null) {
			return foundLines;
		}
		Long id = Long.parseLong(newLineDeliveryAddress.trim());
		return foundLines.stream().filter(line -> line.getRoute().getDestination().getId() == id)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByCargoName(Set<Line> foundLines, String newLineCargoName) {
		if (newLineCargoName == null) {
			return foundLines;
		}
		return foundLines.stream()
				.filter(line -> line.getTruckload().getName().toLowerCase().contains(newLineCargoName.toLowerCase()))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByLoadUnits(Set<Line> foundLines, Integer newLineLoadUnits) {
		if (newLineLoadUnits == null) {
			return foundLines;
		}
		return foundLines.stream().filter(line -> line.getTruckload().getLoadUnits() == newLineLoadUnits)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByCargoWeight(Set<Line> foundLines, Double newLineCargoWeight) {
		if (newLineCargoWeight == null) {
			return foundLines;
		}
		return foundLines.stream().filter(line -> line.getTruckload().getWeight() == newLineCargoWeight)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByTruckType(Set<Line> foundLines, TruckType newLineTruckType) {
		if (newLineTruckType == null) {
			return foundLines;
		}
		return foundLines.stream().filter(line -> line.getTruckload().getTruckType() == newLineTruckType)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByPickupLocality(Set<Line> foundLines, String newLinePickupLocality) {
		if (newLinePickupLocality == null) {
			return foundLines;
		}
		Long id = Long.parseLong(newLinePickupLocality);
		return foundLines.stream().filter(line -> line.getRoute().getStart().getLocality().getId() == id)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Line> filterByRouteConditions(Set<Line> foundLines, Route.Conditions newLineRouteConditions) {
		return foundLines.stream().filter(line -> line.getRoute().getConditions() == newLineRouteConditions)
				.collect(Collectors.toSet());
	}

	@Override
	@Transactional
	public void persist(Line newLine) throws ServiceException {
		try {
				lineDAO.save(newLine);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Line get(long id) throws ServiceException {
		try {
			return lineDAO.getById(id);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Line> getByTender(long tenderId) throws ServiceException {
		try {
			return lineDAO.findByTender(tenderId);
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException (e);
		}
	}

	@Override
	public Set<Line> sort(Collection<Line> source, Comparator<Line> comparator) {
		Set<Line> sortedSet = new TreeSet<>(comparator);
		sortedSet.addAll(source);
		return sortedSet;
	}
	
	
}
