package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.ToolSet.KeyPerformanceIndicators;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.user.UserEntity;

public interface SpotOrderService {
	
	SpotTransportOrder getSpotOrder (TransportOrder order) throws ServiceException;
	
	int countTspSpotExecutedOrders(UserEntity tsp) throws ServiceException;
	
	KeyPerformanceIndicators calculateTspSpotOrdersExecutionKPI (UserEntity tsp) throws ServiceException;

	boolean hasOpenSpotAuction (TransportOrder order) throws ServiceException;
	
	boolean commitNewSpotAuction (TransportOrder order) throws ServiceException;

	boolean commitOngoingSpotAbort(TransportOrder order) throws ServiceException;
	
	Set<Bid> obtainRelativeBids(SpotTransportOrder order) throws ServiceException;
	
	String reflectSpotOrderAssignment (SpotTransportOrder order, UserEntity tsp) throws ServiceException;
	
	Set<SpotTransportOrder> retrieveTspAvailableSpotAuctions (UserEntity tsp) throws ServiceException;
}
