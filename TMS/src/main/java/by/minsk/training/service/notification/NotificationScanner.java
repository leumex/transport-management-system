package by.minsk.training.service.notification;

import java.time.LocalDateTime;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.service.ServiceException;
import by.minsk.training.user.UserEntity;

@Bean
public class NotificationScanner implements NotificationsTracker {

	private static final Logger logger = LogManager.getLogger(NotificationScanner.class);

	private NotificationExtractor fromTendersExtractor;
	private NotificationExtractor fromOrdersExtractor;

	public NotificationScanner(
			@BeanQualifier(value = "fromTendersExtractor") NotificationExtractor fromTendersExtractor,
			@BeanQualifier(value = "fromTransportOrdersExtractor") NotificationExtractor fromOrdersExtractor) {
		super();
		this.fromTendersExtractor = fromTendersExtractor;
		this.fromOrdersExtractor = fromOrdersExtractor;
	}

	@Override
	public Set<UserNotification> updateNotifications(UserEntity user) throws ServiceException {
		if (user == null) {
			return null;
		}
		Set<UserNotification> notifications = fromOrdersExtractor.extractNotifications(user);
		Set<UserNotification> tenderNotifications = fromTendersExtractor.extractNotifications(user);
		if (tenderNotifications != null) {
			notifications.addAll(tenderNotifications);
		}
		logger.debug("last update made at: " + LocalDateTime.now());
		return notifications;
	}
}