package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.Country;

public interface CountryService {
	Set<Country> getAll() throws ServiceException;

	void persist(Country newCountry) throws ServiceException;

	Country get(Long newCountryId) throws ServiceException;
}
