package by.minsk.training.service;

import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.ToolSet.KeyPerformanceIndicators;
import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.dao.basic.BidDAO;
import by.minsk.training.dao.basic.LineDAO;
import by.minsk.training.dao.basic.SpotDAO;
import by.minsk.training.dao.basic.TenderDAO;
import by.minsk.training.dao.basic.TenderLineQueryDAO;
import by.minsk.training.dao.basic.TransportOrderDAO;
import by.minsk.training.dao.basic.TspPerformanceDAO;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.entity.TspPerformance.OrderFootprint;
import by.minsk.training.entity.TspPerformance.TspResponse;
import by.minsk.training.user.UserDao;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
@TransactionSupport
public class TransportOrderServiceImpl implements TransportOrderService {

	private static final Logger logger = LogManager.getLogger(TransportOrderServiceImpl.class);

	private TransportOrderDAO transportOrderDAO;
	private TspPerformanceDAO tspPerformanceDAO;
	private TenderLineQueryDAO tenderLineQueryDAO;
	private TenderDAO tenderDAO;
	private UserDao userEntityDAO;
	private LineDAO lineDAO;
	private SpotDAO spotDAO;
	private BidDAO bidDAO;

	/*
	 * returns number of execution started transport orders of given customer user
	 */
	@Override
	public int getCustomerEverExecutedOrdersNumber(UserEntity customer) throws ServiceException {
		try {
			return transportOrderDAO.countCustomerEverExecutedOrders(customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean updateOrder(TransportOrder order) throws ServiceException {
		try {
			return transportOrderDAO.update(order);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/* returns number of execution started transport orders by given tsp user */
	@Override
	public int getTspEverExecutedOrdersNumber(UserEntity tsp) throws ServiceException {
		try {
			return transportOrderDAO.countTspEverExecutedOrders(tsp.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/*
	 * The result of the recall operation differs depending on the values of
	 * order.state and respective latest tspPerformance.tspResponse. - if
	 * (order.state is ASSIGNED, tspPerformance.orderFootprint.state is ASSIGNED,
	 * tspPerformance.tspPersponse is OUTSTANDING and 2 hours have passed since
	 * tspPerformance.notified value) => order.state, OrderFootprint.state and
	 * tspResponse - all become WITHDRAWN; - if 2 hours haven't passed yet =>
	 * order.state becomes WITHDRAWN, OrderFootprint.state becomes CANCELLED,
	 * TspResponse remains OUTSTANDING; - if order.state is ASSIGNED,
	 * OrderFootprint.State is ASSIGNED, tspResponse is DECLINED => both order.state
	 * and orderFootprint.state becomes WITHDRAWN; - if order.state is ACCEPTED and
	 * tspResponse is ACCEPTED but loading date has not come yet => order.state
	 * becomes WITHDRAWN, tspResponse remains ACCEPTED, orderFootprint.state becomes
	 * CANCELLED; - if order.state is ACCEPTED, tspResponse is ACCEPTED, but loading
	 * date has passed, and no tracking events have been registered (arrival at
	 * loading first of all), also per some kind of correspondence parties have
	 * found out that tsp is not able to fulfill the order, order.state becomes
	 * WITHDRAWN, tspResponse remains ACCEPTED, orderFootprint.state becomes
	 * WITHDRAWN; - if order.state is IN_ACTION, then it becomes WITHDRAWN,
	 * correspondent orderFootprint.state becomes INTERRUPTED, tspResponse remains
	 * ACCEPTED;
	 */
	@Transactional
	@Override
	public boolean commitOrderRecall(UserEntity user, long orderId) throws ServiceException {
		if (!user.getRole().equals(Roles.CUSTOMER) && !user.getRole().equals(Roles.ADMIN)) {
			throw new ServiceException("Not authorized user tries to recall the order, order id = " + orderId);
		}
		try {
			TransportOrder order = transportOrderDAO.getById(orderId);
			TspPerformance performance = tspPerformanceDAO.selectOrderReferringAssignments(orderId).stream()
					.max((o1, o2) -> o1.getNotified().compareTo(o2.getNotified()))
					.orElseThrow(() -> new ServiceException(
							"order does not have assignments, thus it does not needed to recall the order"));
			OrderFootprint footPrint = performance.getOrderFootprint();
			Duration duration = Duration.between(performance.getNotified(), LocalDateTime.now());
			long seconds = duration.getSeconds();
			long hours = seconds / 3600L;
			if (order.getState().equals(State.ASSIGNED)) {
				if (performance.getTspResponse().equals(TspResponse.OUTSTANDING)
						& footPrint.getState().equals(OrderFootprint.State.ASSIGNED)) {
					if (hours > 2L) {
						order.setState(State.WITHDRAWN);
						footPrint.setState(OrderFootprint.State.WITHDRAWN);
						performance.setTspResponse(TspResponse.WITHDRAWN);
						performance.setComment("Order has not been timely accepted by tsp");
					} else {
						order.setState(State.WITHDRAWN);
						footPrint.setState(OrderFootprint.State.CANCELLED);
						performance
								.setComment("Customer has recalled the order before the deadline to accept order came");
					}
				} else if (performance.getTspResponse().equals(TspResponse.DECLINED)
						& footPrint.getState().equals(OrderFootprint.State.ASSIGNED)) {
					order.setState(State.WITHDRAWN);
					performance.getOrderFootprint().setState(OrderFootprint.State.WITHDRAWN);
					performance.setComment("Order has been recalled because tsp has declined the assignment");
				}
			} else if (order.getState().equals(State.ACCEPTED)) {
				if (performance.getTspResponse().equals(TspResponse.ACCEPTED)) {
					if (order.getLoadingDate().isAfter(LocalDate.now())) {
						order.setState(State.WITHDRAWN);
						footPrint.setState(OrderFootprint.State.CANCELLED);
						performance.setComment("Order has been recalled due to the customer decision");
					} else if (performance.getArrivedForLoading() == null) {
						order.setState(State.WITHDRAWN);
						footPrint.setState(OrderFootprint.State.WITHDRAWN);
						performance.setComment(
								"Order has been recalled because truck arrival event has not been timely registered (truck has not arrived)");
					}
				}
			} else if (order.getState().equals(State.IN_ACTION)) {
				order.setState(State.WITHDRAWN);
				footPrint.setState(OrderFootprint.State.INTERRUPTED);
				performance
						.setComment("Order had to be recalled by the customer despite the penalties that might arise");
			} else
				throw new ServiceException("Given order cannot be recalled");
			return transportOrderDAO.update(order) & tspPerformanceDAO.update(performance);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public Set<TransportOrder> gatherOrders(UserEntity user) throws ServiceException {
		Long userId = user.getId();
		UserEntity.Roles role = user.getRole();
		try {
			if (role == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
				return tspPerformanceDAO.selectTspOrderAssingments(userId).stream().map(performance -> {
					try {
						return restoreOrder(performance);
					} catch (ServiceException e) {
						logger.error(e.getMessage());
						throw new RuntimeException(e);
					}
				}).collect(Collectors.toSet());
			} else if (role == UserEntity.Roles.CUSTOMER) {
				return transportOrderDAO.findAll().stream()
						.filter(order -> order.getCustomer() == null ? order.getTender().getCustomer().getId() == userId
								: order.getCustomer().getId() == userId)
						.collect(Collectors.toSet());
			} else if (role == UserEntity.Roles.ADMIN) {
				return new HashSet<TransportOrder>(transportOrderDAO.findAll());
			} else
				return null;
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/*
	 * retrieves most recent TspPerformance record, that is bound to 1. given
	 * Transport Order, 2. authorized UserEntity user. NEEDS TO BE TESTED!!!
	 */
	@Override
	public TspPerformance getLatestAssignment(UserEntity user, TransportOrder transportOrder) throws ServiceException {
		long userId = user.getId();
		try {
			TspPerformance latestAssignment;
			Roles role = user.getRole();
			if (role == Roles.TRANSPORT_SERVICE_PROVIDER) {
				latestAssignment = tspPerformanceDAO.selectOrderReferringAssignments(transportOrder.getId()).stream()
						.filter(assignment -> assignment.getTsp().getId() == user.getId())
						.max((assignment1, assigment2) -> assignment1.getNotified().compareTo(assigment2.getNotified()))
						.orElse(null);
			} else if (role == Roles.CUSTOMER) {
				long orderUserId = transportOrder.getCustomer() != null ? transportOrder.getCustomer().getId()
						: transportOrder.getTender().getCustomer().getId();
				if (orderUserId != userId) {
					throw new ServiceException("Given transport order does not refer to given user!");
				}
				latestAssignment = tspPerformanceDAO.getByCustomer(userId).stream()
						.filter(assignment -> assignment.getTransportOrder().getId() == transportOrder.getId())
						.max((assignment1, assigment2) -> assignment1.getNotified().compareTo(assigment2.getNotified()))
						.orElse(null);
			} else if (role == Roles.ADMIN) {
				latestAssignment = tspPerformanceDAO.selectOrderReferringAssignments(transportOrder.getId()).stream()
						.max((assignment1, assigment2) -> assignment1.getNotified().compareTo(assigment2.getNotified()))
						.orElse(null);
			} else {
				latestAssignment = null;
			}
			return latestAssignment;
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/*
	 * returns the order record from persistence storage with its up to date
	 * configuration. The order with configuration when assigned to a tsp has to be
	 * retrieved using correspondent tspPerformance record and by invoking
	 * restoreOrder() method
	 */
	@Override
	public TransportOrder findOrder(long orderId) throws ServiceException {
		try {
			return transportOrderDAO.getById(orderId);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public KeyPerformanceIndicators calculateTSPGeneralKPI(UserEntity tsp) throws ServiceException {
		try {
			int receivedOrders = transportOrderDAO.countTspEverReceivedOrders(tsp.getId());
			Set<TspPerformance> acceptedAssignments = tspPerformanceDAO.selectTspAcceptedAssignments(tsp.getId());
			return KeyPerformanceIndicators.calculateKPI(receivedOrders, acceptedAssignments);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/*
	 * method is used to restore the order shape it had at the time it has been
	 * assigned to the tsp (because the tsp could not accept it or the customer
	 * could recall it, and the order could be subsequently modified, re-assigned to
	 * other tsps, etc.)
	 */
	@Override
	@Transactional
	public TransportOrder restoreOrder(TspPerformance performance) throws ServiceException {
		TransportOrder order = performance.getTransportOrder();
		TspPerformance.OrderFootprint orderFootprint = performance.getOrderFootprint();
		/*
		 * the method is designated to deliver the order configuration to a tsp user,
		 * thus the order 'issued' datetime for the tsp is the datetime the tsp has
		 * learned about the order when he's received its assignment. So below statement
		 * is valid to form the order representation for a tsp user. Though when to
		 * retrieve the order by id from a persistent storage, the order 'issued'
		 * property value will undoubtedly be another
		 */
		order.setIssued(performance.getNotified());
		order.setLoadingDate(orderFootprint.getLoadingDate());
		order.setDeliveryTerm(orderFootprint.getDeliveryTerm());
		order.setState(orderFootprint.getState() == OrderFootprint.State.INTERRUPTED
				|| orderFootprint.getState() == OrderFootprint.State.CANCELLED ? TransportOrder.State.WITHDRAWN
						: TransportOrder.State.valueOf(orderFootprint.getState().toString()));
		try {
			order.setTender(orderFootprint.getTenderId() == 0 ? null : tenderDAO.getById(orderFootprint.getTenderId()));
			order.setCustomer(
					orderFootprint.getCustomerId() == 0 ? null : userEntityDAO.getById(orderFootprint.getCustomerId()));
			order.setLine(lineDAO.getById(orderFootprint.getLineId()));
			return order;
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean trackingEventsToRender(UserEntity user, TspPerformance assignment) {
		TransportOrder order = assignment.getTransportOrder();
		OrderFootprint footprint = assignment.getOrderFootprint();
		Roles role = user.getRole();
		if (role == Roles.CUSTOMER) {
			return !order.getState().equals(State.CREATED) || !order.getState().equals(State.ASSIGNED)
					|| !order.getState().equals(State.ANNOUNCED);
		} else if (role == Roles.ADMIN) {
			return !order.getState().equals(State.CREATED) || !order.getState().equals(State.ASSIGNED)
					|| !order.getState().equals(State.ANNOUNCED);
		} else if (role == Roles.TRANSPORT_SERVICE_PROVIDER) {
			if (order.getState().equals(State.WITHDRAWN)) {
				return footprint.getState().equals(OrderFootprint.State.CANCELLED)
						|| footprint.getState().equals(OrderFootprint.State.INTERRUPTED) ? true : false;
			} else
				return order.getState().equals(State.ACCEPTED) || order.getState().equals(State.IN_ACTION)
						|| order.getState().equals(State.COMPLETED);
		} else
			return false;
	}

	/*
	 * order.state , tspPerformance.OrderFootprint.state - both remain ASSIGNED !!!
	 */
	@Override
	@Transactional
	public boolean сommitOrderRejection(long orderId, UserEntity user) throws ServiceException {
		TspPerformance performance = definePerformanceForCommit(orderId, user);
		performance.setTspResponse(TspResponse.DECLINED);
		performance.setResponseTime(LocalDateTime.now());
		try {
			return tspPerformanceDAO.update(performance);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public boolean сommitOrderAcceptance(long orderId, UserEntity user) throws ServiceException {
		TspPerformance performance = definePerformanceForCommit(orderId, user);
		performance.getTransportOrder().setState(State.ACCEPTED);
		performance.getOrderFootprint().setState(TspPerformance.OrderFootprint.State.ACCEPTED);
		performance.setTspResponse(TspResponse.ACCEPTED);
		performance.setResponseTime(LocalDateTime.now());
		try {
			return tspPerformanceDAO.update(performance) && transportOrderDAO.update(performance.getTransportOrder());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	private TspPerformance definePerformanceForCommit(long orderId, UserEntity user) throws ServiceException {
		Roles userRole = user.getRole();
		TspPerformance performance;
		if (userRole == Roles.ADMIN) {
			try {
				performance = tspPerformanceDAO.selectOrderReferringAssignments(orderId).stream()
						.max((assignment1, assigment2) -> assignment1.getNotified().compareTo(assigment2.getNotified()))
						.orElseThrow(() -> new ServiceException(
								"No TspPerformance records bound the transport order are found!"));
			} catch (SQLException e) {
				logger.error(e.getMessage());
				throw new ServiceException(e);
			}
		} else if (userRole == Roles.TRANSPORT_SERVICE_PROVIDER) {
			try {
				performance = tspPerformanceDAO.selectOrderReferringAssignments(orderId).stream()
						.filter(assignment -> assignment.getTsp().getId() == user.getId())
						.max((assignment1, assigment2) -> assignment1.getNotified().compareTo(assigment2.getNotified()))
						.orElseThrow(() -> new ServiceException("There's no TspPerformance records "
								+ "correspondent to the given user and to the given transport order"));
			} catch (SQLException e) {
				logger.error(e.getMessage());
				throw new ServiceException(e);
			}
		} else {
			throw new ServiceException(
					"Only users with ADMIN and TRANSPORT_SERVICE_PROVIDER roles are authorized to commit order acceptance!");
		}
		return performance;
	}

	@Override
	public Set<TransportOrder> selectUncompletedOrder(UserEntity customer) throws ServiceException {
		Roles role = customer.getRole();
		if (role != Roles.CUSTOMER) {
			throw new ServiceException("method is called with invalid UserEntity parameter (Not a Customer)!");
		}
		try {
			return transportOrderDAO.findUncompletedOrders(customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<TransportOrder> findTopicalOrders(UserEntity user) throws ServiceException {
		if (user == null) {
			return null;
		}
		Roles role = user.getRole();
		Set<TransportOrder> ordersInFocus;
		try {
			if (role == Roles.ADMIN) {
				ordersInFocus = transportOrderDAO.findAll().stream()
						.filter(order -> order.getState() != State.COMPLETED).collect(Collectors.toSet());
			} else if (role == Roles.TRANSPORT_SERVICE_PROVIDER) {
				ordersInFocus = tspPerformanceDAO.getTspUncompletedAssignments(user.getId()).stream()
						.map(assignment -> assignment.getTransportOrder()).collect(Collectors.toSet());
			} else {
				ordersInFocus = transportOrderDAO.findUncompletedOrders(user.getId());
			}
			return ordersInFocus;
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public long persist(TransportOrder order) throws ServiceException {
		try {
			return transportOrderDAO.save(order);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public double defineOrderRate(UserEntity user, TransportOrder order) throws ServiceException {
		try {
			State orderState = order.getState();
			Roles userRole = user.getRole();
			if (orderState == State.CREATED || orderState == State.ANNOUNCED) {
				return 0;
			} else if (orderState == State.WITHDRAWN && (userRole == Roles.ADMIN || userRole == Roles.CUSTOMER)) {
				return 0;
			}
			Set<TspPerformance> orderAssignments = tspPerformanceDAO.selectOrderReferringAssignments(order.getId());
			if (userRole == Roles.TRANSPORT_SERVICE_PROVIDER) {
				orderAssignments = orderAssignments.stream()
						.filter(assignment -> assignment.getTsp().getId() == user.getId()).collect(Collectors.toSet());
			}
			TspPerformance keyAssignment = orderAssignments.stream().max((assignment1, assignment2) -> assignment1.getNotified().compareTo(assignment2.getNotified()))
					.get();
			long tenderId = keyAssignment.getOrderFootprint().getTenderId();
			SpotTransportOrder spotOrder = spotDAO.getByOrder(order.getId());
			return (tenderId != 0) ? tenderLineQueryDAO.getTenderLineQuery(tenderId, order.getLine().getId(), keyAssignment.getTsp().getId()).getRate()
					: bidDAO.getBySpotOrder(spotOrder.getId()).stream().filter(bid -> bid.getTsp().getId() == keyAssignment.getTsp().getId()).findFirst()
					.orElseThrow(()-> new ServiceException ("For the transport order #" + order.getId() + " of a spot scope - no referring bids have been found!")).getBid();
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
}
