package by.minsk.training.service;

import java.sql.SQLException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.basic.TspPerformanceDAO;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.entity.TspPerformance.TspResponse;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TspPerformanceServiceImpl implements TspPerformanceService {

	private static final Logger logger = LogManager.getLogger(TspPerformanceServiceImpl.class);

	private TspPerformanceDAO tspPerformanceDAO;

	@Override
	public Set<TspPerformance> retrieveAllOrderAssignments (TransportOrder transportOrder) throws ServiceException {
		try {
			Set<TspPerformance> assignments = tspPerformanceDAO.selectOrderReferringAssignments(transportOrder.getId());
			return assignments;
		} catch (SQLException e) {
			logger.debug(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public TspPerformance getSpotOrderImplementedAssignment(TransportOrder transportOrder) throws ServiceException {
		try {
			if (transportOrder.getTender() != null)
				throw new ServiceException("Transport order " + transportOrder.getId() + " is under tender "
						+ transportOrder.getTender().getId() + " scope!");
			Set<TspPerformance> performances = tspPerformanceDAO
					.selectOrderReferringAssignments(transportOrder.getId());
			return performances.stream()
					.filter(performance -> performance.getTspResponse() == TspResponse.ACCEPTED
							& (performance.getTransportOrder().getState() == TransportOrder.State.IN_ACTION
									|| performance.getTransportOrder().getState() == TransportOrder.State.COMPLETED))
					.findFirst()
					.orElseThrow(() -> new ServiceException("There's no tspResponse record to describe the order "
							+ transportOrder.getId() + " execution"));
		} catch (SQLException e) {
			logger.debug(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public TspPerformance getSpotOrderAssignment(Bid bid) throws ServiceException {
		TransportOrder order = bid.getSpotOrder().getTransportOrder();
		try {
		Set<TspPerformance> assignments = tspPerformanceDAO.selectOrderReferringAssignments(order.getId());
		return assignments.stream().filter(assignment -> assignment.getOrderFootprint().getTenderId() == 0)
				.filter(assignment -> assignment.getTsp().getId().equals(bid.getTsp().getId()))
				.findAny().orElse(null);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public void persist(TspPerformance assignment) throws ServiceException {
		try {
			tspPerformanceDAO.update(assignment);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}	
	}

	@Override
	public long commitAssignment(TransportOrder order, UserEntity tsp) throws ServiceException {
		TspPerformance tspPerformance = new TspPerformance();
		tspPerformance.setTspResponse(TspResponse.OUTSTANDING);
		tspPerformance.setTsp(tsp);
		order.setState(State.ASSIGNED);
		tspPerformance.setTransportOrder(order);
		try {
		 return tspPerformanceDAO.save(tspPerformance);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<TspPerformance> findUncompletedAssignments(UserEntity tsp) throws ServiceException {
		try {
			return tspPerformanceDAO.getTspUncompletedAssignments(tsp.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<TspPerformance> getOrderAssignments(TransportOrder order) throws ServiceException {
		try {
			return tspPerformanceDAO.selectOrderReferringAssignments(order.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}	
}
