package by.minsk.training.service.notification;

public enum Alert {
	TO_REJECTION("transportOrder"), TO_ASSIGNMENT_RESPONSE("transportOrder"), TRUCK_PLATES("transportOrder"), 
	ARRIVAL_FOR_LOADNING("transportOrder"), SPOT_DEADLINE("spotContest"), 
	TENDER_COMPETITION_DEADLINE("tenderContest"), NEW_TO("transportOrder"), NEW_TENDER("tenderContest"), 
	NEW_SPOT_TO("spotContest"), TO_TRACKING("transportOrder"), TO_RECALL("transportOrder");
	
	private String targetObject;
	
	private Alert(String targetObject){
		this.targetObject =targetObject;
	}
	
	public String getTargetObject() {
		return this.targetObject;
	}
	
	public String toString() {
		return this.name();
	}
}
