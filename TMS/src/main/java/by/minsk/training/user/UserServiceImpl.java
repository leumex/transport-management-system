package by.minsk.training.user;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.Transactional;
import by.minsk.training.dao.basic.ContractDAO;
import by.minsk.training.entity.Contract;
import by.minsk.training.service.ServiceException;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class UserServiceImpl implements UserService {

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	private UserDao userDao;
	private ContractDAO contractDAO;

	@Override
	public Set<UserEntity> findContractedTsps(UserEntity customer) throws ServiceException {
		try {
			return userDao.getContractedTsps(customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public UserEntity findById(Long id) {
		try {
			return userDao.getById(id);
		} catch (SQLException e) {
			logger.error("Failed to retrieve UserEntity record by Lond id parameter");
			return null;
		}
	}

	@Override
	public boolean loginUser(UserEntity entity) {
		Optional<UserEntity> byLogin;
		try {
			byLogin = userDao.findByLogin(entity.getLogin());
			logger.debug("UserDao#findBylogin has completed execution");
		} catch (SQLException e) {
			logger.error("Failed to read user", e);
			byLogin = Optional.empty();
		}
		return byLogin.filter(user -> user.getPassword().equals(BCrypt.hashpw(entity.getPassword(), user.getSalt())))
				.isPresent();
	}

	@Override
	@Transactional
	public boolean registerUser(UserEntity entity) {
		try {
			userDao.save(entity);
			return true;
		} catch (SQLException e) {
			logger.error("Failed to save user", e);
			return false;
		}
	}

	@Override
	public List<UserEntity> getAllUsers() {
		try {
			return (List<UserEntity>) userDao.findAll();
		} catch (SQLException e) {
			logger.error("Failed to read users", e);
			return new ArrayList<>();
		}
	}

	@Override
	public UserEntity findUser(String login) {
		try {
			Optional<UserEntity> result = userDao.findByLogin(login);
			return (result.orElse(null));
		} catch (SQLException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	@Override
	public Contract findContract(UserEntity tsp, UserEntity customer) throws ServiceException {
		try {
			return contractDAO.getContract(tsp.getId(), customer.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public UserEntity findCompanyById(long id) throws ServiceException {
		try {
			UserEntity company = userDao.getById(id);
			company.setLogin(null);
			company.setPassword(null);
			company.setSalt(null);
			return company;
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<UserEntity> findByName(String receivedInput) throws ServiceException {
		try {
			return userDao.findAll().stream()
					.filter(company -> company.getName().toLowerCase().contains(receivedInput.toLowerCase()))
					.peek(company -> {
						company.setLogin(null);
						company.setPassword(null);
						company.setSalt(null);
					}).collect(Collectors.toSet());
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
}
