package by.minsk.training.user;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class UserDaoImpl implements UserDao {

	private final static Logger logger = LogManager.getLogger(UserDaoImpl.class);
	/* private final static AtomicLong COUNTER = new AtomicLong(1); */

	private static final String SELECT_ALL_QUERY = "select id, name, login, role, password, salt, registration_date from user;";
	private static final String INSERT_QUERY = "insert into user(name, role, login, password, salt, registration_date) values (?,?,?,?,?,?);";
	private static final String UPDATE_QUERY = "update user set name=?, login = ?, password = ?, salt = ? where id = ?;";
	private static final String DELETE_QUERY = "delete from user where id = ?;";
	private static final String SELECT_BY_ID_QUERY = "select id, name, login, role, password, salt, registration_date from user where id = ?;";
	private static final String SELECT_BY_LOGIN_QUERY = "select id, name, login, role, password, salt, registration_date from user where login = ?;";
	private static final String CONTRACTED_TSPS_QUERY = "select tsp.id, tsp.name, tsp.role, tsp.registration_date from user tsp "
			+ "where exists (select c.id from contract c where c.customer_id = ? and c.tsp_id = tsp.id) and tsp.role = 'transport service provider';";

	private ConnectionManager connectionManager;

	@Override
	public Set<UserEntity> getContractedTsps(long customerId) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStatement = connection.prepareStatement(CONTRACTED_TSPS_QUERY)) {
			selectStatement.setLong(1, customerId);
			ResultSet rs = selectStatement.executeQuery();
			Set<UserEntity> tsps = new HashSet<>();
			while (rs.next()) {
				UserEntity tsp = new UserEntity();
				tsp.setId(rs.getLong(1));
				tsp.setName(rs.getString(2));
				Roles role = Roles.obtainRole(rs.getString(3));
				if (role != Roles.TRANSPORT_SERVICE_PROVIDER) {
					throw new SQLException("Result set of rows contains row (id = " + tsp.getId()
							+ ") where role is other than transport service provider!");
				}
				tsp.setRole(role);
				tsps.add(tsp);
			}
			return tsps;
		}
	}

	@Override
	public Optional<UserEntity> findByLogin(String login) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_LOGIN_QUERY)) {
			logger.debug("UserDao#findBylogin is invoked..");
			selectStmt.setString(1, login);
			ResultSet resultSet = selectStmt.executeQuery();
			List<UserEntity> entities = parseResultSet(resultSet);
			if (entities.size() > 1) {
				throw new SQLException("2 or more user records have same login!");
			}
			return entities.stream().findFirst();
		}
	}

	@Override
	public Long save(UserEntity entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(INSERT_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			insertStatement.setString(++i, entity.getName());
			insertStatement.setString(++i, entity.getRole().printOut());
			insertStatement.setString(++i, entity.getLogin());
			insertStatement.setString(++i, entity.getPassword());
			insertStatement.setString(++i, entity.getSalt());
			insertStatement.setDate(++i, Date.valueOf(entity.getRegistrationDate()));
			insertStatement.executeUpdate();
			ResultSet generatedKeys = insertStatement.getGeneratedKeys();
			while (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(1));
			}
		}
		return entity.getId();
	}

	@Override
	public boolean update(UserEntity entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
			int i = 0;
			updateStmt.setString(++i, entity.getName());
			updateStmt.setString(++i, entity.getLogin());
			updateStmt.setString(++i, entity.getPassword());
			updateStmt.setString(++i, entity.getSalt());
			updateStmt.setString(++i, entity.getRole().printOut());
			updateStmt.setLong(++i, entity.getId());
			return updateStmt.executeUpdate() > 0;
		}
	}

	@Override
	public boolean delete(UserEntity entity) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
			updateStmt.setLong(1, entity.getId());
			return updateStmt.executeUpdate() > 0;
		}
	}

	public UserEntity getById(Long id) throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
			selectStmt.setLong(1, id);
			ResultSet resultSet = selectStmt.executeQuery();
			return parseResultSet(resultSet).stream().findFirst()
					.orElseThrow(() -> new IllegalArgumentException("Entity not found with given id: " + id));
		}
	}

	public List<UserEntity> findAll() throws SQLException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
			ResultSet resultSet = selectStmt.executeQuery();
			return parseResultSet(resultSet);
		}
	}

	private List<UserEntity> parseResultSet(ResultSet resultSet) throws SQLException {
		List<UserEntity> users = new ArrayList<>();
		while (resultSet.next()) {
			int i = 1;
			long entityId = resultSet.getLong(i++);
			String name = resultSet.getString(i++);
			String login = resultSet.getString(i++);
			UserEntity.Roles role = UserEntity.Roles.obtainRole(resultSet.getString(i++));
			String password = resultSet.getString(i++);
			String salt = resultSet.getString(i++);
			LocalDate registrationDate = resultSet.getDate(i++).toLocalDate();
			UserEntity user = new UserEntity.Builder().setId(entityId).setLogin(login).setPassword(password)
					.setSalt(salt).setName(name).setRole(role).setRegistrationDate(registrationDate).build();
			users.add(user);
		}
		resultSet.close();
		return users;
	}
}
