package by.minsk.training.user;

import java.util.List;
import java.util.Set;

import by.minsk.training.entity.Contract;
import by.minsk.training.service.ServiceException;

public interface UserService {

    boolean loginUser(UserEntity entity);

    boolean registerUser(UserEntity entity);

    UserEntity findUser(String login);

    List<UserEntity> getAllUsers();
    
    UserEntity findById(Long id);
    
    UserEntity findCompanyById (long id) throws ServiceException;
    
    Contract findContract (UserEntity tsp, UserEntity customer) throws ServiceException;
    
    Set<UserEntity> findContractedTsps(UserEntity customer) throws ServiceException;

	Set<UserEntity> findByName(String receivedInput) throws ServiceException;
}
