package by.minsk.training.user;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import by.minsk.training.dao.basic.CRUDDao;

public interface UserDao extends CRUDDao<UserEntity, Long> {

	Optional<UserEntity> findByLogin(String login) throws SQLException;

	UserEntity getById(Long id) throws SQLException;

	Collection<UserEntity> findAll() throws SQLException;
	
	Set<UserEntity> getContractedTsps(long customerId) throws SQLException;
 
}
