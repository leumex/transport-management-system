package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.service.LocalityService;
import by.minsk.training.service.ServiceException;
import lombok.AllArgsConstructor;

@Bean(name = "validateNewLocality")
@AllArgsConstructor
public class LocalityValidator implements Validator {

	private LocalityService localityService;

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> validationResult = new HashMap<>();
		String newLocalityCountry = req.getParameter("localityCountry");
		String newLocality = req.getParameter("locality");
		if (newLocality == null || newLocality.length() == 0) {
			validationResult.put("wrongLocality", "Insert new locality name!");
		}
		long newLocalityCountryId;
		try {
			newLocalityCountryId = Long.parseLong(newLocalityCountry);
			if (localityService.getAll().stream()
					.anyMatch(locality -> locality.getCountry().getId() == newLocalityCountryId
							&& locality.getName().toLowerCase().contains(newLocality.toLowerCase()))) {
				validationResult.put("wrongLocality", "Such locality already exists in the system!");
			}
		} catch (NumberFormatException e) {
			validationResult.put("wrongLocalityCountry", "Check country selection!");
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		return validationResult;
	}
}
