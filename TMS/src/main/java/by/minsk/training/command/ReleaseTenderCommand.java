package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.RELEASE_TENDER;
import static by.minsk.training.dao.basic.CRUDDao.DATE_FORMATTER;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Tender;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TenderParticipationService;
import by.minsk.training.service.TenderService;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean(name = RELEASE_TENDER)
@AllArgsConstructor
@TransactionSupport
public class ReleaseTenderCommand implements ServletCommand {
	
	private static final Logger logger = LogManager.getLogger(ReleaseTenderCommand.class);
	
	private TenderService tenderService;
	private TenderParticipationService tenderParticipationService;

	@Override
	@Transactional
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		Set<UserEntity> tenderCarriers = (Set<UserEntity>)session.getAttribute("tenderCarriers");
		long tenderId = Long.parseLong(req.getParameter("id"));
		try {
			Tender tender = tenderService.get(tenderId);
		LocalDate tenderDeadline = req.getParameter("contestDeadline").equals("") || req.getParameter("contestDeadline") == null ? 
				null:LocalDate.parse(req.getParameter("contestDeadline"), DATE_FORMATTER);
		tender.setCompetitionDeadline(tenderDeadline);
		tenderCarriers.stream().peek(tsp -> {
			try {
				tenderParticipationService.sendTenderInvitation(tender, tsp);
			} catch (ServiceException e) {
				logger.error(e.getMessage());
				throw new IllegalArgumentException(e);
			}
		});
		tender.setState(Tender.State.RELEASED);
		tenderService.confirmTenderRelease(tender);
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
		session.removeAttribute("tenderCarriers");
		session.removeAttribute("availableTsps");
		try {
			String url = req.getParameter("local_url");
			session.setAttribute("renderedTenderId", tenderId);
			resp.sendRedirect(req.getContextPath() + url);		
		}
		catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
