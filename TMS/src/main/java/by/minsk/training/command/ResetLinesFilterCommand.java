package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.LINES_FILTER_RESET;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;

@Bean(name = LINES_FILTER_RESET)
public class ResetLinesFilterCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(ResetLinesFilterCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		String prefix1 = req.getParameter("prefix1");
		String url = req.getParameter("local_url");
		session.removeAttribute(prefix1 + "PickupCountry");
		session.removeAttribute(prefix1 + "PickupLocality");
		session.removeAttribute(prefix1 + "PickupAddress");
		session.removeAttribute(prefix1 + "DeliveryCountry");
		session.removeAttribute(prefix1 + "DeliveryLocality");
		session.removeAttribute(prefix1 + "DeliveryAddress");
		session.removeAttribute(prefix1 + "CargoName");
		session.removeAttribute(prefix1 + "LoadUnits");
		session.removeAttribute(prefix1 + "CargoWeight");
		session.removeAttribute(prefix1 + "CargoTruckType");
		session.removeAttribute(prefix1 + "RouteConditions");
		session.removeAttribute(prefix1 + "sToRender");
		try {
			resp.sendRedirect(req.getContextPath() + url);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
