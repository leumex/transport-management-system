package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDER_ASSIGN;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.service.TspPerformanceService;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserService;

@Bean(name = ORDER_ASSIGN)
@TransactionSupport
public class OrderAssignOverTenderCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(OrderAssignOverTenderCommand.class);

	private Validator validator;
	private TransportOrderService transportOrderService;
	private TspPerformanceService tspPerformanceService;
	private UserService userService;

	public OrderAssignOverTenderCommand(@BeanQualifier(value = "validateTenderOrderAssignment") Validator validator,
			TransportOrderService transportOrderService, TspPerformanceService tspPerformanceService,
			UserService userService) {
		super();
		this.validator = validator;
		this.transportOrderService = transportOrderService;
		this.tspPerformanceService = tspPerformanceService;
		this.userService = userService;
	}

	/*
	 * To assign the order to a tender nominee: 1. Create a TspPeformance entity and
	 * reflect order, tender and tsp identificators in it. 2. Change order state to
	 * ASSIGNED.
	 */

	@Override
	@Transactional
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getParameter("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (omissions != null) {
			omissions.keySet().stream().peek(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		long orderId = Long.parseLong(req.getParameter("order_id"));
		try {
			TransportOrder order = transportOrderService.findOrder(orderId);
			long tspId = Long.parseLong(req.getParameter("assignee"));
			UserEntity tsp = userService.findById(tspId);
			tspPerformanceService.commitAssignment(order, tsp);
			url = req.getContextPath() + url;
			resp.sendRedirect(url);
		} catch (ServiceException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
