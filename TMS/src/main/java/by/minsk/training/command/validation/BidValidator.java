package by.minsk.training.command.validation;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.minsk.training.ToolSet;
import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TransportOrderService;
import lombok.AllArgsConstructor;

@Bean(name = "validateNewBid")
@AllArgsConstructor
public class BidValidator implements Validator {

	private TransportOrderService transportOrderService;
	private SpotOrderService spotOrderService;

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = null;
		HttpSession session = req.getSession();
		String rate = req.getParameter("tsp_bid");
		if (!ToolSet.notEmpty(rate)) {
			omissions = new HashMap<>();
			omissions.put("wrongBid", "Input is blank");
		} else {
			try {
				Double.parseDouble(rate);
			} catch (NumberFormatException e) {
				omissions = omissions == null ? new HashMap<>() : omissions;
				omissions.put("wrongBid", "Invalid input, not a number");
			}
		}
		try {
			SpotTransportOrder spotOrder = spotOrderService.getSpotOrder(
					transportOrderService.findOrder(Long.parseLong((String) session.getAttribute("order_id"))));
			if (spotOrder.getSpotDeadline().isBefore(LocalDateTime.now())) {
				omissions = omissions == null ? new HashMap<>() : omissions;
				omissions.put("bidOutOfTime", "Spot auction deadline has already occured");
			}
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		return omissions;
	}
}
