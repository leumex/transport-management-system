package by.minsk.training.command.validation;

import java.time.LocalDate;
import static by.minsk.training.dao.basic.CRUDDao.DATE_FORMATTER;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.ToolSet;
import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import lombok.AllArgsConstructor;

@Bean(name = "validateOrderTerms")
@AllArgsConstructor
public class OrderReviseTermsValidator implements Validator {

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = null;
		String loadingDate = req.getParameter("new_loading_date");
		String deliveryTerm = req.getParameter("new_delivery_term");
		if (!ToolSet.notEmpty(loadingDate)) {
			omissions = new HashMap<>();
			omissions.put("wrongLoadingDate", "please insert new loading date value");
		} else {
			try {
				LocalDate date = LocalDate.parse(loadingDate, DATE_FORMATTER);
				if (date.isBefore(LocalDate.now())) {
					omissions = new HashMap<>();
					omissions.put("wrongLoadingDate", "the new loading date is in the past!");
				}
			} catch (NumberFormatException e) {
				omissions = new HashMap<>();
				omissions.put("wrongLoadingDate", "invalid loading date input");
			}
		}
		if (!ToolSet.notEmpty(deliveryTerm)) {
			omissions = omissions != null ? omissions : new HashMap<>();
			omissions.put("wrongDeliveryTerm", "please insert new delivery term value");
		} else {
			try {
				Integer term = Integer.parseInt(deliveryTerm);
				if (term < 2 || term > 90) {
					omissions = omissions != null ? omissions : new HashMap<>();
					omissions.put("wrongDeliveryTerm", "inserted value is beyond adequate boundaries");
				}
			} catch (NumberFormatException e) {
				omissions = omissions != null ? omissions : new HashMap<>();
				omissions.put("wrongDeliveryTerm", "invalid delivery term input");
			}
		}
		String orderId = req.getParameter("orderId");
		if (!ToolSet.notEmpty(orderId)) {
			omissions = omissions != null ? omissions : new HashMap<>();
			omissions.put("wrongOrderId", "transport order id is not defined");
		} else {
			try {
				Long.parseLong(orderId);
			} catch (NumberFormatException e) {
				omissions = omissions != null ? omissions : new HashMap<>();
				omissions.put("wrongOrderId", "transport order id has invalid (not parsable) value");
			}
		}
		return omissions;
	}
}
