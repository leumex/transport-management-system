package by.minsk.training.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.service.ServiceException;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserService;
import lombok.AllArgsConstructor;

import static by.minsk.training.ApplicationConstants.COMPANY_SEARCH;

import java.io.IOException;
import java.util.Set;

@Bean(name = COMPANY_SEARCH)
@AllArgsConstructor
public class LookForCompanyCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(LookForCompanyCommand.class);

	private UserService userService;

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String receivedInput = req.getParameter("userInput");
		String url = req.getParameter("local_url");
		long id;
		UserEntity company = null;
		Set<UserEntity> foundCompanies = null;
		try {
			id = Long.parseLong(receivedInput);
		} catch (NumberFormatException e) {
			id = 0;
		}
		try {
			if (id != 0) {
				company = userService.findCompanyById(id);
			} else {
				foundCompanies = userService.findByName(receivedInput);
			}
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
		if (company == null & foundCompanies == null) {
			req.setAttribute("emptyResult", "Nothing has been found");
		}

		if (foundCompanies != null & foundCompanies.size() > 1) {
			req.setAttribute("foundCompanies", foundCompanies);
		} else if (foundCompanies != null & foundCompanies.size() > 0) {
			company = foundCompanies.iterator().next();
			req.getSession().setAttribute("renderedCompany", company);
		}
		try {
			req.getServletContext().getRequestDispatcher(url).forward(req, resp);
		} catch (IOException | ServletException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
