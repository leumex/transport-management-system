package by.minsk.training.command.pagination;

/*unique pagination indices store of all diverse collections rendering throughout the app*/
public enum PaginationIndices {
	TENDER_LINES_INDEX, LINE_AWARDS_INDEX, UNCOMPLETED_ORDERS_INDEX,  //tender page
	FILTERED_LINES, SELECTED_LINES,  // tender design page
	TENDERS_INDEX,   //tenders page
	SHIPMENTS_CONTENT,   //shipments page
	HOT_ORDERS, NOTIFICATIONS_INDEX,  //layout page
	BIDS_INDEX,             // shipment page
	SPOTS_INDEX,   // spot page
	AVAILABLE_TENDERS, AVAILABLE_LINES; //shipment design page
 
	public String provideIndex() {
		return this.toString().toLowerCase();
	}
}