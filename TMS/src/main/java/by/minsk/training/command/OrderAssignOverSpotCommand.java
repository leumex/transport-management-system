package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDER_BESTOW;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.service.BidsService;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TspPerformanceService;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean(name = ORDER_BESTOW)
@AllArgsConstructor
@TransactionSupport
public class OrderAssignOverSpotCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(OrderAssignOverSpotCommand.class);

	private BidsService bidsService;
	private TspPerformanceService tspPerformanceService;

	@Override
	@Transactional
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getContextPath() + req.getParameter("local_url");
		long bidId = Long.parseLong(req.getParameter("bid_id"));
		try {
			Bid bid = bidsService.find(bidId);
			TransportOrder order = bid.getSpotOrder().getTransportOrder();
			UserEntity tsp = bid.getTsp();
			tspPerformanceService.commitAssignment(order, tsp);
			resp.sendRedirect(url);
		} catch (ServiceException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
