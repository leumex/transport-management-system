package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.LOGIN_USER;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.security.AuthorizedException;
import by.minsk.training.security.SecurityService;
import lombok.AllArgsConstructor;

@Bean(name = LOGIN_USER)
@AllArgsConstructor
public class LoginUserCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(LoginUserCommand.class);
	private SecurityService service;

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		logger.debug("LoginUser command has been invoked");
		String login = req.getParameter("Login");
		String password = req.getParameter("Password");
		boolean loggedOn;
		try {
			loggedOn = service.logIn(req, login, password);
			if (loggedOn) {
				logger.debug("successful logging");
				req.setAttribute("loginSuccess", true);
			} else {
				req.setAttribute("loginFailure", true);
			}
			req.getServletContext().getRequestDispatcher("/auth").forward(req, resp);
		} catch (AuthorizedException | ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
