package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDER_REVISE;
import static by.minsk.training.dao.basic.CRUDDao.DATE_FORMATTER;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;

@Bean(name = ORDER_REVISE)
@TransactionSupport
public class OrderReviseCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(OrderReviseCommand.class);

	private Validator validator;
	private TransportOrderService transportOrderService;

	public OrderReviseCommand(@BeanQualifier("validateOrderTerms") Validator validator,
			TransportOrderService transportOrderService) {
		super();
		this.validator = validator;
		this.transportOrderService = transportOrderService;
	}

	@Override
	@Transactional
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getParameter("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (omissions != null) {
			omissions.keySet().stream().peek(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		long orderId = Long.parseLong(req.getParameter("orderId"));
		try {
			TransportOrder order = transportOrderService.findOrder(orderId);
			LocalDate newLoadingDate = LocalDate.parse(req.getParameter("new_loading_date"), DATE_FORMATTER);
			int newDeliveryTerm = Integer.parseInt(req.getParameter("new_delivery_term"));
			order.setLoadingDate(newLoadingDate);
			order.setDeliveryTerm(newDeliveryTerm);
			if (transportOrderService.updateOrder(order)) {
				resp.sendRedirect(req.getContextPath() + url);
				return;
			} else {
				req.setAttribute("updateFailure", "error while order " + orderId + " terms update");
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
			}
		} catch (ServiceException | ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
