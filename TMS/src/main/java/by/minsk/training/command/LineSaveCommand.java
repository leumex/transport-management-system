package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.NEW_LINE;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.Truckload;
import by.minsk.training.entity.Truckload.TruckType;
import by.minsk.training.service.AddressService;
import by.minsk.training.service.LineService;
import by.minsk.training.service.RouteService;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TruckloadService;

@Bean(name = NEW_LINE)
@TransactionSupport
public class LineSaveCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(LineSaveCommand.class);

	private LineService lineService;
	private Validator lineValidator;
	private TruckloadService truckloadService;
	private AddressService addressService;
	private RouteService routeService;

	public LineSaveCommand(LineService lineService, @BeanQualifier(value = "lineValidator") Validator lineValidator,
			TruckloadService truckloadService, AddressService addressService, RouteService routeService) {
		super();
		this.lineService = lineService;
		this.lineValidator = lineValidator;
		this.truckloadService = truckloadService;
		this.addressService = addressService;
		this.routeService = routeService;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		Map<String, String> omissions = lineValidator.validate(req);
		String url = req.getParameter("local_url");
		if (!omissions.isEmpty()) {
			omissions.keySet().forEach(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		HttpSession session = req.getSession();
		String prefix1 = req.getParameter("prefix1");
		String prefix2 = req.getParameter("prefix2");
		try {
			long newLinePickupAddress = Long.parseLong(req.getParameter(prefix2 + "PickupAddress"));
			long newLineDeliveryAddress = Long.parseLong(req.getParameter(prefix2 + "DeliveryAddress"));
			String newLineCargoName = req.getParameter(prefix2 + "CargoName");
			String lineLoadUnits = req.getParameter(prefix2 + "LoadUnits");
			String lineCargoWeight = req.getParameter(prefix2 + "CargoWeight");
			Integer newLineLoadUnits = Integer.parseInt(lineLoadUnits);
			Double newLineCargoWeight = Double.parseDouble(lineCargoWeight);
			String lineCargoTruckType = req.getParameter(prefix2 + "CargoTruckType");
			Truckload.TruckType newLineCargoTruckType = TruckType.getType(lineCargoTruckType.toLowerCase().trim());
			String newLineRouteConditions = req.getParameter(prefix2 + "RouteConditions");
			logger.debug("LineSaveCommand is invoked with a set of parameters: \n" + "newLinePickupAddress =  "
					+ newLinePickupAddress + ";\n " + "newLineDeliveryAddress =  " + newLineDeliveryAddress + ";\n "
					+ "newLineCargoName =  " + newLineCargoName + ";\n " + "newLineLoadUnits =  " + newLineLoadUnits
					+ ";\n " + "newLineCargoWeight =  " + newLineCargoWeight + ";\n " + "newLineCargoTruckType =  "
					+ newLineCargoTruckType + ";\n " + "newLineRouteConditions =  " + newLineRouteConditions + ";\n");
			Route.Conditions conditions = Route.Conditions.getCondition(newLineRouteConditions.trim());
			Truckload truckload = new Truckload();
			truckload.setName(newLineCargoName);
			truckload.setWeight(newLineCargoWeight);
			truckload.setLoadUnits(newLineLoadUnits);
			truckload.setTruckType(newLineCargoTruckType);
			truckloadService.persist(truckload);
			Address pickupAddress = addressService.get(newLinePickupAddress);
			Address deliveryAddress = addressService.get(newLineDeliveryAddress);
			Route route = new Route();
			route.setConditions(conditions);
			route.setStart(pickupAddress);
			route.setDestination(deliveryAddress);
			routeService.persist(route);
			Line newLine = new Line();
			newLine.setRoute(route);
			newLine.setTruckload(truckload);
			lineService.persist(newLine);
			Set<Line> linesToRender = (Set<Line>) session.getAttribute(prefix1 + "sToRender");
			linesToRender.add(newLine);
			session.removeAttribute(prefix1 + "PickupCountry");
			session.removeAttribute(prefix1 + "PickupLocality");
			session.removeAttribute(prefix1 + "PickupAddress");
			session.removeAttribute(prefix1 + "DeliveryCountry");
			session.removeAttribute(prefix1 + "DeliveryLocality");
			session.removeAttribute(prefix1 + "CargoName");
			session.removeAttribute(prefix1 + "LoadUnits");
			session.removeAttribute(prefix1 + "CargoWeight");
			session.removeAttribute(prefix1 + "CargoTruckType");
			session.removeAttribute(prefix1 + "RouteConditions");
			session.removeAttribute("newShipmentNewTender");
			resp.sendRedirect(req.getContextPath() + url);
		} catch (ServiceException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
