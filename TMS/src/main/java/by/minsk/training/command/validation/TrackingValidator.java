package by.minsk.training.command.validation;

import static by.minsk.training.dao.basic.CRUDDao.DATETIME_FORMATTER;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.ToolSet;
import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;

@Bean(name = "validateTracking")
public class TrackingValidator implements Validator {

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = new HashMap<>();
		String plateTemplate = "/[A-Z\\d]{5,9}";
		String tractor = ToolSet.notEmpty(req.getParameter("tractor")) ? req.getParameter("tractor") : null;
		String semitrailer = ToolSet.notEmpty(req.getParameter("semitrailer")) ? req.getParameter("semitrailer") : null;
		String arrivedForLoading = ToolSet.notEmpty(req.getParameter("loading_arrival"))
				? req.getParameter("loading_arrival")
				: null;
		String leftLoading = ToolSet.notEmpty(req.getParameter("loading_departure"))
				? req.getParameter("loading_departure")
				: null;
		String arrivedForUnloading = ToolSet.notEmpty(req.getParameter("unloading_arrival"))
				? req.getParameter("unloading_arrival")
				: null;
		String leftUnloading = ToolSet.notEmpty(req.getParameter("unloading_departure"))
				? req.getParameter("unloading_departure")
				: null;
		if (tractor != null) {
			if (!Pattern.matches(plateTemplate, tractor)) {
				omissions.put("wrongTractor",
						"Check 'tractor' input. Only UpperCase Latin letters and digits are allowed. Input has to be 5 to 9 characters long.");
			}
		}
		if (semitrailer != null) {
			if (!Pattern.matches(plateTemplate, semitrailer)) {
				omissions.put("wrongSemitrailer",
						"Check 'semitrailer' input. Only UpperCase Latin letters and digits are allowed. Input has to be 5 to 9 characters long.");
			}
		}
		if (arrivedForLoading != null) {
			try {
				LocalDateTime.parse(arrivedForLoading, DATETIME_FORMATTER);
			} catch (NumberFormatException e) {
				omissions.put("wrongLoading_arrival", "invalid input");
			}
		}
		if (leftLoading != null) {
			try {
				LocalDateTime.parse(leftLoading, DATETIME_FORMATTER);
			} catch (NumberFormatException e) {
				omissions.put("wrongLoading_departure", "invalid input");
			}
		}
		if (arrivedForUnloading != null) {
			try {
				LocalDateTime.parse(arrivedForUnloading, DATETIME_FORMATTER);
			} catch (NumberFormatException e) {
				omissions.put("wrongUnloading_arrival", "invalid input");
			}
		}
		if (leftUnloading != null) {
			try {
				LocalDateTime.parse(leftUnloading, DATETIME_FORMATTER);
			} catch (NumberFormatException e) {
				omissions.put("wrongUnloading_departure", "invalid input");
			}
		}
		return omissions;
	}
}
