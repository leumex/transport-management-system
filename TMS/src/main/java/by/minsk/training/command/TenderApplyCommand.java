package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.TENDER_APPLY;
import static by.minsk.training.ToolSet.notEmpty;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Tender;
import by.minsk.training.service.LineService;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TenderService;
import lombok.AllArgsConstructor;

@Bean(name = TENDER_APPLY)
@AllArgsConstructor
public class TenderApplyCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(TenderApplyCommand.class);

	private TenderService tenderService;
	private LineService lineService;

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		try {
			String url = req.getParameter("local_url");
			String tenderId = req.getParameter("tender_id");
			long id;
			try {
				if (!notEmpty(tenderId))
					throw new NumberFormatException("Tender id value is not clarified!");
				id = Long.parseLong(tenderId);
			} catch (NumberFormatException e) {
				req.setAttribute("tenderNotSelected", e.getMessage());
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			}
			Tender tender = tenderService.get(id);
			Set<Line> lines = lineService.getByTender(id);
			HttpSession session = req.getSession();
			session.setAttribute("newShipmentTender", tender);
			session.setAttribute("newShipmentNewLinesToRender", lines);
			resp.sendRedirect(req.getServletContext().getContextPath() + url);
		} catch (ServiceException | ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
