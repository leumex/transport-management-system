package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.CANCEL_SPOT;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TransportOrderService;
import lombok.AllArgsConstructor;

@Bean(name = CANCEL_SPOT)
@TransactionSupport
@AllArgsConstructor
public class SpotRecallCommand implements ServletCommand {
	
	private static final Logger logger = LogManager.getLogger(SpotRecallCommand.class);

	private TransportOrderService transportOrderService;
	private SpotOrderService spotOrderService;

	@Override
	@Transactional
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getContextPath() + req.getParameter("local_url");
		long orderId = Long.parseLong(req.getParameter("order_id"));
		try {
			TransportOrder order = transportOrderService.findOrder(orderId);
			if (spotOrderService.commitOngoingSpotAbort(order)) {
				resp.sendRedirect(url);
			} else {
				req.setAttribute("spotAbortFailure",
						"Failed to abort the ongoing spot auction over transport order " + orderId);
				req.getRequestDispatcher(url).forward(req, resp);
			}
		} catch (ServiceException | ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
