package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static by.minsk.training.ToolSet.notEmpty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.Line;

@Bean(name = "validateTenderLineRemoval")
public class NewTenderLinePullOutValidator implements Validator {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = null;
		HttpSession session = req.getSession();
		Set<Line> newTenderLines = (Set<Line>) session.getAttribute("newTenderSelectedLines");
		if (newTenderLines == null || newTenderLines.isEmpty()) {
			omissions = new HashMap<>();
			omissions.put("newTenderSelectedLinesEmptySet", "New tender lines set is empty!");
		} else {
			String lineId = req.getParameter("lineToRemoveFromNewTender");
			if (!notEmpty(lineId)) {
				omissions = new HashMap<>();
				omissions.put("wrongLineToRemoveFromNewTender",
						"No line has been chosen for removal from new tender selected lines set!");
			}
		}
		return omissions;
	}
}
