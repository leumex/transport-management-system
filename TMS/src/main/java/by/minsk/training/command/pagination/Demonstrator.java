package by.minsk.training.command.pagination;

import java.util.Collection;
import java.util.List;

public interface Demonstrator {
	<T> List<Collection<T>> divideCollection(Collection<T> collection, int sectorSize);
}
