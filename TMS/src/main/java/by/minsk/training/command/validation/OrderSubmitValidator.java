package by.minsk.training.command.validation;

import static by.minsk.training.ToolSet.notEmpty;
import static by.minsk.training.dao.basic.CRUDDao.DATE_FORMATTER;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;

@Bean(name = "validateNewOrder")
public class OrderSubmitValidator implements Validator {

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = null;
		String loadingDate = req.getParameter("loadingDate");
		String deliveryTerm = req.getParameter("deliveryTerm");
		if (!notEmpty(loadingDate)) {
			omissions = new HashMap<>();
			omissions.put("invalidLoadingDate", "Loading date value is missing!");
		}
		if (!notEmpty(deliveryTerm)) {
			omissions = omissions == null ? new HashMap<>() : omissions;
			omissions.put("invalidDeliveryTerm", "Delivery term value is missing!");
		}
		try {
			LocalDate lDate = LocalDate.parse(loadingDate, DATE_FORMATTER);
			if (lDate.isBefore(LocalDate.now().plusDays(1))) {
				omissions = omissions == null ? new HashMap<>() : omissions;
				omissions.put("invalidLoadingDate", "Loading date input must be more than 1 day ahead of presence!");
			}
		} catch (DateTimeException e) {
			omissions = omissions == null ? new HashMap<>() : omissions;
			omissions.put("invalidLoadingDate", "Loading date input is not a valid date!");
		}
		try {
			int dTerm = Integer.parseInt(deliveryTerm);
			if (dTerm < 1 && dTerm > 20) {
				omissions = omissions == null ? new HashMap<>() : omissions;
				omissions.put("invalidDeliveryTerm", "Delivery term input value must be from 2 to 19 days!");
			}
		} catch (NumberFormatException e) {
			omissions = omissions == null ? new HashMap<>() : omissions;
			omissions.put("invalidDeliveryTerm", "Delivery term input is not a valid number!");
		}
		return omissions;
	}
}
