package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import lombok.AllArgsConstructor;

@Bean(name = "regValidator")
@AllArgsConstructor
public class RegValidator implements Validator {

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		Map<String, String> omissions = new HashMap<>();
		if (!Pattern.matches("^[a-zA-Z]([a-zA-Z0-9.-]){0,8}([a-zA-Z0-9])?$", login)) {
			omissions.put("incorrect_login", "incorrectLogin");
		}
		if (!Pattern.matches(".{5,}$", password)) {
			omissions.put("poor_password", "poorPassword");
		}
		return omissions;
	}
}
