package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.NEW_BID;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.service.BidsService;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.user.UserEntity;

@Bean(name = NEW_BID)
public class BidSaveCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(BidSaveCommand.class);

	private Validator validator;
	private BidsService bidsService;
	private TransportOrderService transportOrderService;
	private SpotOrderService spotOrderService;

	public BidSaveCommand(@BeanQualifier(value = "validateNewBid") Validator validator, BidsService bidsService,
			TransportOrderService transportOrderService, SpotOrderService spotOrderService) {
		super();
		this.validator = validator;
		this.bidsService = bidsService;
		this.transportOrderService = transportOrderService;
		this.spotOrderService = spotOrderService;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getContextPath() + req.getParameter("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (omissions != null) {
			omissions.keySet().stream().peek(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		Double rate = Double.parseDouble(req.getParameter("tsp_bid"));
		HttpSession session = req.getSession();
		long orderId = Long.parseLong(req.getParameter("orderId"));
		try {
			SpotTransportOrder spotOrder = spotOrderService.getSpotOrder(transportOrderService.findOrder(orderId));
			UserEntity tsp = (UserEntity) session.getAttribute("user");
			Bid bid = new Bid();
			bid.setBid(rate);
			bid.setMadeAt(LocalDateTime.now());
			bid.setTsp(tsp);
			bid.setSpotOrder(spotOrder);
			bidsService.commitNewBid(bid);
			req.getRequestDispatcher(url).forward(req, resp);
		} catch (ServiceException | ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
