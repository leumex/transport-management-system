package by.minsk.training.command;

public class CommandException extends Exception {
 
	private static final long serialVersionUID = -195073159932910132L;

	public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }

    protected CommandException(String message, Throwable cause, boolean enableSuppression, boolean writeStackTrace) {
        super(message, cause, enableSuppression, writeStackTrace);
    }
}
