package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.ToolSet;
import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;
import lombok.AllArgsConstructor;

@Bean(name = "validateOrderAcceptance")
@AllArgsConstructor
public class OrderAcceptanceValidator implements Validator {

	private TransportOrderService transportOrderService;

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = new HashMap<>();
		String orderId = req.getParameter("order_id");
		if (!ToolSet.notEmpty(orderId)) {
			omissions.put("wrong_order_id", "Check if the order is chosen for acceptance");
			return omissions;
		}
		try {
			TransportOrder order = transportOrderService.findOrder(Long.parseLong(orderId));
			if (order.getState() != State.ASSIGNED) {
				omissions.put("wrong_order_id", "found order has not the ASSIGNED state!");
			}
		} catch (NumberFormatException e) {
			throw new CommandException("HttpRequest contains order_id parameter that isn't parsable into long type");
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		return omissions;
	}
}
