package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.OPEN_SPOT;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TransportOrderService;
import lombok.AllArgsConstructor;

@Bean(name = OPEN_SPOT)
@AllArgsConstructor
public class SpotReleaseCommand implements ServletCommand {
	
	private static final Logger logger = LogManager.getLogger(SpotReleaseCommand.class);
	
	private TransportOrderService transportOrderService;
	private SpotOrderService spotOrderService;

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getContextPath() + req.getParameter("local_url");
		long orderId = Long.parseLong(req.getParameter("order_id"));
		try {
		TransportOrder order = transportOrderService.findOrder(orderId);
		if (spotOrderService.commitNewSpotAuction(order)) {
			resp.sendRedirect(url);
		} else {
			req.setAttribute("spotReleaseFailure", "Failed to send transport order " + orderId + " on spot");
			req.getRequestDispatcher(url).forward(req,resp);
		}
		}
		catch (ServiceException | IOException | ServletException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}		
	}
}
