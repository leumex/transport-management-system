package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.NEW_COUNTRY;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.entity.Country;
import by.minsk.training.service.CountryService;
import by.minsk.training.service.ServiceException;

@Bean(name = NEW_COUNTRY)
public class CountrySaveCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(CountrySaveCommand.class);

	private CountryService countryService;
	private Validator validator;

	public CountrySaveCommand(CountryService countryService,
			@BeanQualifier(value = "validateNewCountry") Validator validator) {
		super();
		this.countryService = countryService;
		this.validator = validator;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		String url = (String)session.getAttribute("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (!omissions.isEmpty()) {
			omissions.entrySet().forEach(entry -> req.setAttribute(entry.getKey(), entry.getValue()));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		logger.debug("Request parameter validation is passed successfully");
		String newCountryName = req.getParameter("country");
		Country newCountry = new Country();
		newCountry.setName(newCountryName);
		try {
			countryService.persist(newCountry);
			resp.sendRedirect(req.getContextPath() + url);
		} catch (ServiceException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
