package by.minsk.training.command.pagination;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import by.minsk.training.core.Bean;

@Bean
public class TableDemonstrator implements Demonstrator {

	@Override
	public <T> List<Collection<T>> divideCollection(Collection<T> collection, int sectorSize) {
		if (collection == null) {
			return null;
		}
		Iterator<T> iterator = collection.iterator();
		Integer sectorsNumber = (collection.size() % sectorSize == 0 ? collection.size() / sectorSize
				: (collection.size() / sectorSize + 1));
		List<Collection<T>> sectors = new ArrayList<Collection<T>>(sectorsNumber);
		int count = 0;
		Collection<T> sector = new ArrayList<T>();
		while (iterator.hasNext()) {
			if (count < sectorSize) {
				sector.add(iterator.next());
				count++;
			} else {
				sectors.add(sector);
				sector = new ArrayList<T>();
				count = 0;
			}
		}
		if (sector.size() > 0) {
			sectors.add(sector);
		}
		return sectors;
	}
}
