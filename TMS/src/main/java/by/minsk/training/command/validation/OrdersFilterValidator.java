package by.minsk.training.command.validation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import static by.minsk.training.dao.basic.CRUDDao.DATETIME_FORMATTER;
import static by.minsk.training.dao.basic.CRUDDao.DATE_FORMATTER;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;

@Bean(name = "ordersFilterValidator")
public class OrdersFilterValidator implements Validator {

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = new HashMap<>();
		String id_filter = req.getParameter("id_filter");
		String date_filter = req.getParameter("date_filter");
		String pickup_date_filter = req.getParameter("pickup_date_filter");
		String rate_filter = req.getParameter("rate_filter");

		try {
			if (notEmpty(id_filter)) {
				Long.parseLong(id_filter);
			}
		} catch (NumberFormatException e) {
			omissions.put("wrong_id_filter", "incorrect id value input");
		}
		try {
			if (notEmpty(date_filter)) {
				LocalDateTime.parse(date_filter, DATETIME_FORMATTER);
			}
		} catch (DateTimeParseException e) {
			omissions.put("wrong_date_filter", "incorrect date value input");
		}
		try {
			if (notEmpty(pickup_date_filter)) {
				LocalDate.parse(pickup_date_filter, DATE_FORMATTER);
			}
		} catch (DateTimeParseException e) {
			omissions.put("wrong_pickup_date_filter", "incorrect pickup date value input");
		}
		try {
			if (notEmpty(rate_filter)) {
				Double.parseDouble(rate_filter);
			}
		} catch (NumberFormatException e) {
			omissions.put("wrong_rate_filter", "incorrect rate value input");
		}
		return omissions;
	}

	private boolean notEmpty(String value) {
		return value != null && !value.equals("");
	}

}
