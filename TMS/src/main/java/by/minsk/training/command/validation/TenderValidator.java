package by.minsk.training.command.validation;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.Line;

@Bean(name = "validateTender")
public class TenderValidator implements Validator {

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> validationResult = new HashMap<>();
		String title = req.getParameter("tenderTitle");
		if (title == null || !title.matches("(^[A-Z]){1}([A-Z]|[a-z]|[0-9]){2,25}(\\s)?([A-Z]|[a-z]|[0-9]){1,25}$")) {
			validationResult.put("wrongNewTenderTitle", "New tender title should begins with a capital letter; "
					+ "should contain only letters and digits, and 1 space at most; should be min 5 and max 52 characters long!");
		}
		try {
			Integer.parseInt(req.getParameter("tripsQuantity"));
		} catch (NumberFormatException e) {
			validationResult.put("wrongNewTenderTripsQuantity", "invalid trips quantity input!");
		}
		try {
			LocalDate.parse(req.getParameter("tenderStart"));
		} catch (DateTimeParseException e) {
			validationResult.put("wrongNewTenderStart", "Check tender start date input!");
		}
		try {
			LocalDate.parse(req.getParameter("tenderEnd"));
		} catch (DateTimeParseException e) {
			validationResult.put("wrongNewTenderEnd", "Check tender end date input!");
		}
		@SuppressWarnings("unchecked")
		Set<Line> lines = (Set<Line>) req.getSession().getAttribute("newTenderSelectedLines");
		if (lines == null || lines.isEmpty()) {
			validationResult.put("wrongNewTenderLinesSet", "New tender' lines set is empty!");
		}
		return validationResult;
	}
}
