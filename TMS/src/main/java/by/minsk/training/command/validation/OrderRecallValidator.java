package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.ToolSet;
import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;

@Bean(name = "validateOrderRecall")
public class OrderRecallValidator implements Validator {

	private TransportOrderService transportOrderService;

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = null;
		String order_id = req.getParameter("order_id");
		if (!ToolSet.notEmpty(order_id)) {
			omissions = omissions == null ? new HashMap<>() : omissions;
			omissions.put("wrong_order_id", "check the order selection");
		} else {
			try {
				long id = Long.parseLong(order_id);
				TransportOrder order = transportOrderService.findOrder(id);
				if (order == null) {
					omissions = omissions == null ? new HashMap<>() : omissions;
					omissions.put("wrong_order_id", "No order is found with given order_id value");
				} else {
					State state = order.getState();
					if (state.equals(State.CREATED) || state.equals(State.COMPLETED) || state.equals(State.ANNOUNCED)
							|| state.equals(State.WITHDRAWN)) {
						omissions = omissions == null ? new HashMap<>() : omissions;
						omissions.put("invalid_order_state", "order has state " + order.getState().toString()
								+ ", that denies order recall operation");
					}
				}
			} catch (NumberFormatException e) {
				omissions = omissions == null ? new HashMap<>() : omissions;
				omissions.put("wrong_order_id", "received order id is not a number");
			} catch (ServiceException e) {
				throw new CommandException(e);
			}
		}
		return omissions;
	}
}
