package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.BID_REMOVE;
import static by.minsk.training.ToolSet.notEmpty;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.service.BidsService;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.service.TspPerformanceService;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean(name = BID_REMOVE)
@AllArgsConstructor
@TransactionSupport
public class BidDeleteCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(BidDeleteCommand.class);
	private BidsService bidsService;
	private TspPerformanceService tspPerformanceService;
	private TransportOrderService transportOrderService;
	private SpotOrderService spotOrderService;

	@Override
	@Transactional
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getContextPath() + req.getParameter("local_url");
		HttpSession session = req.getSession();
		long bidId = notEmpty(req.getParameter("bid_id")) ? Long.parseLong(req.getParameter("bid_id")) : 0;
		Bid bid = null;
		try {
			if (bidId != 0) {
				bid = bidsService.find(bidId);
			} else if (notEmpty(req.getParameter("orderId"))) {
				long orderId = Long.parseLong(req.getParameter("orderId"));
				TransportOrder transportOrder = transportOrderService.findOrder(orderId);
				SpotTransportOrder spotOrder = spotOrderService.getSpotOrder(transportOrder);
				bid = bidsService.retrieveBid(spotOrder, (UserEntity) session.getAttribute("user"));
			}
			if (bid != null) {
				TspPerformance spotAssignment = tspPerformanceService.getSpotOrderAssignment(bid);
				if (spotAssignment == null) {
					bidsService.removeBid(bid);
				} else {
					req.setAttribute("bidEliminationError", "there's an assignment bound to the bid " + bid.getId());
					req.getRequestDispatcher(url).forward(req, resp);
					return;
				}
			} else {
				req.setAttribute("bidEliminationError", "invalid bid id value has been sent for processing");
				req.getRequestDispatcher(url).forward(req, resp);
				return;
			}
			resp.sendRedirect(url);
		} catch (ServiceException | ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
