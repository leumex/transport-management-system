package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDER_REJECT;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.user.UserEntity;

@Bean(name = ORDER_REJECT)
public class OrderRejectCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(OrderRejectCommand.class);

	private TransportOrderService transportOrderService;
	private Validator validator;

	public OrderRejectCommand(TransportOrderService transportOrderService,
			@BeanQualifier(value = "validateOrderAcceptance") Validator validator) {
		super();
		this.transportOrderService = transportOrderService;
		this.validator = validator;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getParameter("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (!omissions.isEmpty()) {
			omissions.keySet().stream().forEach(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (IOException | ServletException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		HttpSession session = req.getSession();
		UserEntity user = (UserEntity) session.getAttribute("user");
		long orderId = Long.parseLong(req.getParameter("order_id"));
		try {
			transportOrderService.сommitOrderRejection(orderId, user);
			req.getServletContext().getRequestDispatcher(url).forward(req, resp);
		} catch (ServiceException | IOException | ServletException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
