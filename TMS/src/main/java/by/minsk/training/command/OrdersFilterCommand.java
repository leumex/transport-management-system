package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDERS_FILTER;
import static by.minsk.training.ToolSet.notEmpty;
import static by.minsk.training.dao.basic.CRUDDao.DATETIME_FORMATTER;
import static by.minsk.training.dao.basic.CRUDDao.DATE_FORMATTER;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.service.RouteService;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TransportOrderServiceImpl;
import by.minsk.training.service.TruckloadService;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;

@Bean(name = ORDERS_FILTER)
public class OrdersFilterCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(OrdersFilterCommand.class);
	private TransportOrderServiceImpl transportOrderService;
	private RouteService routeService;
	private TruckloadService truckloadService;
	private SpotOrderService spotOrderService;
	private Validator validator;

	public OrdersFilterCommand(TransportOrderServiceImpl transportOrderService, RouteService routeService,
			TruckloadService truckloadService, SpotOrderService spotOrderService,
			@BeanQualifier(value = "ordersFilterValidator") Validator validator) {
		super();
		this.transportOrderService = transportOrderService;
		this.routeService = routeService;
		this.truckloadService = truckloadService;
		this.spotOrderService = spotOrderService;
		this.validator = validator;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getParameter("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (!omissions.isEmpty()) {
			omissions.keySet().forEach(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		UserEntity user = (UserEntity) req.getSession().getAttribute("user");
		String id_filter = req.getParameter("id_filter");
		String date_filter = req.getParameter("date_filter");
		String pickup_date_filter = req.getParameter("pickup_date_filter");
		String route_filter = req.getParameter("route_filter");
		String truckload_filter = req.getParameter("truckload_filter");
		String company_filter = req.getParameter("company_filter");
		String scope_filter = req.getParameter("scope_filter");
		String state_filter = req.getParameter("state_filter");
		String rate_filter = req.getParameter("rate_filter");
		String plates_filter = req.getParameter("plates_filter");
		Predicate<TransportOrder> filter = (t1) -> true;
		filter = notEmpty(id_filter) ? filter.and((t1) -> t1.getId() == Long.parseLong(id_filter)) : filter;
		filter = notEmpty(date_filter)
				? filter.and((t1) -> t1.getIssued().equals(LocalDateTime.parse(date_filter, DATETIME_FORMATTER)))
				: filter;
		filter = notEmpty(pickup_date_filter)
				? filter.and(t1 -> t1.getLoadingDate().equals(LocalDate.parse(pickup_date_filter, DATE_FORMATTER)))
				: filter;
		filter = notEmpty(route_filter) ? filter.and(t1 -> routeService.deliverRepresentation(t1.getLine().getRoute())
				.toLowerCase().contains(route_filter.toLowerCase())) : filter;
		filter = notEmpty(truckload_filter)
				? filter.and(t1 -> truckloadService.deliverRepresentation(t1.getLine().getTruckload()).toLowerCase()
						.equals(truckload_filter.toLowerCase()))
				: filter;
		filter = notEmpty(company_filter) ? filter.and(t1 -> {
			try {
				return restoreCompanyRepresentation(t1, user).toLowerCase().contains(company_filter.toLowerCase());
			} catch (ServiceException e) {
				logger.error(e.getMessage());
				throw new RuntimeException(e);
			}
		}) : filter;
		filter = notEmpty(scope_filter)
				? filter.and(t1 -> restoreScope(t1).toLowerCase().contains(scope_filter.toLowerCase()))
				: filter;
		filter = notEmpty(state_filter) ? filter.and(t1 -> {
			try {
				return restoreOrderState(t1, user).toLowerCase().contains(state_filter.toLowerCase());
			} catch (ServiceException e1) {
				logger.error(e1.getMessage());
				throw new RuntimeException(e1);
			}
		}) : filter;
		filter = notEmpty(rate_filter) ? filter.and(t1 -> {
			try {
				return transportOrderService.defineOrderRate(user, t1) == Double.parseDouble(rate_filter);
			} catch (NumberFormatException | ServiceException e) {
				logger.error(e.getMessage());
				throw new RuntimeException(e);
			}
		}) : filter;
		filter = notEmpty(plates_filter) ? filter.and(t1 -> {
			try {
				return restoreTruckPlates(user, t1).toLowerCase().contains(plates_filter.toLowerCase());
			} catch (ServiceException e) {
				logger.error(e.getMessage());
				throw new RuntimeException(e);
			}
		}) : filter;
		try {
			Set<TransportOrder> user_shipments = transportOrderService.gatherOrders(user).stream().filter(filter)
					.collect(Collectors.toSet());
			req.getSession().setAttribute("user_shipments", user_shipments);
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
		try {
			req.getServletContext().getRequestDispatcher(url).forward(req, resp);
		} catch (ServletException | IOException e) {
			logger.debug(e.getMessage());
			throw new CommandException("failure during executing of request forwarding back to the page", e);
		}
	}

	private String restoreCompanyRepresentation(TransportOrder order, UserEntity user) throws ServiceException {
		if (user.getRole().equals(Roles.CUSTOMER)) {
			if (spotOrderService.hasOpenSpotAuction(order)) {
				return "spot";
			} else if (order.getState().equals(State.CREATED)) {
				return "";
			} else
				return transportOrderService.getLatestAssignment(user, order).getTsp().getName();
		} else if (user.getRole().equals(Roles.TRANSPORT_SERVICE_PROVIDER)) {
			return order.getCustomer() == null ? order.getTender().getCustomer().getName()
					: order.getCustomer().getName();
		} else {
			StringBuilder builder = new StringBuilder();
			builder.append(order.getCustomer() == null ? order.getTender().getCustomer().getName()
					: order.getCustomer().getName());
			builder.append(" / ");
			if (spotOrderService.hasOpenSpotAuction(order)) {
				builder.append("spot");
			} else if (order.getState().equals(State.CREATED)) {
				return builder.toString();
			} else {
				builder.append(transportOrderService.getLatestAssignment(user, order).getTsp().getName());
			}
			return builder.toString();
		}
	}

	private String restoreScope(TransportOrder order) {
		return order.getCustomer() != null ? (order.getState().equals(State.CREATED) ? "no scope" : "spot")
				: new StringBuffer().append(order.getTender().getId()).append(" ").append(order.getTender().getTitle())
						.toString();
	}

	private String restoreTruckPlates(UserEntity user, TransportOrder order) throws ServiceException {
		TspPerformance assignment = transportOrderService.getLatestAssignment(user, order);
		return assignment != null ? (assignment.getTruckPlates() != null ? assignment.getTruckPlates() : "") : "";
	}

	private String restoreOrderState(TransportOrder order, UserEntity user) throws ServiceException {
		return new StringBuffer().append(order.getState().toString()).append(" / ")
				.append(transportOrderService.getLatestAssignment(user, order).getTspResponse().toString()).toString();
	}
}
