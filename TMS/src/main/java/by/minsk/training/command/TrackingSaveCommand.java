package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.TRACKING_UPDATE;
import static by.minsk.training.dao.basic.CRUDDao.DATETIME_FORMATTER;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.ToolSet;
import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TspPerformanceService;

@Bean(name = TRACKING_UPDATE)
public class TrackingSaveCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(TrackingSaveCommand.class);

	private TspPerformanceService tspPerformanceService;
	private Validator validator;

	public TrackingSaveCommand(TspPerformanceService tspPerformanceService,
			@BeanQualifier(value = "validateTracking") Validator validator) {
		super();
		this.tspPerformanceService = tspPerformanceService;
		this.validator = validator;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getContextPath() + req.getParameter("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (!omissions.isEmpty()) {
			omissions.keySet().stream().peek(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		String tractor = ToolSet.notEmpty(req.getParameter("tractor")) ? req.getParameter("tractor") : null;
		String semitrailer = ToolSet.notEmpty(req.getParameter("semitrailer")) ? req.getParameter("semitrailer") : null;

		String truckPlates = (tractor != null || semitrailer != null)
				? new StringBuilder().append(tractor != null ? tractor : "").append("/")
						.append(semitrailer != null ? semitrailer : "").toString()
				: null;
		LocalDateTime arrivedForLoading = ToolSet.notEmpty(req.getParameter("loading_arrival"))
				? LocalDateTime.parse(req.getParameter("loading_arrival"), DATETIME_FORMATTER)
				: null;
		LocalDateTime leftLoading = ToolSet.notEmpty(req.getParameter("loading_departure"))
				? LocalDateTime.parse(req.getParameter("loading_departure"), DATETIME_FORMATTER)
				: null;
		LocalDateTime arrivedForUnloading = ToolSet.notEmpty(req.getParameter("unloading_arrival"))
				? LocalDateTime.parse(req.getParameter("unloading_arrival"), DATETIME_FORMATTER)
				: null;
		LocalDateTime leftUnloading = ToolSet.notEmpty(req.getParameter("unloading_departure"))
				? LocalDateTime.parse(req.getParameter("unloading_departure"), DATETIME_FORMATTER)
				: null;
		TspPerformance assignment = (TspPerformance) req.getAttribute("assignment");
		boolean flag = false;
		if (truckPlates != null) {
			assignment.setTruckPlates(truckPlates);
			flag = true;
		}
		if (arrivedForLoading != null) {
			assignment.setArrivedForLoading(arrivedForLoading);
			flag = true;
		}
		if (leftLoading != null) {
			assignment.setLeftLoading(leftLoading);
			flag = true;
		}
		if (arrivedForUnloading != null) {
			assignment.setArrivedForUnloading(arrivedForUnloading);
			flag = true;
		}
		if (leftUnloading != null) {
			assignment.setLeftUnloading(leftUnloading);
			flag = true;
		}

		if (flag) {
			try {
				tspPerformanceService.persist(assignment);
			} catch (ServiceException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		try {
			resp.sendRedirect(url);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
