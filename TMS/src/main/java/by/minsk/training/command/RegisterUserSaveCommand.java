package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.REG_NEW_USER;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserService;

@Bean(name = REG_NEW_USER)
public class RegisterUserSaveCommand implements ServletCommand {
	private static final Logger logger = LogManager.getLogger(RegisterUserSaveCommand.class);

	private UserService userService;
	private Validator regValidator;

	public RegisterUserSaveCommand(UserService userService,
			@BeanQualifier(value = "regValidator") Validator regValidator) {
		super();
		this.userService = userService;
		this.regValidator = regValidator;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		logger.debug("from within RegisterSave command");
		Map<String, String> omissions = regValidator.validate(req);
		if (!omissions.isEmpty()) {
			for (Map.Entry<String, String> entry : omissions.entrySet()) {
				req.setAttribute(entry.getKey(), entry.getValue());
			}
			RequestDispatcher rd = req.getServletContext().getRequestDispatcher("/reg");
			try {
				rd.forward(req, resp);
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
			return;
		}
		UserEntity entity = new UserEntity();
		UserEntity.Roles role;
		try {
			String name = req.getParameter("name");
			switch ((req.getParameter("role")).trim().toLowerCase()) {
			case ("customer"): {
				role = UserEntity.Roles.CUSTOMER;
				break;
			}
			case ("carrier"): {
				role = UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER;
				break;
			}
			case ("admin"): {
				role = UserEntity.Roles.ADMIN;
				break;
			}
			default: {
				role = null;
				break;
			}
			}
			if (role == null) {
				throw new ServletException("New user hasn't defined his role");
			}

			String login = req.getParameter("login");
			String password = req.getParameter("password");

			String salt = BCrypt.gensalt();
			String hashPassword = BCrypt.hashpw(password, salt);
			LocalDate registrationDate = LocalDate.now();
			logger.debug(name + ", " + role + ", " + login + ", " + hashPassword
					+ " user details prepared for insert on " + registrationDate);
			entity.setLogin(login);
			entity.setName(name);
			entity.setSalt(salt);
			entity.setPassword(hashPassword);
			entity.setRole(role);
			entity.setRegistrationDate(registrationDate);
			final boolean saved = userService.registerUser(entity);
			if (saved) {
				req.setAttribute("registerSuccess", true);
				req.getServletContext().getRequestDispatcher("/reg").forward(req, resp);
				return;
			} else
				req.setAttribute("registerSuccess", false);
			req.getRequestDispatcher("/reg").forward(req, resp);
		} catch (ServletException | IOException e) {
			throw new CommandException("failed to register user", e);
		}
	}
}
