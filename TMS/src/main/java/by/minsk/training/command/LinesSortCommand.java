package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.LINES_SORT;

import java.io.IOException;
import java.util.Comparator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Line;
import lombok.AllArgsConstructor;

@Bean(name = LINES_SORT)
@AllArgsConstructor
public class LinesSortCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(LinesSortCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		String url = req.getParameter("local_url");
		String prefix1 = req.getParameter("prefix1");
		Comparator<Line> lineComparator;
		String sortArg = req.getParameter("sortArg");
		switch (sortArg) {
		case "id":
			lineComparator = (line1, line2) -> Long.compare(line1.getId(), line2.getId());
			break;
		case "startCountry":
			lineComparator = (line1, line2) -> line1.getRoute().getStart().getLocality().getCountry().getName()
					.compareTo(line2.getRoute().getStart().getLocality().getCountry().getName());
			break;
		case "startLocality":
			lineComparator = (line1, line2) -> line1.getRoute().getStart().getLocality().getName()
					.compareTo(line2.getRoute().getStart().getLocality().getName());
			break;
		case "endCountry":
			lineComparator = (line1, line2) -> line1.getRoute().getDestination().getLocality().getCountry().getName()
					.compareTo(line2.getRoute().getDestination().getLocality().getCountry().getName());
			break;
		case "endLocality":
			lineComparator = (line1, line2) -> line1.getRoute().getDestination().getLocality().getName()
					.compareTo(line2.getRoute().getDestination().getLocality().getName());
			break;
		case "conditions":
			lineComparator = (line1, line2) -> line1.getRoute().getConditions().toString()
					.compareTo(line2.getRoute().getConditions().toString());
			break;
		case "load":
			lineComparator = (line1, line2) -> line1.getTruckload().getName().compareTo(line2.getTruckload().getName());
			break;
		case "weight":
			lineComparator = (line1, line2) -> Double.compare(line1.getTruckload().getWeight(),
					line2.getTruckload().getWeight());
			break;
		case "loadUnits":
			lineComparator = (line1, line2) -> Integer.compare(line1.getTruckload().getLoadUnits(),
					line2.getTruckload().getLoadUnits());
			break;
		case "truckType":
			lineComparator = (line1, line2) -> line1.getTruckload().getTruckType().toSqlValue()
					.compareTo(line2.getTruckload().getTruckType().toSqlValue());
			break;
		default:
			throw new CommandException("LinesSortCommand has been invoked with invalid sortArg parameter!");
		}
		Comparator<Line> finalLineComparator = (line1, line2) -> {
			int i = lineComparator.compare(line1, line2);
			if (i == 0) {
				return 1;
			} else {
				return i;
			}
		};
		session.setAttribute((prefix1 + "sComparator"), finalLineComparator);
		try {
			req.getServletContext().getRequestDispatcher(url).forward(req, resp);
		} catch (ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
