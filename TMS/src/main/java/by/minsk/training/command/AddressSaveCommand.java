package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.NEW_ADDRESS;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Locality;
import by.minsk.training.service.AddressService;
import by.minsk.training.service.LocalityService;
import by.minsk.training.service.ServiceException;

@Bean(name = NEW_ADDRESS)
public class AddressSaveCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(AddressSaveCommand.class);

	private AddressService addressService;
	private Validator addressValidator;
	private LocalityService localityService;

	public AddressSaveCommand(AddressService addressService,
			@BeanQualifier(value = "validateNewAddress") Validator addressValidator, LocalityService localityService) {
		super();
		this.addressService = addressService;
		this.addressValidator = addressValidator;
		this.localityService = localityService;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		String url = (String)session.getAttribute("local_url");
		Map<String, String> omissions = addressValidator.validate(req);
		if (!omissions.isEmpty()) {
			for (String key : omissions.keySet()) {
				req.setAttribute(key, omissions.get(key));
			}
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}

		Long localityId = Long.parseLong(req.getParameter("addressLocality"));
		String newAddressDetails = req.getParameter("address");
		try {
			Locality locality = localityService.get(localityId);
			Address newAddress = new Address();
			newAddress.setDetails(newAddressDetails);
			newAddress.setLocality(locality);
			addressService.persist(newAddress);
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
		try {
			resp.sendRedirect(req.getContextPath() + url);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
