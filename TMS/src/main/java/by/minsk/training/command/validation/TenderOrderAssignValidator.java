package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TenderService;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserService;
import lombok.AllArgsConstructor;

@Bean(name = "validateTenderOrderAssignment")
@AllArgsConstructor
public class TenderOrderAssignValidator implements Validator {

	private TransportOrderService transportOrderService;
	private TenderService tenderService;
	private UserService userService;

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = null;
		String orderId = req.getParameter("order_id");
		String tspId = req.getParameter("assignee");
		long order_id = 0;
		long tsp_id = 0;
		try {
			tsp_id = Long.parseLong(tspId);
		} catch (NumberFormatException e) {
			omissions = omissions == null ? new HashMap<>() : omissions;
			omissions.put("wrongTspId", "invalid tsp id parameter");
		}
		try {
			order_id = Long.parseLong(orderId);
			TransportOrder order = transportOrderService.findOrder(order_id);
			if (order == null) {
				omissions = omissions == null ? new HashMap<>() : omissions;
				omissions.put("wrongOrderId", "there's no restored TransportOrder record with given id");
			} else {
				Tender tender = order.getTender();
				if (tender == null) {
					omissions = omissions == null ? new HashMap<>() : omissions;
					omissions.put("systemMistake", "the order with id = " + orderId + " is not of a tender scope");
				} else {
					UserEntity tsp = userService.findById(tsp_id);
					if (tsp == null) {
						omissions = omissions == null ? new HashMap<>() : omissions;
						omissions.put("systemMistake", "the tsp with id = " + tspId + " has  not been found");
					} else {
						final long id = tsp_id;
						Set<TenderLineQuery> queries = tenderService.retrieveLineWinners(tender, order.getLine(),
								(UserEntity) req.getSession().getAttribute("user"));
						TenderLineQuery query = queries.stream()
								.filter(q -> q.getParticipation().getTsp().getId() == id).findFirst().orElse(null);
						if (query == null) {
							omissions = omissions == null ? new HashMap<>() : omissions;
							omissions.put("systemMistake",
									"the tsp with id = " + tspId + " has been not nominated within tender "
											+ tender.getId() + " on the line " + order.getLine().getId());
						}
					}
				}
			}
		} catch (NumberFormatException e) {
			omissions = omissions == null ? new HashMap<>() : omissions;
			omissions.put("wrongOrderId", "invalid order id parameter");
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		return omissions;
	}
}
