package by.minsk.training.command.validation;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.command.CommandException;

public interface Validator {
	Map<String, String> validate(HttpServletRequest req) throws CommandException;
}
