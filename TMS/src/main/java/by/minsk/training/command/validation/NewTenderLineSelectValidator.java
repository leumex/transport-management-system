package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static by.minsk.training.ToolSet.notEmpty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.Line;

@Bean(name = "validateNewTenderLine")
public class NewTenderLineSelectValidator implements Validator {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> omissions = null;
		String id = req.getParameter("lineToAddInNewTender");
		if (!notEmpty(id)) {
			omissions = new HashMap<>();
			omissions.put("newTenderNewLineEmptyId",
					"No line value have been sent for new tender lines set replenishment!");
		} else {
			long lineId;
			try {
				lineId = Long.parseLong(id);
			} catch (NumberFormatException e) {
				omissions = new HashMap<>();
				omissions.put("newTenderNewLineEmptyId",
						"Invalid line id value has been sent for new tender lines set replenishment!");
				return omissions;
			}
			HttpSession session = req.getSession();
			Set<Line> newTenderLines = (Set<Line>) session.getAttribute("newTenderSelectedLines");
			if (newTenderLines != null && newTenderLines.stream().anyMatch(line -> line.getId() == lineId)) {
				omissions = new HashMap<>();
				omissions.put("lineDoubleSelection", "The line " + lineId + " has already been selected for new tender lines set!");
			}
		}
		return omissions;
	}
}
