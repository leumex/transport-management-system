package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.NEW_ORDER;
import static by.minsk.training.dao.basic.CRUDDao.DATE_FORMATTER;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.user.UserEntity;

@Bean(name = NEW_ORDER)
public class OrderSaveCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(OrderSaveCommand.class);

	private TransportOrderService transportOrderService;
	private Validator validator;

	public OrderSaveCommand(TransportOrderService transportOrderService,
			@BeanQualifier(value = "validateNewOrder") Validator validator) {
		super();
		this.transportOrderService = transportOrderService;
		this.validator = validator;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		Map<String, String> omissions = validator.validate(req);
		String url = req.getParameter("local_url");
		try {
			if (omissions != null) {
				omissions.entrySet().stream().peek(entry -> req.setAttribute(entry.getKey(), entry.getValue()));
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			}
			HttpSession session = req.getSession();
			Tender tender = (Tender) session.getAttribute("newShipmentTender");
			Line line = (Line) session.getAttribute("newShipmentLine");
			LocalDate loadingDate = LocalDate.parse(req.getParameter("loadingDate"), DATE_FORMATTER);
			int deliveryTerm = Integer.parseInt(req.getParameter("deliveryTerm"));
			TransportOrder order = new TransportOrder();
			UserEntity customer = (UserEntity) session.getAttribute("user");
			if (tender != null) {
				order.setTender(tender);
			} else {
				order.setCustomer(customer);
			}
			order.setLoadingDate(loadingDate);
			order.setDeliveryTerm(deliveryTerm);
			order.setLine(line);
			order.setState(State.CREATED);
			long id = transportOrderService.persist(order);
			session.setAttribute("newShipmentId", id);
			resp.sendRedirect(req.getContextPath() + url);
		} catch (ServletException | IOException | ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
