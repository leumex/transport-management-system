package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.NEW_TENDER;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Tender;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TenderService;
import by.minsk.training.user.UserEntity;

@Bean(name = NEW_TENDER)
@TransactionSupport
public class TenderSaveCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(TenderSaveCommand.class);

	private Validator validator;
	private TenderService tenderService;

	public TenderSaveCommand(@BeanQualifier(value = "validateTender") Validator validator,
			TenderService tenderService) {
		super();
		this.validator = validator;
		this.tenderService = tenderService;
	}

	@Override
	@Transactional
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		Map<String, String> omissions = validator.validate(req);
		String url = req.getParameter("local_url");
		if (!omissions.isEmpty()) {
			Set<Entry<String, String>> entrySet = omissions.entrySet();
			entrySet.forEach(entry -> req.setAttribute(entry.getKey(), entry.getValue()));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		String title = req.getParameter("tenderTitle");
		int tripsQuantity = Integer.parseInt(req.getParameter("tripsQuantity"));
		LocalDate tenderStart = LocalDate.parse(req.getParameter("tenderStart"));
		logger.debug("dates are coming in the format " + req.getParameter("tenderStart"));
		LocalDate tenderEnd = LocalDate.parse(req.getParameter("tenderEnd"));
		@SuppressWarnings("unchecked")
		Set<Line> lines = (Set<Line>) session.getAttribute("newTenderSelectedLines");
		Tender tender = new Tender();
		tender.setTitle(title);
		tender.setTenderStart(tenderStart);
		tender.setTenderEnd(tenderEnd);
		tender.setEstimatedQuantity(tripsQuantity);
		tender.setCustomer((UserEntity)session.getAttribute("user"));
		try {
			tenderService.persist(tender, lines);
			session.setAttribute("newTenderId", tender.getId());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
		try {
			session.removeAttribute("newTenderSelectedLines");
			session.removeAttribute("tripsQuantity");
			session.removeAttribute("tenderEnd");
			session.removeAttribute("tenderStart");
			session.removeAttribute("newTenderTitle");
			session.removeAttribute("tendersToRender");
			resp.sendRedirect(req.getContextPath() + url);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
