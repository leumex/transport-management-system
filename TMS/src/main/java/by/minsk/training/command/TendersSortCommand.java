package by.minsk.training.command;

import java.io.IOException;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.Tender.State;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TenderService;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import static by.minsk.training.ApplicationConstants.TENDERS_SORT;

@Bean(name = TENDERS_SORT)
@AllArgsConstructor
public class TendersSortCommand implements ServletCommand {

	private TenderService tenderService;

	private static final Logger logger = LogManager.getLogger(TendersSortCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		try {
			String comparingParameter = req.getParameter("comparingParameter");
			String url = req.getParameter("local_url");
			String tendersSet = req.getParameter("tendersSet");
			HttpSession session = req.getSession();
			UserEntity user = (UserEntity) session.getAttribute("user");
			@SuppressWarnings("unchecked")
			Set<Tender> tendersToRender = (Set<Tender>) session.getAttribute(tendersSet);
			if (tendersToRender == null || tendersToRender.isEmpty()) {
				logger.debug("url : "  + url);
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			}
			Comparator<Tender> comparator;
			if (comparingParameter.equals("id")) {
				comparator = (t1, t2) -> (int) (t1.getId() - t2.getId());
			} else if (comparingParameter.equals("release_date")) {
				comparator = (t1, t2) -> t1.getReleaseDate().compareTo(t2.getReleaseDate());
			} else if (comparingParameter.equals("title")) {
				comparator = (t1, t2) -> t1.getTitle().compareTo(t2.getTitle());
			} else if (comparingParameter.equals("customer")) {
				comparator = (t1, t2) -> t1.getCustomer().getName().compareTo(t2.getCustomer().getName());
			} else if (comparingParameter.equals("set_date")) {
				comparator = (t1, t2) -> t1.getSetDate().compareTo(t2.getSetDate());
			} else if (comparingParameter.equals("state")) {
				comparator = (t1, t2) -> t1.getState().printOut().compareTo(t2.getState().printOut());
			} else if (comparingParameter.equals("deadline")) {
				comparator = (t1, t2) -> {
					if (t1.getState() == State.CREATED) {return -1;}
					if (t2.getState() == State.CREATED) {return 1;}
					return t1.getCompetitionDeadline().compareTo(t2.getCompetitionDeadline());};
			} else if (comparingParameter.equals("tender_start")) {
				comparator = (t1, t2) -> t1.getTenderStart().compareTo(t2.getTenderEnd());
			} else if (comparingParameter.equals("tender_end")) {
				comparator = (t1, t2) -> t1.getTenderEnd().compareTo(t2.getTenderEnd());
			} else if (comparingParameter.equals("shipments_quantity")) {
				comparator = (t1, t2) -> {
					try {
						return tenderService.countOrders(t1, user) - tenderService.countOrders(t2, user);
					} catch (ServiceException e) {
						logger.error(e.getMessage());
						throw new RuntimeException(e);
					}
				};
			} else {
				throw new CommandException("command has been invoked with no arguments to sort the tenders set");
			}
			Comparator<Tender> finalComparator = (t1, t2) -> {
				int i = comparator.compare(t1, t2);
				if (i == 0) {
					return 1;
				} else {
					return i;
				}
			};
			Set<Tender> sortedTenders = new TreeSet<Tender>(finalComparator);
			sortedTenders.addAll(tendersToRender);
			session.setAttribute(tendersSet, sortedTenders);
			logger.debug("tendersSort command has been executed with a copmaring parameter `" + comparingParameter + "`");
			req.getServletContext().getRequestDispatcher(url).forward(req, resp);
		} catch (IOException | ServletException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
