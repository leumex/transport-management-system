package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.NEW_TENDER_LINE;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.entity.Line;
import by.minsk.training.service.LineService;
import by.minsk.training.service.ServiceException;

@Bean(name = NEW_TENDER_LINE)
public class SelectLineForNewTenderCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(SelectLineForNewTenderCommand.class);

	private LineService lineService;
	private Validator validator;

	public SelectLineForNewTenderCommand(LineService lineService,
			@BeanQualifier(value = "validateNewTenderLine") Validator validator) {
		super();
		this.lineService = lineService;
		this.validator = validator;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		Map<String, String> omissions = validator.validate(req);
		String url = req.getParameter("local_url");
		if (omissions != null) {
			omissions.keySet().forEach(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (IOException | ServletException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		HttpSession session = req.getSession();
		long id = Long.parseLong(req.getParameter("lineToAddInNewTender"));
		Line line;
		try {
			line = lineService.get(id);
			logger.debug("Line " + line.toString() + " has been selected for new tender");
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
		Set<Line> newTenderLines = (Set<Line>) session.getAttribute("newTenderSelectedLines");
		newTenderLines = newTenderLines == null ? new HashSet<>() : newTenderLines;
		newTenderLines.add(line);
		Set<Line> newTenderLinesToRender = (Set<Line>) session.getAttribute("newTenderSelectedLinesToRender");
		if (newTenderLinesToRender != null) {
			newTenderLinesToRender.add(line);
		}
		logger.debug("New tender lines set size is " + newTenderLines.size());
		session.setAttribute("newTenderSelectedLines", newTenderLines);
		try {
			resp.sendRedirect(req.getContextPath() + url);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
