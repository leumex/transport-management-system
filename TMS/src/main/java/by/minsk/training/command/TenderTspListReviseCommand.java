package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.TSP_TENDER_EDIT;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.service.ServiceException;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserService;
import lombok.AllArgsConstructor;

@Bean(name = TSP_TENDER_EDIT)
@AllArgsConstructor
public class TenderTspListReviseCommand implements ServletCommand {
	
	private static final Logger logger = LogManager.getLogger(TenderTspListReviseCommand.class);
	
	private UserService userService;

	@SuppressWarnings("unchecked")
	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		Set<UserEntity> tenderTsps = (Set<UserEntity>)session.getAttribute("tenderCarriers");
		Set<UserEntity> outOfTenderTsps = (Set<UserEntity>)session.getAttribute("availableTsps");
		String local_url = req.getParameter("local_url");
		long excludeTspId = req.getParameter("tspToExclude").equals("")||req.getParameter("tspToExclude")== null ? 0 : 
				Long.parseLong(req.getParameter("tspToExclude"));
		long includeTspId = req.getParameter("tspToInclude").equals("")||req.getParameter("tspToInclude") == null ? 0 : 
			Long.parseLong(req.getParameter("tspToInclude"));
		try {
		if (excludeTspId!=0){
			tenderTsps = tenderTsps.stream().filter(company -> company.getId()!=excludeTspId).collect(Collectors.toSet());
			session.setAttribute("tenderCarriers", tenderTsps);
			outOfTenderTsps.add(userService.findCompanyById(excludeTspId));
			session.setAttribute("availableTsps", outOfTenderTsps);
		} 
		if (includeTspId!=0) {
			outOfTenderTsps = outOfTenderTsps.stream().filter(company->company.getId()!=includeTspId).collect(Collectors.toSet());
			session.setAttribute("availableTsps", outOfTenderTsps);
			tenderTsps.add(userService.findCompanyById(includeTspId));
			session.setAttribute("tenderCarriers", tenderTsps);		
		}
		}catch(ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
		try {
			resp.sendRedirect(req.getServletContext().getContextPath()+local_url);
		}catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}	
	}
}
