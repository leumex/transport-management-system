package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDER_RECALL;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.user.UserEntity;

@TransactionSupport
@Bean(name = ORDER_RECALL)
public class OrderRecallCommand implements ServletCommand {

	private final static Logger logger = LogManager.getLogger(OrderRecallCommand.class);

	private Validator validator;
	private TransportOrderService transportOrderService;

	public OrderRecallCommand(@BeanQualifier(value = "validateOrderRecall") Validator validator,
			TransportOrderService transportOrderService) {
		super();
		this.validator = validator;
		this.transportOrderService = transportOrderService;
	}

	@Transactional
	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getParameter("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (omissions != null) {
			omissions.keySet().forEach(k -> req.setAttribute(k, omissions.get(k)));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (ServletException | IOException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		HttpSession session = req.getSession();
		UserEntity user = (UserEntity) session.getAttribute("user");
		long orderId = Long.parseLong(req.getParameter("order_id"));
		@SuppressWarnings("unchecked")
		Set<TransportOrder> userOrders = (Set<TransportOrder>) session.getAttribute("user_shipments");
		TransportOrder order = userOrders.stream().filter(o -> o.getId() == orderId).findFirst().orElseThrow(
				() -> new CommandException("user orders set does not contain the order with id = " + orderId));
		try {
			if (transportOrderService.commitOrderRecall(user, orderId)) {
				userOrders.remove(order);
				order = transportOrderService.findOrder(orderId);
				userOrders.add(order);
				session.setAttribute("user_shipments", userOrders);
			}
			req.getServletContext().getRequestDispatcher(url).forward(req, resp);
		} catch (ServiceException | IOException | ServletException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
