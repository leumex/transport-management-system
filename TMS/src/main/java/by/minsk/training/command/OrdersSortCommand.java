package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDERS_SORT;

import java.io.IOException;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.service.RouteService;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.service.TruckloadService;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserEntity.Roles;
import lombok.AllArgsConstructor;
import lombok.Data;

@Bean(name = ORDERS_SORT)
@Data
@AllArgsConstructor
public class OrdersSortCommand implements ServletCommand {

	private TransportOrderService transportOrderService;
	private RouteService routeService;
	private TruckloadService truckloadService;
	private SpotOrderService spotOrderService;

	private static final Logger logger = LogManager.getLogger(OrdersSortCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {		
		String url = req.getParameter("local_url");
		HttpSession session = req.getSession();
		@SuppressWarnings("unchecked")
		Set<TransportOrder> orders = (Set<TransportOrder>) req.getSession().getAttribute("user_shipments");
		UserEntity user = (UserEntity) session.getAttribute("user");
		String comparingParameter = req.getParameter("comparingParameter").trim();
		Comparator<TransportOrder> comparator;
		if (comparingParameter.equals("id")) {
			comparator = (o1, o2) -> (int) (o1.getId() - o2.getId());
		} else if (comparingParameter.equals("date")) {
			comparator = (o1, o2) -> o1.getIssued().compareTo(o2.getIssued());
		} else if (comparingParameter.equals("loadingDate")) {
			comparator = (o1, o2) -> o1.getLoadingDate().compareTo(o2.getLoadingDate());
		} else if (comparingParameter.equals("route")) {
			comparator = (o1, o2) -> routeService.deliverRepresentation(o1.getLine().getRoute())
					.compareTo(routeService.deliverRepresentation(o2.getLine().getRoute()));
		} else if (comparingParameter.equals("truckload")) {
			comparator = (o1, o2) -> truckloadService.deliverRepresentation(o1.getLine().getTruckload())
					.compareTo(truckloadService.deliverRepresentation(o2.getLine().getTruckload()));
		} else if (comparingParameter.equals("company")) {
			comparator = (o1, o2) -> {
				try {
					return restoreCompanyRepresentation(o1, user).compareTo(restoreCompanyRepresentation(o2, user));
				} catch (ServiceException e) {
					logger.error(e.getMessage());
					throw new RuntimeException(e);
				}
			};
		} else if (comparingParameter.equals("scope")) {
			comparator = (o1, o2) -> restoreScope(o1).compareTo(restoreScope(o2));
		} else if (comparingParameter.equals("state")) {
			comparator = (o1, o2) -> {
				try {
					return restoreOrderState(o1, user).compareTo(restoreOrderState(o2, user));
				} catch (ServiceException e) {
					logger.error(e.getMessage());
					throw new RuntimeException(e);
				}
			};
		} else
			throw new CommandException("command is invoked with no arguments to sort the transport orders set");
		Set<TransportOrder> sortedOrders = new TreeSet<>(comparator);
		sortedOrders.addAll(orders);
		session.setAttribute("user_shipments", sortedOrders);
		try {
			req.getServletContext().getRequestDispatcher(url).forward(req, resp);
		} catch (ServletException | IOException e) {
			logger.debug(e.getMessage());
			throw new CommandException("failure during executing the request forwarding back to the page", e);
		}
	}

	private String restoreCompanyRepresentation(TransportOrder order, UserEntity user) throws ServiceException {
		if (user.getRole().equals(Roles.CUSTOMER)) {
			if (spotOrderService.hasOpenSpotAuction(order)) {
				return "spot";
			} else if (order.getState().equals(State.CREATED)) {
				return "";
			} else
				return transportOrderService.getLatestAssignment(user, order).getTsp().getName();
		} else if (user.getRole().equals(Roles.TRANSPORT_SERVICE_PROVIDER)) {
			return order.getCustomer() == null ? order.getTender().getCustomer().getName()
					: order.getCustomer().getName();
		} else {
			StringBuilder builder = new StringBuilder();
			builder.append(order.getCustomer() == null ? order.getTender().getCustomer().getName()
					: order.getCustomer().getName());
			builder.append(" / ");
			if (spotOrderService.hasOpenSpotAuction(order)) {
				builder.append("spot");
			} else if (order.getState().equals(State.CREATED)) {
				return builder.toString();
			} else {
				builder.append(transportOrderService.getLatestAssignment(user, order).getTsp().getName());
			}
			return builder.toString();
		}
	}

	private String restoreScope(TransportOrder order) {
		return order.getCustomer() != null ? (order.getState().equals(State.CREATED) ? "no scope" : "spot")
				: new StringBuffer().append(order.getTender().getId()).append(" ").append(order.getTender().getTitle())
						.toString();
	}
	
	private String restoreOrderState(TransportOrder order, UserEntity user) throws ServiceException {
		return new StringBuffer().append(order.getState().toString())
				.append(" / ").append(transportOrderService.getLatestAssignment(user, order).getTspResponse().toString()).toString();
	}
}
