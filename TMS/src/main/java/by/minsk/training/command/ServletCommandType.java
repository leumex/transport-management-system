package by.minsk.training.command;

import by.minsk.training.ApplicationConstants;
import by.minsk.training.user.UserEntity;

public enum ServletCommandType {

    DEFAULT("default", UserEntity.Roles.ALL),
    LOGIN(ApplicationConstants.LOGIN_USER, UserEntity.Roles.ALL),
    LOGOUT(ApplicationConstants.LOGOUT_USER, UserEntity.Roles.ALL),
    REGISTER(ApplicationConstants.REG_NEW_USER, UserEntity.Roles.ALL),
	ORDERS_FILTER(ApplicationConstants.ORDERS_FILTER, UserEntity.Roles.ALL),
	ORDERS_SORT(ApplicationConstants.ORDERS_SORT, UserEntity.Roles.ALL),
	ORDER_ACCEPT_BY_TSP("", UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER),
	ORDER_ACCEPT_BY_ADMIN("", UserEntity.Roles.ADMIN),
	ORDER_RECALL_BY_CUSTOMER(ApplicationConstants.ORDER_RECALL, UserEntity.Roles.CUSTOMER),
	ORDER_RECALL_BY_ADMIN(ApplicationConstants.ORDER_RECALL, UserEntity.Roles.ADMIN),
	ORDER_DELETE("", UserEntity.Roles.ADMIN),
	ORDER_REJECT_BY_TSP("",UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER),
	ORDER_REJECT_BY_ADMIN("",UserEntity.Roles.ADMIN);
	
    private String name;
    private UserEntity.Roles role;

    ServletCommandType(String name, UserEntity.Roles role) {
        this.name = name;
        this.role = role;
    }
}
