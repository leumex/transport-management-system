package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.LINES_FILTER;
import static by.minsk.training.ToolSet.notEmpty;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Line;
import by.minsk.training.entity.Route.Conditions;
import by.minsk.training.entity.Truckload.TruckType;
import by.minsk.training.service.LineService;
import lombok.AllArgsConstructor;

@Bean(name = LINES_FILTER)
@AllArgsConstructor
public class LinesFilterCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(LinesFilterCommand.class);

	private LineService lineService;

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		String prefix1 = req.getParameter("prefix1");
		@SuppressWarnings("unchecked")
		Set<Line> givenLines = (Set<Line>) session.getAttribute(prefix1 + "s");
		if (givenLines != null) {
			givenLines = handleLines(req, givenLines);
			session.setAttribute((prefix1 + "sToRender"), givenLines);
		}
		String url = req.getParameter("local_url");
		try {
			req.getServletContext().getRequestDispatcher(url).forward(req, resp);
		} catch (IOException | ServletException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}

	private Set<Line> handleLines(HttpServletRequest req, Set<Line> givenSet) {
		HttpSession session = req.getSession();
		Map<String, Object> parameters = new HashMap<>();
		String prefix1 = req.getParameter("prefix1");
		String prefix2 = req.getParameter("prefix2");
		String pickupCountry = req.getParameter(prefix2 + "PickupCountry");
		String pickupLocality = req.getParameter(prefix2 + "PickupLocality");
		String pickupAddress = req.getParameter(prefix2 + "PickupAddress");
		String deliveryCountry = req.getParameter(prefix2 + "DeliveryCountry");
		String deliveryLocality = req.getParameter(prefix2 + "DeliveryLocality");
		String deliveryAddress = req.getParameter(prefix2 + "DeliveryAddress");
		String cargoName = req.getParameter(prefix2 + "CargoName");
		String lineLoadUnits = req.getParameter(prefix2 + "LoadUnits");
		String lineCargoWeight = req.getParameter(prefix2 + "CargoWeight");
		try {
			parameters.put("loadUnits", notEmpty(lineLoadUnits) ? Integer.parseInt(lineLoadUnits) : null);
		} catch (NumberFormatException e) {
			req.setAttribute((prefix2 + "LoadUnits"), "INVALID INPUT");
		}
		try {
			parameters.put("cargoWeight", notEmpty(lineCargoWeight) ? Double.parseDouble(lineCargoWeight) : null);
		} catch (NumberFormatException e) {
			req.setAttribute((prefix2 + "CargoWeight"), "INVALID INPUT");
		}
		String cargoTruckType = req.getParameter(prefix2 + "CargoTruckType");
		parameters.put("truckType", notEmpty(cargoTruckType) ? TruckType.getType(cargoTruckType) : null);
		String routeConditions = req.getParameter(prefix2 + "RouteConditions");
		parameters.put("routeConditions", notEmpty(routeConditions) ? Conditions.getCondition(routeConditions) : null);
		parameters.put("pickupCountry", pickupCountry);
		if (notEmpty(pickupCountry) && pickupCountry.equals((String) session.getAttribute(prefix1 + "PickupCountry"))) {
			parameters.put("pickupLocality", pickupLocality);
			if (notEmpty(pickupLocality)
					&& pickupLocality.equals((String) session.getAttribute(prefix1 + "PickupLocality"))) {
				parameters.put("pickupAddress", pickupAddress);
			}
		}
		parameters.put("deliveryCountry", deliveryCountry);
		if (notEmpty(deliveryCountry)
				&& deliveryCountry.equals((String) session.getAttribute(prefix1 + "DeliveryCountry"))) {
			parameters.put("deliveryLocality", deliveryLocality);
			if (notEmpty(deliveryLocality)
					&& deliveryLocality.equals((String) session.getAttribute(prefix1 + "DeliveryLocality"))) {
				parameters.put("deliveryAddress", deliveryAddress);
			}
		}
		parameters.put("cargoName", cargoName);
		return filter(givenSet, parameters);
	}

	private Set<Line> filter(Set<Line> givenSet, Map<String, Object> filterParams) {
		String pickupCountry = (String) filterParams.get("pickupCountry");
		Set<Line> sortedLines = givenSet;
		sortedLines = notEmpty(pickupCountry) ? lineService.filterByPickupCountry(sortedLines, pickupCountry) : sortedLines;
		String pickupLocality = (String) filterParams.get("pickupLocality");
		sortedLines = notEmpty(pickupLocality) ? lineService.filterByPickupLocality(sortedLines, pickupLocality) : sortedLines;
		String pickupAddress = (String) filterParams.get("pickupAddress");
		sortedLines = notEmpty(pickupAddress) ? lineService.filterByPickupAddress(sortedLines, pickupAddress) : sortedLines;
		String deliveryCountry = (String) filterParams.get("deliveryCountry");
		sortedLines = notEmpty(deliveryCountry) ? lineService.filterByDeliveryCountry(sortedLines, deliveryCountry) : sortedLines;
		String deliveryLocality = (String) filterParams.get("deliveryLocality");
		sortedLines = notEmpty(deliveryLocality) ? lineService.filterByDeliveryLocality(sortedLines, deliveryLocality) : sortedLines;
		String deliveryAddress = (String) filterParams.get("deliveryAddress");
		sortedLines = notEmpty(deliveryAddress) ? lineService.filterByDeliveryAddress(sortedLines, deliveryAddress) : sortedLines;
		String cargoName = (String) filterParams.get("cargoName");
		sortedLines = notEmpty(cargoName) ? lineService.filterByCargoName(sortedLines, cargoName) : sortedLines;
		Integer loadUnits = (Integer) filterParams.get("loadUnits");
		sortedLines = loadUnits == null ? sortedLines : lineService.filterByLoadUnits(sortedLines, loadUnits);
		Double cargoWeight = (Double) filterParams.get("cargoWeight");
		sortedLines = cargoWeight == null ? sortedLines : lineService.filterByCargoWeight(sortedLines, cargoWeight);
		TruckType truckType = (TruckType) filterParams.get("truckType");
		sortedLines = truckType == null ? sortedLines : lineService.filterByTruckType(sortedLines, truckType);
		Conditions routeConditions = (Conditions) filterParams.get("routeConditions");
		sortedLines = routeConditions == null ? sortedLines
				: lineService.filterByRouteConditions(sortedLines, routeConditions);
		return sortedLines;
	}
}
