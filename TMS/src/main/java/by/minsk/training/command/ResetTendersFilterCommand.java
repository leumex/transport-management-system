package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.TENDERS_FILTER_RESET;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;

@Bean(name = TENDERS_FILTER_RESET)
public class ResetTendersFilterCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(ResetTendersFilterCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession session = req.getSession();
		String tendersSet = req.getParameter("tendersSet");
		session.removeAttribute(tendersSet);
		try {
			String path = req.getContextPath() + req.getParameter("local_url");
			resp.sendRedirect(path);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
