package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.Address;
import by.minsk.training.service.AddressService;
import by.minsk.training.service.ServiceException;
import lombok.AllArgsConstructor;

@Bean(name = "validateNewAddress")
@AllArgsConstructor
public class AddressValidator implements Validator {

	private AddressService addressService;

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> validationResult = new HashMap<>();
		String newAddress = req.getParameter("address");
		try {
			Long.parseLong(req.getParameter("addressCountry"));
		} catch (NumberFormatException e) {
			validationResult.put("wrongAddressCountry", "check Country selection");
		}

		Set<Address> existingAddresses;
		try {
			existingAddresses = addressService.getAll();
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		try {
			Long localityId = Long.parseLong(req.getParameter("addressLocality"));
			if (existingAddresses.stream()
					.filter(address -> localityId != null ? address.getLocality().getId() == localityId : true)
					.anyMatch(
							address -> address.getDetails().toLowerCase().contains(newAddress.toLowerCase().trim()))) {
				validationResult.put("wrongAddress", "such address already exists in the system!");
			}
		} catch (NumberFormatException e) {
			validationResult.put("wrongAddressLocality", "check Locality selection");
		}
		if (newAddress == null || newAddress.length() == 0) {
			validationResult.put("wrongAddress", "place new address value!");
		}
		return validationResult;
	}
}