package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDERS_FILTER_RESET;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import lombok.AllArgsConstructor;

@Bean(name=ORDERS_FILTER_RESET)
@AllArgsConstructor
public class ResetOrdersFilterCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(ResetOrdersFilterCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		try {
			HttpSession session = req.getSession();
			session.removeAttribute("user_shipments");
			String url = req.getParameter("local_url");
			resp.sendRedirect(req.getContextPath() + url);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
