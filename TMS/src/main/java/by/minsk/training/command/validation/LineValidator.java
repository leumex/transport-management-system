package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.entity.Route;
import by.minsk.training.entity.Truckload;
import by.minsk.training.entity.Truckload.TruckType;

@Bean(name = "lineValidator")
public class LineValidator implements Validator {

	private static final Logger logger = LogManager.getLogger(LineValidator.class);

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException{
		Map<String, String> validationResult = new HashMap<>();
		String prefix1 = req.getParameter("prefix1");
		String prefix2 = req.getParameter("prefix2");
		if (req.getParameter(prefix2 + "PickupCountry") == null) {
			validationResult.put((prefix1 + "WrongPickupCountry"), "please select start country");
		}
		if (req.getParameter(prefix2 + "PickupLocality") == null) {
			validationResult.put((prefix1 + "WrongPickupLocality"), "please select start locality");
		}
		if (req.getParameter(prefix2 + "PickupAddress") == null) {
			validationResult.put((prefix1 + "WrongPickupAddress"), "please select start address");
		}
		if (req.getParameter(prefix2 + "DeliveryCountry") == null) {
			validationResult.put((prefix1 + "WrongDeliveryCountry"), "please select country of destination");
		}
		if (req.getParameter(prefix2 + "DeliveryLocality") == null) {
			validationResult.put((prefix1 + "WrongDeliveryLocality"), "please select locality of destination");
		}
		if (req.getParameter(prefix2 + "DeliveryAddress") == null) {
			validationResult.put((prefix1 + "WrongDeliveryAddress"), "please select address of destination");
		}
		String newLineCargoName = req.getParameter(prefix2 + "CargoName");
		if (newLineCargoName.length() == 0) {
			validationResult.put((prefix1 + "WrongCargoName"), "Goods title is needed!");
		}
		String lineLoadUnits = req.getParameter(prefix2 + "LoadUnits");
		if (lineLoadUnits.length() == 0) {
			validationResult.put((prefix1 + "WrongLoadUnits"), "field has no value!");
		} else {
			try {
				Integer.parseInt(lineLoadUnits);
			} catch (NumberFormatException e) {
				validationResult.put((prefix1 + "WrongLineLoadUnits"), "INVALID INPUT");
			}
		}
		String lineCargoWeight = req.getParameter(prefix2 + "CargoWeight");
		if (lineCargoWeight.length() == 0) {
			validationResult.put((prefix1 + "WrongCargoWeight"), "field has no value!");
		} else {
			try {
				Double.parseDouble(lineCargoWeight);
			} catch (NumberFormatException e) {
				validationResult.put((prefix1 + "WrongCargoWeight"), "INVALID INPUT");
			}
		}
		String newLineCargoTruckType = req.getParameter(prefix2 + "CargoTruckType");
		if (newLineCargoTruckType == null) {
			validationResult.put((prefix1 + "WrongCargoTruckType"), "select requested truck type");
		} else {
			Truckload.TruckType truckType = TruckType.getType(newLineCargoTruckType.toLowerCase().trim());
			if (truckType == null) {
				validationResult.put((prefix1 + "WrongCargoTruckType"), "invalid TruckType value is sent!");
			}
		}
		String newLineRouteConditions = req.getParameter(prefix2 + "RouteConditions");
		if (newLineRouteConditions == null) {
			validationResult.put((prefix1 + "WrongRouteConditions"), "select requested conditions");
		} else {
			Route.Conditions conditions = Route.Conditions.getCondition(newLineRouteConditions.trim());
			if (conditions == null) {
				validationResult.put((prefix1 + "WrongRouteConditions"), "invalid Route Conditions value is sent!");
			}
		}
		return validationResult;
	}
}
