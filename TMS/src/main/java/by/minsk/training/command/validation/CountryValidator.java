package by.minsk.training.command.validation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.CommandException;
import by.minsk.training.core.Bean;
import by.minsk.training.service.CountryService;
import by.minsk.training.service.ServiceException;
import lombok.AllArgsConstructor;

@Bean(name="validateNewCountry")
@AllArgsConstructor
public class CountryValidator implements Validator {

	private static final Logger logger = LogManager.getLogger(CountryValidator.class);

	private CountryService countryService;

	@Override
	public Map<String, String> validate(HttpServletRequest req) throws CommandException {
		Map<String, String> validationResult = new HashMap<>();
		String newCountry = req.getParameter("country");
		logger.debug(CountryValidator.class + " has incoming parameter " + newCountry);
		if (newCountry == null || newCountry.length() == 0) {
			validationResult.put("wrongCountry", "Insert new country name!");
			logger.debug("new country name is empty!");
		} else {
			try {
				if (countryService.getAll().stream().anyMatch(
						country -> country.getName().toLowerCase().contains(newCountry.toLowerCase().trim()))) {
					validationResult.put("wrongCountry", "Such country already exists in the system!");
					logger.debug("country name is not unique!");
				}
			} catch (ServiceException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		return validationResult;
	}
}
