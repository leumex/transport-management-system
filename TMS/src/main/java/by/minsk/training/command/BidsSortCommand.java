package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.BIDS_SORT;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Bid;
import lombok.AllArgsConstructor;

@Bean(name = BIDS_SORT)
@AllArgsConstructor
public class BidsSortCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(BidsSortCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getContextPath() + req.getParameter("local_url");
		HttpSession session = req.getSession();
		@SuppressWarnings("unchecked")
		Set<Bid> spotBids = (Set<Bid>) session.getAttribute("spotBids");
		if (spotBids != null) {
			Set<Bid> bids = new TreeSet<>((b1, b2) -> (int) (b2.getBid() * 1000 - b1.getBid() * 1000 == 0 ? 1
					: b2.getBid() * 1000 - b1.getBid() * 1000));
			bids.addAll(spotBids);
			session.setAttribute("spotBids", bids);
		}
		try {
			req.getRequestDispatcher(url).forward(req, resp);
		} catch (ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
