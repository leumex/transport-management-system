package by.minsk.training.command;

import static by.minsk.training.ApplicationConstants.ORDER_ACCEPT;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.command.validation.Validator;
import by.minsk.training.core.Bean;
import by.minsk.training.core.BeanQualifier;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TransportOrder.State;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.user.UserEntity;

@Bean(name = ORDER_ACCEPT)
public class OrderAcceptCommand implements ServletCommand {

	private static final Logger logger = LogManager.getLogger(OrderAcceptCommand.class);

	private Validator validator;
	private TransportOrderService transportOrderService;

	public OrderAcceptCommand(@BeanQualifier(value = "validateOrderAcceptance") Validator validator,
			TransportOrderService transportOrderService) {
		super();
		this.validator = validator;
		this.transportOrderService = transportOrderService;
	}

	/*
	 * If order.state has other value else ASSIGNED, the command ends with error
	 * dispatch to the current page. Acceptance means order.state,
	 * tspPerformance.tspResponse, tspPerformance.orderFootprint.state - all are set
	 * to ACCEPTED value, and tspPerformance.responseTime is initialized with the
	 * current datetime. If command is invoked on order with state other than
	 * ASSIGNED, an exception follows.
	 */

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String url = req.getParameter("local_url");
		Map<String, String> omissions = validator.validate(req);
		if (!omissions.isEmpty()) {
			omissions.keySet().stream().forEach(key -> req.setAttribute(key, omissions.get(key)));
			try {
				req.getServletContext().getRequestDispatcher(url).forward(req, resp);
				return;
			} catch (IOException | ServletException e) {
				logger.error(e.getMessage());
				throw new CommandException(e);
			}
		}
		HttpSession session = req.getSession();
		UserEntity user = (UserEntity) session.getAttribute("user");
		long orderId = Long.parseLong(req.getParameter("order_id"));
		try {
			transportOrderService.сommitOrderAcceptance(orderId, user);
			@SuppressWarnings("unchecked")
			Set<TransportOrder> userShipments = (Set<TransportOrder>) session.getAttribute("user_shipments");
			TransportOrder order = userShipments.stream().filter(shipment -> shipment.getId() == orderId).findFirst()
					.orElseThrow(() -> new CommandException(
							"after order acceptance is completed, the order is not found by id among rendered orders list!"));
			/* userShipments.remove(order); */
			order.setState(State.ACCEPTED);
			/* userShipments.add(order); */
			session.setAttribute("user_shipments", userShipments);
			resp.sendRedirect(req.getContextPath() + url);
		} catch (IOException | ServiceException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
}
