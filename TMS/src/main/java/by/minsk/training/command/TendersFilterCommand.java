package by.minsk.training.command;

import java.io.IOException;
import static by.minsk.training.dao.basic.CRUDDao.DATE_FORMATTER;
import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Tender;
import by.minsk.training.service.TenderService;
import lombok.AllArgsConstructor;
import static by.minsk.training.ApplicationConstants.TENDERS_FILTER;

@Bean(name = TENDERS_FILTER)
@AllArgsConstructor
public class TendersFilterCommand implements ServletCommand {

	private TenderService tenderService;

	private static final Logger logger = LogManager.getLogger(TendersFilterCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

		String id = req.getParameter("id_field");
		String title = req.getParameter("title_field");
		String releaseDateFrom = req.getParameter("releasedate_from");
		LocalDate releaseDate_From = recognize(releaseDateFrom);
		String customer = req.getParameter("customer");
		String releaseDateTo = req.getParameter("releasedate_to");
		LocalDate releaseDate_To = recognize(releaseDateTo);
		String setDateFrom = req.getParameter("setDate_from");
		LocalDate setDate_From = recognize(setDateFrom);
		String setDateTo = req.getParameter("setDate_to");
		LocalDate setDate_To = recognize(setDateTo);
		String state = req.getParameter("state_field");
		String deadlineFrom = req.getParameter("deadline_from");
		LocalDate deadline_From = recognize(deadlineFrom);
		String deadlineTo = req.getParameter("deadline_to");
		LocalDate deadline_To = recognize(deadlineTo);
		String tenderStartFrom = req.getParameter("tenderstart_from");
		LocalDate tenderStart_From = recognize(tenderStartFrom);
		String tenderStartTo = req.getParameter("tenderstart_to");
		LocalDate tenderStart_To = recognize(tenderStartTo);
		String tenderEndFrom = req.getParameter("tenderend_from");
		LocalDate tenderEnd_From = recognize(tenderEndFrom);
		String tenderEndTo = req.getParameter("tenderend_to");
		LocalDate tenderEnd_To = recognize(tenderEndTo);

		String tendersSet = req.getParameter("tendersSet");
		
		@SuppressWarnings("unchecked")
		Set<Tender> tenders = (Set<Tender>) req.getSession().getAttribute(tendersSet);
		tenders = tenders.stream().filter(tender -> id != null ? tenderService.matchId(tender, id) : true)
				.filter(tender -> title != null ? tenderService.matchTitle(tender, title) : true)
				.filter(tender -> releaseDateFrom != null || releaseDateTo != null
						? tenderService.matchReleasePeriod(tender, releaseDate_From, releaseDate_To)
						: true)
				.filter(tender -> customer != null ? tenderService.matchCustomer(tender, customer) : true)
				.filter(tender -> setDateFrom != null || setDateTo != null
						? tenderService.matchSetPeriod(tender, setDate_From, setDate_To)
						: true)
				.filter(tender -> state != null ? tenderService.matchState(tender, state) : true)
				.filter(tender -> deadlineFrom != null || deadlineTo != null
						? tenderService.matchDeadlinePeriod(tender, deadline_From, deadline_To)
						: true)
				.filter(tender -> tenderStartFrom != null || tenderStartTo != null
						? tenderService.matchTenderStartPeriod(tender, tenderStart_From, tenderStart_To)
						: true)
				.filter(tender -> tenderEndFrom != null || tenderEndTo != null
						? tenderService.matchTenderEndPeriod(tender, tenderEnd_From, tenderEnd_To)
						: true)
				.collect(Collectors.toSet());
		req.getSession().setAttribute(tendersSet, tenders);
		String uri = req.getRequestURI();
		try {
			req.getServletContext().getRequestDispatcher(uri).forward(req, resp);
		} catch (ServletException | IOException e) {
			logger.error(e.getMessage());
			throw new CommandException(e);
		}
	}
	
	private LocalDate recognize(String s) {
		return s != null ? LocalDate.parse(s, DATE_FORMATTER) : null;
	}
}
