package by.minsk.training;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import by.minsk.training.entity.TspPerformance;
import by.minsk.training.entity.TspPerformance.OrderFootprint;
import by.minsk.training.entity.TspPerformance.TspResponse;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ToolSet {

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	public static boolean notEmpty (String param) {
		return param != null && !param.isEmpty();
	}

	@Data
	@NoArgsConstructor
	public static class KeyPerformanceIndicators {
		private double assignedTOsAccepted;
		private double intimePickups;
		private double acceptedTOsExecution;
		private double deliveryTermCompliance;

		/*
		 * KPI : 
		 * "% of accepted TOs from assigned TOs"
		 * "% of in-time arrivals for loading"
		 * "% of accepted TOs got executed"
		 * "% of TOs desired delivery term has been met"
		 */
		public static KeyPerformanceIndicators calculateKPI(int releasedOrders,
				Set<TspPerformance> foundAcceptedAssignments) {
			KeyPerformanceIndicators indicators = new KeyPerformanceIndicators();
			int acceptedOrdersQuantity = foundAcceptedAssignments.stream()
					.map(assignment -> assignment.getTransportOrder()).collect(Collectors.toSet()).size();
			Stream<TspPerformance> implementationStartedAssignments = foundAcceptedAssignments.stream()
					.filter(assignment -> (assignment.getTspResponse() == TspResponse.ACCEPTED
							& (assignment.getOrderFootprint().getState() == OrderFootprint.State.COMPLETED
									|| assignment.getOrderFootprint().getState() == OrderFootprint.State.IN_ACTION
									|| assignment.getOrderFootprint().getState() == OrderFootprint.State.INTERRUPTED)));
			int executedOrdersQuantity = implementationStartedAssignments.map(assignment -> assignment.getTransportOrder())
					.collect(Collectors.toSet()).size();
			indicators
					.setAssignedTOsAccepted(ToolSet.round(((double) acceptedOrdersQuantity / releasedOrders) * 100, 2));
			indicators.setAcceptedTOsExecution(
					ToolSet.round(((double) executedOrdersQuantity / acceptedOrdersQuantity) * 100, 2));
			int intimeArrivalsForLoading = implementationStartedAssignments.filter(performance -> {
				LocalDateTime arrivedForLoadingTime = performance.getArrivedForLoading();
				LocalDate arrivedForLoadingDate = performance.getArrivedForLoading().toLocalDate();
				LocalDate requestedArrivalDate = performance.getTransportOrder().getLoadingDate();
				LocalDateTime requestedArrivalTime = requestedArrivalDate.atTime(12, 5, 0);
				return requestedArrivalDate.isBefore(arrivedForLoadingDate)
						|| (arrivedForLoadingDate.isEqual(requestedArrivalDate)
								&& requestedArrivalTime.isAfter(arrivedForLoadingTime));
			}).map(assignment -> assignment.getTransportOrder()).collect(Collectors.toSet()).size();
			indicators.setIntimePickups(
					ToolSet.round(((double) intimeArrivalsForLoading / executedOrdersQuantity) * 100, 2));
			Stream<TspPerformance> completedAssignments = implementationStartedAssignments
					.filter(performance -> performance.getOrderFootprint().getState() == OrderFootprint.State.COMPLETED);
			int completedTripsNumber = completedAssignments.map(assignment -> assignment.getTransportOrder()).collect(Collectors.toSet()).size();
			int actualDeliveryTermCompletionTrips = completedAssignments.filter(performance -> {
					int actualDeliveryTerm = performance.getLeftLoading().toLocalDate()
							.until(performance.getArrivedForUnloading().toLocalDate()).getDays();
					return actualDeliveryTerm == performance.getTransportOrder().getDeliveryTerm();
				}).map(assignment -> assignment.getTransportOrder()).collect(Collectors.toSet()).size();
			indicators.setDeliveryTermCompliance(
					ToolSet.round(((double) actualDeliveryTermCompletionTrips / completedTripsNumber) * 100, 2));
			return indicators;
		}
	}
}
