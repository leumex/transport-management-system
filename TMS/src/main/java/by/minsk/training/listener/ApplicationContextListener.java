package by.minsk.training.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.ApplicationContext;


public class ApplicationContextListener implements ServletContextListener {

    private final static Logger logger = LogManager.getLogger(ApplicationContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce){		
		   ServletContext context = sce.getServletContext();
		/*
		 * System.setProperty("appPath", context.getRealPath("/"));
		 * logger.debug("real path of '/' is " + context.getRealPath("/"));
		 */
        logger.info(this.getClass().getName() + " tries to initialize Application context...");
        ApplicationContext.initialize();
        context.setAttribute("language","English");
        logger.info("Context has been initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce){
    	logger.info("Context is about to be destroyed");
        ApplicationContext.getInstance().destroy();
        
    }
}