package by.minsk.training.localization;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LocalesCollection {

    private Map<String, Locale> localesMap = new HashMap<>();

    {
        localesMap.put("English", new Locale("en"));
        localesMap.put("German", new Locale("de", "DE"));
        localesMap.put("Dutch", new Locale("nl", "NL"));
        localesMap.put("Russian", new Locale("ru"));
    }

    public Locale get(String lang) {
        return localesMap.get(lang);
    }

    public int size() {
        return localesMap.size();
    }
}
