package by.minsk.training.localization;

import java.util.Locale;
import java.util.ResourceBundle;

import by.minsk.training.core.Bean;

@Bean
public class BundleProvider {

    private LocalesCollection localesCollection = new LocalesCollection();


    public ResourceBundle getBundle(String lang){
        return ResourceBundle.getBundle("resource", localesCollection.get(lang));
    }

    public Locale getLocale (String lang){
        return localesCollection.get(lang);
    }
}
