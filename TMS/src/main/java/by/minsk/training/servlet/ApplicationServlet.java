package by.minsk.training.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.ApplicationContext;
import by.minsk.training.command.CommandException;
import by.minsk.training.command.ServletCommand;
import static by.minsk.training.ToolSet.notEmpty;

@WebServlet(urlPatterns = "/", loadOnStartup = 1, name = "app")
public class ApplicationServlet extends HttpServlet {

	private static final long serialVersionUID = 33452345852384930L;
	private static final Logger logger = LogManager.getLogger(ApplicationServlet.class);
	private static final ApplicationContext CONTEXT = ApplicationContext.getInstance();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String commandName = req.getParameter("commandName");
		if (!notEmpty(commandName)) {
			req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
		} else {
			try {
				ServletCommand command = CONTEXT.getBean(commandName);
				logger.debug(commandName + " command is about to be invoked");
				command.execute(req, resp);
				logger.debug(commandName + " command has been executed completely");
			} catch (CommandException e) {
				logger.error(e.getMessage());
				throw new ServletException(e);
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
