package by.minsk.training.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.user.UserEntity;

@WebFilter("/*")
public class RoleAccessFilter implements Filter {

	private static final Logger logger = LogManager.getLogger(RoleAccessFilter.class);
	private static final String[] tspOnlyURIes = { "/spot" };
	private static final String[] customerOnlyURIes = { "/shipments/create", "/tenders/create" };

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		String path = req.getRequestURI().substring(req.getContextPath().length());
		logger.debug("next received request has path " + path);
		boolean tspRoleRequired = false;
		for (String s : tspOnlyURIes) {
			if (path.equals(s)) {
				tspRoleRequired = true;
				break;
			}
		}
		boolean customerRoleRequired = false;
		for (String s : customerOnlyURIes) {
			if (path.equals(s)) {
				customerRoleRequired = true;
				break;
			}
		}
		UserEntity user = (UserEntity) req.getSession().getAttribute("user");
		UserEntity.Roles role = (user == null) ? UserEntity.Roles.ALL : user.getRole();
		if (tspRoleRequired) {
			if (role != UserEntity.Roles.ADMIN & role != UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
				res.sendRedirect(req.getServletContext() + "/");
				return;
			}
		} else if (customerRoleRequired) {
			if (role != UserEntity.Roles.CUSTOMER & role != UserEntity.Roles.ADMIN) {
				res.sendRedirect(req.getServletContext() + "/");
				return;
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

}
