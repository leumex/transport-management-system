package by.minsk.training.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.ApplicationContext;
import by.minsk.training.entity.Tender;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TenderParticipationService;
import by.minsk.training.service.TenderService;
import by.minsk.training.user.UserEntity;

@WebFilter(urlPatterns = { "/tenders/tender" })
public class TenderDetailsAccessFilter implements Filter {

	private static final ApplicationContext CONTEXT = ApplicationContext.getInstance();
	private static final Logger logger = LogManager.getLogger(TenderDetailsAccessFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/*
	 * Is available to CUSTOMER user , if the tender belongs to user; is always
	 * available to ADMIN user; is available to TSP user just if there's a tender
	 * participation record. So the page is anyway not accessible for TSP if tender
	 * is only in 'created' state.
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		UserEntity user = (UserEntity) session.getAttribute("user");
		long tenderId = (req.getParameter("id")==null || req.getParameter("id").equals("")) ? 0 : Long.parseLong(req.getParameter("id"));
		if (tenderId == 0) {
			resp.sendRedirect(req.getContextPath() + "/");
			return;
		}
		TenderService tenderService = (TenderService) CONTEXT.getBean("TenderService");
		Tender tender;
		try {
			tender = tenderService.get(tenderId);
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			throw new ServletException(e);
		}
		if (user.getRole().equals(UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER)) {
			if (tender.getState() != Tender.State.CREATED) {
				TenderParticipationService tenderParticipationService = (TenderParticipationService) CONTEXT
						.getBean("TenderParticipationService");
				try {
					if (tenderParticipationService.findParticipation(user, tender) == null) {
						resp.sendRedirect(req.getContextPath() + "/");
						return;
					}
				} catch (ServiceException e) {
					logger.error(e.getMessage());
					throw new ServletException(e);
				}
			} else {
				resp.sendRedirect(req.getContextPath() + "/");
				return;
			}
		} else if (user.getRole().equals(UserEntity.Roles.CUSTOMER)) {
			long customerId = tender.getCustomer().getId();
			if (customerId != user.getId()) {
				resp.sendRedirect(req.getContextPath() + "/");
				return;
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}
}
