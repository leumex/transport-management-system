package by.minsk.training.servlet.tag;

import static by.minsk.training.ToolSet.notEmpty;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import by.minsk.training.ApplicationContext;
import by.minsk.training.command.pagination.Demonstrator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class PagingTagHandler extends SimpleTagSupport {

	@Setter
	@Getter
	private Demonstrator tableDemonstrator = ApplicationContext.getInstance().getBean("Demonstrator");
	@Setter
	@Getter
	private Collection<Object> source;
	@Setter
	@Getter
	private int size = 10; // default size of a set to be rendered at a single page
	@Setter
	@Getter
	private String indexName; // has to be unique for each tag utilization!
	@Setter
	@Getter
	private Collection<Object> bunch;

	@Override
	public void doTag() throws JspException, IOException {
		JspContext context = getJspContext();
		PageContext pageContext = (PageContext) context;
		JspWriter out = context.getOut();
		if (source == null || source.isEmpty()) {
			getJspBody().invoke(out);
		} else {
			HttpSession session = pageContext.getSession();
			HttpServletRequest req = (HttpServletRequest) pageContext.getRequest();
			String url = req.getRequestURI();
			Integer indexToRender = 0;
			if (notEmpty(req.getParameter(indexName))) {
				indexToRender = Integer.parseInt(req.getParameter(indexName));
			} else if (session.getAttribute(indexName) != null) {
				indexToRender = (Integer)session.getAttribute(indexName);
			} else {
				indexToRender = 1;
			}
			List<Collection<Object>> bunches = tableDemonstrator.divideCollection(source, size);
			indexToRender = bunches.size() < indexToRender ? bunches.size() : indexToRender;
			session.setAttribute(indexName, indexToRender);
			bunch = bunches.get(indexToRender - 1);
			context.setAttribute("bunch", bunch);
			getJspBody().invoke(out);
			out.write("<br>");
			if (source.size() > size) {
				if (indexToRender <= 4 & ((bunches.size() - indexToRender) <= 4)) {
					for (int i = 1; i <= bunches.size(); i++) {
						out.write("<a href='" + url + "?" + indexName + "=" + i + "'>" + i + " " + "</a>");
					}
				} else {
					if (indexToRender > 4 & ((bunches.size() - indexToRender) <= 4)) {
						out.write("<a href='" + url + "?" + indexName + "=" + 1 + "'>" + 1 + " " + "</a>");
						out.write("<a href='" + url + "?" + indexName + "=" + 2 + "'>" + 2 + " " + "</a>");
						out.write(" ... ");
						out.write("<a href='" + url + "?" + indexName + "=" + (indexToRender - 1) + "'>"
								+ (indexToRender - 1) + " " + "</a>");
						for (int i = indexToRender; i <= bunches.size(); i++) {
							out.write("<a href='" + url + "?" + indexName + "=" + i + "'>" + i + " " + "</a>");
						}
					}
					if (indexToRender <= 4 & ((bunches.size() - indexToRender) > 4)) {
						for (int i = 1; i <= indexToRender; i++) {
							out.write("<a href='" + url + "?" + indexName + "=" + i + "'>" + i + " " + "</a>");
						}
						out.write("<a href='" + url + "?" + indexName + "=" + (indexToRender + 1) + "'>"
								+ (indexToRender + 1) + " " + "</a>");
						out.write(" ... ");
						out.write("<a href='" + url + "?" + indexName + "=" + (bunches.size() - 1) + "'>"
								+ (bunches.size() - 1) + " " + "</a>");
						out.write("<a href='" + url + "?" + indexName + "=" + bunches.size() + "'>" + bunches.size()
								+ " " + "</a>");
					}
					if (indexToRender > 4 & ((bunches.size() - indexToRender) > 4)) {
						out.write("<a href='" + url + "?" + indexName + "=" + 1 + "'>" + 1 + " " + "</a>");
						out.write("<a href='" + url + "?" + indexName + "=" + 2 + "'>" + 2 + " " + "</a>");
						out.write(" ... ");
						out.write("<a href='" + url + "?" + indexName + "=" + (indexToRender - 1) + "'>"
								+ (indexToRender - 1) + " " + "</a>");
						out.write("<a href='" + url + "?" + indexName + "=" + indexToRender + "'>" + indexToRender + " "
								+ "</a>");
						out.write("<a href='" + url + "?" + indexName + "=" + (indexToRender + 1) + "'>"
								+ (indexToRender + 1) + " " + "</a>");
						out.write(" ... ");
						out.write("<a href='" + url + "?" + indexName + "=" + (bunches.size() - 1) + "'>"
								+ (bunches.size() - 1) + " " + "</a>");
						out.write("<a href='" + url + "?" + indexName + "=" + bunches.size() + "'>" + bunches.size()
								+ " " + "</a>");
					}
				}
			}
			out.write("<br>");
			context.removeAttribute("bunch");
		}
	}
}
