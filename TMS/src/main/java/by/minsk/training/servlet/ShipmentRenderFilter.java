package by.minsk.training.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.minsk.training.ToolSet;

@WebFilter(urlPatterns = { "/shipments/shipment" })
public class ShipmentRenderFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpSession session = req.getSession();
		String param_order_id  = req.getParameter("order_id");
		String session_order_id = (String)session.getAttribute("order_id");
		long param_id = ToolSet.notEmpty(param_order_id) ? Long.parseLong(param_order_id):0;
		long session_id = ToolSet.notEmpty(session_order_id) ? Long.parseLong(session_order_id):0;
		if (param_id != 0 && param_id != session_id) {
			session.removeAttribute("spotBids");
		}
	chain.doFilter(request, response);
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}
}
