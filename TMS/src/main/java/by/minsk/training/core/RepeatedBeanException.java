package by.minsk.training.core;

public class RepeatedBeanException extends BeanRegistrationException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RepeatedBeanException(String message) {
        super(message);
    }
}
