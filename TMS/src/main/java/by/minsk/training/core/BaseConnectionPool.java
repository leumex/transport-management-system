package by.minsk.training.core;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseConnectionPool implements ConnectionPool {

    private static final Logger logger = LogManager.getLogger(BaseConnectionPool.class);

    private final LinkedList<Connection> availableConnections = new LinkedList<>();
    private final LinkedList<Connection> occupiedConnections = new LinkedList<>();

    private final int poolCapacity;
    private final String user;
    private final String driverClass;
    private final String password;
    private final String jdbcUrl;

    private final Lock poolLock = new ReentrantLock();
    private final Condition poolCondition = poolLock.newCondition();

    public BaseConnectionPool(String jdbcUrl, String driverClass, String user, String password, int poolCapacity) {
        this.jdbcUrl = jdbcUrl;
        this.user = user;
        this.driverClass=driverClass;
        this.password = password;
        this.poolCapacity = poolCapacity;

        initDriver(this.driverClass);
    }


    @Override
    public Connection getConnection() {
        poolLock.lock();
        Connection freeConnection = null;
        try {
            if (availableConnections.isEmpty() && occupiedConnections.size() == poolCapacity) {
                try {
                    poolCondition.await();
                } catch (InterruptedException e) {
                    logger.error(e.getMessage());
                    throw new IllegalThreadStateException();
                }
            }
            if (availableConnections.isEmpty() && occupiedConnections.size() < poolCapacity) {
                Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
                availableConnections.add(connection);
            } else if (availableConnections.isEmpty()) {
                throw new IllegalStateException("number of open connections exceeds poolCapacity");
            }
            Connection connection = availableConnections.removeFirst();
            occupiedConnections.add(connection);
            freeConnection = createProxyConnection(connection);
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            poolLock.unlock();
        }
        return freeConnection;
    }

    private void initDriver(String driverClass) {
        try {
            Class.forName(driverClass);
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
            throw new IllegalStateException("Driver cannot be found", e);
        }
    }

    void releaseConnection(Connection connection) {
        try {
            poolLock.lock();
            if (availableConnections.size() >= poolCapacity) {
                throw new IllegalThreadStateException();
            }
            if (!connection.isClosed()) {
                occupiedConnections.remove(connection);
                availableConnections.add(connection);
                poolCondition.signal();
            } else throw new IllegalStateException("Connection instance is somehow closed after its utilization");
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            poolLock.unlock();
        }
    }

    private Connection createProxyConnection(Connection connection) {
        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(), new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        return null;
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }

    private void clearList(LinkedList<Connection> list) {
        if (!list.isEmpty()) {
            list.stream().filter(connection -> {
                try {
                    return !connection.isClosed();
                } catch (SQLException e) {
                    logger.error(e.getMessage());
                    throw new IllegalStateException("database access is lost", e);
                }
            }).forEach(connection -> {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage());
                    throw new IllegalStateException("database access is lost", e);
                }
            });
            list.clear();
        }
    }

    @Override
    public void close() {
        try {
            poolLock.lock();
            clearList(occupiedConnections);
            clearList(availableConnections);
        } finally {
            poolLock.unlock();
        }

    }


}
