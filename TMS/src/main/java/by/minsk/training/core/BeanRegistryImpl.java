package by.minsk.training.core;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

public class BeanRegistryImpl implements BeanRegistry {

	private static final Logger logger = LogManager.getLogger(BeanRegistryImpl.class);

	private FactoryBean factoryBean = new FactoryBean();
	private Set<RegistryInfo> beanRegistry = new HashSet<>();

	@Override
	public <T> void registerBean(T bean) {
		logger.debug(bean.getClass().getSimpleName() + " bean is being registered");
		RegistryInfo info = calculateRegistryInfo(bean.getClass());
		info.setConcreteBean(bean);
		addRegistryInfo(info);
		logger.debug(bean.getClass().getSimpleName() + " bean has been successfully registered");
	}

	private void addRegistryInfo(RegistryInfo info) {
		beanRegistry.stream().filter(registryInfo -> registryInfo.getName().equals(info.getName())).findFirst()
				.ifPresent(registryInfo -> {
					throw new RepeatedBeanException(
							"Bean with name " + registryInfo.getName() + " is already registered!");
				});
		beanRegistry.add(info);
	}

	private RegistryInfo calculateRegistryInfo(Class<?> beanClass) {
		Bean bean = beanClass.getAnnotation(Bean.class);
		if (bean == null) {
			throw new AnnotationNotFoundException(beanClass.getName() + " doesn't have @Bean annotation");
		}
		RegistryInfo info = new RegistryInfo();
		logger.debug(beanClass.getSimpleName() + " is a value of Type field");
		info.setClazz(beanClass);

		Class<?>[] interfaces = beanClass.getInterfaces();
		info.setInterfaces(Arrays.stream(interfaces).collect(Collectors.toSet()));

		Annotation[] annotations = beanClass.getAnnotations();
		info.setAnnotations(Arrays.stream(annotations).collect(Collectors.toSet()));

		Interceptor interceptor = beanClass.getAnnotation(Interceptor.class);
		if (interceptor != null) {
			info.setInterceptor(interceptor);
		}
		String beanName = bean.name();
		if (beanName.trim().length() > 0) {
			info.setName(beanName);
		} else if (beanClass.getInterfaces().length == 1) {
			info.setName(beanClass.getInterfaces()[0].getSimpleName());
		} else {
			info.setName(beanClass.getSimpleName());
		}
		logger.debug(info.getName() + " is a value of Name field");
		return info;
	}

	@Override
	public <T> void registerBean(Class<T> beanClass) {
		logger.debug(beanClass.getSimpleName() + " bean is being registered");
		RegistryInfo info = calculateRegistryInfo(beanClass);
		final Supplier<Object> factory = createFactory(info);
		info.setFactory(factory);
		addRegistryInfo(info);
		logger.debug(beanClass.getSimpleName() + " bean has been successfully registered");
	}

	private Supplier<Object> createFactory(RegistryInfo info) {
		Class<?> clazz = info.getClazz();
		Constructor<?>[] constructors = clazz.getDeclaredConstructors();
		if (constructors.length > 1) {
			throw new BeanInstantionException(
					"More than 1 constructor is announced for class " + clazz.getSimpleName());
		}
		return () -> {
			Constructor<?> constructor = constructors[0];
			logger.debug(constructor.toString() + " - constructor is found to initialize bean of "
					+ clazz.getSimpleName() + " type.");
			if (constructor.getParameterCount() > 0) {
				Parameter[] parameters = constructor.getParameters();
				Object[] args = new Object[parameters.length];
				for (int i = 0; i < parameters.length; i++) {
					Class<?> type = parameters[i].getType();
					logger.debug("the " + i + "th(rd) parameter of " + clazz.getSimpleName()
							+ "(...) constructor has type " + type.getSimpleName());
					BeanQualifier beanQualifier = parameters[i].getAnnotation(BeanQualifier.class);
					if (beanQualifier != null) {
						Predicate<RegistryInfo> searchBean = searchInfo -> searchInfo.getName()
								.equals(beanQualifier.value());
						args[i] = getBean(searchBean);
					} else {
						args[i] = getBean(type);
					}
				}
				logger.debug("These are args to the constructor of class " + clazz.getSimpleName()
						+ " that is about to be invoked: ");
				int i = 0;
				for (Object arg : args) {
					logger.debug("Value of " + i++ + ". parameter to the given " + clazz.getSimpleName()
							+ "(...) constructor: " + arg.toString());
				}
				try {
					return constructor.newInstance(args);
				} catch (InstantiationException | InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
					logger.error(e.getMessage());
					throw new BeanInstantionException("Failed to instantiate bean", e);
				}
			} else {
				try {
					logger.debug("Zero arg constructor of class " + clazz.getSimpleName() + " is about to be invoked");
					return clazz.getDeclaredConstructor().newInstance();
				} catch (InstantiationException | IllegalAccessException | NoSuchMethodException
						| InvocationTargetException e) {
					throw new BeanInstantionException("Failed to instantiate bean", e);
				}
			}
		};
	}

	@SuppressWarnings("unchecked")
	private <T> T getBean(Predicate<RegistryInfo> searchBean) {
		List<RegistryInfo> registryInfoList = beanRegistry.stream().filter(searchBean).collect(Collectors.toList());
		if (registryInfoList.size() > 1) {
			String multipleNames = registryInfoList.stream().map(RegistryInfo::getName)
					.collect(Collectors.joining(", "));
			throw new RepeatedBeanException("Multiple implementations found: " + multipleNames);
		} else {
			return (T) registryInfoList.stream().map(this::mapToBean).findFirst().orElse(null);
		}
	}

	@SuppressWarnings("unchecked")
	private <R> R mapToBean(RegistryInfo registryInfo) {
		R service = (R) factoryBean.getBean(registryInfo);
		/*
		 * selection of those registryInfos where Class value has the Interceptor
		 * annotation declaration
		 */
		Set<RegistryInfo> availableInterceptors = beanRegistry.stream().filter(RegistryInfo::isInterceptor)
				/* selection of those Interceptor registryInfos that meet following predicate */
				.filter(interceptorInfo -> registryInfo.getAnnotations().stream()
						/*
						 * checks whether @param given registryInfo's Class value is annotated with the
						 * annotation that is also declared as @Interceptor clazz() value of the
						 * Interceptor bean
						 */
						.anyMatch(a -> a.annotationType().equals(interceptorInfo.getInterceptor().clazz())))
				.collect(Collectors.toSet());
		if (availableInterceptors.isEmpty()) {
			return service;
		} else {
			logger.debug("Bean " + registryInfo.getName() + " requires following interceptor bean(s) "
					+ availableInterceptors.toString());
			List<BeanInterceptor> interceptors = availableInterceptors.stream()
					.map(interceptorInfo -> (BeanInterceptor) factoryBean.getBean(interceptorInfo))
					.collect(Collectors.toList());
			return (R)getServiceProxy(service, registryInfo, interceptors);
		}
	}

	@SuppressWarnings("unchecked")
	private <R> R getServiceProxy(R service, RegistryInfo registryInfo, List<BeanInterceptor> interceptors) {
		logger.debug("To construct bean " + registryInfo.getName() + " of type "
				+ registryInfo.getClazz().getSimpleName() + " proxy service is being set");
		Class<?>[] toProxy = new Class[registryInfo.getInterfaces().size()];
		Class<?>[] interfaces = registryInfo.getInterfaces().toArray(toProxy);
		return (R) Proxy.newProxyInstance(this.getClass().getClassLoader(), interfaces, (proxy, method, args) -> {
			try {
				for (BeanInterceptor interceptor : interceptors) {
					interceptor.before(proxy, service, method, args);
				}
				Object invoked = method.invoke(service, args);
				for (BeanInterceptor interceptor : interceptors) {
					interceptor.success(proxy, service, method, args);
				}
				return invoked;
			} catch (Exception e) {
				for (BeanInterceptor interceptor : interceptors) {
					interceptor.fail(proxy, service, method, args);
				}
				throw new IllegalStateException("Exception during proxy invocation", e);
			}
		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getBean(Class<T> beanClass) {
		logger.debug("getBean(Class<T> beanClass) method has been invoked with parameter " + beanClass.getSimpleName()
				+ ".class");
		Bean bean = beanClass.getAnnotation(Bean.class);
		String beanName = bean != null && bean.name().trim().length() > 0 ? bean.name().trim() : null;
		Predicate<RegistryInfo> searchBean = info -> info.getName().equals(beanName)
				|| info.getClazz().equals(beanClass) || info.getInterfaces().contains(beanClass);
		return (T) getBean(searchBean);

	}

	@Override
	public <T> T getBean(String name) {
		logger.debug("getBean(String name) method has been invoked with parameter '" + name + "'");
		Predicate<RegistryInfo> searchBean = info -> info.getName().equals(name)
				|| info.getClazz().getSimpleName().equals(name);
		return getBean(searchBean);
	}

	@Override
	public <T> boolean removeBean(T bean) {
		RegistryInfo registryInfo = calculateRegistryInfo(bean.getClass());
		return beanRegistry.remove(registryInfo);
	}

	@Override
	public void destroy() {
		factoryBean.destroy();
		beanRegistry.clear();
	}

	@Data
	@EqualsAndHashCode
	@NoArgsConstructor
	private static class RegistryInfo {

		private String name;
		private Class<?> clazz;
		private Set<Class<?>> interfaces;
		private Set<Annotation> annotations;
		private Interceptor interceptor;
		private Supplier<?> factory;
		private Object concreteBean;

		private boolean isInterceptor() {
			return this.interceptor != null;
		}
	}

	private static class FactoryBean {
		private Map<RegistryInfo, Object> beans = new ConcurrentHashMap<>();

		Object getBean(RegistryInfo info) {
			if (info.getConcreteBean() != null) {
				beans.put(info, info.getConcreteBean());
				logger.debug("Bean of type " + info.getClazz().getSimpleName() + " already exists and will be utilized here.");
			} else if (!beans.containsKey(info)) {
				final Object bean = info.getFactory().get();
				logger.debug("Bean of type " + info.getClazz().getSimpleName() + " has just been initialized here.");
				beans.put(info, bean);
			} else {
				logger.debug("Bean of type " + info.getClazz().getSimpleName()
						+ " already exists and will be utilized here.");
			}
			return beans.get(info);
		}

		void destroy() {
			beans.clear();
		}
	}
}
