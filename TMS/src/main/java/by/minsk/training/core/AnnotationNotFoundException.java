package by.minsk.training.core;

public class AnnotationNotFoundException extends BeanRegistrationException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8042809566556096549L;

	public AnnotationNotFoundException(String message) {
        super(message);
    }

    public AnnotationNotFoundException(String message, Exception e) {
        super(message, e);
    }
}
