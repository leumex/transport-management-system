package by.minsk.training.entity;

import java.time.LocalDate;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TenderParticipation {
	
	private static final long serialVersionUID = 12L;
	
	private long id;
	private LocalDate invitation;
	private LocalDate feedback;
	private Tender tender;
	private UserEntity tsp;
}
