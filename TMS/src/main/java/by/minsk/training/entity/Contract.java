package by.minsk.training.entity;

import java.io.Serializable;
import java.time.LocalDate;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contract implements Serializable {
   
	private static final long serialVersionUID = 3L;
	private long id;
    private UserEntity customer;
    private UserEntity tsp;
    private LocalDate signingDate;
}
