package by.minsk.training.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransportOrder implements Serializable {
 
	private static final long serialVersionUID = 13L;
	
	private long id;
    private LocalDateTime issued;
    private LocalDate loadingDate;
    private int deliveryTerm;
    private Line line;
    private Tender tender;
    private UserEntity customer;
    private State state;

    public enum State {
        CREATED, ANNOUNCED, ASSIGNED, IN_ACTION, COMPLETED, WITHDRAWN, ACCEPTED
    }
}
