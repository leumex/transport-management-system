package by.minsk.training.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Truckload implements Serializable {
   
	private static final long serialVersionUID = 14L;
	
	private long id;
    private String name;
    private double weight;
    private int loadUnits;
    private TruckType truckType;

    public enum TruckType {
        TILT("tilt"), BOX("box"), AMBIENT("no matter");

        private final String sql;

        TruckType(String sql) {
            this.sql = sql;
        }

        public static Set<String> getValues() {
            return Arrays.stream(TruckType.values()).map(TruckType::toSqlValue).collect(Collectors.toSet());
        }

        public String toSqlValue() {
            return this.sql;
        }

        public static TruckType getType(String s) {
            TruckType type;
            switch (s) {
                case "tilt":
                type = TILT;
                    break;
                case "box":
                    type = BOX;
                    break;
                case "no matter":
                    type = AMBIENT;
                    break;
                default:
                    type = null;
                    break;
            }
            return type;
        }
    }
}
