package by.minsk.training.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Locality implements Serializable {
 
	private static final long serialVersionUID = 6L;
	private long id;
    private String name;
    private Country country;
}
