package by.minsk.training.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TspPerformance implements Serializable {

	private static final long serialVersionUID = 15L;

	private long shipment_id;
	private TransportOrder transportOrder;
	private UserEntity tsp;
	private TspResponse tspResponse;
	private LocalDateTime notified;
	private String truckPlates;
	private String comment;
	private LocalDateTime responseTime;
	private LocalDateTime arrivedForLoading;
	private LocalDateTime leftLoading;
	private LocalDateTime arrivedForUnloading;
	private LocalDateTime leftUnloading;
	private OrderFootprint orderFootprint;

	public enum TspResponse {
		OUTSTANDING, ACCEPTED, WITHDRAWN, DECLINED
	}
	
	@Data
	@AllArgsConstructor
	public static class OrderFootprint {
		private final LocalDate loadingDate;
		private final int deliveryTerm;
		private final long lineId;
		private final long tenderId;
		private final long customerId;	
		private State state;	
		
		
		public enum State {
			ASSIGNED, ACCEPTED, IN_ACTION, COMPLETED, WITHDRAWN, CANCELLED, INTERRUPTED
		}
	}
}