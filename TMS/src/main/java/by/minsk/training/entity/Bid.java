package by.minsk.training.entity;

import java.time.LocalDateTime;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bid {
	
	private static final long serialVersionUID = 2L;
	private long id;
	private UserEntity tsp;
	private double bid;
	private SpotTransportOrder spotOrder;
	private LocalDateTime madeAt;
}
