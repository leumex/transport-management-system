package by.minsk.training.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Route implements Serializable {
	 
	private static final long serialVersionUID = 7L;
	private long id;
	private Conditions conditions = Conditions.FREE;
	private Address start;
	private Address destination;

	public enum Conditions {
		FREE("free"), OVER_UA_PROHIBITED("over Ukraine prohibited"), VIA_KOZLOVICHI_ONLY("via Kozlovichi only"),
		CUSTOMS_IN_BRYANSK("customs in Bryansk"), SANITARY_INSPECTION_IN_BENYAKONI("sanitary inspection in Benyakoni"),
		OVER_BY_PROHIBITED("through Belarus prohibited");

		String sql;

		Conditions(String sql) {
			this.sql = sql;
		}

		@Override
		public String toString() {
			return this.sql;
		}

		public static Conditions getCondition(String sql) {
			Conditions definedValue;
			try {
				definedValue = Arrays.stream(Conditions.values()).filter(condition -> condition.toString().equals(sql))
						.findFirst().orElseThrow(NoSuchElementException::new);
			} catch (NoSuchElementException e) {
				return null;
			}
			return definedValue;
		}

		public static List<String> getValues() {
			return Arrays.stream(Conditions.values()).map(Conditions::toString).collect(Collectors.toList());
		}
	}
}
