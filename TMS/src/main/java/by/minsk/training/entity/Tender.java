package by.minsk.training.entity;

import java.io.Serializable;
import java.time.LocalDate;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tender implements Serializable {
   
	private static final long serialVersionUID = 9L;
	private long id;
    private String title;
    private UserEntity customer;
    private LocalDate setDate;
    private LocalDate releaseDate;
    private LocalDate competitionDeadline;
    private LocalDate tenderStart;
    private LocalDate tenderEnd;
    private int estimatedQuantity;
    private State state = State.CREATED;

    public enum State {
        CREATED, RELEASED, COMPLETED, CONTRACTED, IN_ACTION;
        
        public String printOut() {
        	return this.toString().toLowerCase();
        }
    }
}