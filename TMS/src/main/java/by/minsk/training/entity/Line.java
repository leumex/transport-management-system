package by.minsk.training.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Line implements Serializable {
    
	private static final long serialVersionUID = 5L;
	private long id;
    private Truckload truckload;
    private Route route;
}
