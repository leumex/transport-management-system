package by.minsk.training.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Country implements Serializable {
 
	private static final long serialVersionUID = 4L;
	private long id;
    private String name;
}
