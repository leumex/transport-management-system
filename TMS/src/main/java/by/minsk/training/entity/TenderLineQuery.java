package by.minsk.training.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TenderLineQuery {
	
	private static final long serialVersionUID = 11L;
	
	private long id;
	private Double rate;
	private Integer deliveryTerm;
	private double awardedShare;
	private TenderParticipation participation;
	private TenderLine tenderLine;
}
