package by.minsk.training.entity;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TenderLine {
	
	private static final long serialVersionUID = 10L;
	
	private long id;
	private Tender tender;
	private Line line;
	private Set<TenderLineQuery> lineQueries;
}
