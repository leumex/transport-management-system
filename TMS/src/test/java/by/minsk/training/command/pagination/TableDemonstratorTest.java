package by.minsk.training.command.pagination;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TableDemonstratorTest {

	private Collection<String> collection;
	int sectorSize;
	private TableDemonstrator demonstrator;

	@Before
	public void initializeCollection() {
		collection = new ArrayList<>();
		collection.add("one");
		collection.add("two");
		collection.add("three");
		collection.add("four");
		collection.add("five");
		collection.add("six");
		collection.add("seven");
		collection.add("eight");
		collection.add("nine");

		sectorSize = 4;
		demonstrator = new TableDemonstrator();
	}

	@Test
	public void executeTest() {
		List<Collection<String>> sectors = demonstrator.divideCollection(collection, sectorSize);
		Assert.assertTrue(sectors.size() == 3);
		List<List<String>> expectedList = new ArrayList<>();
		List<String> sector1 = new ArrayList<>();
		sector1.add("one");
		sector1.add("two");
		sector1.add("three");
		sector1.add("four");
		expectedList.add(sector1);
		List<String> sector2 = new ArrayList<>();
		sector2.add("five");
		sector2.add("six");
		sector2.add("seven");
		sector2.add("eight");
		expectedList.add(sector2);
		List<String> sector3 = new ArrayList<>();
		sector3.add("nine");
		expectedList.add(sector3);
		Assert.assertTrue(expectedList.equals(sectors));
	}
}
