package by.minsk.training.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DataSourceTest {

	@Test
	public void shouldBeSingleton() {
		DataSource ds1 = DataSourceImpl.getInstance();
		DataSource ds2 = DataSourceImpl.getInstance();
		Assert.assertSame(ds1, ds2);
	}

	@Test
	public void shouldInsertAndSelect() throws SQLException {
		DataSource ds = DataSourceImpl.getInstance();
		Connection connection = null;
		try {
			connection = ds.getConnection();
			connection.setAutoCommit(false);
			PreparedStatement createStatement = connection.prepareStatement("create table if not exists Pictures "
					+ "(id long auto_increment, pictured Date, place varchar(30), description varchar(50));");
			createStatement.executeUpdate();
			PreparedStatement insertStatement = connection
					.prepareStatement("insert into Pictures (pictured, place, description) "
							+ "values ('1976-11-20', 'Voronezh','last year at University');");
			long id = insertStatement.executeUpdate();
			Assert.assertEquals(1, id);
			connection.commit();
		} catch (SQLException e) {
			assert connection != null;
			connection.rollback();
		} finally {
			assert connection != null;
			connection.close();
		}
		try {
			connection = ds.getConnection();
			PreparedStatement selectStatement = connection.prepareStatement("select * from Pictures;");
			ResultSet resultSet = selectStatement.executeQuery();
			int count = 0;
			while (resultSet.next()) {
				++count;
				Date pictured = (Date) resultSet.getDate("pictured");
				Assert.assertEquals(pictured, Date.valueOf("1976-11-20"));
				String place = (String) resultSet.getString("place");
				Assert.assertEquals(place, "Voronezh");
				String description = resultSet.getString("description");
				Assert.assertEquals(description, "last year at University");
			}
			Assert.assertEquals(1, count);
			selectStatement.close();
		} finally {
			connection.close();
		}
	}
}
