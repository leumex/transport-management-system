package by.minsk.training.dao;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

@RunWith(JUnit4.class)
public class TransactionManagerTest {

	TransactionManager transactionManager;
	DataSource source;

	@Before
	public void runBefore() throws SQLException {
		source = Mockito.spy(DataSourceImpl.getInstance());
		transactionManager = new TransactionManagerImpl(source);
		transactionManager.beginTransaction();// 1
		Connection connection = transactionManager.getConnection();
		Statement statement = connection.createStatement();
		statement.execute("create table if not exists users (id long auto_increment,name varchar(30), age int);");
		statement.close();
	}

	@Test
	public void shouldExecuteAsExpected() throws SQLException {
		Connection connection1, connection2, connection3;
		Statement statement = null;
		try {
			connection1 = transactionManager.getConnection();
			Assert.assertTrue(connection1 instanceof Proxy);
			Assert.assertFalse(connection1.getAutoCommit());
			statement = connection1.createStatement();
			statement.executeUpdate("insert into users (name, age) values ('Igor Mann', 51);\n"
					+ "insert into users (name, age) values ('Vladimir Uljanov', 54);\n"
					+ "insert into users (name, age) values ('Alexander Ivanov', 49);");
			statement.close();
			transactionManager.commitTransaction();
			transactionManager.beginTransaction();// 2
			connection2 = transactionManager.getConnection();
			statement = connection2.createStatement();
			Mockito.verify(source, Mockito.times(2)).getConnection();
			statement.executeUpdate("update users set age = 34 where name = 'Alexander Ivanov';");
			statement.execute("insert into users (name, age) values ('Alexander Suvorov','War');");
		} catch (SQLException e) {
			statement.close();
			transactionManager.rollbackTransaction();
		}
		connection3 = transactionManager.getConnection();// 3
		Mockito.verify(source, Mockito.times(3)).getConnection();
		statement = connection3.createStatement();
		ResultSet rs = statement.executeQuery("select age from users where name = 'Alexander Ivanov';");
		int age = 0;
		while (rs.next()) {
			age = rs.getInt(1);
		}
		Assert.assertEquals(49, age);
		rs.close();
		statement.close();
	}

	@After
	public void executeAfter() {
		source.close();
	}
}
