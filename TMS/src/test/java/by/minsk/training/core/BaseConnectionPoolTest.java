package by.minsk.training.core;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

@RunWith(JUnit4.class)
public class BaseConnectionPoolTest {

    private ConnectionPool pool;
    private static final int THREAD_NUMBER = 200;
    private static final int THREAD_SLEEP = 1000;
    private static final Logger logger = LogManager.getLogger(BaseConnectionPool.class);
    private ExecutorService service;

    @Before
    public void runBefore() {
        Properties properties = new Properties();
        try {
            InputStream inputStream = Objects.requireNonNull(getClass().getClassLoader().
                    getResource("datasource.properties")).openStream();
            properties.load(inputStream);
            service = Executors.newFixedThreadPool(THREAD_NUMBER);
        } catch (IOException e) {
            throw new IllegalStateException("Check datasource.properties file", e);
        }
        pool = Mockito.spy(new BaseConnectionPool(properties.getProperty("jdbcUrl"), properties.getProperty("driverClass"),
                properties.getProperty("user"), properties.getProperty("password"),
                Integer.parseInt(properties.getProperty("maximumPoolSize"))));
    }

    @Test
    public void executeTest() throws InterruptedException {
        Set<Connection> connections = Collections.synchronizedSet(new HashSet<>());
        for (int i = 0; i < THREAD_NUMBER; i++) {
            service.submit(() -> {
                sleep();
                Connection connection = pool.getConnection();
                Assert.assertTrue(connection instanceof Proxy);
                connections.add(connection);
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.debug("exception while connection pool testing: " + e);
                }
            });
        }
        Assert.assertTrue(connections.size() <= 100);
        Assert.assertFalse(connections.stream().anyMatch(connection -> {
            try {
                return connection.isClosed();
            } catch (SQLException e) {
                logger.debug("exception while connection pool testing: " + e);
                return true;
            }
        }));
        logger.info("Number of threads that requested a connection is " + THREAD_NUMBER +
                ", but number of created connections is " + connections.size());
        service.awaitTermination(3, TimeUnit.SECONDS);
        Mockito.verify((BaseConnectionPool) pool, Mockito.times(THREAD_NUMBER)).releaseConnection(Mockito.any());
    }

    private void sleep() {
        try {
            Thread.sleep(Math.abs(new Random().nextInt(THREAD_SLEEP)));
        } catch (InterruptedException e) {
            logger.debug("exception while connection pool testing: " + e);
        }
    }

    @After
    public void runAfter() {
        pool.close();
    }
}