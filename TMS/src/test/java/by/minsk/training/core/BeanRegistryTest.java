package by.minsk.training.core;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import by.minsk.training.command.LoginUserCommand;
import by.minsk.training.command.ServletCommand;
import by.minsk.training.entity.Country;
import by.minsk.training.security.SecurityService;

@RunWith(JUnit4.class)
public class BeanRegistryTest {

    private BeanRegistry registry;
    SecurityService service = Mockito.mock(SecurityService.class);

    @Before
    public void initialize() {
        registry = new BeanRegistryImpl();
    }

    @Test
    public void executeTest() {
        ServletCommand loginUser = new LoginUserCommand(service);
        registry.registerBean(loginUser);
        Assert.assertEquals(registry.getBean("LoginUserCommand"), loginUser);
        Assert.assertEquals(registry.getBean(LoginUserCommand.class), loginUser);
        try {
            registry.registerBean(new LoginUserCommand(service));
        } catch (Exception e) {
            Assert.assertTrue(e instanceof RepeatedBeanException);
        }
        try {
            registry.registerBean(new Country());
        } catch (Exception e) {
            Assert.assertTrue(e instanceof AnnotationNotFoundException);
        }
    }

    @After
    public void dismantle() {
        registry.destroy();
    }
}
